(function(window, document, $, undefined) {
  "use strict";

  window.console = window.console || {
    info: function(stuff) {}
  };

  // If there's no jQuery, fancyBox can't work
  // =========================================

  if (!$) {
    return;
  }

  // Check if fancyBox is already initialized
  // ========================================

  if ($.fn.fancybox) {
    console.info("fancyBox already initialized");

    return;
  }

  // Private default settings
  // ========================

  var defaults = {
    // Enable infinite gallery navigation
    loop: false,

    // Horizontal space between slides
    gutter: 50,

    // Enable keyboard navigation
    keyboard: true,

    // Should display navigation arrows at the screen edges
    arrows: true,

    // Should display counter at the top left corner
    infobar: true,

    // Should display close button (using `btnTpl.smallBtn` template) over the content
    // Can be true, false, "auto"
    // If "auto" - will be automatically enabled for "html", "inline" or "ajax" items
    smallBtn: "auto",

    // Should display toolbar (buttons at the top)
    // Can be true, false, "auto"
    // If "auto" - will be automatically hidden if "smallBtn" is enabled
    toolbar: "auto",

    // What buttons should appear in the top right corner.
    // Buttons will be created using templates from `btnTpl` option
    // and they will be placed into toolbar (class="fancybox-toolbar"` element)
    buttons: [
      "zoom",
      //"share",
      //"slideShow",
      //"fullScreen",
      //"download",
      "thumbs",
      "close"
    ],

    // Detect "idle" time in seconds
    idleTime: 3,

    // Disable right-click and use simple image protection for images
    protect: false,

    // Shortcut to make content "modal" - disable keyboard navigtion, hide buttons, etc
    modal: false,

    image: {
      // Wait for images to load before displaying
      //   true  - wait for image to load and then display;
      //   false - display thumbnail and load the full-sized image over top,
      //           requires predefined image dimensions (`data-width` and `data-height` attributes)
      preload: false
    },

    ajax: {
      // Object containing settings for ajax request
      settings: {
        // This helps to indicate that request comes from the modal
        // Feel free to change naming
        data: {
          fancybox: true
        }
      }
    },

    iframe: {
      // Iframe template
      tpl:
        '<iframe id="fancybox-frame{rnd}" name="fancybox-frame{rnd}" class="fancybox-iframe" frameborder="0" vspace="0" hspace="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen allowtransparency="true" src=""></iframe>',

      // Preload iframe before displaying it
      // This allows to calculate iframe content width and height
      // (note: Due to "Same Origin Policy", you can't get cross domain data).
      preload: true,

      // Custom CSS styling for iframe wrapping element
      // You can use this to set custom iframe dimensions
      css: {},

      // Iframe tag attributes
      attr: {
        scrolling: "auto"
      }
    },

    // Default content type if cannot be detected automatically
    defaultType: "image",

    // Open/close animation type
    // Possible values:
    //   false            - disable
    //   "zoom"           - zoom images from/to thumbnail
    //   "fade"
    //   "zoom-in-out"
    //
    animationEffect: "zoom",

    // Duration in ms for open/close animation
    animationDuration: 366,

    // Should image change opacity while zooming
    // If opacity is "auto", then opacity will be changed if image and thumbnail have different aspect ratios
    zoomOpacity: "auto",

    // Transition effect between slides
    //
    // Possible values:
    //   false            - disable
    //   "fade'
    //   "slide'
    //   "circular'
    //   "tube'
    //   "zoom-in-out'
    //   "rotate'
    //
    transitionEffect: "fade",

    // Duration in ms for transition animation
    transitionDuration: 366,

    // Custom CSS class for slide element
    slideClass: "",

    // Custom CSS class for layout
    baseClass: "",

    // Base template for layout
    baseTpl:
      '<div class="fancybox-container" role="dialog" tabindex="-1">' +
      '<div class="fancybox-bg"></div>' +
      '<div class="fancybox-inner">' +
      '<div class="fancybox-infobar">' +
      "<span data-fancybox-index></span>&nbsp;/&nbsp;<span data-fancybox-count></span>" +
      "</div>" +
      '<div class="fancybox-toolbar">{{buttons}}</div>' +
      '<div class="fancybox-navigation">{{arrows}}</div>' +
      '<div class="fancybox-stage"></div>' +
      '<div class="fancybox-caption"></div>' +
      "</div>" +
      "</div>",

    // Loading indicator template
    spinnerTpl: '<div class="fancybox-loading"></div>',

    // Error message template
    errorTpl: '<div class="fancybox-error"><p>{{ERROR}}</p></div>',

    btnTpl: {
      download:
        '<a download data-fancybox-download class="fancybox-button fancybox-button--download" title="{{DOWNLOAD}}" href="javascript:;">' +
        '<svg viewBox="0 0 40 40">' +
        '<path d="M13,16 L20,23 L27,16 M20,7 L20,23 M10,24 L10,28 L30,28 L30,24" />' +
        "</svg>" +
        "</a>",

      zoom:
        '<button data-fancybox-zoom class="fancybox-button fancybox-button--zoom" title="{{ZOOM}}">' +
        '<svg viewBox="0 0 40 40">' +
        '<path d="M18,17 m-8,0 a8,8 0 1,0 16,0 a8,8 0 1,0 -16,0 M24,22 L31,29" />' +
        "</svg>" +
        "</button>",

      close:
        '<button data-fancybox-close class="fancybox-button fancybox-button--close" title="{{CLOSE}}">' +
        '<svg viewBox="0 0 40 40">' +
        '<path d="M10,10 L30,30 M30,10 L10,30" />' +
        "</svg>" +
        "</button>",

      // This small close button will be appended to your html/inline/ajax content by default,
      // if "smallBtn" option is not set to false
      smallBtn:
        '<button data-fancybox-close class="fancybox-close-small" title="{{CLOSE}}"><svg viewBox="0 0 32 32"><path d="M10,10 L22,22 M22,10 L10,22"></path></svg></button>',

      // Arrows
      arrowLeft:
        '<a data-fancybox-prev class="fancybox-button fancybox-button--arrow_left" title="{{PREV}}" href="javascript:;">' +
        '<svg viewBox="0 0 40 40">' +
        '<path d="M18,12 L10,20 L18,28 M10,20 L30,20"></path>' +
        "</svg>" +
        "</a>",

      arrowRight:
        '<a data-fancybox-next class="fancybox-button fancybox-button--arrow_right" title="{{NEXT}}" href="javascript:;">' +
        '<svg viewBox="0 0 40 40">' +
        '<path d="M10,20 L30,20 M22,12 L30,20 L22,28"></path>' +
        "</svg>" +
        "</a>"
    },

    // Container is injected into this element
    parentEl: "body",

    // Focus handling
    // ==============

    // Try to focus on the first focusable element after opening
    autoFocus: false,

    // Put focus back to active element after closing
    backFocus: true,

    // Do not let user to focus on element outside modal content
    trapFocus: true,

    // Module specific options
    // =======================

    fullScreen: {
      autoStart: false
    },

    // Set `touch: false` to disable dragging/swiping
    touch: {
      vertical: true, // Allow to drag content vertically
      momentum: true // Continue movement after releasing mouse/touch when panning
    },

    // Hash value when initializing manually,
    // set `false` to disable hash change
    hash: null,

    // Customize or add new media types
    // Example:
    /*
        media : {
            youtube : {
                params : {
                    autoplay : 0
                }
            }
        }
        */
    media: {},

    slideShow: {
      autoStart: false,
      speed: 4000
    },

    thumbs: {
      autoStart: false, // Display thumbnails on opening
      hideOnClose: true, // Hide thumbnail grid when closing animation starts
      parentEl: ".fancybox-container", // Container is injected into this element
      axis: "y" // Vertical (y) or horizontal (x) scrolling
    },

    // Use mousewheel to navigate gallery
    // If 'auto' - enabled for images only
    wheel: "auto",

    // Callbacks
    //==========

    // See Documentation/API/Events for more information
    // Example:
    /*
		afterShow: function( instance, current ) {
			console.info( 'Clicked element:' );
			console.info( current.opts.$orig );
		}
	*/

    onInit: $.noop, // When instance has been initialized

    beforeLoad: $.noop, // Before the content of a slide is being loaded
    afterLoad: $.noop, // When the content of a slide is done loading

    beforeShow: $.noop, // Before open animation starts
    afterShow: $.noop, // When content is done loading and animating

    beforeClose: $.noop, // Before the instance attempts to close. Return false to cancel the close.
    afterClose: $.noop, // After instance has been closed

    onActivate: $.noop, // When instance is brought to front
    onDeactivate: $.noop, // When other instance has been activated

    // Interaction
    // ===========

    // Use options below to customize taken action when user clicks or double clicks on the fancyBox area,
    // each option can be string or method that returns value.
    //
    // Possible values:
    //   "close"           - close instance
    //   "next"            - move to next gallery item
    //   "nextOrClose"     - move to next gallery item or close if gallery has only one item
    //   "toggleControls"  - show/hide controls
    //   "zoom"            - zoom image (if loaded)
    //   false             - do nothing

    // Clicked on the content
    clickContent: function(current, event) {
      return current.type === "image" ? "zoom" : false;
    },

    // Clicked on the slide
    clickSlide: "close",

    // Clicked on the background (backdrop) element;
    // if you have not changed the layout, then most likely you need to use `clickSlide` option
    clickOutside: "close",

    // Same as previous two, but for double click
    dblclickContent: false,
    dblclickSlide: false,
    dblclickOutside: false,

    // Custom options when mobile device is detected
    // =============================================

    mobile: {
      idleTime: false,
      clickContent: function(current, event) {
        return current.type === "image" ? "toggleControls" : false;
      },
      clickSlide: function(current, event) {
        return current.type === "image" ? "toggleControls" : "close";
      },
      dblclickContent: function(current, event) {
        return current.type === "image" ? "zoom" : false;
      },
      dblclickSlide: function(current, event) {
        return current.type === "image" ? "zoom" : false;
      }
    },

    // Internationalization
    // ====================

    lang: "en",
    i18n: {
      en: {
        CLOSE: "Close",
        NEXT: "Next",
        PREV: "Previous",
        ERROR: "The requested content cannot be loaded. <br/> Please try again later.",
        PLAY_START: "Start slideshow",
        PLAY_STOP: "Pause slideshow",
        FULL_SCREEN: "Full screen",
        THUMBS: "Thumbnails",
        DOWNLOAD: "Download",
        SHARE: "Share",
        ZOOM: "Zoom"
      },
      de: {
        CLOSE: "Schliessen",
        NEXT: "Weiter",
        PREV: "Zurück",
        ERROR: "Die angeforderten Daten konnten nicht geladen werden. <br/> Bitte versuchen Sie es später nochmal.",
        PLAY_START: "Diaschau starten",
        PLAY_STOP: "Diaschau beenden",
        FULL_SCREEN: "Vollbild",
        THUMBS: "Vorschaubilder",
        DOWNLOAD: "Herunterladen",
        SHARE: "Teilen",
        ZOOM: "Maßstab"
      }
    }
  };

  // Few useful variables and methods
  // ================================

  var $W = $(window);
  var $D = $(document);

  var called = 0;

  // Check if an object is a jQuery object and not a native JavaScript object
  // ========================================================================
  var isQuery = function(obj) {
    return obj && obj.hasOwnProperty && obj instanceof $;
  };

  // Handle multiple browsers for "requestAnimationFrame" and "cancelAnimationFrame"
  // ===============================================================================
  var requestAFrame = (function() {
    return (
      window.requestAnimationFrame ||
      window.webkitRequestAnimationFrame ||
      window.mozRequestAnimationFrame ||
      window.oRequestAnimationFrame ||
      // if all else fails, use setTimeout
      function(callback) {
        return window.setTimeout(callback, 1000 / 60);
      }
    );
  })();

  // Detect the supported transition-end event property name
  // =======================================================
  var transitionEnd = (function() {
    var el = document.createElement("fakeelement"),
      t;

    var transitions = {
      transition: "transitionend",
      OTransition: "oTransitionEnd",
      MozTransition: "transitionend",
      WebkitTransition: "webkitTransitionEnd"
    };

    for (t in transitions) {
      if (el.style[t] !== undefined) {
        return transitions[t];
      }
    }

    return "transitionend";
  })();

  // Force redraw on an element.
  // This helps in cases where the browser doesn't redraw an updated element properly
  // ================================================================================
  var forceRedraw = function($el) {
    return $el && $el.length && $el[0].offsetHeight;
  };

  // Exclude array (`buttons`) options from deep merging
  // ===================================================
  var mergeOpts = function(opts1, opts2) {
    var rez = $.extend(true, {}, opts1, opts2);

    $.each(opts2, function(key, value) {
      if ($.isArray(value)) {
        rez[key] = value;
      }
    });

    return rez;
  };

  // Class definition
  // ================

  var FancyBox = function(content, opts, index) {
    var self = this;

    self.opts = mergeOpts({index: index}, $.fancybox.defaults);

    if ($.isPlainObject(opts)) {
      self.opts = mergeOpts(self.opts, opts);
    }

    if ($.fancybox.isMobile) {
      self.opts = mergeOpts(self.opts, self.opts.mobile);
    }

    self.id = self.opts.id || ++called;

    self.currIndex = parseInt(self.opts.index, 10) || 0;
    self.prevIndex = null;

    self.prevPos = null;
    self.currPos = 0;

    self.firstRun = true;

    // All group items
    self.group = [];

    // Existing slides (for current, next and previous gallery items)
    self.slides = {};

    // Create group elements
    self.addContent(content);

    if (!self.group.length) {
      return;
    }

    // Save last active element
    self.$lastFocus = $(document.activeElement).trigger("blur");

    self.init();
  };

  $.extend(FancyBox.prototype, {
    // Create DOM structure
    // ====================

    init: function() {
      var self = this,
        firstItem = self.group[self.currIndex],
        firstItemOpts = firstItem.opts,
        scrollbarWidth = $.fancybox.scrollbarWidth,
        $scrollDiv,
        $container,
        buttonStr;

      // Hide scrollbars
      // ===============

      if (!$.fancybox.getInstance() && firstItemOpts.hideScrollbar !== false) {
        $("body").addClass("fancybox-active");

        if (!$.fancybox.isMobile && document.body.scrollHeight > window.innerHeight) {
          if (scrollbarWidth === undefined) {
            $scrollDiv = $('<div style="width:100px;height:100px;overflow:scroll;" />').appendTo("body");

            scrollbarWidth = $.fancybox.scrollbarWidth = $scrollDiv[0].offsetWidth - $scrollDiv[0].clientWidth;

            $scrollDiv.remove();
          }

          $("head").append(
            '<style id="fancybox-style-noscroll" type="text/css">.compensate-for-scrollbar { margin-right: ' +
              scrollbarWidth +
              "px; }</style>"
          );

          $("body").addClass("compensate-for-scrollbar");
        }
      }

      // Build html markup and set references
      // ====================================

      // Build html code for buttons and insert into main template
      buttonStr = "";

      $.each(firstItemOpts.buttons, function(index, value) {
        buttonStr += firstItemOpts.btnTpl[value] || "";
      });

      // Create markup from base template, it will be initially hidden to
      // avoid unnecessary work like painting while initializing is not complete
      $container = $(
        self.translate(
          self,
          firstItemOpts.baseTpl
            .replace("{{buttons}}", buttonStr)
            .replace("{{arrows}}", firstItemOpts.btnTpl.arrowLeft + firstItemOpts.btnTpl.arrowRight)
        )
      )
        .attr("id", "fancybox-container-" + self.id)
        .addClass("fancybox-is-hidden")
        .addClass(firstItemOpts.baseClass)
        .data("FancyBox", self)
        .appendTo(firstItemOpts.parentEl);

      // Create object holding references to jQuery wrapped nodes
      self.$refs = {
        container: $container
      };

      ["bg", "inner", "infobar", "toolbar", "stage", "caption", "navigation"].forEach(function(item) {
        self.$refs[item] = $container.find(".fancybox-" + item);
      });

      self.trigger("onInit");

      // Enable events, deactive previous instances
      self.activate();

      // Build slides, load and reveal content
      self.jumpTo(self.currIndex);
    },

    // Simple i18n support - replaces object keys found in template
    // with corresponding values
    // ============================================================

    translate: function(obj, str) {
      var arr = obj.opts.i18n[obj.opts.lang];

      return str.replace(/\{\{(\w+)\}\}/g, function(match, n) {
        var value = arr[n];

        if (value === undefined) {
          return match;
        }

        return value;
      });
    },

    // Populate current group with fresh content
    // Check if each object has valid type and content
    // ===============================================

    addContent: function(content) {
      var self = this,
        items = $.makeArray(content),
        thumbs;

      $.each(items, function(i, item) {
        var obj = {},
          opts = {},
          $item,
          type,
          found,
          src,
          srcParts;

        // Step 1 - Make sure we have an object
        // ====================================

        if ($.isPlainObject(item)) {
          // We probably have manual usage here, something like
          // $.fancybox.open( [ { src : "image.jpg", type : "image" } ] )

          obj = item;
          opts = item.opts || item;
        } else if ($.type(item) === "object" && $(item).length) {
          // Here we probably have jQuery collection returned by some selector
          $item = $(item);

          // Support attributes like `data-options='{"touch" : false}'` and `data-touch='false'`
          opts = $item.data() || {};
          opts = $.extend(true, {}, opts, opts.options);

          // Here we store clicked element
          opts.$orig = $item;

          obj.src = self.opts.src || opts.src || $item.attr("href");

          // Assume that simple syntax is used, for example:
          //   `$.fancybox.open( $("#test"), {} );`
          if (!obj.type && !obj.src) {
            obj.type = "inline";
            obj.src = item;
          }
        } else {
          // Assume we have a simple html code, for example:
          //   $.fancybox.open( '<div><h1>Hi!</h1></div>' );
          obj = {
            type: "html",
            src: item + ""
          };
        }

        // Each gallery object has full collection of options
        obj.opts = $.extend(true, {}, self.opts, opts);

        // Do not merge buttons array
        if ($.isArray(opts.buttons)) {
          obj.opts.buttons = opts.buttons;
        }

        // Step 2 - Make sure we have content type, if not - try to guess
        // ==============================================================

        type = obj.type || obj.opts.type;
        src = obj.src || "";

        if (!type && src) {
          if ((found = src.match(/\.(mp4|mov|ogv)((\?|#).*)?$/i))) {
            type = "video";

            if (!obj.opts.videoFormat) {
              obj.opts.videoFormat = "video/" + (found[1] === "ogv" ? "ogg" : found[1]);
            }
          } else if (src.match(/(^data:image\/[a-z0-9+\/=]*,)|(\.(jp(e|g|eg)|gif|png|bmp|webp|svg|ico)((\?|#).*)?$)/i)) {
            type = "image";
          } else if (src.match(/\.(pdf)((\?|#).*)?$/i)) {
            type = "iframe";
          } else if (src.charAt(0) === "#") {
            type = "inline";
          }
        }

        if (type) {
          obj.type = type;
        } else {
          self.trigger("objectNeedsType", obj);
        }

        if (!obj.contentType) {
          obj.contentType = $.inArray(obj.type, ["html", "inline", "ajax"]) > -1 ? "html" : obj.type;
        }

        // Step 3 - Some adjustments
        // =========================

        obj.index = self.group.length;

        if (obj.opts.smallBtn == "auto") {
          obj.opts.smallBtn = $.inArray(obj.type, ["html", "inline", "ajax"]) > -1;
        }

        if (obj.opts.toolbar === "auto") {
          obj.opts.toolbar = !obj.opts.smallBtn;
        }

        // Find thumbnail image
        if (obj.opts.$trigger && obj.index === self.opts.index) {
          obj.opts.$thumb = obj.opts.$trigger.find("img:first");
        }

        if ((!obj.opts.$thumb || !obj.opts.$thumb.length) && obj.opts.$orig) {
          obj.opts.$thumb = obj.opts.$orig.find("img:first");
        }

        // "caption" is a "special" option, it can be used to customize caption per gallery item ..
        if ($.type(obj.opts.caption) === "function") {
          obj.opts.caption = obj.opts.caption.apply(item, [self, obj]);
        }

        if ($.type(self.opts.caption) === "function") {
          obj.opts.caption = self.opts.caption.apply(item, [self, obj]);
        }

        // Make sure we have caption as a string or jQuery object
        if (!(obj.opts.caption instanceof $)) {
          obj.opts.caption = obj.opts.caption === undefined ? "" : obj.opts.caption + "";
        }

        // Check if url contains "filter" used to filter the content
        // Example: "ajax.html #something"
        if (obj.type === "ajax") {
          srcParts = src.split(/\s+/, 2);

          if (srcParts.length > 1) {
            obj.src = srcParts.shift();

            obj.opts.filter = srcParts.shift();
          }
        }

        // Hide all buttons and disable interactivity for modal items
        if (obj.opts.modal) {
          obj.opts = $.extend(true, obj.opts, {
            // Remove buttons
            infobar: 0,
            toolbar: 0,

            smallBtn: 0,

            // Disable keyboard navigation
            keyboard: 0,

            // Disable some modules
            slideShow: 0,
            fullScreen: 0,
            thumbs: 0,
            touch: 0,

            // Disable click event handlers
            clickContent: false,
            clickSlide: false,
            clickOutside: false,
            dblclickContent: false,
            dblclickSlide: false,
            dblclickOutside: false
          });
        }

        // Step 4 - Add processed object to group
        // ======================================

        self.group.push(obj);
      });

      // Update controls if gallery is already opened
      if (Object.keys(self.slides).length) {
        self.updateControls();

        // Update thumbnails, if needed
        thumbs = self.Thumbs;

        if (thumbs && thumbs.isActive) {
          thumbs.create();

          thumbs.focus();
        }
      }
    },

    // Attach an event handler functions for:
    //   - navigation buttons
    //   - browser scrolling, resizing;
    //   - focusing
    //   - keyboard
    //   - detect idle
    // ======================================

    addEvents: function() {
      var self = this;

      self.removeEvents();

      // Make navigation elements clickable
      self.$refs.container
        .on("click.fb-close", "[data-fancybox-close]", function(e) {
          e.stopPropagation();
          e.preventDefault();

          self.close(e);
        })
        .on("touchstart.fb-prev click.fb-prev", "[data-fancybox-prev]", function(e) {
          e.stopPropagation();
          e.preventDefault();

          self.previous();
        })
        .on("touchstart.fb-next click.fb-next", "[data-fancybox-next]", function(e) {
          e.stopPropagation();
          e.preventDefault();

          self.next();
        })
        .on("click.fb", "[data-fancybox-zoom]", function(e) {
          // Click handler for zoom button
          self[self.isScaledDown() ? "scaleToActual" : "scaleToFit"]();
        });

      // Handle page scrolling and browser resizing
      $W.on("orientationchange.fb resize.fb", function(e) {
        if (e && e.originalEvent && e.originalEvent.type === "resize") {
          requestAFrame(function() {
            self.update();
          });
        } else {
          self.$refs.stage.hide();

          setTimeout(function() {
            self.$refs.stage.show();

            self.update();
          }, $.fancybox.isMobile ? 600 : 250);
        }
      });

      // Trap keyboard focus inside of the modal, so the user does not accidentally tab outside of the modal
      // (a.k.a. "escaping the modal")
      $D.on("focusin.fb", function(e) {
        var instance = $.fancybox ? $.fancybox.getInstance() : null;

        if (
          instance.isClosing ||
          !instance.current ||
          !instance.current.opts.trapFocus ||
          $(e.target).hasClass("fancybox-container") ||
          $(e.target).is(document)
        ) {
          return;
        }

        if (instance && $(e.target).css("position") !== "fixed" && !instance.$refs.container.has(e.target).length) {
          e.stopPropagation();

          instance.focus();
        }
      });

      // Enable keyboard navigation
      $D.on("keydown.fb", function(e) {
        var current = self.current,
          keycode = e.keyCode || e.which;

        if (!current || !current.opts.keyboard) {
          return;
        }

        if (e.ctrlKey || e.altKey || e.shiftKey || $(e.target).is("input") || $(e.target).is("textarea")) {
          return;
        }

        // Backspace and Esc keys
        if (keycode === 8 || keycode === 27) {
          e.preventDefault();

          self.close(e);

          return;
        }

        // Left arrow and Up arrow
        if (keycode === 37 || keycode === 38) {
          e.preventDefault();

          self.previous();

          return;
        }

        // Righ arrow and Down arrow
        if (keycode === 39 || keycode === 40) {
          e.preventDefault();

          self.next();

          return;
        }

        self.trigger("afterKeydown", e, keycode);
      });

      // Hide controls after some inactivity period
      if (self.group[self.currIndex].opts.idleTime) {
        self.idleSecondsCounter = 0;

        $D.on(
          "mousemove.fb-idle mouseleave.fb-idle mousedown.fb-idle touchstart.fb-idle touchmove.fb-idle scroll.fb-idle keydown.fb-idle",
          function(e) {
            self.idleSecondsCounter = 0;

            if (self.isIdle) {
              self.showControls();
            }

            self.isIdle = false;
          }
        );

        self.idleInterval = window.setInterval(function() {
          self.idleSecondsCounter++;

          if (self.idleSecondsCounter >= self.group[self.currIndex].opts.idleTime && !self.isDragging) {
            self.isIdle = true;
            self.idleSecondsCounter = 0;

            self.hideControls();
          }
        }, 1000);
      }
    },

    // Remove events added by the core
    // ===============================

    removeEvents: function() {
      var self = this;

      $W.off("orientationchange.fb resize.fb");
      $D.off("focusin.fb keydown.fb .fb-idle");

      this.$refs.container.off(".fb-close .fb-prev .fb-next");

      if (self.idleInterval) {
        window.clearInterval(self.idleInterval);

        self.idleInterval = null;
      }
    },

    // Change to previous gallery item
    // ===============================

    previous: function(duration) {
      return this.jumpTo(this.currPos - 1, duration);
    },

    // Change to next gallery item
    // ===========================

    next: function(duration) {
      return this.jumpTo(this.currPos + 1, duration);
    },

    // Switch to selected gallery item
    // ===============================

    jumpTo: function(pos, duration) {
      var self = this,
        groupLen = self.group.length,
        firstRun,
        loop,
        current,
        previous,
        canvasWidth,
        currentPos,
        transitionProps;

      if (self.isDragging || self.isClosing || (self.isAnimating && self.firstRun)) {
        return;
      }

      pos = parseInt(pos, 10);

      // Should loop?
      loop = self.current ? self.current.opts.loop : self.opts.loop;

      if (!loop && (pos < 0 || pos >= groupLen)) {
        return false;
      }

      firstRun = self.firstRun = !Object.keys(self.slides).length;

      if (groupLen < 2 && !firstRun && !!self.isDragging) {
        return;
      }

      previous = self.current;

      self.prevIndex = self.currIndex;
      self.prevPos = self.currPos;

      // Create slides
      current = self.createSlide(pos);

      if (groupLen > 1) {
        if (loop || current.index > 0) {
          self.createSlide(pos - 1);
        }

        if (loop || current.index < groupLen - 1) {
          self.createSlide(pos + 1);
        }
      }

      self.current = current;
      self.currIndex = current.index;
      self.currPos = current.pos;

      self.trigger("beforeShow", firstRun);

      self.updateControls();

      currentPos = $.fancybox.getTranslate(current.$slide);

      current.isMoved = (currentPos.left !== 0 || currentPos.top !== 0) && !current.$slide.hasClass("fancybox-animated");

      // Validate duration length
      current.forcedDuration = undefined;

      if ($.isNumeric(duration)) {
        current.forcedDuration = duration;
      } else {
        duration = current.opts[firstRun ? "animationDuration" : "transitionDuration"];
      }

      duration = parseInt(duration, 10);

      // Fresh start - reveal container, current slide and start loading content
      if (firstRun) {
        if (current.opts.animationEffect && duration) {
          self.$refs.container.css("transition-duration", duration + "ms");
        }

        self.$refs.container.removeClass("fancybox-is-hidden");

        forceRedraw(self.$refs.container);

        self.$refs.container.addClass("fancybox-is-open");

        forceRedraw(self.$refs.container);

        // Make current slide visible
        current.$slide.addClass("fancybox-slide--previous");

        // Attempt to load content into slide;
        // at this point image would start loading, but inline/html content would load immediately
        self.loadSlide(current);

        current.$slide.removeClass("fancybox-slide--previous").addClass("fancybox-slide--current");

        self.preload("image");

        return;
      }

      // Clean up
      $.each(self.slides, function(index, slide) {
        $.fancybox.stop(slide.$slide);
      });

      // Make current that slide is visible even if content is still loading
      current.$slide.removeClass("fancybox-slide--next fancybox-slide--previous").addClass("fancybox-slide--current");

      // If slides have been dragged, animate them to correct position
      if (current.isMoved) {
        canvasWidth = Math.round(current.$slide.width());

        $.each(self.slides, function(index, slide) {
          var pos = slide.pos - current.pos;

          $.fancybox.animate(
            slide.$slide,
            {
              top: 0,
              left: pos * canvasWidth + pos * slide.opts.gutter
            },
            duration,
            function() {
              slide.$slide.removeAttr("style").removeClass("fancybox-slide--next fancybox-slide--previous");

              if (slide.pos === self.currPos) {
                current.isMoved = false;

                self.complete();
              }
            }
          );
        });
      } else {
        self.$refs.stage.children().removeAttr("style");
      }

      // Start transition that reveals current content
      // or wait when it will be loaded

      if (current.isLoaded) {
        self.revealContent(current);
      } else {
        self.loadSlide(current);
      }

      self.preload("image");

      if (previous.pos === current.pos) {
        return;
      }

      // Handle previous slide
      // =====================

      transitionProps = "fancybox-slide--" + (previous.pos > current.pos ? "next" : "previous");

      previous.$slide.removeClass("fancybox-slide--complete fancybox-slide--current fancybox-slide--next fancybox-slide--previous");

      previous.isComplete = false;

      if (!duration || (!current.isMoved && !current.opts.transitionEffect)) {
        return;
      }

      if (current.isMoved) {
        previous.$slide.addClass(transitionProps);
      } else {
        transitionProps = "fancybox-animated " + transitionProps + " fancybox-fx-" + current.opts.transitionEffect;

        $.fancybox.animate(previous.$slide, transitionProps, duration, function() {
          previous.$slide.removeClass(transitionProps).removeAttr("style");
        });
      }
    },

    // Create new "slide" element
    // These are gallery items  that are actually added to DOM
    // =======================================================

    createSlide: function(pos) {
      var self = this,
        $slide,
        index;

      index = pos % self.group.length;
      index = index < 0 ? self.group.length + index : index;

      if (!self.slides[pos] && self.group[index]) {
        $slide = $('<div class="fancybox-slide"></div>').appendTo(self.$refs.stage);

        self.slides[pos] = $.extend(true, {}, self.group[index], {
          pos: pos,
          $slide: $slide,
          isLoaded: false
        });

        self.updateSlide(self.slides[pos]);
      }

      return self.slides[pos];
    },

    // Scale image to the actual size of the image;
    // x and y values should be relative to the slide
    // ==============================================

    scaleToActual: function(x, y, duration) {
      var self = this,
        current = self.current,
        $content = current.$content,
        canvasWidth = $.fancybox.getTranslate(current.$slide).width,
        canvasHeight = $.fancybox.getTranslate(current.$slide).height,
        newImgWidth = current.width,
        newImgHeight = current.height,
        imgPos,
        posX,
        posY,
        scaleX,
        scaleY;

      if (self.isAnimating || !$content || !(current.type == "image" && current.isLoaded && !current.hasError)) {
        return;
      }

      $.fancybox.stop($content);

      self.isAnimating = true;

      x = x === undefined ? canvasWidth * 0.5 : x;
      y = y === undefined ? canvasHeight * 0.5 : y;

      imgPos = $.fancybox.getTranslate($content);

      imgPos.top -= $.fancybox.getTranslate(current.$slide).top;
      imgPos.left -= $.fancybox.getTranslate(current.$slide).left;

      scaleX = newImgWidth / imgPos.width;
      scaleY = newImgHeight / imgPos.height;

      // Get center position for original image
      posX = canvasWidth * 0.5 - newImgWidth * 0.5;
      posY = canvasHeight * 0.5 - newImgHeight * 0.5;

      // Make sure image does not move away from edges
      if (newImgWidth > canvasWidth) {
        posX = imgPos.left * scaleX - (x * scaleX - x);

        if (posX > 0) {
          posX = 0;
        }

        if (posX < canvasWidth - newImgWidth) {
          posX = canvasWidth - newImgWidth;
        }
      }

      if (newImgHeight > canvasHeight) {
        posY = imgPos.top * scaleY - (y * scaleY - y);

        if (posY > 0) {
          posY = 0;
        }

        if (posY < canvasHeight - newImgHeight) {
          posY = canvasHeight - newImgHeight;
        }
      }

      self.updateCursor(newImgWidth, newImgHeight);

      $.fancybox.animate(
        $content,
        {
          top: posY,
          left: posX,
          scaleX: scaleX,
          scaleY: scaleY
        },
        duration || 330,
        function() {
          self.isAnimating = false;
        }
      );

      // Stop slideshow
      if (self.SlideShow && self.SlideShow.isActive) {
        self.SlideShow.stop();
      }
    },

    // Scale image to fit inside parent element
    // ========================================

    scaleToFit: function(duration) {
      var self = this,
        current = self.current,
        $content = current.$content,
        end;

      if (self.isAnimating || !$content || !(current.type == "image" && current.isLoaded && !current.hasError)) {
        return;
      }

      $.fancybox.stop($content);

      self.isAnimating = true;

      end = self.getFitPos(current);

      self.updateCursor(end.width, end.height);

      $.fancybox.animate(
        $content,
        {
          top: end.top,
          left: end.left,
          scaleX: end.width / $content.width(),
          scaleY: end.height / $content.height()
        },
        duration || 330,
        function() {
          self.isAnimating = false;
        }
      );
    },

    // Calculate image size to fit inside viewport
    // ===========================================

    getFitPos: function(slide) {
      var self = this,
        $content = slide.$content,
        width = slide.width || slide.opts.width,
        height = slide.height || slide.opts.height,
        maxWidth,
        maxHeight,
        minRatio,
        margin,
        aspectRatio,
        rez = {};

      if (!slide.isLoaded || !$content || !$content.length) {
        return false;
      }

      margin = {
        top: parseInt(slide.$slide.css("paddingTop"), 10),
        right: parseInt(slide.$slide.css("paddingRight"), 10),
        bottom: parseInt(slide.$slide.css("paddingBottom"), 10),
        left: parseInt(slide.$slide.css("paddingLeft"), 10)
      };

      // We can not use $slide width here, because it can have different diemensions while in transiton
      maxWidth = parseInt(self.$refs.stage.width(), 10) - (margin.left + margin.right);
      maxHeight = parseInt(self.$refs.stage.height(), 10) - (margin.top + margin.bottom);

      if (!width || !height) {
        width = maxWidth;
        height = maxHeight;
      }

      minRatio = Math.min(1, maxWidth / width, maxHeight / height);

      // Use floor rounding to make sure it really fits
      width = Math.floor(minRatio * width);
      height = Math.floor(minRatio * height);

      if (slide.type === "image") {
        rez.top = Math.floor((maxHeight - height) * 0.5) + margin.top;
        rez.left = Math.floor((maxWidth - width) * 0.5) + margin.left;
      } else if (slide.contentType === "video") {
        // Force aspect ratio for the video
        // "I say the whole world must learn of our peaceful ways… by force!"
        aspectRatio = slide.opts.width && slide.opts.height ? width / height : slide.opts.ratio || 16 / 9;

        if (height > width / aspectRatio) {
          height = width / aspectRatio;
        } else if (width > height * aspectRatio) {
          width = height * aspectRatio;
        }
      }

      rez.width = width;
      rez.height = height;

      return rez;
    },

    // Update content size and position for all slides
    // ==============================================

    update: function() {
      var self = this;

      $.each(self.slides, function(key, slide) {
        self.updateSlide(slide);
      });
    },

    // Update slide content position and size
    // ======================================

    updateSlide: function(slide, duration) {
      var self = this,
        $content = slide && slide.$content,
        width = slide.width || slide.opts.width,
        height = slide.height || slide.opts.height;

      if ($content && (width || height || slide.contentType === "video") && !slide.hasError) {
        $.fancybox.stop($content);

        $.fancybox.setTranslate($content, self.getFitPos(slide));

        if (slide.pos === self.currPos) {
          self.isAnimating = false;

          self.updateCursor();
        }
      }

      slide.$slide.trigger("refresh");

      self.$refs.toolbar.toggleClass("compensate-for-scrollbar", slide.$slide.get(0).scrollHeight > slide.$slide.get(0).clientHeight);

      self.trigger("onUpdate", slide);
    },

    // Horizontally center slide
    // =========================

    centerSlide: function(slide, duration) {
      var self = this,
        canvasWidth,
        pos;

      if (self.current) {
        canvasWidth = Math.round(slide.$slide.width());
        pos = slide.pos - self.current.pos;

        $.fancybox.animate(
          slide.$slide,
          {
            top: 0,
            left: pos * canvasWidth + pos * slide.opts.gutter,
            opacity: 1
          },
          duration === undefined ? 0 : duration,
          null,
          false
        );
      }
    },

    // Update cursor style depending if content can be zoomed
    // ======================================================

    updateCursor: function(nextWidth, nextHeight) {
      var self = this,
        current = self.current,
        $container = self.$refs.container.removeClass("fancybox-is-zoomable fancybox-can-zoomIn fancybox-can-drag fancybox-can-zoomOut"),
        isZoomable;

      if (!current || self.isClosing) {
        return;
      }

      isZoomable = self.isZoomable();

      $container.toggleClass("fancybox-is-zoomable", isZoomable);

      $("[data-fancybox-zoom]").prop("disabled", !isZoomable);

      // Set cursor to zoom in/out if click event is 'zoom'
      if (
        isZoomable &&
        (current.opts.clickContent === "zoom" || ($.isFunction(current.opts.clickContent) && current.opts.clickContent(current) === "zoom"))
      ) {
        if (self.isScaledDown(nextWidth, nextHeight)) {
          // If image is scaled down, then, obviously, it can be zoomed to full size
          $container.addClass("fancybox-can-zoomIn");
        } else {
          if (current.opts.touch) {
            // If image size ir largen than available available and touch module is not disable,
            // then user can do panning
            $container.addClass("fancybox-can-drag");
          } else {
            $container.addClass("fancybox-can-zoomOut");
          }
        }
      } else if (current.opts.touch && current.contentType !== "video") {
        $container.addClass("fancybox-can-drag");
      }
    },

    // Check if current slide is zoomable
    // ==================================

    isZoomable: function() {
      var self = this,
        current = self.current,
        fitPos;

      // Assume that slide is zoomable if:
      //   - image is still loading
      //   - actual size of the image is smaller than available area
      if (current && !self.isClosing && current.type === "image" && !current.hasError) {
        if (!current.isLoaded) {
          return true;
        }

        fitPos = self.getFitPos(current);

        if (current.width > fitPos.width || current.height > fitPos.height) {
          return true;
        }
      }

      return false;
    },

    // Check if current image dimensions are smaller than actual
    // =========================================================

    isScaledDown: function(nextWidth, nextHeight) {
      var self = this,
        rez = false,
        current = self.current,
        $content = current.$content;

      if (nextWidth !== undefined && nextHeight !== undefined) {
        rez = nextWidth < current.width && nextHeight < current.height;
      } else if ($content) {
        rez = $.fancybox.getTranslate($content);
        rez = rez.width < current.width && rez.height < current.height;
      }

      return rez;
    },

    // Check if image dimensions exceed parent element
    // ===============================================

    canPan: function() {
      var self = this,
        rez = false,
        current = self.current,
        $content;

      if (current.type === "image" && ($content = current.$content) && !current.hasError) {
        rez = self.getFitPos(current);
        rez = Math.abs($content.width() - rez.width) > 1 || Math.abs($content.height() - rez.height) > 1;
      }

      return rez;
    },

    // Load content into the slide
    // ===========================

    loadSlide: function(slide) {
      var self = this,
        type,
        $slide,
        ajaxLoad;

      if (slide.isLoading || slide.isLoaded) {
        return;
      }

      slide.isLoading = true;

      self.trigger("beforeLoad", slide);

      type = slide.type;
      $slide = slide.$slide;

      $slide
        .off("refresh")
        .trigger("onReset")
        .addClass(slide.opts.slideClass);

      // Create content depending on the type
      switch (type) {
        case "image":
          self.setImage(slide);

          break;

        case "iframe":
          self.setIframe(slide);

          break;

        case "html":
          self.setContent(slide, slide.src || slide.content);

          break;

        case "video":
          self.setContent(
            slide,
            '<video class="fancybox-video" controls controlsList="nodownload">' +
              '<source src="' +
              slide.src +
              '" type="' +
              slide.opts.videoFormat +
              '">' +
              "Your browser doesn't support HTML5 video" +
              "</video"
          );

          break;

        case "inline":
          if ($(slide.src).length) {
            self.setContent(slide, $(slide.src));
          } else {
            self.setError(slide);
          }

          break;

        case "ajax":
          self.showLoading(slide);

          ajaxLoad = $.ajax(
            $.extend({}, slide.opts.ajax.settings, {
              url: slide.src,
              success: function(data, textStatus) {
                if (textStatus === "success") {
                  self.setContent(slide, data);
                }
              },
              error: function(jqXHR, textStatus) {
                if (jqXHR && textStatus !== "abort") {
                  self.setError(slide);
                }
              }
            })
          );

          $slide.one("onReset", function() {
            ajaxLoad.abort();
          });

          break;

        default:
          self.setError(slide);

          break;
      }

      return true;
    },

    // Use thumbnail image, if possible
    // ================================

    setImage: function(slide) {
      var self = this,
        srcset = slide.opts.srcset || slide.opts.image.srcset,
        thumbSrc,
        found,
        temp,
        pxRatio,
        windowWidth;

      // Check if need to show loading icon
      slide.timouts = setTimeout(function() {
        var $img = slide.$image;

        if (slide.isLoading && (!$img || !$img[0].complete) && !slide.hasError) {
          self.showLoading(slide);
        }
      }, 350);

      // If we have "srcset", then we need to find first matching "src" value.
      // This is necessary, because when you set an src attribute, the browser will preload the image
      // before any javascript or even CSS is applied.
      if (srcset) {
        pxRatio = window.devicePixelRatio || 1;
        windowWidth = window.innerWidth * pxRatio;

        temp = srcset.split(",").map(function(el) {
          var ret = {};

          el
            .trim()
            .split(/\s+/)
            .forEach(function(el, i) {
              var value = parseInt(el.substring(0, el.length - 1), 10);

              if (i === 0) {
                return (ret.url = el);
              }

              if (value) {
                ret.value = value;
                ret.postfix = el[el.length - 1];
              }
            });

          return ret;
        });

        // Sort by value
        temp.sort(function(a, b) {
          return a.value - b.value;
        });

        // Ok, now we have an array of all srcset values
        for (var j = 0; j < temp.length; j++) {
          var el = temp[j];

          if ((el.postfix === "w" && el.value >= windowWidth) || (el.postfix === "x" && el.value >= pxRatio)) {
            found = el;
            break;
          }
        }

        // If not found, take the last one
        if (!found && temp.length) {
          found = temp[temp.length - 1];
        }

        if (found) {
          slide.src = found.url;

          // If we have default width/height values, we can calculate height for matching source
          if (slide.width && slide.height && found.postfix == "w") {
            slide.height = slide.width / slide.height * found.value;
            slide.width = found.value;
          }

          slide.opts.srcset = srcset;
        }
      }

      // This will be wrapper containing both ghost and actual image
      slide.$content = $('<div class="fancybox-content"></div>')
        .addClass("fancybox-is-hidden")
        .appendTo(slide.$slide.addClass("fancybox-slide--image"));

      // If we have a thumbnail, we can display it while actual image is loading
      // Users will not stare at black screen and actual image will appear gradually
      thumbSrc = slide.opts.thumb || (slide.opts.$thumb && slide.opts.$thumb.length ? slide.opts.$thumb.attr("src") : false);

      if (slide.opts.preload !== false && slide.opts.width && slide.opts.height && thumbSrc) {
        slide.width = slide.opts.width;
        slide.height = slide.opts.height;

        slide.$ghost = $("<img />")
          .one("error", function() {
            $(this).remove();

            slide.$ghost = null;
          })
          .one("load", function() {
            self.afterLoad(slide);
          })
          .addClass("fancybox-image")
          .appendTo(slide.$content)
          .attr("src", thumbSrc);
      }

      // Start loading actual image
      self.setBigImage(slide);
    },

    // Create full-size image
    // ======================

    setBigImage: function(slide) {
      var self = this,
        $img = $("<img />");

      slide.$image = $img
        .one("error", function() {
          self.setError(slide);
        })
        .one("load", function() {
          var sizes;

          if (!slide.$ghost) {
            self.resolveImageSlideSize(slide, this.naturalWidth, this.naturalHeight);

            self.afterLoad(slide);
          }

          // Clear timeout that checks if loading icon needs to be displayed
          if (slide.timouts) {
            clearTimeout(slide.timouts);
            slide.timouts = null;
          }

          if (self.isClosing) {
            return;
          }

          if (slide.opts.srcset) {
            sizes = slide.opts.sizes;

            if (!sizes || sizes === "auto") {
              sizes =
                (slide.width / slide.height > 1 && $W.width() / $W.height() > 1 ? "100" : Math.round(slide.width / slide.height * 100)) +
                "vw";
            }

            $img.attr("sizes", sizes).attr("srcset", slide.opts.srcset);
          }

          // Hide temporary image after some delay
          if (slide.$ghost) {
            setTimeout(function() {
              if (slide.$ghost && !self.isClosing) {
                slide.$ghost.hide();
              }
            }, Math.min(300, Math.max(1000, slide.height / 1600)));
          }

          self.hideLoading(slide);
        })
        .addClass("fancybox-image")
        .attr("src", slide.src)
        .appendTo(slide.$content);

      if (($img[0].complete || $img[0].readyState == "complete") && $img[0].naturalWidth && $img[0].naturalHeight) {
        $img.trigger("load");
      } else if ($img[0].error) {
        $img.trigger("error");
      }
    },

    // Computes the slide size from image size and maxWidth/maxHeight
    // ==============================================================

    resolveImageSlideSize: function(slide, imgWidth, imgHeight) {
      var maxWidth = parseInt(slide.opts.width, 10),
        maxHeight = parseInt(slide.opts.height, 10);

      // Sets the default values from the image
      slide.width = imgWidth;
      slide.height = imgHeight;

      if (maxWidth > 0) {
        slide.width = maxWidth;
        slide.height = Math.floor(maxWidth * imgHeight / imgWidth);
      }

      if (maxHeight > 0) {
        slide.width = Math.floor(maxHeight * imgWidth / imgHeight);
        slide.height = maxHeight;
      }
    },

    // Create iframe wrapper, iframe and bindings
    // ==========================================

    setIframe: function(slide) {
      var self = this,
        opts = slide.opts.iframe,
        $slide = slide.$slide,
        $iframe;

      slide.$content = $('<div class="fancybox-content' + (opts.preload ? " fancybox-is-hidden" : "") + '"></div>')
        .css(opts.css)
        .appendTo($slide);

      $slide.addClass("fancybox-slide--" + slide.contentType);

      slide.$iframe = $iframe = $(opts.tpl.replace(/\{rnd\}/g, new Date().getTime()))
        .attr(opts.attr)
        .appendTo(slide.$content);

      if (opts.preload) {
        self.showLoading(slide);

        // Unfortunately, it is not always possible to determine if iframe is successfully loaded
        // (due to browser security policy)

        $iframe.on("load.fb error.fb", function(e) {
          this.isReady = 1;

          slide.$slide.trigger("refresh");

          self.afterLoad(slide);
        });

        // Recalculate iframe content size
        // ===============================

        $slide.on("refresh.fb", function() {
          var $content = slide.$content,
            frameWidth = opts.css.width,
            frameHeight = opts.css.height,
            $contents,
            $body;

          if ($iframe[0].isReady !== 1) {
            return;
          }

          try {
            $contents = $iframe.contents();
            $body = $contents.find("body");
          } catch (ignore) {}

          // Calculate contnet dimensions if it is accessible
          if ($body && $body.length && $body.children().length) {
            $content.css({
              width: "",
              height: ""
            });

            if (frameWidth === undefined) {
              frameWidth = Math.ceil(Math.max($body[0].clientWidth, $body.outerWidth(true)));
            }

            if (frameWidth) {
              $content.width(frameWidth);
            }

            if (frameHeight === undefined) {
              frameHeight = Math.ceil(Math.max($body[0].clientHeight, $body.outerHeight(true)));
            }

            if (frameHeight) {
              $content.height(frameHeight);
            }
          }

          $content.removeClass("fancybox-is-hidden");
        });
      } else {
        this.afterLoad(slide);
      }

      $iframe.attr("src", slide.src);

      // Remove iframe if closing or changing gallery item
      $slide.one("onReset", function() {
        // This helps IE not to throw errors when closing
        try {
          $(this)
            .find("iframe")
            .hide()
            .unbind()
            .attr("src", "//about:blank");
        } catch (ignore) {}

        $(this)
          .off("refresh.fb")
          .empty();

        slide.isLoaded = false;
      });
    },

    // Wrap and append content to the slide
    // ======================================

    setContent: function(slide, content) {
      var self = this;

      if (self.isClosing) {
        return;
      }

      self.hideLoading(slide);

      if (slide.$content) {
        $.fancybox.stop(slide.$content);
      }

      slide.$slide.empty();

      // If content is a jQuery object, then it will be moved to the slide.
      // The placeholder is created so we will know where to put it back.
      if (isQuery(content) && content.parent().length) {
        // Make sure content is not already moved to fancyBox
        content
          .parent()
          .parent(".fancybox-slide--inline")
          .trigger("onReset");

        // Create temporary element marking original place of the content
        slide.$placeholder = $("<div>")
          .hide()
          .insertAfter(content);

        // Make sure content is visible
        content.css("display", "inline-block");
      } else if (!slide.hasError) {
        // If content is just a plain text, try to convert it to html
        if ($.type(content) === "string") {
          content = $("<div>")
            .append($.trim(content))
            .contents();

          // If we have text node, then add wrapping element to make vertical alignment work
          if (content[0].nodeType === 3) {
            content = $("<div>").html(content);
          }
        }

        // If "filter" option is provided, then filter content
        if (slide.opts.filter) {
          content = $("<div>")
            .html(content)
            .find(slide.opts.filter);
        }
      }

      slide.$slide.one("onReset", function() {
        // Pause all html5 video/audio
        $(this)
          .find("video,audio")
          .trigger("pause");

        // Put content back
        if (slide.$placeholder) {
          slide.$placeholder.after(content.hide()).remove();

          slide.$placeholder = null;
        }

        // Remove custom close button
        if (slide.$smallBtn) {
          slide.$smallBtn.remove();

          slide.$smallBtn = null;
        }

        // Remove content and mark slide as not loaded
        if (!slide.hasError) {
          $(this).empty();

          slide.isLoaded = false;
        }
      });

      $(content).appendTo(slide.$slide);

      if ($(content).is("video,audio")) {
        $(content).addClass("fancybox-video");

        $(content).wrap("<div></div>");

        slide.contentType = "video";

        slide.opts.width = slide.opts.width || $(content).attr("width");
        slide.opts.height = slide.opts.height || $(content).attr("height");
      }

      slide.$content = slide.$slide
        .children()
        .filter("div,form,main,video,audio")
        .first()
        .addClass("fancybox-content");

      slide.$slide.addClass("fancybox-slide--" + slide.contentType);

      this.afterLoad(slide);
    },

    // Display error message
    // =====================

    setError: function(slide) {
      slide.hasError = true;

      slide.$slide
        .trigger("onReset")
        .removeClass("fancybox-slide--" + slide.contentType)
        .addClass("fancybox-slide--error");

      slide.contentType = "html";

      this.setContent(slide, this.translate(slide, slide.opts.errorTpl));

      if (slide.pos === this.currPos) {
        this.isAnimating = false;
      }
    },

    // Show loading icon inside the slide
    // ==================================

    showLoading: function(slide) {
      var self = this;

      slide = slide || self.current;

      if (slide && !slide.$spinner) {
        slide.$spinner = $(self.translate(self, self.opts.spinnerTpl)).appendTo(slide.$slide);
      }
    },

    // Remove loading icon from the slide
    // ==================================

    hideLoading: function(slide) {
      var self = this;

      slide = slide || self.current;

      if (slide && slide.$spinner) {
        slide.$spinner.remove();

        delete slide.$spinner;
      }
    },

    // Adjustments after slide content has been loaded
    // ===============================================

    afterLoad: function(slide) {
      var self = this;

      if (self.isClosing) {
        return;
      }

      slide.isLoading = false;
      slide.isLoaded = true;

      self.trigger("afterLoad", slide);

      self.hideLoading(slide);

      if (slide.pos === self.currPos) {
        self.updateCursor();
      }

      if (slide.opts.smallBtn && (!slide.$smallBtn || !slide.$smallBtn.length)) {
        slide.$smallBtn = $(self.translate(slide, slide.opts.btnTpl.smallBtn)).prependTo(slide.$content);
      }

      if (slide.opts.protect && slide.$content && !slide.hasError) {
        // Disable right click
        slide.$content.on("contextmenu.fb", function(e) {
          if (e.button == 2) {
            e.preventDefault();
          }

          return true;
        });

        // Add fake element on top of the image
        // This makes a bit harder for user to select image
        if (slide.type === "image") {
          $('<div class="fancybox-spaceball"></div>').appendTo(slide.$content);
        }
      }

      self.revealContent(slide);
    },

    // Make content visible
    // This method is called right after content has been loaded or
    // user navigates gallery and transition should start
    // ============================================================

    revealContent: function(slide) {
      var self = this,
        $slide = slide.$slide,
        end = false,
        start = false,
        effect,
        effectClassName,
        duration,
        opacity;

      effect = slide.opts[self.firstRun ? "animationEffect" : "transitionEffect"];
      duration = slide.opts[self.firstRun ? "animationDuration" : "transitionDuration"];

      duration = parseInt(slide.forcedDuration === undefined ? duration : slide.forcedDuration, 10);

      // Do not animate if revealing the same slide
      if (slide.pos === self.currPos) {
        if (slide.isComplete) {
          effect = false;
        } else {
          self.isAnimating = true;
        }
      }

      if (slide.isMoved || slide.pos !== self.currPos || !duration) {
        effect = false;
      }

      // Check if can zoom
      if (effect === "zoom") {
        if (slide.pos === self.currPos && duration && slide.type === "image" && !slide.hasError && (start = self.getThumbPos(slide))) {
          end = self.getFitPos(slide);
        } else {
          effect = "fade";
        }
      }

      // Zoom animation
      // ==============
      if (effect === "zoom") {
        end.scaleX = end.width / start.width;
        end.scaleY = end.height / start.height;

        // Check if we need to animate opacity
        opacity = slide.opts.zoomOpacity;

        if (opacity == "auto") {
          opacity = Math.abs(slide.width / slide.height - start.width / start.height) > 0.1;
        }

        if (opacity) {
          start.opacity = 0.1;
          end.opacity = 1;
        }

        // Draw image at start position
        $.fancybox.setTranslate(slide.$content.removeClass("fancybox-is-hidden"), start);

        forceRedraw(slide.$content);

        // Start animation
        $.fancybox.animate(slide.$content, end, duration, function() {
          self.isAnimating = false;

          self.complete();
        });

        return;
      }

      self.updateSlide(slide);

      // Simply show content
      // ===================

      if (!effect) {
        forceRedraw($slide);

        slide.$content.removeClass("fancybox-is-hidden");

        if (slide.pos === self.currPos) {
          self.complete();
        }

        return;
      }

      $.fancybox.stop($slide);

      effectClassName = "fancybox-animated fancybox-slide--" + (slide.pos >= self.prevPos ? "next" : "previous") + " fancybox-fx-" + effect;

      $slide
        .removeAttr("style")
        .removeClass("fancybox-slide--current fancybox-slide--next fancybox-slide--previous")
        .addClass(effectClassName);

      slide.$content.removeClass("fancybox-is-hidden");

      // Force reflow for CSS3 transitions
      forceRedraw($slide);

      $.fancybox.animate(
        $slide,
        "fancybox-slide--current",
        duration,
        function(e) {
          $slide.removeClass(effectClassName).removeAttr("style");

          if (slide.pos === self.currPos) {
            self.complete();
          }
        },
        true
      );
    },

    // Check if we can and have to zoom from thumbnail
    //================================================

    getThumbPos: function(slide) {
      var self = this,
        rez = false,
        $thumb = slide.opts.$thumb,
        thumbPos = $thumb && $thumb.length && $thumb[0].ownerDocument === document ? $thumb.offset() : 0,
        slidePos;

      // Check if element is inside the viewport by at least 1 pixel
      var isElementVisible = function($el) {
        var element = $el[0],
          elementRect = element.getBoundingClientRect(),
          parentRects = [],
          visibleInAllParents;

        while (element.parentElement !== null) {
          if ($(element.parentElement).css("overflow") === "hidden" || $(element.parentElement).css("overflow") === "auto") {
            parentRects.push(element.parentElement.getBoundingClientRect());
          }

          element = element.parentElement;
        }

        visibleInAllParents = parentRects.every(function(parentRect) {
          var visiblePixelX = Math.min(elementRect.right, parentRect.right) - Math.max(elementRect.left, parentRect.left);
          var visiblePixelY = Math.min(elementRect.bottom, parentRect.bottom) - Math.max(elementRect.top, parentRect.top);

          return visiblePixelX > 0 && visiblePixelY > 0;
        });

        return (
          visibleInAllParents &&
          elementRect.bottom > 0 &&
          elementRect.right > 0 &&
          elementRect.left < $(window).width() &&
          elementRect.top < $(window).height()
        );
      };

      if (thumbPos && isElementVisible($thumb)) {
        slidePos = self.$refs.stage.offset();

        rez = {
          top: thumbPos.top - slidePos.top + parseFloat($thumb.css("border-top-width") || 0),
          left: thumbPos.left - slidePos.left + parseFloat($thumb.css("border-left-width") || 0),
          width: $thumb.width(),
          height: $thumb.height(),
          scaleX: 1,
          scaleY: 1
        };
      }

      return rez;
    },

    // Final adjustments after current gallery item is moved to position
    // and it`s content is loaded
    // ==================================================================

    complete: function() {
      var self = this,
        current = self.current,
        slides = {};

      if (current.isMoved || !current.isLoaded) {
        return;
      }

      if (!current.isComplete) {
        current.isComplete = true;

        current.$slide.siblings().trigger("onReset");

        self.preload("inline");

        // Trigger any CSS3 transiton inside the slide
        forceRedraw(current.$slide);

        current.$slide.addClass("fancybox-slide--complete");

        // Remove unnecessary slides
        $.each(self.slides, function(key, slide) {
          if (slide.pos >= self.currPos - 1 && slide.pos <= self.currPos + 1) {
            slides[slide.pos] = slide;
          } else if (slide) {
            $.fancybox.stop(slide.$slide);

            slide.$slide.off().remove();
          }
        });

        self.slides = slides;
      }

      self.isAnimating = false;

      self.updateCursor();

      self.trigger("afterShow");

      // Play first html5 video/audio
      current.$slide
        .find("video,audio")
        .filter(":visible:first")
        .trigger("play");

      // Try to focus on the first focusable element
      if (
        $(document.activeElement).is("[disabled]") ||
        (current.opts.autoFocus && !(current.type == "image" || current.type === "iframe"))
      ) {
        self.focus();
      }
    },

    // Preload next and previous slides
    // ================================

    preload: function(type) {
      var self = this,
        next = self.slides[self.currPos + 1],
        prev = self.slides[self.currPos - 1];

      if (next && next.type === type) {
        self.loadSlide(next);
      }

      if (prev && prev.type === type) {
        self.loadSlide(prev);
      }
    },

    // Try to find and focus on the first focusable element
    // ====================================================

    focus: function() {
      var current = this.current,
        $el;

      if (this.isClosing) {
        return;
      }

      if (current && current.isComplete && current.$content) {
        // Look for first input with autofocus attribute
        $el = current.$content.find("input[autofocus]:enabled:visible:first");

        if (!$el.length) {
          $el = current.$content.find("button,:input,[tabindex],a").filter(":enabled:visible:first");
        }

        $el = $el && $el.length ? $el : current.$content;

        $el.trigger("focus");
      }
    },

    // Activates current instance - brings container to the front and enables keyboard,
    // notifies other instances about deactivating
    // =================================================================================

    activate: function() {
      var self = this;

      // Deactivate all instances
      $(".fancybox-container").each(function() {
        var instance = $(this).data("FancyBox");

        // Skip self and closing instances
        if (instance && instance.id !== self.id && !instance.isClosing) {
          instance.trigger("onDeactivate");

          instance.removeEvents();

          instance.isVisible = false;
        }
      });

      self.isVisible = true;

      if (self.current || self.isIdle) {
        self.update();

        self.updateControls();
      }

      self.trigger("onActivate");

      self.addEvents();
    },

    // Start closing procedure
    // This will start "zoom-out" animation if needed and clean everything up afterwards
    // =================================================================================

    close: function(e, d) {
      var self = this,
        current = self.current,
        effect,
        duration,
        $content,
        domRect,
        opacity,
        start,
        end;

      var done = function() {
        self.cleanUp(e);
      };

      if (self.isClosing) {
        return false;
      }

      self.isClosing = true;

      // If beforeClose callback prevents closing, make sure content is centered
      if (self.trigger("beforeClose", e) === false) {
        self.isClosing = false;

        requestAFrame(function() {
          self.update();
        });

        return false;
      }

      // Remove all events
      // If there are multiple instances, they will be set again by "activate" method
      self.removeEvents();

      if (current.timouts) {
        clearTimeout(current.timouts);
      }

      $content = current.$content;
      effect = current.opts.animationEffect;
      duration = $.isNumeric(d) ? d : effect ? current.opts.animationDuration : 0;

      // Remove other slides
      current.$slide
        .off(transitionEnd)
        .removeClass("fancybox-slide--complete fancybox-slide--next fancybox-slide--previous fancybox-animated");

      current.$slide
        .siblings()
        .trigger("onReset")
        .remove();

      // Trigger animations
      if (duration) {
        self.$refs.container.removeClass("fancybox-is-open").addClass("fancybox-is-closing");
      }

      // Clean up
      self.hideLoading(current);

      self.hideControls();

      self.updateCursor();

      // Check if possible to zoom-out
      if (
        effect === "zoom" &&
        !(e !== true && $content && duration && current.type === "image" && !current.hasError && (end = self.getThumbPos(current)))
      ) {
        effect = "fade";
      }

      if (effect === "zoom") {
        $.fancybox.stop($content);

        domRect = $.fancybox.getTranslate($content);

        start = {
          top: domRect.top,
          left: domRect.left,
          scaleX: domRect.width / end.width,
          scaleY: domRect.height / end.height,
          width: end.width,
          height: end.height
        };

        // Check if we need to animate opacity
        opacity = current.opts.zoomOpacity;

        if (opacity == "auto") {
          opacity = Math.abs(current.width / current.height - end.width / end.height) > 0.1;
        }

        if (opacity) {
          end.opacity = 0;
        }

        $.fancybox.setTranslate($content, start);

        forceRedraw($content);

        $.fancybox.animate($content, end, duration, done);

        return true;
      }

      if (effect && duration) {
        // If skip animation
        if (e === true) {
          setTimeout(done, duration);
        } else {
          $.fancybox.animate(
            current.$slide.removeClass("fancybox-slide--current"),
            "fancybox-animated fancybox-slide--previous fancybox-fx-" + effect,
            duration,
            done
          );
        }
      } else {
        done();
      }

      return true;
    },

    // Final adjustments after removing the instance
    // =============================================

    cleanUp: function(e) {
      var self = this,
        $body = $("body"),
        instance,
        scrollTop;

      self.current.$slide.trigger("onReset");

      self.$refs.container.empty().remove();

      self.trigger("afterClose", e);

      // Place back focus
      if (self.$lastFocus && !!self.current.opts.backFocus) {
        self.$lastFocus.trigger("focus");
      }

      self.current = null;

      // Check if there are other instances
      instance = $.fancybox.getInstance();

      if (instance) {
        instance.activate();
      } else {
        $body.removeClass("fancybox-active compensate-for-scrollbar");

        $("#fancybox-style-noscroll").remove();
      }
    },

    // Call callback and trigger an event
    // ==================================

    trigger: function(name, slide) {
      var args = Array.prototype.slice.call(arguments, 1),
        self = this,
        obj = slide && slide.opts ? slide : self.current,
        rez;

      if (obj) {
        args.unshift(obj);
      } else {
        obj = self;
      }

      args.unshift(self);

      if ($.isFunction(obj.opts[name])) {
        rez = obj.opts[name].apply(obj, args);
      }

      if (rez === false) {
        return rez;
      }

      if (name === "afterClose" || !self.$refs) {
        $D.trigger(name + ".fb", args);
      } else {
        self.$refs.container.trigger(name + ".fb", args);
      }
    },

    // Update infobar values, navigation button states and reveal caption
    // ==================================================================

    updateControls: function(force) {
      var self = this,
        current = self.current,
        index = current.index,
        caption = current.opts.caption,
        $container = self.$refs.container,
        $caption = self.$refs.caption;

      // Recalculate content dimensions
      current.$slide.trigger("refresh");

      self.$caption = caption && caption.length ? $caption.html(caption) : null;

      if (!self.isHiddenControls && !self.isIdle) {
        self.showControls();
      }

      // Update info and navigation elements
      $container.find("[data-fancybox-count]").html(self.group.length);
      $container.find("[data-fancybox-index]").html(index + 1);

      $container.find("[data-fancybox-prev]").toggleClass("disabled", !current.opts.loop && index <= 0);
      $container.find("[data-fancybox-next]").toggleClass("disabled", !current.opts.loop && index >= self.group.length - 1);

      if (current.type === "image") {
        // Re-enable buttons; update download button source
        $container
          .find("[data-fancybox-zoom]")
          .show()
          .end()
          .find("[data-fancybox-download]")
          .attr("href", current.opts.image.src || current.src)
          .show();
      } else if (current.opts.toolbar) {
        $container.find("[data-fancybox-download],[data-fancybox-zoom]").hide();
      }
    },

    // Hide toolbar and caption
    // ========================

    hideControls: function() {
      this.isHiddenControls = true;

      this.$refs.container.removeClass("fancybox-show-infobar fancybox-show-toolbar fancybox-show-caption fancybox-show-nav");
    },

    showControls: function() {
      var self = this,
        opts = self.current ? self.current.opts : self.opts,
        $container = self.$refs.container;

      self.isHiddenControls = false;
      self.idleSecondsCounter = 0;

      $container
        .toggleClass("fancybox-show-toolbar", !!(opts.toolbar && opts.buttons))
        .toggleClass("fancybox-show-infobar", !!(opts.infobar && self.group.length > 1))
        .toggleClass("fancybox-show-nav", !!(opts.arrows && self.group.length > 1))
        .toggleClass("fancybox-is-modal", !!opts.modal);

      if (self.$caption) {
        $container.addClass("fancybox-show-caption ");
      } else {
        $container.removeClass("fancybox-show-caption");
      }
    },

    // Toggle toolbar and caption
    // ==========================

    toggleControls: function() {
      if (this.isHiddenControls) {
        this.showControls();
      } else {
        this.hideControls();
      }
    }
  });

  $.fancybox = {
    version: "{fancybox-version}",
    defaults: defaults,

    // Get current instance and execute a command.
    //
    // Examples of usage:
    //
    //   $instance = $.fancybox.getInstance();
    //   $.fancybox.getInstance().jumpTo( 1 );
    //   $.fancybox.getInstance( 'jumpTo', 1 );
    //   $.fancybox.getInstance( function() {
    //       console.info( this.currIndex );
    //   });
    // ======================================================

    getInstance: function(command) {
      var instance = $('.fancybox-container:not(".fancybox-is-closing"):last').data("FancyBox"),
        args = Array.prototype.slice.call(arguments, 1);

      if (instance instanceof FancyBox) {
        if ($.type(command) === "string") {
          instance[command].apply(instance, args);
        } else if ($.type(command) === "function") {
          command.apply(instance, args);
        }

        return instance;
      }

      return false;
    },

    // Create new instance
    // ===================

    open: function(items, opts, index) {
      return new FancyBox(items, opts, index);
    },

    // Close current or all instances
    // ==============================

    close: function(all) {
      var instance = this.getInstance();

      if (instance) {
        instance.close();

        // Try to find and close next instance

        if (all === true) {
          this.close();
        }
      }
    },

    // Close all instances and unbind all events
    // =========================================

    destroy: function() {
      this.close(true);

      $D.add("body").off("click.fb-start", "**");
    },

    // Try to detect mobile devices
    // ============================

    isMobile:
      document.createTouch !== undefined && /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent),

    // Detect if 'translate3d' support is available
    // ============================================

    use3d: (function() {
      var div = document.createElement("div");

      return (
        window.getComputedStyle &&
        window.getComputedStyle(div) &&
        window.getComputedStyle(div).getPropertyValue("transform") &&
        !(document.documentMode && document.documentMode < 11)
      );
    })(),

    // Helper function to get current visual state of an element
    // returns array[ top, left, horizontal-scale, vertical-scale, opacity ]
    // =====================================================================

    getTranslate: function($el) {
      var domRect;

      if (!$el || !$el.length) {
        return false;
      }

      domRect = $el[0].getBoundingClientRect();

      return {
        top: domRect.top || 0,
        left: domRect.left || 0,
        width: domRect.width,
        height: domRect.height,
        opacity: parseFloat($el.css("opacity"))
      };
    },

    // Shortcut for setting "translate3d" properties for element
    // Can set be used to set opacity, too
    // ========================================================

    setTranslate: function($el, props) {
      var str = "",
        css = {};

      if (!$el || !props) {
        return;
      }

      if (props.left !== undefined || props.top !== undefined) {
        str =
          (props.left === undefined ? $el.position().left : props.left) +
          "px, " +
          (props.top === undefined ? $el.position().top : props.top) +
          "px";

        if (this.use3d) {
          str = "translate3d(" + str + ", 0px)";
        } else {
          str = "translate(" + str + ")";
        }
      }

      if (props.scaleX !== undefined && props.scaleY !== undefined) {
        str = (str.length ? str + " " : "") + "scale(" + props.scaleX + ", " + props.scaleY + ")";
      }

      if (str.length) {
        css.transform = str;
      }

      if (props.opacity !== undefined) {
        css.opacity = props.opacity;
      }

      if (props.width !== undefined) {
        css.width = props.width;
      }

      if (props.height !== undefined) {
        css.height = props.height;
      }

      return $el.css(css);
    },

    // Simple CSS transition handler
    // =============================

    animate: function($el, to, duration, callback, leaveAnimationName) {
      var final = false;

      if ($.isFunction(duration)) {
        callback = duration;
        duration = null;
      }

      if (!$.isPlainObject(to)) {
        $el.removeAttr("style");
      }

      $.fancybox.stop($el);

      $el.on(transitionEnd, function(e) {
        // Skip events from child elements and z-index change
        if (e && e.originalEvent && (!$el.is(e.originalEvent.target) || e.originalEvent.propertyName == "z-index")) {
          return;
        }

        $.fancybox.stop($el);

        if (final) {
          $.fancybox.setTranslate($el, final);
        }

        if ($.isPlainObject(to)) {
          if (leaveAnimationName === false) {
            $el.removeAttr("style");
          }
        } else if (leaveAnimationName !== true) {
          $el.removeClass(to);
        }

        if ($.isFunction(callback)) {
          callback(e);
        }
      });

      if ($.isNumeric(duration)) {
        $el.css("transition-duration", duration + "ms");
      }

      // Start animation by changing CSS properties or class name
      if ($.isPlainObject(to)) {
        if (to.scaleX !== undefined && to.scaleY !== undefined) {
          final = $.extend({}, to, {
            width: $el.width() * to.scaleX,
            height: $el.height() * to.scaleY,
            scaleX: 1,
            scaleY: 1
          });

          delete to.width;
          delete to.height;

          if ($el.parent().hasClass("fancybox-slide--image")) {
            $el.parent().addClass("fancybox-is-scaling");
          }
        }

        $.fancybox.setTranslate($el, to);
      } else {
        $el.addClass(to);
      }

      // Make sure that `transitionend` callback gets fired
      $el.data(
        "timer",
        setTimeout(function() {
          $el.trigger("transitionend");
        }, duration + 16)
      );
    },

    stop: function($el) {
      if ($el && $el.length) {
        clearTimeout($el.data("timer"));

        $el.off("transitionend").css("transition-duration", "");

        $el.parent().removeClass("fancybox-is-scaling");
      }
    }
  };

  // Default click handler for "fancyboxed" links
  // ============================================

  function _run(e, opts) {
    var items = [],
      index = 0,
      $target,
      value;

    // Avoid opening multiple times
    if (e && e.isDefaultPrevented()) {
      return;
    }

    e.preventDefault();

    opts = e && e.data ? e.data.options : opts || {};

    $target = opts.$target || $(e.currentTarget);
    value = $target.attr("data-fancybox") || "";

    // Get all related items and find index for clicked one
    if (value) {
      items = opts.selector ? $(opts.selector) : e.data ? e.data.items : [];
      items = items.length ? items.filter('[data-fancybox="' + value + '"]') : $('[data-fancybox="' + value + '"]');

      index = items.index($target);

      // Sometimes current item can not be found (for example, if some script clones items)
      if (index < 0) {
        index = 0;
      }
    } else {
      items = [$target];
    }

    $.fancybox.open(items, opts, index);
  }

  // Create a jQuery plugin
  // ======================

  $.fn.fancybox = function(options) {
    var selector;

    options = options || {};
    selector = options.selector || false;

    if (selector) {
      // Use body element instead of document so it executes first
      $("body")
        .off("click.fb-start", selector)
        .on("click.fb-start", selector, {options: options}, _run);
    } else {
      this.off("click.fb-start").on(
        "click.fb-start",
        {
          items: this,
          options: options
        },
        _run
      );
    }

    return this;
  };

  // Self initializing plugin for all elements having `data-fancybox` attribute
  // ==========================================================================

  $D.on("click.fb-start", "[data-fancybox]", _run);

  // Enable "trigger elements"
  // =========================

  $D.on("click.fb-start", "[data-trigger]", function(e) {
    _run(e, {
      $target: $('[data-fancybox="' + $(e.currentTarget).attr("data-trigger") + '"]').eq($(e.currentTarget).attr("data-index") || 0),
      $trigger: $(this)
    });
  });
})(window, document, window.jQuery || jQuery);

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJjb3JlLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbih3aW5kb3csIGRvY3VtZW50LCAkLCB1bmRlZmluZWQpIHtcbiAgXCJ1c2Ugc3RyaWN0XCI7XG5cbiAgd2luZG93LmNvbnNvbGUgPSB3aW5kb3cuY29uc29sZSB8fCB7XG4gICAgaW5mbzogZnVuY3Rpb24oc3R1ZmYpIHt9XG4gIH07XG5cbiAgLy8gSWYgdGhlcmUncyBubyBqUXVlcnksIGZhbmN5Qm94IGNhbid0IHdvcmtcbiAgLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cblxuICBpZiAoISQpIHtcbiAgICByZXR1cm47XG4gIH1cblxuICAvLyBDaGVjayBpZiBmYW5jeUJveCBpcyBhbHJlYWR5IGluaXRpYWxpemVkXG4gIC8vID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cblxuICBpZiAoJC5mbi5mYW5jeWJveCkge1xuICAgIGNvbnNvbGUuaW5mbyhcImZhbmN5Qm94IGFscmVhZHkgaW5pdGlhbGl6ZWRcIik7XG5cbiAgICByZXR1cm47XG4gIH1cblxuICAvLyBQcml2YXRlIGRlZmF1bHQgc2V0dGluZ3NcbiAgLy8gPT09PT09PT09PT09PT09PT09PT09PT09XG5cbiAgdmFyIGRlZmF1bHRzID0ge1xuICAgIC8vIEVuYWJsZSBpbmZpbml0ZSBnYWxsZXJ5IG5hdmlnYXRpb25cbiAgICBsb29wOiBmYWxzZSxcblxuICAgIC8vIEhvcml6b250YWwgc3BhY2UgYmV0d2VlbiBzbGlkZXNcbiAgICBndXR0ZXI6IDUwLFxuXG4gICAgLy8gRW5hYmxlIGtleWJvYXJkIG5hdmlnYXRpb25cbiAgICBrZXlib2FyZDogdHJ1ZSxcblxuICAgIC8vIFNob3VsZCBkaXNwbGF5IG5hdmlnYXRpb24gYXJyb3dzIGF0IHRoZSBzY3JlZW4gZWRnZXNcbiAgICBhcnJvd3M6IHRydWUsXG5cbiAgICAvLyBTaG91bGQgZGlzcGxheSBjb3VudGVyIGF0IHRoZSB0b3AgbGVmdCBjb3JuZXJcbiAgICBpbmZvYmFyOiB0cnVlLFxuXG4gICAgLy8gU2hvdWxkIGRpc3BsYXkgY2xvc2UgYnV0dG9uICh1c2luZyBgYnRuVHBsLnNtYWxsQnRuYCB0ZW1wbGF0ZSkgb3ZlciB0aGUgY29udGVudFxuICAgIC8vIENhbiBiZSB0cnVlLCBmYWxzZSwgXCJhdXRvXCJcbiAgICAvLyBJZiBcImF1dG9cIiAtIHdpbGwgYmUgYXV0b21hdGljYWxseSBlbmFibGVkIGZvciBcImh0bWxcIiwgXCJpbmxpbmVcIiBvciBcImFqYXhcIiBpdGVtc1xuICAgIHNtYWxsQnRuOiBcImF1dG9cIixcblxuICAgIC8vIFNob3VsZCBkaXNwbGF5IHRvb2xiYXIgKGJ1dHRvbnMgYXQgdGhlIHRvcClcbiAgICAvLyBDYW4gYmUgdHJ1ZSwgZmFsc2UsIFwiYXV0b1wiXG4gICAgLy8gSWYgXCJhdXRvXCIgLSB3aWxsIGJlIGF1dG9tYXRpY2FsbHkgaGlkZGVuIGlmIFwic21hbGxCdG5cIiBpcyBlbmFibGVkXG4gICAgdG9vbGJhcjogXCJhdXRvXCIsXG5cbiAgICAvLyBXaGF0IGJ1dHRvbnMgc2hvdWxkIGFwcGVhciBpbiB0aGUgdG9wIHJpZ2h0IGNvcm5lci5cbiAgICAvLyBCdXR0b25zIHdpbGwgYmUgY3JlYXRlZCB1c2luZyB0ZW1wbGF0ZXMgZnJvbSBgYnRuVHBsYCBvcHRpb25cbiAgICAvLyBhbmQgdGhleSB3aWxsIGJlIHBsYWNlZCBpbnRvIHRvb2xiYXIgKGNsYXNzPVwiZmFuY3lib3gtdG9vbGJhclwiYCBlbGVtZW50KVxuICAgIGJ1dHRvbnM6IFtcbiAgICAgIFwiem9vbVwiLFxuICAgICAgLy9cInNoYXJlXCIsXG4gICAgICAvL1wic2xpZGVTaG93XCIsXG4gICAgICAvL1wiZnVsbFNjcmVlblwiLFxuICAgICAgLy9cImRvd25sb2FkXCIsXG4gICAgICBcInRodW1ic1wiLFxuICAgICAgXCJjbG9zZVwiXG4gICAgXSxcblxuICAgIC8vIERldGVjdCBcImlkbGVcIiB0aW1lIGluIHNlY29uZHNcbiAgICBpZGxlVGltZTogMyxcblxuICAgIC8vIERpc2FibGUgcmlnaHQtY2xpY2sgYW5kIHVzZSBzaW1wbGUgaW1hZ2UgcHJvdGVjdGlvbiBmb3IgaW1hZ2VzXG4gICAgcHJvdGVjdDogZmFsc2UsXG5cbiAgICAvLyBTaG9ydGN1dCB0byBtYWtlIGNvbnRlbnQgXCJtb2RhbFwiIC0gZGlzYWJsZSBrZXlib2FyZCBuYXZpZ3Rpb24sIGhpZGUgYnV0dG9ucywgZXRjXG4gICAgbW9kYWw6IGZhbHNlLFxuXG4gICAgaW1hZ2U6IHtcbiAgICAgIC8vIFdhaXQgZm9yIGltYWdlcyB0byBsb2FkIGJlZm9yZSBkaXNwbGF5aW5nXG4gICAgICAvLyAgIHRydWUgIC0gd2FpdCBmb3IgaW1hZ2UgdG8gbG9hZCBhbmQgdGhlbiBkaXNwbGF5O1xuICAgICAgLy8gICBmYWxzZSAtIGRpc3BsYXkgdGh1bWJuYWlsIGFuZCBsb2FkIHRoZSBmdWxsLXNpemVkIGltYWdlIG92ZXIgdG9wLFxuICAgICAgLy8gICAgICAgICAgIHJlcXVpcmVzIHByZWRlZmluZWQgaW1hZ2UgZGltZW5zaW9ucyAoYGRhdGEtd2lkdGhgIGFuZCBgZGF0YS1oZWlnaHRgIGF0dHJpYnV0ZXMpXG4gICAgICBwcmVsb2FkOiBmYWxzZVxuICAgIH0sXG5cbiAgICBhamF4OiB7XG4gICAgICAvLyBPYmplY3QgY29udGFpbmluZyBzZXR0aW5ncyBmb3IgYWpheCByZXF1ZXN0XG4gICAgICBzZXR0aW5nczoge1xuICAgICAgICAvLyBUaGlzIGhlbHBzIHRvIGluZGljYXRlIHRoYXQgcmVxdWVzdCBjb21lcyBmcm9tIHRoZSBtb2RhbFxuICAgICAgICAvLyBGZWVsIGZyZWUgdG8gY2hhbmdlIG5hbWluZ1xuICAgICAgICBkYXRhOiB7XG4gICAgICAgICAgZmFuY3lib3g6IHRydWVcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0sXG5cbiAgICBpZnJhbWU6IHtcbiAgICAgIC8vIElmcmFtZSB0ZW1wbGF0ZVxuICAgICAgdHBsOlxuICAgICAgICAnPGlmcmFtZSBpZD1cImZhbmN5Ym94LWZyYW1le3JuZH1cIiBuYW1lPVwiZmFuY3lib3gtZnJhbWV7cm5kfVwiIGNsYXNzPVwiZmFuY3lib3gtaWZyYW1lXCIgZnJhbWVib3JkZXI9XCIwXCIgdnNwYWNlPVwiMFwiIGhzcGFjZT1cIjBcIiB3ZWJraXRBbGxvd0Z1bGxTY3JlZW4gbW96YWxsb3dmdWxsc2NyZWVuIGFsbG93RnVsbFNjcmVlbiBhbGxvd3RyYW5zcGFyZW5jeT1cInRydWVcIiBzcmM9XCJcIj48L2lmcmFtZT4nLFxuXG4gICAgICAvLyBQcmVsb2FkIGlmcmFtZSBiZWZvcmUgZGlzcGxheWluZyBpdFxuICAgICAgLy8gVGhpcyBhbGxvd3MgdG8gY2FsY3VsYXRlIGlmcmFtZSBjb250ZW50IHdpZHRoIGFuZCBoZWlnaHRcbiAgICAgIC8vIChub3RlOiBEdWUgdG8gXCJTYW1lIE9yaWdpbiBQb2xpY3lcIiwgeW91IGNhbid0IGdldCBjcm9zcyBkb21haW4gZGF0YSkuXG4gICAgICBwcmVsb2FkOiB0cnVlLFxuXG4gICAgICAvLyBDdXN0b20gQ1NTIHN0eWxpbmcgZm9yIGlmcmFtZSB3cmFwcGluZyBlbGVtZW50XG4gICAgICAvLyBZb3UgY2FuIHVzZSB0aGlzIHRvIHNldCBjdXN0b20gaWZyYW1lIGRpbWVuc2lvbnNcbiAgICAgIGNzczoge30sXG5cbiAgICAgIC8vIElmcmFtZSB0YWcgYXR0cmlidXRlc1xuICAgICAgYXR0cjoge1xuICAgICAgICBzY3JvbGxpbmc6IFwiYXV0b1wiXG4gICAgICB9XG4gICAgfSxcblxuICAgIC8vIERlZmF1bHQgY29udGVudCB0eXBlIGlmIGNhbm5vdCBiZSBkZXRlY3RlZCBhdXRvbWF0aWNhbGx5XG4gICAgZGVmYXVsdFR5cGU6IFwiaW1hZ2VcIixcblxuICAgIC8vIE9wZW4vY2xvc2UgYW5pbWF0aW9uIHR5cGVcbiAgICAvLyBQb3NzaWJsZSB2YWx1ZXM6XG4gICAgLy8gICBmYWxzZSAgICAgICAgICAgIC0gZGlzYWJsZVxuICAgIC8vICAgXCJ6b29tXCIgICAgICAgICAgIC0gem9vbSBpbWFnZXMgZnJvbS90byB0aHVtYm5haWxcbiAgICAvLyAgIFwiZmFkZVwiXG4gICAgLy8gICBcInpvb20taW4tb3V0XCJcbiAgICAvL1xuICAgIGFuaW1hdGlvbkVmZmVjdDogXCJ6b29tXCIsXG5cbiAgICAvLyBEdXJhdGlvbiBpbiBtcyBmb3Igb3Blbi9jbG9zZSBhbmltYXRpb25cbiAgICBhbmltYXRpb25EdXJhdGlvbjogMzY2LFxuXG4gICAgLy8gU2hvdWxkIGltYWdlIGNoYW5nZSBvcGFjaXR5IHdoaWxlIHpvb21pbmdcbiAgICAvLyBJZiBvcGFjaXR5IGlzIFwiYXV0b1wiLCB0aGVuIG9wYWNpdHkgd2lsbCBiZSBjaGFuZ2VkIGlmIGltYWdlIGFuZCB0aHVtYm5haWwgaGF2ZSBkaWZmZXJlbnQgYXNwZWN0IHJhdGlvc1xuICAgIHpvb21PcGFjaXR5OiBcImF1dG9cIixcblxuICAgIC8vIFRyYW5zaXRpb24gZWZmZWN0IGJldHdlZW4gc2xpZGVzXG4gICAgLy9cbiAgICAvLyBQb3NzaWJsZSB2YWx1ZXM6XG4gICAgLy8gICBmYWxzZSAgICAgICAgICAgIC0gZGlzYWJsZVxuICAgIC8vICAgXCJmYWRlJ1xuICAgIC8vICAgXCJzbGlkZSdcbiAgICAvLyAgIFwiY2lyY3VsYXInXG4gICAgLy8gICBcInR1YmUnXG4gICAgLy8gICBcInpvb20taW4tb3V0J1xuICAgIC8vICAgXCJyb3RhdGUnXG4gICAgLy9cbiAgICB0cmFuc2l0aW9uRWZmZWN0OiBcImZhZGVcIixcblxuICAgIC8vIER1cmF0aW9uIGluIG1zIGZvciB0cmFuc2l0aW9uIGFuaW1hdGlvblxuICAgIHRyYW5zaXRpb25EdXJhdGlvbjogMzY2LFxuXG4gICAgLy8gQ3VzdG9tIENTUyBjbGFzcyBmb3Igc2xpZGUgZWxlbWVudFxuICAgIHNsaWRlQ2xhc3M6IFwiXCIsXG5cbiAgICAvLyBDdXN0b20gQ1NTIGNsYXNzIGZvciBsYXlvdXRcbiAgICBiYXNlQ2xhc3M6IFwiXCIsXG5cbiAgICAvLyBCYXNlIHRlbXBsYXRlIGZvciBsYXlvdXRcbiAgICBiYXNlVHBsOlxuICAgICAgJzxkaXYgY2xhc3M9XCJmYW5jeWJveC1jb250YWluZXJcIiByb2xlPVwiZGlhbG9nXCIgdGFiaW5kZXg9XCItMVwiPicgK1xuICAgICAgJzxkaXYgY2xhc3M9XCJmYW5jeWJveC1iZ1wiPjwvZGl2PicgK1xuICAgICAgJzxkaXYgY2xhc3M9XCJmYW5jeWJveC1pbm5lclwiPicgK1xuICAgICAgJzxkaXYgY2xhc3M9XCJmYW5jeWJveC1pbmZvYmFyXCI+JyArXG4gICAgICBcIjxzcGFuIGRhdGEtZmFuY3lib3gtaW5kZXg+PC9zcGFuPiZuYnNwOy8mbmJzcDs8c3BhbiBkYXRhLWZhbmN5Ym94LWNvdW50Pjwvc3Bhbj5cIiArXG4gICAgICBcIjwvZGl2PlwiICtcbiAgICAgICc8ZGl2IGNsYXNzPVwiZmFuY3lib3gtdG9vbGJhclwiPnt7YnV0dG9uc319PC9kaXY+JyArXG4gICAgICAnPGRpdiBjbGFzcz1cImZhbmN5Ym94LW5hdmlnYXRpb25cIj57e2Fycm93c319PC9kaXY+JyArXG4gICAgICAnPGRpdiBjbGFzcz1cImZhbmN5Ym94LXN0YWdlXCI+PC9kaXY+JyArXG4gICAgICAnPGRpdiBjbGFzcz1cImZhbmN5Ym94LWNhcHRpb25cIj48L2Rpdj4nICtcbiAgICAgIFwiPC9kaXY+XCIgK1xuICAgICAgXCI8L2Rpdj5cIixcblxuICAgIC8vIExvYWRpbmcgaW5kaWNhdG9yIHRlbXBsYXRlXG4gICAgc3Bpbm5lclRwbDogJzxkaXYgY2xhc3M9XCJmYW5jeWJveC1sb2FkaW5nXCI+PC9kaXY+JyxcblxuICAgIC8vIEVycm9yIG1lc3NhZ2UgdGVtcGxhdGVcbiAgICBlcnJvclRwbDogJzxkaXYgY2xhc3M9XCJmYW5jeWJveC1lcnJvclwiPjxwPnt7RVJST1J9fTwvcD48L2Rpdj4nLFxuXG4gICAgYnRuVHBsOiB7XG4gICAgICBkb3dubG9hZDpcbiAgICAgICAgJzxhIGRvd25sb2FkIGRhdGEtZmFuY3lib3gtZG93bmxvYWQgY2xhc3M9XCJmYW5jeWJveC1idXR0b24gZmFuY3lib3gtYnV0dG9uLS1kb3dubG9hZFwiIHRpdGxlPVwie3tET1dOTE9BRH19XCIgaHJlZj1cImphdmFzY3JpcHQ6O1wiPicgK1xuICAgICAgICAnPHN2ZyB2aWV3Qm94PVwiMCAwIDQwIDQwXCI+JyArXG4gICAgICAgICc8cGF0aCBkPVwiTTEzLDE2IEwyMCwyMyBMMjcsMTYgTTIwLDcgTDIwLDIzIE0xMCwyNCBMMTAsMjggTDMwLDI4IEwzMCwyNFwiIC8+JyArXG4gICAgICAgIFwiPC9zdmc+XCIgK1xuICAgICAgICBcIjwvYT5cIixcblxuICAgICAgem9vbTpcbiAgICAgICAgJzxidXR0b24gZGF0YS1mYW5jeWJveC16b29tIGNsYXNzPVwiZmFuY3lib3gtYnV0dG9uIGZhbmN5Ym94LWJ1dHRvbi0tem9vbVwiIHRpdGxlPVwie3taT09NfX1cIj4nICtcbiAgICAgICAgJzxzdmcgdmlld0JveD1cIjAgMCA0MCA0MFwiPicgK1xuICAgICAgICAnPHBhdGggZD1cIk0xOCwxNyBtLTgsMCBhOCw4IDAgMSwwIDE2LDAgYTgsOCAwIDEsMCAtMTYsMCBNMjQsMjIgTDMxLDI5XCIgLz4nICtcbiAgICAgICAgXCI8L3N2Zz5cIiArXG4gICAgICAgIFwiPC9idXR0b24+XCIsXG5cbiAgICAgIGNsb3NlOlxuICAgICAgICAnPGJ1dHRvbiBkYXRhLWZhbmN5Ym94LWNsb3NlIGNsYXNzPVwiZmFuY3lib3gtYnV0dG9uIGZhbmN5Ym94LWJ1dHRvbi0tY2xvc2VcIiB0aXRsZT1cInt7Q0xPU0V9fVwiPicgK1xuICAgICAgICAnPHN2ZyB2aWV3Qm94PVwiMCAwIDQwIDQwXCI+JyArXG4gICAgICAgICc8cGF0aCBkPVwiTTEwLDEwIEwzMCwzMCBNMzAsMTAgTDEwLDMwXCIgLz4nICtcbiAgICAgICAgXCI8L3N2Zz5cIiArXG4gICAgICAgIFwiPC9idXR0b24+XCIsXG5cbiAgICAgIC8vIFRoaXMgc21hbGwgY2xvc2UgYnV0dG9uIHdpbGwgYmUgYXBwZW5kZWQgdG8geW91ciBodG1sL2lubGluZS9hamF4IGNvbnRlbnQgYnkgZGVmYXVsdCxcbiAgICAgIC8vIGlmIFwic21hbGxCdG5cIiBvcHRpb24gaXMgbm90IHNldCB0byBmYWxzZVxuICAgICAgc21hbGxCdG46XG4gICAgICAgICc8YnV0dG9uIGRhdGEtZmFuY3lib3gtY2xvc2UgY2xhc3M9XCJmYW5jeWJveC1jbG9zZS1zbWFsbFwiIHRpdGxlPVwie3tDTE9TRX19XCI+PHN2ZyB2aWV3Qm94PVwiMCAwIDMyIDMyXCI+PHBhdGggZD1cIk0xMCwxMCBMMjIsMjIgTTIyLDEwIEwxMCwyMlwiPjwvcGF0aD48L3N2Zz48L2J1dHRvbj4nLFxuXG4gICAgICAvLyBBcnJvd3NcbiAgICAgIGFycm93TGVmdDpcbiAgICAgICAgJzxhIGRhdGEtZmFuY3lib3gtcHJldiBjbGFzcz1cImZhbmN5Ym94LWJ1dHRvbiBmYW5jeWJveC1idXR0b24tLWFycm93X2xlZnRcIiB0aXRsZT1cInt7UFJFVn19XCIgaHJlZj1cImphdmFzY3JpcHQ6O1wiPicgK1xuICAgICAgICAnPHN2ZyB2aWV3Qm94PVwiMCAwIDQwIDQwXCI+JyArXG4gICAgICAgICc8cGF0aCBkPVwiTTE4LDEyIEwxMCwyMCBMMTgsMjggTTEwLDIwIEwzMCwyMFwiPjwvcGF0aD4nICtcbiAgICAgICAgXCI8L3N2Zz5cIiArXG4gICAgICAgIFwiPC9hPlwiLFxuXG4gICAgICBhcnJvd1JpZ2h0OlxuICAgICAgICAnPGEgZGF0YS1mYW5jeWJveC1uZXh0IGNsYXNzPVwiZmFuY3lib3gtYnV0dG9uIGZhbmN5Ym94LWJ1dHRvbi0tYXJyb3dfcmlnaHRcIiB0aXRsZT1cInt7TkVYVH19XCIgaHJlZj1cImphdmFzY3JpcHQ6O1wiPicgK1xuICAgICAgICAnPHN2ZyB2aWV3Qm94PVwiMCAwIDQwIDQwXCI+JyArXG4gICAgICAgICc8cGF0aCBkPVwiTTEwLDIwIEwzMCwyMCBNMjIsMTIgTDMwLDIwIEwyMiwyOFwiPjwvcGF0aD4nICtcbiAgICAgICAgXCI8L3N2Zz5cIiArXG4gICAgICAgIFwiPC9hPlwiXG4gICAgfSxcblxuICAgIC8vIENvbnRhaW5lciBpcyBpbmplY3RlZCBpbnRvIHRoaXMgZWxlbWVudFxuICAgIHBhcmVudEVsOiBcImJvZHlcIixcblxuICAgIC8vIEZvY3VzIGhhbmRsaW5nXG4gICAgLy8gPT09PT09PT09PT09PT1cblxuICAgIC8vIFRyeSB0byBmb2N1cyBvbiB0aGUgZmlyc3QgZm9jdXNhYmxlIGVsZW1lbnQgYWZ0ZXIgb3BlbmluZ1xuICAgIGF1dG9Gb2N1czogZmFsc2UsXG5cbiAgICAvLyBQdXQgZm9jdXMgYmFjayB0byBhY3RpdmUgZWxlbWVudCBhZnRlciBjbG9zaW5nXG4gICAgYmFja0ZvY3VzOiB0cnVlLFxuXG4gICAgLy8gRG8gbm90IGxldCB1c2VyIHRvIGZvY3VzIG9uIGVsZW1lbnQgb3V0c2lkZSBtb2RhbCBjb250ZW50XG4gICAgdHJhcEZvY3VzOiB0cnVlLFxuXG4gICAgLy8gTW9kdWxlIHNwZWNpZmljIG9wdGlvbnNcbiAgICAvLyA9PT09PT09PT09PT09PT09PT09PT09PVxuXG4gICAgZnVsbFNjcmVlbjoge1xuICAgICAgYXV0b1N0YXJ0OiBmYWxzZVxuICAgIH0sXG5cbiAgICAvLyBTZXQgYHRvdWNoOiBmYWxzZWAgdG8gZGlzYWJsZSBkcmFnZ2luZy9zd2lwaW5nXG4gICAgdG91Y2g6IHtcbiAgICAgIHZlcnRpY2FsOiB0cnVlLCAvLyBBbGxvdyB0byBkcmFnIGNvbnRlbnQgdmVydGljYWxseVxuICAgICAgbW9tZW50dW06IHRydWUgLy8gQ29udGludWUgbW92ZW1lbnQgYWZ0ZXIgcmVsZWFzaW5nIG1vdXNlL3RvdWNoIHdoZW4gcGFubmluZ1xuICAgIH0sXG5cbiAgICAvLyBIYXNoIHZhbHVlIHdoZW4gaW5pdGlhbGl6aW5nIG1hbnVhbGx5LFxuICAgIC8vIHNldCBgZmFsc2VgIHRvIGRpc2FibGUgaGFzaCBjaGFuZ2VcbiAgICBoYXNoOiBudWxsLFxuXG4gICAgLy8gQ3VzdG9taXplIG9yIGFkZCBuZXcgbWVkaWEgdHlwZXNcbiAgICAvLyBFeGFtcGxlOlxuICAgIC8qXG4gICAgICAgIG1lZGlhIDoge1xuICAgICAgICAgICAgeW91dHViZSA6IHtcbiAgICAgICAgICAgICAgICBwYXJhbXMgOiB7XG4gICAgICAgICAgICAgICAgICAgIGF1dG9wbGF5IDogMFxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICAqL1xuICAgIG1lZGlhOiB7fSxcblxuICAgIHNsaWRlU2hvdzoge1xuICAgICAgYXV0b1N0YXJ0OiBmYWxzZSxcbiAgICAgIHNwZWVkOiA0MDAwXG4gICAgfSxcblxuICAgIHRodW1iczoge1xuICAgICAgYXV0b1N0YXJ0OiBmYWxzZSwgLy8gRGlzcGxheSB0aHVtYm5haWxzIG9uIG9wZW5pbmdcbiAgICAgIGhpZGVPbkNsb3NlOiB0cnVlLCAvLyBIaWRlIHRodW1ibmFpbCBncmlkIHdoZW4gY2xvc2luZyBhbmltYXRpb24gc3RhcnRzXG4gICAgICBwYXJlbnRFbDogXCIuZmFuY3lib3gtY29udGFpbmVyXCIsIC8vIENvbnRhaW5lciBpcyBpbmplY3RlZCBpbnRvIHRoaXMgZWxlbWVudFxuICAgICAgYXhpczogXCJ5XCIgLy8gVmVydGljYWwgKHkpIG9yIGhvcml6b250YWwgKHgpIHNjcm9sbGluZ1xuICAgIH0sXG5cbiAgICAvLyBVc2UgbW91c2V3aGVlbCB0byBuYXZpZ2F0ZSBnYWxsZXJ5XG4gICAgLy8gSWYgJ2F1dG8nIC0gZW5hYmxlZCBmb3IgaW1hZ2VzIG9ubHlcbiAgICB3aGVlbDogXCJhdXRvXCIsXG5cbiAgICAvLyBDYWxsYmFja3NcbiAgICAvLz09PT09PT09PT1cblxuICAgIC8vIFNlZSBEb2N1bWVudGF0aW9uL0FQSS9FdmVudHMgZm9yIG1vcmUgaW5mb3JtYXRpb25cbiAgICAvLyBFeGFtcGxlOlxuICAgIC8qXG5cdFx0YWZ0ZXJTaG93OiBmdW5jdGlvbiggaW5zdGFuY2UsIGN1cnJlbnQgKSB7XG5cdFx0XHRjb25zb2xlLmluZm8oICdDbGlja2VkIGVsZW1lbnQ6JyApO1xuXHRcdFx0Y29uc29sZS5pbmZvKCBjdXJyZW50Lm9wdHMuJG9yaWcgKTtcblx0XHR9XG5cdCovXG5cbiAgICBvbkluaXQ6ICQubm9vcCwgLy8gV2hlbiBpbnN0YW5jZSBoYXMgYmVlbiBpbml0aWFsaXplZFxuXG4gICAgYmVmb3JlTG9hZDogJC5ub29wLCAvLyBCZWZvcmUgdGhlIGNvbnRlbnQgb2YgYSBzbGlkZSBpcyBiZWluZyBsb2FkZWRcbiAgICBhZnRlckxvYWQ6ICQubm9vcCwgLy8gV2hlbiB0aGUgY29udGVudCBvZiBhIHNsaWRlIGlzIGRvbmUgbG9hZGluZ1xuXG4gICAgYmVmb3JlU2hvdzogJC5ub29wLCAvLyBCZWZvcmUgb3BlbiBhbmltYXRpb24gc3RhcnRzXG4gICAgYWZ0ZXJTaG93OiAkLm5vb3AsIC8vIFdoZW4gY29udGVudCBpcyBkb25lIGxvYWRpbmcgYW5kIGFuaW1hdGluZ1xuXG4gICAgYmVmb3JlQ2xvc2U6ICQubm9vcCwgLy8gQmVmb3JlIHRoZSBpbnN0YW5jZSBhdHRlbXB0cyB0byBjbG9zZS4gUmV0dXJuIGZhbHNlIHRvIGNhbmNlbCB0aGUgY2xvc2UuXG4gICAgYWZ0ZXJDbG9zZTogJC5ub29wLCAvLyBBZnRlciBpbnN0YW5jZSBoYXMgYmVlbiBjbG9zZWRcblxuICAgIG9uQWN0aXZhdGU6ICQubm9vcCwgLy8gV2hlbiBpbnN0YW5jZSBpcyBicm91Z2h0IHRvIGZyb250XG4gICAgb25EZWFjdGl2YXRlOiAkLm5vb3AsIC8vIFdoZW4gb3RoZXIgaW5zdGFuY2UgaGFzIGJlZW4gYWN0aXZhdGVkXG5cbiAgICAvLyBJbnRlcmFjdGlvblxuICAgIC8vID09PT09PT09PT09XG5cbiAgICAvLyBVc2Ugb3B0aW9ucyBiZWxvdyB0byBjdXN0b21pemUgdGFrZW4gYWN0aW9uIHdoZW4gdXNlciBjbGlja3Mgb3IgZG91YmxlIGNsaWNrcyBvbiB0aGUgZmFuY3lCb3ggYXJlYSxcbiAgICAvLyBlYWNoIG9wdGlvbiBjYW4gYmUgc3RyaW5nIG9yIG1ldGhvZCB0aGF0IHJldHVybnMgdmFsdWUuXG4gICAgLy9cbiAgICAvLyBQb3NzaWJsZSB2YWx1ZXM6XG4gICAgLy8gICBcImNsb3NlXCIgICAgICAgICAgIC0gY2xvc2UgaW5zdGFuY2VcbiAgICAvLyAgIFwibmV4dFwiICAgICAgICAgICAgLSBtb3ZlIHRvIG5leHQgZ2FsbGVyeSBpdGVtXG4gICAgLy8gICBcIm5leHRPckNsb3NlXCIgICAgIC0gbW92ZSB0byBuZXh0IGdhbGxlcnkgaXRlbSBvciBjbG9zZSBpZiBnYWxsZXJ5IGhhcyBvbmx5IG9uZSBpdGVtXG4gICAgLy8gICBcInRvZ2dsZUNvbnRyb2xzXCIgIC0gc2hvdy9oaWRlIGNvbnRyb2xzXG4gICAgLy8gICBcInpvb21cIiAgICAgICAgICAgIC0gem9vbSBpbWFnZSAoaWYgbG9hZGVkKVxuICAgIC8vICAgZmFsc2UgICAgICAgICAgICAgLSBkbyBub3RoaW5nXG5cbiAgICAvLyBDbGlja2VkIG9uIHRoZSBjb250ZW50XG4gICAgY2xpY2tDb250ZW50OiBmdW5jdGlvbihjdXJyZW50LCBldmVudCkge1xuICAgICAgcmV0dXJuIGN1cnJlbnQudHlwZSA9PT0gXCJpbWFnZVwiID8gXCJ6b29tXCIgOiBmYWxzZTtcbiAgICB9LFxuXG4gICAgLy8gQ2xpY2tlZCBvbiB0aGUgc2xpZGVcbiAgICBjbGlja1NsaWRlOiBcImNsb3NlXCIsXG5cbiAgICAvLyBDbGlja2VkIG9uIHRoZSBiYWNrZ3JvdW5kIChiYWNrZHJvcCkgZWxlbWVudDtcbiAgICAvLyBpZiB5b3UgaGF2ZSBub3QgY2hhbmdlZCB0aGUgbGF5b3V0LCB0aGVuIG1vc3QgbGlrZWx5IHlvdSBuZWVkIHRvIHVzZSBgY2xpY2tTbGlkZWAgb3B0aW9uXG4gICAgY2xpY2tPdXRzaWRlOiBcImNsb3NlXCIsXG5cbiAgICAvLyBTYW1lIGFzIHByZXZpb3VzIHR3bywgYnV0IGZvciBkb3VibGUgY2xpY2tcbiAgICBkYmxjbGlja0NvbnRlbnQ6IGZhbHNlLFxuICAgIGRibGNsaWNrU2xpZGU6IGZhbHNlLFxuICAgIGRibGNsaWNrT3V0c2lkZTogZmFsc2UsXG5cbiAgICAvLyBDdXN0b20gb3B0aW9ucyB3aGVuIG1vYmlsZSBkZXZpY2UgaXMgZGV0ZWN0ZWRcbiAgICAvLyA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cblxuICAgIG1vYmlsZToge1xuICAgICAgaWRsZVRpbWU6IGZhbHNlLFxuICAgICAgY2xpY2tDb250ZW50OiBmdW5jdGlvbihjdXJyZW50LCBldmVudCkge1xuICAgICAgICByZXR1cm4gY3VycmVudC50eXBlID09PSBcImltYWdlXCIgPyBcInRvZ2dsZUNvbnRyb2xzXCIgOiBmYWxzZTtcbiAgICAgIH0sXG4gICAgICBjbGlja1NsaWRlOiBmdW5jdGlvbihjdXJyZW50LCBldmVudCkge1xuICAgICAgICByZXR1cm4gY3VycmVudC50eXBlID09PSBcImltYWdlXCIgPyBcInRvZ2dsZUNvbnRyb2xzXCIgOiBcImNsb3NlXCI7XG4gICAgICB9LFxuICAgICAgZGJsY2xpY2tDb250ZW50OiBmdW5jdGlvbihjdXJyZW50LCBldmVudCkge1xuICAgICAgICByZXR1cm4gY3VycmVudC50eXBlID09PSBcImltYWdlXCIgPyBcInpvb21cIiA6IGZhbHNlO1xuICAgICAgfSxcbiAgICAgIGRibGNsaWNrU2xpZGU6IGZ1bmN0aW9uKGN1cnJlbnQsIGV2ZW50KSB7XG4gICAgICAgIHJldHVybiBjdXJyZW50LnR5cGUgPT09IFwiaW1hZ2VcIiA/IFwiem9vbVwiIDogZmFsc2U7XG4gICAgICB9XG4gICAgfSxcblxuICAgIC8vIEludGVybmF0aW9uYWxpemF0aW9uXG4gICAgLy8gPT09PT09PT09PT09PT09PT09PT1cblxuICAgIGxhbmc6IFwiZW5cIixcbiAgICBpMThuOiB7XG4gICAgICBlbjoge1xuICAgICAgICBDTE9TRTogXCJDbG9zZVwiLFxuICAgICAgICBORVhUOiBcIk5leHRcIixcbiAgICAgICAgUFJFVjogXCJQcmV2aW91c1wiLFxuICAgICAgICBFUlJPUjogXCJUaGUgcmVxdWVzdGVkIGNvbnRlbnQgY2Fubm90IGJlIGxvYWRlZC4gPGJyLz4gUGxlYXNlIHRyeSBhZ2FpbiBsYXRlci5cIixcbiAgICAgICAgUExBWV9TVEFSVDogXCJTdGFydCBzbGlkZXNob3dcIixcbiAgICAgICAgUExBWV9TVE9QOiBcIlBhdXNlIHNsaWRlc2hvd1wiLFxuICAgICAgICBGVUxMX1NDUkVFTjogXCJGdWxsIHNjcmVlblwiLFxuICAgICAgICBUSFVNQlM6IFwiVGh1bWJuYWlsc1wiLFxuICAgICAgICBET1dOTE9BRDogXCJEb3dubG9hZFwiLFxuICAgICAgICBTSEFSRTogXCJTaGFyZVwiLFxuICAgICAgICBaT09NOiBcIlpvb21cIlxuICAgICAgfSxcbiAgICAgIGRlOiB7XG4gICAgICAgIENMT1NFOiBcIlNjaGxpZXNzZW5cIixcbiAgICAgICAgTkVYVDogXCJXZWl0ZXJcIixcbiAgICAgICAgUFJFVjogXCJadXLDvGNrXCIsXG4gICAgICAgIEVSUk9SOiBcIkRpZSBhbmdlZm9yZGVydGVuIERhdGVuIGtvbm50ZW4gbmljaHQgZ2VsYWRlbiB3ZXJkZW4uIDxici8+IEJpdHRlIHZlcnN1Y2hlbiBTaWUgZXMgc3DDpHRlciBub2NobWFsLlwiLFxuICAgICAgICBQTEFZX1NUQVJUOiBcIkRpYXNjaGF1IHN0YXJ0ZW5cIixcbiAgICAgICAgUExBWV9TVE9QOiBcIkRpYXNjaGF1IGJlZW5kZW5cIixcbiAgICAgICAgRlVMTF9TQ1JFRU46IFwiVm9sbGJpbGRcIixcbiAgICAgICAgVEhVTUJTOiBcIlZvcnNjaGF1YmlsZGVyXCIsXG4gICAgICAgIERPV05MT0FEOiBcIkhlcnVudGVybGFkZW5cIixcbiAgICAgICAgU0hBUkU6IFwiVGVpbGVuXCIsXG4gICAgICAgIFpPT006IFwiTWHDn3N0YWJcIlxuICAgICAgfVxuICAgIH1cbiAgfTtcblxuICAvLyBGZXcgdXNlZnVsIHZhcmlhYmxlcyBhbmQgbWV0aG9kc1xuICAvLyA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuXG4gIHZhciAkVyA9ICQod2luZG93KTtcbiAgdmFyICREID0gJChkb2N1bWVudCk7XG5cbiAgdmFyIGNhbGxlZCA9IDA7XG5cbiAgLy8gQ2hlY2sgaWYgYW4gb2JqZWN0IGlzIGEgalF1ZXJ5IG9iamVjdCBhbmQgbm90IGEgbmF0aXZlIEphdmFTY3JpcHQgb2JqZWN0XG4gIC8vID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuICB2YXIgaXNRdWVyeSA9IGZ1bmN0aW9uKG9iaikge1xuICAgIHJldHVybiBvYmogJiYgb2JqLmhhc093blByb3BlcnR5ICYmIG9iaiBpbnN0YW5jZW9mICQ7XG4gIH07XG5cbiAgLy8gSGFuZGxlIG11bHRpcGxlIGJyb3dzZXJzIGZvciBcInJlcXVlc3RBbmltYXRpb25GcmFtZVwiIGFuZCBcImNhbmNlbEFuaW1hdGlvbkZyYW1lXCJcbiAgLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuICB2YXIgcmVxdWVzdEFGcmFtZSA9IChmdW5jdGlvbigpIHtcbiAgICByZXR1cm4gKFxuICAgICAgd2luZG93LnJlcXVlc3RBbmltYXRpb25GcmFtZSB8fFxuICAgICAgd2luZG93LndlYmtpdFJlcXVlc3RBbmltYXRpb25GcmFtZSB8fFxuICAgICAgd2luZG93Lm1velJlcXVlc3RBbmltYXRpb25GcmFtZSB8fFxuICAgICAgd2luZG93Lm9SZXF1ZXN0QW5pbWF0aW9uRnJhbWUgfHxcbiAgICAgIC8vIGlmIGFsbCBlbHNlIGZhaWxzLCB1c2Ugc2V0VGltZW91dFxuICAgICAgZnVuY3Rpb24oY2FsbGJhY2spIHtcbiAgICAgICAgcmV0dXJuIHdpbmRvdy5zZXRUaW1lb3V0KGNhbGxiYWNrLCAxMDAwIC8gNjApO1xuICAgICAgfVxuICAgICk7XG4gIH0pKCk7XG5cbiAgLy8gRGV0ZWN0IHRoZSBzdXBwb3J0ZWQgdHJhbnNpdGlvbi1lbmQgZXZlbnQgcHJvcGVydHkgbmFtZVxuICAvLyA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG4gIHZhciB0cmFuc2l0aW9uRW5kID0gKGZ1bmN0aW9uKCkge1xuICAgIHZhciBlbCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJmYWtlZWxlbWVudFwiKSxcbiAgICAgIHQ7XG5cbiAgICB2YXIgdHJhbnNpdGlvbnMgPSB7XG4gICAgICB0cmFuc2l0aW9uOiBcInRyYW5zaXRpb25lbmRcIixcbiAgICAgIE9UcmFuc2l0aW9uOiBcIm9UcmFuc2l0aW9uRW5kXCIsXG4gICAgICBNb3pUcmFuc2l0aW9uOiBcInRyYW5zaXRpb25lbmRcIixcbiAgICAgIFdlYmtpdFRyYW5zaXRpb246IFwid2Via2l0VHJhbnNpdGlvbkVuZFwiXG4gICAgfTtcblxuICAgIGZvciAodCBpbiB0cmFuc2l0aW9ucykge1xuICAgICAgaWYgKGVsLnN0eWxlW3RdICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgcmV0dXJuIHRyYW5zaXRpb25zW3RdO1xuICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybiBcInRyYW5zaXRpb25lbmRcIjtcbiAgfSkoKTtcblxuICAvLyBGb3JjZSByZWRyYXcgb24gYW4gZWxlbWVudC5cbiAgLy8gVGhpcyBoZWxwcyBpbiBjYXNlcyB3aGVyZSB0aGUgYnJvd3NlciBkb2Vzbid0IHJlZHJhdyBhbiB1cGRhdGVkIGVsZW1lbnQgcHJvcGVybHlcbiAgLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cbiAgdmFyIGZvcmNlUmVkcmF3ID0gZnVuY3Rpb24oJGVsKSB7XG4gICAgcmV0dXJuICRlbCAmJiAkZWwubGVuZ3RoICYmICRlbFswXS5vZmZzZXRIZWlnaHQ7XG4gIH07XG5cbiAgLy8gRXhjbHVkZSBhcnJheSAoYGJ1dHRvbnNgKSBvcHRpb25zIGZyb20gZGVlcCBtZXJnaW5nXG4gIC8vID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuICB2YXIgbWVyZ2VPcHRzID0gZnVuY3Rpb24ob3B0czEsIG9wdHMyKSB7XG4gICAgdmFyIHJleiA9ICQuZXh0ZW5kKHRydWUsIHt9LCBvcHRzMSwgb3B0czIpO1xuXG4gICAgJC5lYWNoKG9wdHMyLCBmdW5jdGlvbihrZXksIHZhbHVlKSB7XG4gICAgICBpZiAoJC5pc0FycmF5KHZhbHVlKSkge1xuICAgICAgICByZXpba2V5XSA9IHZhbHVlO1xuICAgICAgfVxuICAgIH0pO1xuXG4gICAgcmV0dXJuIHJlejtcbiAgfTtcblxuICAvLyBDbGFzcyBkZWZpbml0aW9uXG4gIC8vID09PT09PT09PT09PT09PT1cblxuICB2YXIgRmFuY3lCb3ggPSBmdW5jdGlvbihjb250ZW50LCBvcHRzLCBpbmRleCkge1xuICAgIHZhciBzZWxmID0gdGhpcztcblxuICAgIHNlbGYub3B0cyA9IG1lcmdlT3B0cyh7aW5kZXg6IGluZGV4fSwgJC5mYW5jeWJveC5kZWZhdWx0cyk7XG5cbiAgICBpZiAoJC5pc1BsYWluT2JqZWN0KG9wdHMpKSB7XG4gICAgICBzZWxmLm9wdHMgPSBtZXJnZU9wdHMoc2VsZi5vcHRzLCBvcHRzKTtcbiAgICB9XG5cbiAgICBpZiAoJC5mYW5jeWJveC5pc01vYmlsZSkge1xuICAgICAgc2VsZi5vcHRzID0gbWVyZ2VPcHRzKHNlbGYub3B0cywgc2VsZi5vcHRzLm1vYmlsZSk7XG4gICAgfVxuXG4gICAgc2VsZi5pZCA9IHNlbGYub3B0cy5pZCB8fCArK2NhbGxlZDtcblxuICAgIHNlbGYuY3VyckluZGV4ID0gcGFyc2VJbnQoc2VsZi5vcHRzLmluZGV4LCAxMCkgfHwgMDtcbiAgICBzZWxmLnByZXZJbmRleCA9IG51bGw7XG5cbiAgICBzZWxmLnByZXZQb3MgPSBudWxsO1xuICAgIHNlbGYuY3VyclBvcyA9IDA7XG5cbiAgICBzZWxmLmZpcnN0UnVuID0gdHJ1ZTtcblxuICAgIC8vIEFsbCBncm91cCBpdGVtc1xuICAgIHNlbGYuZ3JvdXAgPSBbXTtcblxuICAgIC8vIEV4aXN0aW5nIHNsaWRlcyAoZm9yIGN1cnJlbnQsIG5leHQgYW5kIHByZXZpb3VzIGdhbGxlcnkgaXRlbXMpXG4gICAgc2VsZi5zbGlkZXMgPSB7fTtcblxuICAgIC8vIENyZWF0ZSBncm91cCBlbGVtZW50c1xuICAgIHNlbGYuYWRkQ29udGVudChjb250ZW50KTtcblxuICAgIGlmICghc2VsZi5ncm91cC5sZW5ndGgpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICAvLyBTYXZlIGxhc3QgYWN0aXZlIGVsZW1lbnRcbiAgICBzZWxmLiRsYXN0Rm9jdXMgPSAkKGRvY3VtZW50LmFjdGl2ZUVsZW1lbnQpLnRyaWdnZXIoXCJibHVyXCIpO1xuXG4gICAgc2VsZi5pbml0KCk7XG4gIH07XG5cbiAgJC5leHRlbmQoRmFuY3lCb3gucHJvdG90eXBlLCB7XG4gICAgLy8gQ3JlYXRlIERPTSBzdHJ1Y3R1cmVcbiAgICAvLyA9PT09PT09PT09PT09PT09PT09PVxuXG4gICAgaW5pdDogZnVuY3Rpb24oKSB7XG4gICAgICB2YXIgc2VsZiA9IHRoaXMsXG4gICAgICAgIGZpcnN0SXRlbSA9IHNlbGYuZ3JvdXBbc2VsZi5jdXJySW5kZXhdLFxuICAgICAgICBmaXJzdEl0ZW1PcHRzID0gZmlyc3RJdGVtLm9wdHMsXG4gICAgICAgIHNjcm9sbGJhcldpZHRoID0gJC5mYW5jeWJveC5zY3JvbGxiYXJXaWR0aCxcbiAgICAgICAgJHNjcm9sbERpdixcbiAgICAgICAgJGNvbnRhaW5lcixcbiAgICAgICAgYnV0dG9uU3RyO1xuXG4gICAgICAvLyBIaWRlIHNjcm9sbGJhcnNcbiAgICAgIC8vID09PT09PT09PT09PT09PVxuXG4gICAgICBpZiAoISQuZmFuY3lib3guZ2V0SW5zdGFuY2UoKSAmJiBmaXJzdEl0ZW1PcHRzLmhpZGVTY3JvbGxiYXIgIT09IGZhbHNlKSB7XG4gICAgICAgICQoXCJib2R5XCIpLmFkZENsYXNzKFwiZmFuY3lib3gtYWN0aXZlXCIpO1xuXG4gICAgICAgIGlmICghJC5mYW5jeWJveC5pc01vYmlsZSAmJiBkb2N1bWVudC5ib2R5LnNjcm9sbEhlaWdodCA+IHdpbmRvdy5pbm5lckhlaWdodCkge1xuICAgICAgICAgIGlmIChzY3JvbGxiYXJXaWR0aCA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAkc2Nyb2xsRGl2ID0gJCgnPGRpdiBzdHlsZT1cIndpZHRoOjEwMHB4O2hlaWdodDoxMDBweDtvdmVyZmxvdzpzY3JvbGw7XCIgLz4nKS5hcHBlbmRUbyhcImJvZHlcIik7XG5cbiAgICAgICAgICAgIHNjcm9sbGJhcldpZHRoID0gJC5mYW5jeWJveC5zY3JvbGxiYXJXaWR0aCA9ICRzY3JvbGxEaXZbMF0ub2Zmc2V0V2lkdGggLSAkc2Nyb2xsRGl2WzBdLmNsaWVudFdpZHRoO1xuXG4gICAgICAgICAgICAkc2Nyb2xsRGl2LnJlbW92ZSgpO1xuICAgICAgICAgIH1cblxuICAgICAgICAgICQoXCJoZWFkXCIpLmFwcGVuZChcbiAgICAgICAgICAgICc8c3R5bGUgaWQ9XCJmYW5jeWJveC1zdHlsZS1ub3Njcm9sbFwiIHR5cGU9XCJ0ZXh0L2Nzc1wiPi5jb21wZW5zYXRlLWZvci1zY3JvbGxiYXIgeyBtYXJnaW4tcmlnaHQ6ICcgK1xuICAgICAgICAgICAgICBzY3JvbGxiYXJXaWR0aCArXG4gICAgICAgICAgICAgIFwicHg7IH08L3N0eWxlPlwiXG4gICAgICAgICAgKTtcblxuICAgICAgICAgICQoXCJib2R5XCIpLmFkZENsYXNzKFwiY29tcGVuc2F0ZS1mb3Itc2Nyb2xsYmFyXCIpO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIC8vIEJ1aWxkIGh0bWwgbWFya3VwIGFuZCBzZXQgcmVmZXJlbmNlc1xuICAgICAgLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG5cbiAgICAgIC8vIEJ1aWxkIGh0bWwgY29kZSBmb3IgYnV0dG9ucyBhbmQgaW5zZXJ0IGludG8gbWFpbiB0ZW1wbGF0ZVxuICAgICAgYnV0dG9uU3RyID0gXCJcIjtcblxuICAgICAgJC5lYWNoKGZpcnN0SXRlbU9wdHMuYnV0dG9ucywgZnVuY3Rpb24oaW5kZXgsIHZhbHVlKSB7XG4gICAgICAgIGJ1dHRvblN0ciArPSBmaXJzdEl0ZW1PcHRzLmJ0blRwbFt2YWx1ZV0gfHwgXCJcIjtcbiAgICAgIH0pO1xuXG4gICAgICAvLyBDcmVhdGUgbWFya3VwIGZyb20gYmFzZSB0ZW1wbGF0ZSwgaXQgd2lsbCBiZSBpbml0aWFsbHkgaGlkZGVuIHRvXG4gICAgICAvLyBhdm9pZCB1bm5lY2Vzc2FyeSB3b3JrIGxpa2UgcGFpbnRpbmcgd2hpbGUgaW5pdGlhbGl6aW5nIGlzIG5vdCBjb21wbGV0ZVxuICAgICAgJGNvbnRhaW5lciA9ICQoXG4gICAgICAgIHNlbGYudHJhbnNsYXRlKFxuICAgICAgICAgIHNlbGYsXG4gICAgICAgICAgZmlyc3RJdGVtT3B0cy5iYXNlVHBsXG4gICAgICAgICAgICAucmVwbGFjZShcInt7YnV0dG9uc319XCIsIGJ1dHRvblN0cilcbiAgICAgICAgICAgIC5yZXBsYWNlKFwie3thcnJvd3N9fVwiLCBmaXJzdEl0ZW1PcHRzLmJ0blRwbC5hcnJvd0xlZnQgKyBmaXJzdEl0ZW1PcHRzLmJ0blRwbC5hcnJvd1JpZ2h0KVxuICAgICAgICApXG4gICAgICApXG4gICAgICAgIC5hdHRyKFwiaWRcIiwgXCJmYW5jeWJveC1jb250YWluZXItXCIgKyBzZWxmLmlkKVxuICAgICAgICAuYWRkQ2xhc3MoXCJmYW5jeWJveC1pcy1oaWRkZW5cIilcbiAgICAgICAgLmFkZENsYXNzKGZpcnN0SXRlbU9wdHMuYmFzZUNsYXNzKVxuICAgICAgICAuZGF0YShcIkZhbmN5Qm94XCIsIHNlbGYpXG4gICAgICAgIC5hcHBlbmRUbyhmaXJzdEl0ZW1PcHRzLnBhcmVudEVsKTtcblxuICAgICAgLy8gQ3JlYXRlIG9iamVjdCBob2xkaW5nIHJlZmVyZW5jZXMgdG8galF1ZXJ5IHdyYXBwZWQgbm9kZXNcbiAgICAgIHNlbGYuJHJlZnMgPSB7XG4gICAgICAgIGNvbnRhaW5lcjogJGNvbnRhaW5lclxuICAgICAgfTtcblxuICAgICAgW1wiYmdcIiwgXCJpbm5lclwiLCBcImluZm9iYXJcIiwgXCJ0b29sYmFyXCIsIFwic3RhZ2VcIiwgXCJjYXB0aW9uXCIsIFwibmF2aWdhdGlvblwiXS5mb3JFYWNoKGZ1bmN0aW9uKGl0ZW0pIHtcbiAgICAgICAgc2VsZi4kcmVmc1tpdGVtXSA9ICRjb250YWluZXIuZmluZChcIi5mYW5jeWJveC1cIiArIGl0ZW0pO1xuICAgICAgfSk7XG5cbiAgICAgIHNlbGYudHJpZ2dlcihcIm9uSW5pdFwiKTtcblxuICAgICAgLy8gRW5hYmxlIGV2ZW50cywgZGVhY3RpdmUgcHJldmlvdXMgaW5zdGFuY2VzXG4gICAgICBzZWxmLmFjdGl2YXRlKCk7XG5cbiAgICAgIC8vIEJ1aWxkIHNsaWRlcywgbG9hZCBhbmQgcmV2ZWFsIGNvbnRlbnRcbiAgICAgIHNlbGYuanVtcFRvKHNlbGYuY3VyckluZGV4KTtcbiAgICB9LFxuXG4gICAgLy8gU2ltcGxlIGkxOG4gc3VwcG9ydCAtIHJlcGxhY2VzIG9iamVjdCBrZXlzIGZvdW5kIGluIHRlbXBsYXRlXG4gICAgLy8gd2l0aCBjb3JyZXNwb25kaW5nIHZhbHVlc1xuICAgIC8vID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuXG4gICAgdHJhbnNsYXRlOiBmdW5jdGlvbihvYmosIHN0cikge1xuICAgICAgdmFyIGFyciA9IG9iai5vcHRzLmkxOG5bb2JqLm9wdHMubGFuZ107XG5cbiAgICAgIHJldHVybiBzdHIucmVwbGFjZSgvXFx7XFx7KFxcdyspXFx9XFx9L2csIGZ1bmN0aW9uKG1hdGNoLCBuKSB7XG4gICAgICAgIHZhciB2YWx1ZSA9IGFycltuXTtcblxuICAgICAgICBpZiAodmFsdWUgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgIHJldHVybiBtYXRjaDtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiB2YWx1ZTtcbiAgICAgIH0pO1xuICAgIH0sXG5cbiAgICAvLyBQb3B1bGF0ZSBjdXJyZW50IGdyb3VwIHdpdGggZnJlc2ggY29udGVudFxuICAgIC8vIENoZWNrIGlmIGVhY2ggb2JqZWN0IGhhcyB2YWxpZCB0eXBlIGFuZCBjb250ZW50XG4gICAgLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cblxuICAgIGFkZENvbnRlbnQ6IGZ1bmN0aW9uKGNvbnRlbnQpIHtcbiAgICAgIHZhciBzZWxmID0gdGhpcyxcbiAgICAgICAgaXRlbXMgPSAkLm1ha2VBcnJheShjb250ZW50KSxcbiAgICAgICAgdGh1bWJzO1xuXG4gICAgICAkLmVhY2goaXRlbXMsIGZ1bmN0aW9uKGksIGl0ZW0pIHtcbiAgICAgICAgdmFyIG9iaiA9IHt9LFxuICAgICAgICAgIG9wdHMgPSB7fSxcbiAgICAgICAgICAkaXRlbSxcbiAgICAgICAgICB0eXBlLFxuICAgICAgICAgIGZvdW5kLFxuICAgICAgICAgIHNyYyxcbiAgICAgICAgICBzcmNQYXJ0cztcblxuICAgICAgICAvLyBTdGVwIDEgLSBNYWtlIHN1cmUgd2UgaGF2ZSBhbiBvYmplY3RcbiAgICAgICAgLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG5cbiAgICAgICAgaWYgKCQuaXNQbGFpbk9iamVjdChpdGVtKSkge1xuICAgICAgICAgIC8vIFdlIHByb2JhYmx5IGhhdmUgbWFudWFsIHVzYWdlIGhlcmUsIHNvbWV0aGluZyBsaWtlXG4gICAgICAgICAgLy8gJC5mYW5jeWJveC5vcGVuKCBbIHsgc3JjIDogXCJpbWFnZS5qcGdcIiwgdHlwZSA6IFwiaW1hZ2VcIiB9IF0gKVxuXG4gICAgICAgICAgb2JqID0gaXRlbTtcbiAgICAgICAgICBvcHRzID0gaXRlbS5vcHRzIHx8IGl0ZW07XG4gICAgICAgIH0gZWxzZSBpZiAoJC50eXBlKGl0ZW0pID09PSBcIm9iamVjdFwiICYmICQoaXRlbSkubGVuZ3RoKSB7XG4gICAgICAgICAgLy8gSGVyZSB3ZSBwcm9iYWJseSBoYXZlIGpRdWVyeSBjb2xsZWN0aW9uIHJldHVybmVkIGJ5IHNvbWUgc2VsZWN0b3JcbiAgICAgICAgICAkaXRlbSA9ICQoaXRlbSk7XG5cbiAgICAgICAgICAvLyBTdXBwb3J0IGF0dHJpYnV0ZXMgbGlrZSBgZGF0YS1vcHRpb25zPSd7XCJ0b3VjaFwiIDogZmFsc2V9J2AgYW5kIGBkYXRhLXRvdWNoPSdmYWxzZSdgXG4gICAgICAgICAgb3B0cyA9ICRpdGVtLmRhdGEoKSB8fCB7fTtcbiAgICAgICAgICBvcHRzID0gJC5leHRlbmQodHJ1ZSwge30sIG9wdHMsIG9wdHMub3B0aW9ucyk7XG5cbiAgICAgICAgICAvLyBIZXJlIHdlIHN0b3JlIGNsaWNrZWQgZWxlbWVudFxuICAgICAgICAgIG9wdHMuJG9yaWcgPSAkaXRlbTtcblxuICAgICAgICAgIG9iai5zcmMgPSBzZWxmLm9wdHMuc3JjIHx8IG9wdHMuc3JjIHx8ICRpdGVtLmF0dHIoXCJocmVmXCIpO1xuXG4gICAgICAgICAgLy8gQXNzdW1lIHRoYXQgc2ltcGxlIHN5bnRheCBpcyB1c2VkLCBmb3IgZXhhbXBsZTpcbiAgICAgICAgICAvLyAgIGAkLmZhbmN5Ym94Lm9wZW4oICQoXCIjdGVzdFwiKSwge30gKTtgXG4gICAgICAgICAgaWYgKCFvYmoudHlwZSAmJiAhb2JqLnNyYykge1xuICAgICAgICAgICAgb2JqLnR5cGUgPSBcImlubGluZVwiO1xuICAgICAgICAgICAgb2JqLnNyYyA9IGl0ZW07XG4gICAgICAgICAgfVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIC8vIEFzc3VtZSB3ZSBoYXZlIGEgc2ltcGxlIGh0bWwgY29kZSwgZm9yIGV4YW1wbGU6XG4gICAgICAgICAgLy8gICAkLmZhbmN5Ym94Lm9wZW4oICc8ZGl2PjxoMT5IaSE8L2gxPjwvZGl2PicgKTtcbiAgICAgICAgICBvYmogPSB7XG4gICAgICAgICAgICB0eXBlOiBcImh0bWxcIixcbiAgICAgICAgICAgIHNyYzogaXRlbSArIFwiXCJcbiAgICAgICAgICB9O1xuICAgICAgICB9XG5cbiAgICAgICAgLy8gRWFjaCBnYWxsZXJ5IG9iamVjdCBoYXMgZnVsbCBjb2xsZWN0aW9uIG9mIG9wdGlvbnNcbiAgICAgICAgb2JqLm9wdHMgPSAkLmV4dGVuZCh0cnVlLCB7fSwgc2VsZi5vcHRzLCBvcHRzKTtcblxuICAgICAgICAvLyBEbyBub3QgbWVyZ2UgYnV0dG9ucyBhcnJheVxuICAgICAgICBpZiAoJC5pc0FycmF5KG9wdHMuYnV0dG9ucykpIHtcbiAgICAgICAgICBvYmoub3B0cy5idXR0b25zID0gb3B0cy5idXR0b25zO1xuICAgICAgICB9XG5cbiAgICAgICAgLy8gU3RlcCAyIC0gTWFrZSBzdXJlIHdlIGhhdmUgY29udGVudCB0eXBlLCBpZiBub3QgLSB0cnkgdG8gZ3Vlc3NcbiAgICAgICAgLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cblxuICAgICAgICB0eXBlID0gb2JqLnR5cGUgfHwgb2JqLm9wdHMudHlwZTtcbiAgICAgICAgc3JjID0gb2JqLnNyYyB8fCBcIlwiO1xuXG4gICAgICAgIGlmICghdHlwZSAmJiBzcmMpIHtcbiAgICAgICAgICBpZiAoKGZvdW5kID0gc3JjLm1hdGNoKC9cXC4obXA0fG1vdnxvZ3YpKChcXD98IykuKik/JC9pKSkpIHtcbiAgICAgICAgICAgIHR5cGUgPSBcInZpZGVvXCI7XG5cbiAgICAgICAgICAgIGlmICghb2JqLm9wdHMudmlkZW9Gb3JtYXQpIHtcbiAgICAgICAgICAgICAgb2JqLm9wdHMudmlkZW9Gb3JtYXQgPSBcInZpZGVvL1wiICsgKGZvdW5kWzFdID09PSBcIm9ndlwiID8gXCJvZ2dcIiA6IGZvdW5kWzFdKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9IGVsc2UgaWYgKHNyYy5tYXRjaCgvKF5kYXRhOmltYWdlXFwvW2EtejAtOStcXC89XSosKXwoXFwuKGpwKGV8Z3xlZyl8Z2lmfHBuZ3xibXB8d2VicHxzdmd8aWNvKSgoXFw/fCMpLiopPyQpL2kpKSB7XG4gICAgICAgICAgICB0eXBlID0gXCJpbWFnZVwiO1xuICAgICAgICAgIH0gZWxzZSBpZiAoc3JjLm1hdGNoKC9cXC4ocGRmKSgoXFw/fCMpLiopPyQvaSkpIHtcbiAgICAgICAgICAgIHR5cGUgPSBcImlmcmFtZVwiO1xuICAgICAgICAgIH0gZWxzZSBpZiAoc3JjLmNoYXJBdCgwKSA9PT0gXCIjXCIpIHtcbiAgICAgICAgICAgIHR5cGUgPSBcImlubGluZVwiO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIGlmICh0eXBlKSB7XG4gICAgICAgICAgb2JqLnR5cGUgPSB0eXBlO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHNlbGYudHJpZ2dlcihcIm9iamVjdE5lZWRzVHlwZVwiLCBvYmopO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKCFvYmouY29udGVudFR5cGUpIHtcbiAgICAgICAgICBvYmouY29udGVudFR5cGUgPSAkLmluQXJyYXkob2JqLnR5cGUsIFtcImh0bWxcIiwgXCJpbmxpbmVcIiwgXCJhamF4XCJdKSA+IC0xID8gXCJodG1sXCIgOiBvYmoudHlwZTtcbiAgICAgICAgfVxuXG4gICAgICAgIC8vIFN0ZXAgMyAtIFNvbWUgYWRqdXN0bWVudHNcbiAgICAgICAgLy8gPT09PT09PT09PT09PT09PT09PT09PT09PVxuXG4gICAgICAgIG9iai5pbmRleCA9IHNlbGYuZ3JvdXAubGVuZ3RoO1xuXG4gICAgICAgIGlmIChvYmoub3B0cy5zbWFsbEJ0biA9PSBcImF1dG9cIikge1xuICAgICAgICAgIG9iai5vcHRzLnNtYWxsQnRuID0gJC5pbkFycmF5KG9iai50eXBlLCBbXCJodG1sXCIsIFwiaW5saW5lXCIsIFwiYWpheFwiXSkgPiAtMTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChvYmoub3B0cy50b29sYmFyID09PSBcImF1dG9cIikge1xuICAgICAgICAgIG9iai5vcHRzLnRvb2xiYXIgPSAhb2JqLm9wdHMuc21hbGxCdG47XG4gICAgICAgIH1cblxuICAgICAgICAvLyBGaW5kIHRodW1ibmFpbCBpbWFnZVxuICAgICAgICBpZiAob2JqLm9wdHMuJHRyaWdnZXIgJiYgb2JqLmluZGV4ID09PSBzZWxmLm9wdHMuaW5kZXgpIHtcbiAgICAgICAgICBvYmoub3B0cy4kdGh1bWIgPSBvYmoub3B0cy4kdHJpZ2dlci5maW5kKFwiaW1nOmZpcnN0XCIpO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKCghb2JqLm9wdHMuJHRodW1iIHx8ICFvYmoub3B0cy4kdGh1bWIubGVuZ3RoKSAmJiBvYmoub3B0cy4kb3JpZykge1xuICAgICAgICAgIG9iai5vcHRzLiR0aHVtYiA9IG9iai5vcHRzLiRvcmlnLmZpbmQoXCJpbWc6Zmlyc3RcIik7XG4gICAgICAgIH1cblxuICAgICAgICAvLyBcImNhcHRpb25cIiBpcyBhIFwic3BlY2lhbFwiIG9wdGlvbiwgaXQgY2FuIGJlIHVzZWQgdG8gY3VzdG9taXplIGNhcHRpb24gcGVyIGdhbGxlcnkgaXRlbSAuLlxuICAgICAgICBpZiAoJC50eXBlKG9iai5vcHRzLmNhcHRpb24pID09PSBcImZ1bmN0aW9uXCIpIHtcbiAgICAgICAgICBvYmoub3B0cy5jYXB0aW9uID0gb2JqLm9wdHMuY2FwdGlvbi5hcHBseShpdGVtLCBbc2VsZiwgb2JqXSk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoJC50eXBlKHNlbGYub3B0cy5jYXB0aW9uKSA9PT0gXCJmdW5jdGlvblwiKSB7XG4gICAgICAgICAgb2JqLm9wdHMuY2FwdGlvbiA9IHNlbGYub3B0cy5jYXB0aW9uLmFwcGx5KGl0ZW0sIFtzZWxmLCBvYmpdKTtcbiAgICAgICAgfVxuXG4gICAgICAgIC8vIE1ha2Ugc3VyZSB3ZSBoYXZlIGNhcHRpb24gYXMgYSBzdHJpbmcgb3IgalF1ZXJ5IG9iamVjdFxuICAgICAgICBpZiAoIShvYmoub3B0cy5jYXB0aW9uIGluc3RhbmNlb2YgJCkpIHtcbiAgICAgICAgICBvYmoub3B0cy5jYXB0aW9uID0gb2JqLm9wdHMuY2FwdGlvbiA9PT0gdW5kZWZpbmVkID8gXCJcIiA6IG9iai5vcHRzLmNhcHRpb24gKyBcIlwiO1xuICAgICAgICB9XG5cbiAgICAgICAgLy8gQ2hlY2sgaWYgdXJsIGNvbnRhaW5zIFwiZmlsdGVyXCIgdXNlZCB0byBmaWx0ZXIgdGhlIGNvbnRlbnRcbiAgICAgICAgLy8gRXhhbXBsZTogXCJhamF4Lmh0bWwgI3NvbWV0aGluZ1wiXG4gICAgICAgIGlmIChvYmoudHlwZSA9PT0gXCJhamF4XCIpIHtcbiAgICAgICAgICBzcmNQYXJ0cyA9IHNyYy5zcGxpdCgvXFxzKy8sIDIpO1xuXG4gICAgICAgICAgaWYgKHNyY1BhcnRzLmxlbmd0aCA+IDEpIHtcbiAgICAgICAgICAgIG9iai5zcmMgPSBzcmNQYXJ0cy5zaGlmdCgpO1xuXG4gICAgICAgICAgICBvYmoub3B0cy5maWx0ZXIgPSBzcmNQYXJ0cy5zaGlmdCgpO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIC8vIEhpZGUgYWxsIGJ1dHRvbnMgYW5kIGRpc2FibGUgaW50ZXJhY3Rpdml0eSBmb3IgbW9kYWwgaXRlbXNcbiAgICAgICAgaWYgKG9iai5vcHRzLm1vZGFsKSB7XG4gICAgICAgICAgb2JqLm9wdHMgPSAkLmV4dGVuZCh0cnVlLCBvYmoub3B0cywge1xuICAgICAgICAgICAgLy8gUmVtb3ZlIGJ1dHRvbnNcbiAgICAgICAgICAgIGluZm9iYXI6IDAsXG4gICAgICAgICAgICB0b29sYmFyOiAwLFxuXG4gICAgICAgICAgICBzbWFsbEJ0bjogMCxcblxuICAgICAgICAgICAgLy8gRGlzYWJsZSBrZXlib2FyZCBuYXZpZ2F0aW9uXG4gICAgICAgICAgICBrZXlib2FyZDogMCxcblxuICAgICAgICAgICAgLy8gRGlzYWJsZSBzb21lIG1vZHVsZXNcbiAgICAgICAgICAgIHNsaWRlU2hvdzogMCxcbiAgICAgICAgICAgIGZ1bGxTY3JlZW46IDAsXG4gICAgICAgICAgICB0aHVtYnM6IDAsXG4gICAgICAgICAgICB0b3VjaDogMCxcblxuICAgICAgICAgICAgLy8gRGlzYWJsZSBjbGljayBldmVudCBoYW5kbGVyc1xuICAgICAgICAgICAgY2xpY2tDb250ZW50OiBmYWxzZSxcbiAgICAgICAgICAgIGNsaWNrU2xpZGU6IGZhbHNlLFxuICAgICAgICAgICAgY2xpY2tPdXRzaWRlOiBmYWxzZSxcbiAgICAgICAgICAgIGRibGNsaWNrQ29udGVudDogZmFsc2UsXG4gICAgICAgICAgICBkYmxjbGlja1NsaWRlOiBmYWxzZSxcbiAgICAgICAgICAgIGRibGNsaWNrT3V0c2lkZTogZmFsc2VcbiAgICAgICAgICB9KTtcbiAgICAgICAgfVxuXG4gICAgICAgIC8vIFN0ZXAgNCAtIEFkZCBwcm9jZXNzZWQgb2JqZWN0IHRvIGdyb3VwXG4gICAgICAgIC8vID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG5cbiAgICAgICAgc2VsZi5ncm91cC5wdXNoKG9iaik7XG4gICAgICB9KTtcblxuICAgICAgLy8gVXBkYXRlIGNvbnRyb2xzIGlmIGdhbGxlcnkgaXMgYWxyZWFkeSBvcGVuZWRcbiAgICAgIGlmIChPYmplY3Qua2V5cyhzZWxmLnNsaWRlcykubGVuZ3RoKSB7XG4gICAgICAgIHNlbGYudXBkYXRlQ29udHJvbHMoKTtcblxuICAgICAgICAvLyBVcGRhdGUgdGh1bWJuYWlscywgaWYgbmVlZGVkXG4gICAgICAgIHRodW1icyA9IHNlbGYuVGh1bWJzO1xuXG4gICAgICAgIGlmICh0aHVtYnMgJiYgdGh1bWJzLmlzQWN0aXZlKSB7XG4gICAgICAgICAgdGh1bWJzLmNyZWF0ZSgpO1xuXG4gICAgICAgICAgdGh1bWJzLmZvY3VzKCk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9LFxuXG4gICAgLy8gQXR0YWNoIGFuIGV2ZW50IGhhbmRsZXIgZnVuY3Rpb25zIGZvcjpcbiAgICAvLyAgIC0gbmF2aWdhdGlvbiBidXR0b25zXG4gICAgLy8gICAtIGJyb3dzZXIgc2Nyb2xsaW5nLCByZXNpemluZztcbiAgICAvLyAgIC0gZm9jdXNpbmdcbiAgICAvLyAgIC0ga2V5Ym9hcmRcbiAgICAvLyAgIC0gZGV0ZWN0IGlkbGVcbiAgICAvLyA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuXG4gICAgYWRkRXZlbnRzOiBmdW5jdGlvbigpIHtcbiAgICAgIHZhciBzZWxmID0gdGhpcztcblxuICAgICAgc2VsZi5yZW1vdmVFdmVudHMoKTtcblxuICAgICAgLy8gTWFrZSBuYXZpZ2F0aW9uIGVsZW1lbnRzIGNsaWNrYWJsZVxuICAgICAgc2VsZi4kcmVmcy5jb250YWluZXJcbiAgICAgICAgLm9uKFwiY2xpY2suZmItY2xvc2VcIiwgXCJbZGF0YS1mYW5jeWJveC1jbG9zZV1cIiwgZnVuY3Rpb24oZSkge1xuICAgICAgICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XG4gICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgICAgICAgc2VsZi5jbG9zZShlKTtcbiAgICAgICAgfSlcbiAgICAgICAgLm9uKFwidG91Y2hzdGFydC5mYi1wcmV2IGNsaWNrLmZiLXByZXZcIiwgXCJbZGF0YS1mYW5jeWJveC1wcmV2XVwiLCBmdW5jdGlvbihlKSB7XG4gICAgICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcbiAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG5cbiAgICAgICAgICBzZWxmLnByZXZpb3VzKCk7XG4gICAgICAgIH0pXG4gICAgICAgIC5vbihcInRvdWNoc3RhcnQuZmItbmV4dCBjbGljay5mYi1uZXh0XCIsIFwiW2RhdGEtZmFuY3lib3gtbmV4dF1cIiwgZnVuY3Rpb24oZSkge1xuICAgICAgICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XG4gICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgICAgICAgc2VsZi5uZXh0KCk7XG4gICAgICAgIH0pXG4gICAgICAgIC5vbihcImNsaWNrLmZiXCIsIFwiW2RhdGEtZmFuY3lib3gtem9vbV1cIiwgZnVuY3Rpb24oZSkge1xuICAgICAgICAgIC8vIENsaWNrIGhhbmRsZXIgZm9yIHpvb20gYnV0dG9uXG4gICAgICAgICAgc2VsZltzZWxmLmlzU2NhbGVkRG93bigpID8gXCJzY2FsZVRvQWN0dWFsXCIgOiBcInNjYWxlVG9GaXRcIl0oKTtcbiAgICAgICAgfSk7XG5cbiAgICAgIC8vIEhhbmRsZSBwYWdlIHNjcm9sbGluZyBhbmQgYnJvd3NlciByZXNpemluZ1xuICAgICAgJFcub24oXCJvcmllbnRhdGlvbmNoYW5nZS5mYiByZXNpemUuZmJcIiwgZnVuY3Rpb24oZSkge1xuICAgICAgICBpZiAoZSAmJiBlLm9yaWdpbmFsRXZlbnQgJiYgZS5vcmlnaW5hbEV2ZW50LnR5cGUgPT09IFwicmVzaXplXCIpIHtcbiAgICAgICAgICByZXF1ZXN0QUZyYW1lKGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgc2VsZi51cGRhdGUoKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBzZWxmLiRyZWZzLnN0YWdlLmhpZGUoKTtcblxuICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBzZWxmLiRyZWZzLnN0YWdlLnNob3coKTtcblxuICAgICAgICAgICAgc2VsZi51cGRhdGUoKTtcbiAgICAgICAgICB9LCAkLmZhbmN5Ym94LmlzTW9iaWxlID8gNjAwIDogMjUwKTtcbiAgICAgICAgfVxuICAgICAgfSk7XG5cbiAgICAgIC8vIFRyYXAga2V5Ym9hcmQgZm9jdXMgaW5zaWRlIG9mIHRoZSBtb2RhbCwgc28gdGhlIHVzZXIgZG9lcyBub3QgYWNjaWRlbnRhbGx5IHRhYiBvdXRzaWRlIG9mIHRoZSBtb2RhbFxuICAgICAgLy8gKGEuay5hLiBcImVzY2FwaW5nIHRoZSBtb2RhbFwiKVxuICAgICAgJEQub24oXCJmb2N1c2luLmZiXCIsIGZ1bmN0aW9uKGUpIHtcbiAgICAgICAgdmFyIGluc3RhbmNlID0gJC5mYW5jeWJveCA/ICQuZmFuY3lib3guZ2V0SW5zdGFuY2UoKSA6IG51bGw7XG5cbiAgICAgICAgaWYgKFxuICAgICAgICAgIGluc3RhbmNlLmlzQ2xvc2luZyB8fFxuICAgICAgICAgICFpbnN0YW5jZS5jdXJyZW50IHx8XG4gICAgICAgICAgIWluc3RhbmNlLmN1cnJlbnQub3B0cy50cmFwRm9jdXMgfHxcbiAgICAgICAgICAkKGUudGFyZ2V0KS5oYXNDbGFzcyhcImZhbmN5Ym94LWNvbnRhaW5lclwiKSB8fFxuICAgICAgICAgICQoZS50YXJnZXQpLmlzKGRvY3VtZW50KVxuICAgICAgICApIHtcbiAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoaW5zdGFuY2UgJiYgJChlLnRhcmdldCkuY3NzKFwicG9zaXRpb25cIikgIT09IFwiZml4ZWRcIiAmJiAhaW5zdGFuY2UuJHJlZnMuY29udGFpbmVyLmhhcyhlLnRhcmdldCkubGVuZ3RoKSB7XG4gICAgICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcblxuICAgICAgICAgIGluc3RhbmNlLmZvY3VzKCk7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuXG4gICAgICAvLyBFbmFibGUga2V5Ym9hcmQgbmF2aWdhdGlvblxuICAgICAgJEQub24oXCJrZXlkb3duLmZiXCIsIGZ1bmN0aW9uKGUpIHtcbiAgICAgICAgdmFyIGN1cnJlbnQgPSBzZWxmLmN1cnJlbnQsXG4gICAgICAgICAga2V5Y29kZSA9IGUua2V5Q29kZSB8fCBlLndoaWNoO1xuXG4gICAgICAgIGlmICghY3VycmVudCB8fCAhY3VycmVudC5vcHRzLmtleWJvYXJkKSB7XG4gICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKGUuY3RybEtleSB8fCBlLmFsdEtleSB8fCBlLnNoaWZ0S2V5IHx8ICQoZS50YXJnZXQpLmlzKFwiaW5wdXRcIikgfHwgJChlLnRhcmdldCkuaXMoXCJ0ZXh0YXJlYVwiKSkge1xuICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIC8vIEJhY2tzcGFjZSBhbmQgRXNjIGtleXNcbiAgICAgICAgaWYgKGtleWNvZGUgPT09IDggfHwga2V5Y29kZSA9PT0gMjcpIHtcbiAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG5cbiAgICAgICAgICBzZWxmLmNsb3NlKGUpO1xuXG4gICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgLy8gTGVmdCBhcnJvdyBhbmQgVXAgYXJyb3dcbiAgICAgICAgaWYgKGtleWNvZGUgPT09IDM3IHx8IGtleWNvZGUgPT09IDM4KSB7XG4gICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgICAgICAgc2VsZi5wcmV2aW91cygpO1xuXG4gICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgLy8gUmlnaCBhcnJvdyBhbmQgRG93biBhcnJvd1xuICAgICAgICBpZiAoa2V5Y29kZSA9PT0gMzkgfHwga2V5Y29kZSA9PT0gNDApIHtcbiAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG5cbiAgICAgICAgICBzZWxmLm5leHQoKTtcblxuICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIHNlbGYudHJpZ2dlcihcImFmdGVyS2V5ZG93blwiLCBlLCBrZXljb2RlKTtcbiAgICAgIH0pO1xuXG4gICAgICAvLyBIaWRlIGNvbnRyb2xzIGFmdGVyIHNvbWUgaW5hY3Rpdml0eSBwZXJpb2RcbiAgICAgIGlmIChzZWxmLmdyb3VwW3NlbGYuY3VyckluZGV4XS5vcHRzLmlkbGVUaW1lKSB7XG4gICAgICAgIHNlbGYuaWRsZVNlY29uZHNDb3VudGVyID0gMDtcblxuICAgICAgICAkRC5vbihcbiAgICAgICAgICBcIm1vdXNlbW92ZS5mYi1pZGxlIG1vdXNlbGVhdmUuZmItaWRsZSBtb3VzZWRvd24uZmItaWRsZSB0b3VjaHN0YXJ0LmZiLWlkbGUgdG91Y2htb3ZlLmZiLWlkbGUgc2Nyb2xsLmZiLWlkbGUga2V5ZG93bi5mYi1pZGxlXCIsXG4gICAgICAgICAgZnVuY3Rpb24oZSkge1xuICAgICAgICAgICAgc2VsZi5pZGxlU2Vjb25kc0NvdW50ZXIgPSAwO1xuXG4gICAgICAgICAgICBpZiAoc2VsZi5pc0lkbGUpIHtcbiAgICAgICAgICAgICAgc2VsZi5zaG93Q29udHJvbHMoKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgc2VsZi5pc0lkbGUgPSBmYWxzZTtcbiAgICAgICAgICB9XG4gICAgICAgICk7XG5cbiAgICAgICAgc2VsZi5pZGxlSW50ZXJ2YWwgPSB3aW5kb3cuc2V0SW50ZXJ2YWwoZnVuY3Rpb24oKSB7XG4gICAgICAgICAgc2VsZi5pZGxlU2Vjb25kc0NvdW50ZXIrKztcblxuICAgICAgICAgIGlmIChzZWxmLmlkbGVTZWNvbmRzQ291bnRlciA+PSBzZWxmLmdyb3VwW3NlbGYuY3VyckluZGV4XS5vcHRzLmlkbGVUaW1lICYmICFzZWxmLmlzRHJhZ2dpbmcpIHtcbiAgICAgICAgICAgIHNlbGYuaXNJZGxlID0gdHJ1ZTtcbiAgICAgICAgICAgIHNlbGYuaWRsZVNlY29uZHNDb3VudGVyID0gMDtcblxuICAgICAgICAgICAgc2VsZi5oaWRlQ29udHJvbHMoKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sIDEwMDApO1xuICAgICAgfVxuICAgIH0sXG5cbiAgICAvLyBSZW1vdmUgZXZlbnRzIGFkZGVkIGJ5IHRoZSBjb3JlXG4gICAgLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuXG4gICAgcmVtb3ZlRXZlbnRzOiBmdW5jdGlvbigpIHtcbiAgICAgIHZhciBzZWxmID0gdGhpcztcblxuICAgICAgJFcub2ZmKFwib3JpZW50YXRpb25jaGFuZ2UuZmIgcmVzaXplLmZiXCIpO1xuICAgICAgJEQub2ZmKFwiZm9jdXNpbi5mYiBrZXlkb3duLmZiIC5mYi1pZGxlXCIpO1xuXG4gICAgICB0aGlzLiRyZWZzLmNvbnRhaW5lci5vZmYoXCIuZmItY2xvc2UgLmZiLXByZXYgLmZiLW5leHRcIik7XG5cbiAgICAgIGlmIChzZWxmLmlkbGVJbnRlcnZhbCkge1xuICAgICAgICB3aW5kb3cuY2xlYXJJbnRlcnZhbChzZWxmLmlkbGVJbnRlcnZhbCk7XG5cbiAgICAgICAgc2VsZi5pZGxlSW50ZXJ2YWwgPSBudWxsO1xuICAgICAgfVxuICAgIH0sXG5cbiAgICAvLyBDaGFuZ2UgdG8gcHJldmlvdXMgZ2FsbGVyeSBpdGVtXG4gICAgLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuXG4gICAgcHJldmlvdXM6IGZ1bmN0aW9uKGR1cmF0aW9uKSB7XG4gICAgICByZXR1cm4gdGhpcy5qdW1wVG8odGhpcy5jdXJyUG9zIC0gMSwgZHVyYXRpb24pO1xuICAgIH0sXG5cbiAgICAvLyBDaGFuZ2UgdG8gbmV4dCBnYWxsZXJ5IGl0ZW1cbiAgICAvLyA9PT09PT09PT09PT09PT09PT09PT09PT09PT1cblxuICAgIG5leHQ6IGZ1bmN0aW9uKGR1cmF0aW9uKSB7XG4gICAgICByZXR1cm4gdGhpcy5qdW1wVG8odGhpcy5jdXJyUG9zICsgMSwgZHVyYXRpb24pO1xuICAgIH0sXG5cbiAgICAvLyBTd2l0Y2ggdG8gc2VsZWN0ZWQgZ2FsbGVyeSBpdGVtXG4gICAgLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuXG4gICAganVtcFRvOiBmdW5jdGlvbihwb3MsIGR1cmF0aW9uKSB7XG4gICAgICB2YXIgc2VsZiA9IHRoaXMsXG4gICAgICAgIGdyb3VwTGVuID0gc2VsZi5ncm91cC5sZW5ndGgsXG4gICAgICAgIGZpcnN0UnVuLFxuICAgICAgICBsb29wLFxuICAgICAgICBjdXJyZW50LFxuICAgICAgICBwcmV2aW91cyxcbiAgICAgICAgY2FudmFzV2lkdGgsXG4gICAgICAgIGN1cnJlbnRQb3MsXG4gICAgICAgIHRyYW5zaXRpb25Qcm9wcztcblxuICAgICAgaWYgKHNlbGYuaXNEcmFnZ2luZyB8fCBzZWxmLmlzQ2xvc2luZyB8fCAoc2VsZi5pc0FuaW1hdGluZyAmJiBzZWxmLmZpcnN0UnVuKSkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIHBvcyA9IHBhcnNlSW50KHBvcywgMTApO1xuXG4gICAgICAvLyBTaG91bGQgbG9vcD9cbiAgICAgIGxvb3AgPSBzZWxmLmN1cnJlbnQgPyBzZWxmLmN1cnJlbnQub3B0cy5sb29wIDogc2VsZi5vcHRzLmxvb3A7XG5cbiAgICAgIGlmICghbG9vcCAmJiAocG9zIDwgMCB8fCBwb3MgPj0gZ3JvdXBMZW4pKSB7XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgIH1cblxuICAgICAgZmlyc3RSdW4gPSBzZWxmLmZpcnN0UnVuID0gIU9iamVjdC5rZXlzKHNlbGYuc2xpZGVzKS5sZW5ndGg7XG5cbiAgICAgIGlmIChncm91cExlbiA8IDIgJiYgIWZpcnN0UnVuICYmICEhc2VsZi5pc0RyYWdnaW5nKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgcHJldmlvdXMgPSBzZWxmLmN1cnJlbnQ7XG5cbiAgICAgIHNlbGYucHJldkluZGV4ID0gc2VsZi5jdXJySW5kZXg7XG4gICAgICBzZWxmLnByZXZQb3MgPSBzZWxmLmN1cnJQb3M7XG5cbiAgICAgIC8vIENyZWF0ZSBzbGlkZXNcbiAgICAgIGN1cnJlbnQgPSBzZWxmLmNyZWF0ZVNsaWRlKHBvcyk7XG5cbiAgICAgIGlmIChncm91cExlbiA+IDEpIHtcbiAgICAgICAgaWYgKGxvb3AgfHwgY3VycmVudC5pbmRleCA+IDApIHtcbiAgICAgICAgICBzZWxmLmNyZWF0ZVNsaWRlKHBvcyAtIDEpO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKGxvb3AgfHwgY3VycmVudC5pbmRleCA8IGdyb3VwTGVuIC0gMSkge1xuICAgICAgICAgIHNlbGYuY3JlYXRlU2xpZGUocG9zICsgMSk7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgc2VsZi5jdXJyZW50ID0gY3VycmVudDtcbiAgICAgIHNlbGYuY3VyckluZGV4ID0gY3VycmVudC5pbmRleDtcbiAgICAgIHNlbGYuY3VyclBvcyA9IGN1cnJlbnQucG9zO1xuXG4gICAgICBzZWxmLnRyaWdnZXIoXCJiZWZvcmVTaG93XCIsIGZpcnN0UnVuKTtcblxuICAgICAgc2VsZi51cGRhdGVDb250cm9scygpO1xuXG4gICAgICBjdXJyZW50UG9zID0gJC5mYW5jeWJveC5nZXRUcmFuc2xhdGUoY3VycmVudC4kc2xpZGUpO1xuXG4gICAgICBjdXJyZW50LmlzTW92ZWQgPSAoY3VycmVudFBvcy5sZWZ0ICE9PSAwIHx8IGN1cnJlbnRQb3MudG9wICE9PSAwKSAmJiAhY3VycmVudC4kc2xpZGUuaGFzQ2xhc3MoXCJmYW5jeWJveC1hbmltYXRlZFwiKTtcblxuICAgICAgLy8gVmFsaWRhdGUgZHVyYXRpb24gbGVuZ3RoXG4gICAgICBjdXJyZW50LmZvcmNlZER1cmF0aW9uID0gdW5kZWZpbmVkO1xuXG4gICAgICBpZiAoJC5pc051bWVyaWMoZHVyYXRpb24pKSB7XG4gICAgICAgIGN1cnJlbnQuZm9yY2VkRHVyYXRpb24gPSBkdXJhdGlvbjtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGR1cmF0aW9uID0gY3VycmVudC5vcHRzW2ZpcnN0UnVuID8gXCJhbmltYXRpb25EdXJhdGlvblwiIDogXCJ0cmFuc2l0aW9uRHVyYXRpb25cIl07XG4gICAgICB9XG5cbiAgICAgIGR1cmF0aW9uID0gcGFyc2VJbnQoZHVyYXRpb24sIDEwKTtcblxuICAgICAgLy8gRnJlc2ggc3RhcnQgLSByZXZlYWwgY29udGFpbmVyLCBjdXJyZW50IHNsaWRlIGFuZCBzdGFydCBsb2FkaW5nIGNvbnRlbnRcbiAgICAgIGlmIChmaXJzdFJ1bikge1xuICAgICAgICBpZiAoY3VycmVudC5vcHRzLmFuaW1hdGlvbkVmZmVjdCAmJiBkdXJhdGlvbikge1xuICAgICAgICAgIHNlbGYuJHJlZnMuY29udGFpbmVyLmNzcyhcInRyYW5zaXRpb24tZHVyYXRpb25cIiwgZHVyYXRpb24gKyBcIm1zXCIpO1xuICAgICAgICB9XG5cbiAgICAgICAgc2VsZi4kcmVmcy5jb250YWluZXIucmVtb3ZlQ2xhc3MoXCJmYW5jeWJveC1pcy1oaWRkZW5cIik7XG5cbiAgICAgICAgZm9yY2VSZWRyYXcoc2VsZi4kcmVmcy5jb250YWluZXIpO1xuXG4gICAgICAgIHNlbGYuJHJlZnMuY29udGFpbmVyLmFkZENsYXNzKFwiZmFuY3lib3gtaXMtb3BlblwiKTtcblxuICAgICAgICBmb3JjZVJlZHJhdyhzZWxmLiRyZWZzLmNvbnRhaW5lcik7XG5cbiAgICAgICAgLy8gTWFrZSBjdXJyZW50IHNsaWRlIHZpc2libGVcbiAgICAgICAgY3VycmVudC4kc2xpZGUuYWRkQ2xhc3MoXCJmYW5jeWJveC1zbGlkZS0tcHJldmlvdXNcIik7XG5cbiAgICAgICAgLy8gQXR0ZW1wdCB0byBsb2FkIGNvbnRlbnQgaW50byBzbGlkZTtcbiAgICAgICAgLy8gYXQgdGhpcyBwb2ludCBpbWFnZSB3b3VsZCBzdGFydCBsb2FkaW5nLCBidXQgaW5saW5lL2h0bWwgY29udGVudCB3b3VsZCBsb2FkIGltbWVkaWF0ZWx5XG4gICAgICAgIHNlbGYubG9hZFNsaWRlKGN1cnJlbnQpO1xuXG4gICAgICAgIGN1cnJlbnQuJHNsaWRlLnJlbW92ZUNsYXNzKFwiZmFuY3lib3gtc2xpZGUtLXByZXZpb3VzXCIpLmFkZENsYXNzKFwiZmFuY3lib3gtc2xpZGUtLWN1cnJlbnRcIik7XG5cbiAgICAgICAgc2VsZi5wcmVsb2FkKFwiaW1hZ2VcIik7XG5cbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICAvLyBDbGVhbiB1cFxuICAgICAgJC5lYWNoKHNlbGYuc2xpZGVzLCBmdW5jdGlvbihpbmRleCwgc2xpZGUpIHtcbiAgICAgICAgJC5mYW5jeWJveC5zdG9wKHNsaWRlLiRzbGlkZSk7XG4gICAgICB9KTtcblxuICAgICAgLy8gTWFrZSBjdXJyZW50IHRoYXQgc2xpZGUgaXMgdmlzaWJsZSBldmVuIGlmIGNvbnRlbnQgaXMgc3RpbGwgbG9hZGluZ1xuICAgICAgY3VycmVudC4kc2xpZGUucmVtb3ZlQ2xhc3MoXCJmYW5jeWJveC1zbGlkZS0tbmV4dCBmYW5jeWJveC1zbGlkZS0tcHJldmlvdXNcIikuYWRkQ2xhc3MoXCJmYW5jeWJveC1zbGlkZS0tY3VycmVudFwiKTtcblxuICAgICAgLy8gSWYgc2xpZGVzIGhhdmUgYmVlbiBkcmFnZ2VkLCBhbmltYXRlIHRoZW0gdG8gY29ycmVjdCBwb3NpdGlvblxuICAgICAgaWYgKGN1cnJlbnQuaXNNb3ZlZCkge1xuICAgICAgICBjYW52YXNXaWR0aCA9IE1hdGgucm91bmQoY3VycmVudC4kc2xpZGUud2lkdGgoKSk7XG5cbiAgICAgICAgJC5lYWNoKHNlbGYuc2xpZGVzLCBmdW5jdGlvbihpbmRleCwgc2xpZGUpIHtcbiAgICAgICAgICB2YXIgcG9zID0gc2xpZGUucG9zIC0gY3VycmVudC5wb3M7XG5cbiAgICAgICAgICAkLmZhbmN5Ym94LmFuaW1hdGUoXG4gICAgICAgICAgICBzbGlkZS4kc2xpZGUsXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIHRvcDogMCxcbiAgICAgICAgICAgICAgbGVmdDogcG9zICogY2FudmFzV2lkdGggKyBwb3MgKiBzbGlkZS5vcHRzLmd1dHRlclxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGR1cmF0aW9uLFxuICAgICAgICAgICAgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgIHNsaWRlLiRzbGlkZS5yZW1vdmVBdHRyKFwic3R5bGVcIikucmVtb3ZlQ2xhc3MoXCJmYW5jeWJveC1zbGlkZS0tbmV4dCBmYW5jeWJveC1zbGlkZS0tcHJldmlvdXNcIik7XG5cbiAgICAgICAgICAgICAgaWYgKHNsaWRlLnBvcyA9PT0gc2VsZi5jdXJyUG9zKSB7XG4gICAgICAgICAgICAgICAgY3VycmVudC5pc01vdmVkID0gZmFsc2U7XG5cbiAgICAgICAgICAgICAgICBzZWxmLmNvbXBsZXRlKCk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICApO1xuICAgICAgICB9KTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHNlbGYuJHJlZnMuc3RhZ2UuY2hpbGRyZW4oKS5yZW1vdmVBdHRyKFwic3R5bGVcIik7XG4gICAgICB9XG5cbiAgICAgIC8vIFN0YXJ0IHRyYW5zaXRpb24gdGhhdCByZXZlYWxzIGN1cnJlbnQgY29udGVudFxuICAgICAgLy8gb3Igd2FpdCB3aGVuIGl0IHdpbGwgYmUgbG9hZGVkXG5cbiAgICAgIGlmIChjdXJyZW50LmlzTG9hZGVkKSB7XG4gICAgICAgIHNlbGYucmV2ZWFsQ29udGVudChjdXJyZW50KTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHNlbGYubG9hZFNsaWRlKGN1cnJlbnQpO1xuICAgICAgfVxuXG4gICAgICBzZWxmLnByZWxvYWQoXCJpbWFnZVwiKTtcblxuICAgICAgaWYgKHByZXZpb3VzLnBvcyA9PT0gY3VycmVudC5wb3MpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICAvLyBIYW5kbGUgcHJldmlvdXMgc2xpZGVcbiAgICAgIC8vID09PT09PT09PT09PT09PT09PT09PVxuXG4gICAgICB0cmFuc2l0aW9uUHJvcHMgPSBcImZhbmN5Ym94LXNsaWRlLS1cIiArIChwcmV2aW91cy5wb3MgPiBjdXJyZW50LnBvcyA/IFwibmV4dFwiIDogXCJwcmV2aW91c1wiKTtcblxuICAgICAgcHJldmlvdXMuJHNsaWRlLnJlbW92ZUNsYXNzKFwiZmFuY3lib3gtc2xpZGUtLWNvbXBsZXRlIGZhbmN5Ym94LXNsaWRlLS1jdXJyZW50IGZhbmN5Ym94LXNsaWRlLS1uZXh0IGZhbmN5Ym94LXNsaWRlLS1wcmV2aW91c1wiKTtcblxuICAgICAgcHJldmlvdXMuaXNDb21wbGV0ZSA9IGZhbHNlO1xuXG4gICAgICBpZiAoIWR1cmF0aW9uIHx8ICghY3VycmVudC5pc01vdmVkICYmICFjdXJyZW50Lm9wdHMudHJhbnNpdGlvbkVmZmVjdCkpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICBpZiAoY3VycmVudC5pc01vdmVkKSB7XG4gICAgICAgIHByZXZpb3VzLiRzbGlkZS5hZGRDbGFzcyh0cmFuc2l0aW9uUHJvcHMpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdHJhbnNpdGlvblByb3BzID0gXCJmYW5jeWJveC1hbmltYXRlZCBcIiArIHRyYW5zaXRpb25Qcm9wcyArIFwiIGZhbmN5Ym94LWZ4LVwiICsgY3VycmVudC5vcHRzLnRyYW5zaXRpb25FZmZlY3Q7XG5cbiAgICAgICAgJC5mYW5jeWJveC5hbmltYXRlKHByZXZpb3VzLiRzbGlkZSwgdHJhbnNpdGlvblByb3BzLCBkdXJhdGlvbiwgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgcHJldmlvdXMuJHNsaWRlLnJlbW92ZUNsYXNzKHRyYW5zaXRpb25Qcm9wcykucmVtb3ZlQXR0cihcInN0eWxlXCIpO1xuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICB9LFxuXG4gICAgLy8gQ3JlYXRlIG5ldyBcInNsaWRlXCIgZWxlbWVudFxuICAgIC8vIFRoZXNlIGFyZSBnYWxsZXJ5IGl0ZW1zICB0aGF0IGFyZSBhY3R1YWxseSBhZGRlZCB0byBET01cbiAgICAvLyA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG5cbiAgICBjcmVhdGVTbGlkZTogZnVuY3Rpb24ocG9zKSB7XG4gICAgICB2YXIgc2VsZiA9IHRoaXMsXG4gICAgICAgICRzbGlkZSxcbiAgICAgICAgaW5kZXg7XG5cbiAgICAgIGluZGV4ID0gcG9zICUgc2VsZi5ncm91cC5sZW5ndGg7XG4gICAgICBpbmRleCA9IGluZGV4IDwgMCA/IHNlbGYuZ3JvdXAubGVuZ3RoICsgaW5kZXggOiBpbmRleDtcblxuICAgICAgaWYgKCFzZWxmLnNsaWRlc1twb3NdICYmIHNlbGYuZ3JvdXBbaW5kZXhdKSB7XG4gICAgICAgICRzbGlkZSA9ICQoJzxkaXYgY2xhc3M9XCJmYW5jeWJveC1zbGlkZVwiPjwvZGl2PicpLmFwcGVuZFRvKHNlbGYuJHJlZnMuc3RhZ2UpO1xuXG4gICAgICAgIHNlbGYuc2xpZGVzW3Bvc10gPSAkLmV4dGVuZCh0cnVlLCB7fSwgc2VsZi5ncm91cFtpbmRleF0sIHtcbiAgICAgICAgICBwb3M6IHBvcyxcbiAgICAgICAgICAkc2xpZGU6ICRzbGlkZSxcbiAgICAgICAgICBpc0xvYWRlZDogZmFsc2VcbiAgICAgICAgfSk7XG5cbiAgICAgICAgc2VsZi51cGRhdGVTbGlkZShzZWxmLnNsaWRlc1twb3NdKTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHNlbGYuc2xpZGVzW3Bvc107XG4gICAgfSxcblxuICAgIC8vIFNjYWxlIGltYWdlIHRvIHRoZSBhY3R1YWwgc2l6ZSBvZiB0aGUgaW1hZ2U7XG4gICAgLy8geCBhbmQgeSB2YWx1ZXMgc2hvdWxkIGJlIHJlbGF0aXZlIHRvIHRoZSBzbGlkZVxuICAgIC8vID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cblxuICAgIHNjYWxlVG9BY3R1YWw6IGZ1bmN0aW9uKHgsIHksIGR1cmF0aW9uKSB7XG4gICAgICB2YXIgc2VsZiA9IHRoaXMsXG4gICAgICAgIGN1cnJlbnQgPSBzZWxmLmN1cnJlbnQsXG4gICAgICAgICRjb250ZW50ID0gY3VycmVudC4kY29udGVudCxcbiAgICAgICAgY2FudmFzV2lkdGggPSAkLmZhbmN5Ym94LmdldFRyYW5zbGF0ZShjdXJyZW50LiRzbGlkZSkud2lkdGgsXG4gICAgICAgIGNhbnZhc0hlaWdodCA9ICQuZmFuY3lib3guZ2V0VHJhbnNsYXRlKGN1cnJlbnQuJHNsaWRlKS5oZWlnaHQsXG4gICAgICAgIG5ld0ltZ1dpZHRoID0gY3VycmVudC53aWR0aCxcbiAgICAgICAgbmV3SW1nSGVpZ2h0ID0gY3VycmVudC5oZWlnaHQsXG4gICAgICAgIGltZ1BvcyxcbiAgICAgICAgcG9zWCxcbiAgICAgICAgcG9zWSxcbiAgICAgICAgc2NhbGVYLFxuICAgICAgICBzY2FsZVk7XG5cbiAgICAgIGlmIChzZWxmLmlzQW5pbWF0aW5nIHx8ICEkY29udGVudCB8fCAhKGN1cnJlbnQudHlwZSA9PSBcImltYWdlXCIgJiYgY3VycmVudC5pc0xvYWRlZCAmJiAhY3VycmVudC5oYXNFcnJvcikpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICAkLmZhbmN5Ym94LnN0b3AoJGNvbnRlbnQpO1xuXG4gICAgICBzZWxmLmlzQW5pbWF0aW5nID0gdHJ1ZTtcblxuICAgICAgeCA9IHggPT09IHVuZGVmaW5lZCA/IGNhbnZhc1dpZHRoICogMC41IDogeDtcbiAgICAgIHkgPSB5ID09PSB1bmRlZmluZWQgPyBjYW52YXNIZWlnaHQgKiAwLjUgOiB5O1xuXG4gICAgICBpbWdQb3MgPSAkLmZhbmN5Ym94LmdldFRyYW5zbGF0ZSgkY29udGVudCk7XG5cbiAgICAgIGltZ1Bvcy50b3AgLT0gJC5mYW5jeWJveC5nZXRUcmFuc2xhdGUoY3VycmVudC4kc2xpZGUpLnRvcDtcbiAgICAgIGltZ1Bvcy5sZWZ0IC09ICQuZmFuY3lib3guZ2V0VHJhbnNsYXRlKGN1cnJlbnQuJHNsaWRlKS5sZWZ0O1xuXG4gICAgICBzY2FsZVggPSBuZXdJbWdXaWR0aCAvIGltZ1Bvcy53aWR0aDtcbiAgICAgIHNjYWxlWSA9IG5ld0ltZ0hlaWdodCAvIGltZ1Bvcy5oZWlnaHQ7XG5cbiAgICAgIC8vIEdldCBjZW50ZXIgcG9zaXRpb24gZm9yIG9yaWdpbmFsIGltYWdlXG4gICAgICBwb3NYID0gY2FudmFzV2lkdGggKiAwLjUgLSBuZXdJbWdXaWR0aCAqIDAuNTtcbiAgICAgIHBvc1kgPSBjYW52YXNIZWlnaHQgKiAwLjUgLSBuZXdJbWdIZWlnaHQgKiAwLjU7XG5cbiAgICAgIC8vIE1ha2Ugc3VyZSBpbWFnZSBkb2VzIG5vdCBtb3ZlIGF3YXkgZnJvbSBlZGdlc1xuICAgICAgaWYgKG5ld0ltZ1dpZHRoID4gY2FudmFzV2lkdGgpIHtcbiAgICAgICAgcG9zWCA9IGltZ1Bvcy5sZWZ0ICogc2NhbGVYIC0gKHggKiBzY2FsZVggLSB4KTtcblxuICAgICAgICBpZiAocG9zWCA+IDApIHtcbiAgICAgICAgICBwb3NYID0gMDtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChwb3NYIDwgY2FudmFzV2lkdGggLSBuZXdJbWdXaWR0aCkge1xuICAgICAgICAgIHBvc1ggPSBjYW52YXNXaWR0aCAtIG5ld0ltZ1dpZHRoO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIGlmIChuZXdJbWdIZWlnaHQgPiBjYW52YXNIZWlnaHQpIHtcbiAgICAgICAgcG9zWSA9IGltZ1Bvcy50b3AgKiBzY2FsZVkgLSAoeSAqIHNjYWxlWSAtIHkpO1xuXG4gICAgICAgIGlmIChwb3NZID4gMCkge1xuICAgICAgICAgIHBvc1kgPSAwO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHBvc1kgPCBjYW52YXNIZWlnaHQgLSBuZXdJbWdIZWlnaHQpIHtcbiAgICAgICAgICBwb3NZID0gY2FudmFzSGVpZ2h0IC0gbmV3SW1nSGVpZ2h0O1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIHNlbGYudXBkYXRlQ3Vyc29yKG5ld0ltZ1dpZHRoLCBuZXdJbWdIZWlnaHQpO1xuXG4gICAgICAkLmZhbmN5Ym94LmFuaW1hdGUoXG4gICAgICAgICRjb250ZW50LFxuICAgICAgICB7XG4gICAgICAgICAgdG9wOiBwb3NZLFxuICAgICAgICAgIGxlZnQ6IHBvc1gsXG4gICAgICAgICAgc2NhbGVYOiBzY2FsZVgsXG4gICAgICAgICAgc2NhbGVZOiBzY2FsZVlcbiAgICAgICAgfSxcbiAgICAgICAgZHVyYXRpb24gfHwgMzMwLFxuICAgICAgICBmdW5jdGlvbigpIHtcbiAgICAgICAgICBzZWxmLmlzQW5pbWF0aW5nID0gZmFsc2U7XG4gICAgICAgIH1cbiAgICAgICk7XG5cbiAgICAgIC8vIFN0b3Agc2xpZGVzaG93XG4gICAgICBpZiAoc2VsZi5TbGlkZVNob3cgJiYgc2VsZi5TbGlkZVNob3cuaXNBY3RpdmUpIHtcbiAgICAgICAgc2VsZi5TbGlkZVNob3cuc3RvcCgpO1xuICAgICAgfVxuICAgIH0sXG5cbiAgICAvLyBTY2FsZSBpbWFnZSB0byBmaXQgaW5zaWRlIHBhcmVudCBlbGVtZW50XG4gICAgLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuXG4gICAgc2NhbGVUb0ZpdDogZnVuY3Rpb24oZHVyYXRpb24pIHtcbiAgICAgIHZhciBzZWxmID0gdGhpcyxcbiAgICAgICAgY3VycmVudCA9IHNlbGYuY3VycmVudCxcbiAgICAgICAgJGNvbnRlbnQgPSBjdXJyZW50LiRjb250ZW50LFxuICAgICAgICBlbmQ7XG5cbiAgICAgIGlmIChzZWxmLmlzQW5pbWF0aW5nIHx8ICEkY29udGVudCB8fCAhKGN1cnJlbnQudHlwZSA9PSBcImltYWdlXCIgJiYgY3VycmVudC5pc0xvYWRlZCAmJiAhY3VycmVudC5oYXNFcnJvcikpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICAkLmZhbmN5Ym94LnN0b3AoJGNvbnRlbnQpO1xuXG4gICAgICBzZWxmLmlzQW5pbWF0aW5nID0gdHJ1ZTtcblxuICAgICAgZW5kID0gc2VsZi5nZXRGaXRQb3MoY3VycmVudCk7XG5cbiAgICAgIHNlbGYudXBkYXRlQ3Vyc29yKGVuZC53aWR0aCwgZW5kLmhlaWdodCk7XG5cbiAgICAgICQuZmFuY3lib3guYW5pbWF0ZShcbiAgICAgICAgJGNvbnRlbnQsXG4gICAgICAgIHtcbiAgICAgICAgICB0b3A6IGVuZC50b3AsXG4gICAgICAgICAgbGVmdDogZW5kLmxlZnQsXG4gICAgICAgICAgc2NhbGVYOiBlbmQud2lkdGggLyAkY29udGVudC53aWR0aCgpLFxuICAgICAgICAgIHNjYWxlWTogZW5kLmhlaWdodCAvICRjb250ZW50LmhlaWdodCgpXG4gICAgICAgIH0sXG4gICAgICAgIGR1cmF0aW9uIHx8IDMzMCxcbiAgICAgICAgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgc2VsZi5pc0FuaW1hdGluZyA9IGZhbHNlO1xuICAgICAgICB9XG4gICAgICApO1xuICAgIH0sXG5cbiAgICAvLyBDYWxjdWxhdGUgaW1hZ2Ugc2l6ZSB0byBmaXQgaW5zaWRlIHZpZXdwb3J0XG4gICAgLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuXG4gICAgZ2V0Rml0UG9zOiBmdW5jdGlvbihzbGlkZSkge1xuICAgICAgdmFyIHNlbGYgPSB0aGlzLFxuICAgICAgICAkY29udGVudCA9IHNsaWRlLiRjb250ZW50LFxuICAgICAgICB3aWR0aCA9IHNsaWRlLndpZHRoIHx8IHNsaWRlLm9wdHMud2lkdGgsXG4gICAgICAgIGhlaWdodCA9IHNsaWRlLmhlaWdodCB8fCBzbGlkZS5vcHRzLmhlaWdodCxcbiAgICAgICAgbWF4V2lkdGgsXG4gICAgICAgIG1heEhlaWdodCxcbiAgICAgICAgbWluUmF0aW8sXG4gICAgICAgIG1hcmdpbixcbiAgICAgICAgYXNwZWN0UmF0aW8sXG4gICAgICAgIHJleiA9IHt9O1xuXG4gICAgICBpZiAoIXNsaWRlLmlzTG9hZGVkIHx8ICEkY29udGVudCB8fCAhJGNvbnRlbnQubGVuZ3RoKSB7XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgIH1cblxuICAgICAgbWFyZ2luID0ge1xuICAgICAgICB0b3A6IHBhcnNlSW50KHNsaWRlLiRzbGlkZS5jc3MoXCJwYWRkaW5nVG9wXCIpLCAxMCksXG4gICAgICAgIHJpZ2h0OiBwYXJzZUludChzbGlkZS4kc2xpZGUuY3NzKFwicGFkZGluZ1JpZ2h0XCIpLCAxMCksXG4gICAgICAgIGJvdHRvbTogcGFyc2VJbnQoc2xpZGUuJHNsaWRlLmNzcyhcInBhZGRpbmdCb3R0b21cIiksIDEwKSxcbiAgICAgICAgbGVmdDogcGFyc2VJbnQoc2xpZGUuJHNsaWRlLmNzcyhcInBhZGRpbmdMZWZ0XCIpLCAxMClcbiAgICAgIH07XG5cbiAgICAgIC8vIFdlIGNhbiBub3QgdXNlICRzbGlkZSB3aWR0aCBoZXJlLCBiZWNhdXNlIGl0IGNhbiBoYXZlIGRpZmZlcmVudCBkaWVtZW5zaW9ucyB3aGlsZSBpbiB0cmFuc2l0b25cbiAgICAgIG1heFdpZHRoID0gcGFyc2VJbnQoc2VsZi4kcmVmcy5zdGFnZS53aWR0aCgpLCAxMCkgLSAobWFyZ2luLmxlZnQgKyBtYXJnaW4ucmlnaHQpO1xuICAgICAgbWF4SGVpZ2h0ID0gcGFyc2VJbnQoc2VsZi4kcmVmcy5zdGFnZS5oZWlnaHQoKSwgMTApIC0gKG1hcmdpbi50b3AgKyBtYXJnaW4uYm90dG9tKTtcblxuICAgICAgaWYgKCF3aWR0aCB8fCAhaGVpZ2h0KSB7XG4gICAgICAgIHdpZHRoID0gbWF4V2lkdGg7XG4gICAgICAgIGhlaWdodCA9IG1heEhlaWdodDtcbiAgICAgIH1cblxuICAgICAgbWluUmF0aW8gPSBNYXRoLm1pbigxLCBtYXhXaWR0aCAvIHdpZHRoLCBtYXhIZWlnaHQgLyBoZWlnaHQpO1xuXG4gICAgICAvLyBVc2UgZmxvb3Igcm91bmRpbmcgdG8gbWFrZSBzdXJlIGl0IHJlYWxseSBmaXRzXG4gICAgICB3aWR0aCA9IE1hdGguZmxvb3IobWluUmF0aW8gKiB3aWR0aCk7XG4gICAgICBoZWlnaHQgPSBNYXRoLmZsb29yKG1pblJhdGlvICogaGVpZ2h0KTtcblxuICAgICAgaWYgKHNsaWRlLnR5cGUgPT09IFwiaW1hZ2VcIikge1xuICAgICAgICByZXoudG9wID0gTWF0aC5mbG9vcigobWF4SGVpZ2h0IC0gaGVpZ2h0KSAqIDAuNSkgKyBtYXJnaW4udG9wO1xuICAgICAgICByZXoubGVmdCA9IE1hdGguZmxvb3IoKG1heFdpZHRoIC0gd2lkdGgpICogMC41KSArIG1hcmdpbi5sZWZ0O1xuICAgICAgfSBlbHNlIGlmIChzbGlkZS5jb250ZW50VHlwZSA9PT0gXCJ2aWRlb1wiKSB7XG4gICAgICAgIC8vIEZvcmNlIGFzcGVjdCByYXRpbyBmb3IgdGhlIHZpZGVvXG4gICAgICAgIC8vIFwiSSBzYXkgdGhlIHdob2xlIHdvcmxkIG11c3QgbGVhcm4gb2Ygb3VyIHBlYWNlZnVsIHdheXPigKYgYnkgZm9yY2UhXCJcbiAgICAgICAgYXNwZWN0UmF0aW8gPSBzbGlkZS5vcHRzLndpZHRoICYmIHNsaWRlLm9wdHMuaGVpZ2h0ID8gd2lkdGggLyBoZWlnaHQgOiBzbGlkZS5vcHRzLnJhdGlvIHx8IDE2IC8gOTtcblxuICAgICAgICBpZiAoaGVpZ2h0ID4gd2lkdGggLyBhc3BlY3RSYXRpbykge1xuICAgICAgICAgIGhlaWdodCA9IHdpZHRoIC8gYXNwZWN0UmF0aW87XG4gICAgICAgIH0gZWxzZSBpZiAod2lkdGggPiBoZWlnaHQgKiBhc3BlY3RSYXRpbykge1xuICAgICAgICAgIHdpZHRoID0gaGVpZ2h0ICogYXNwZWN0UmF0aW87XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgcmV6LndpZHRoID0gd2lkdGg7XG4gICAgICByZXouaGVpZ2h0ID0gaGVpZ2h0O1xuXG4gICAgICByZXR1cm4gcmV6O1xuICAgIH0sXG5cbiAgICAvLyBVcGRhdGUgY29udGVudCBzaXplIGFuZCBwb3NpdGlvbiBmb3IgYWxsIHNsaWRlc1xuICAgIC8vID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cblxuICAgIHVwZGF0ZTogZnVuY3Rpb24oKSB7XG4gICAgICB2YXIgc2VsZiA9IHRoaXM7XG5cbiAgICAgICQuZWFjaChzZWxmLnNsaWRlcywgZnVuY3Rpb24oa2V5LCBzbGlkZSkge1xuICAgICAgICBzZWxmLnVwZGF0ZVNsaWRlKHNsaWRlKTtcbiAgICAgIH0pO1xuICAgIH0sXG5cbiAgICAvLyBVcGRhdGUgc2xpZGUgY29udGVudCBwb3NpdGlvbiBhbmQgc2l6ZVxuICAgIC8vID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG5cbiAgICB1cGRhdGVTbGlkZTogZnVuY3Rpb24oc2xpZGUsIGR1cmF0aW9uKSB7XG4gICAgICB2YXIgc2VsZiA9IHRoaXMsXG4gICAgICAgICRjb250ZW50ID0gc2xpZGUgJiYgc2xpZGUuJGNvbnRlbnQsXG4gICAgICAgIHdpZHRoID0gc2xpZGUud2lkdGggfHwgc2xpZGUub3B0cy53aWR0aCxcbiAgICAgICAgaGVpZ2h0ID0gc2xpZGUuaGVpZ2h0IHx8IHNsaWRlLm9wdHMuaGVpZ2h0O1xuXG4gICAgICBpZiAoJGNvbnRlbnQgJiYgKHdpZHRoIHx8IGhlaWdodCB8fCBzbGlkZS5jb250ZW50VHlwZSA9PT0gXCJ2aWRlb1wiKSAmJiAhc2xpZGUuaGFzRXJyb3IpIHtcbiAgICAgICAgJC5mYW5jeWJveC5zdG9wKCRjb250ZW50KTtcblxuICAgICAgICAkLmZhbmN5Ym94LnNldFRyYW5zbGF0ZSgkY29udGVudCwgc2VsZi5nZXRGaXRQb3Moc2xpZGUpKTtcblxuICAgICAgICBpZiAoc2xpZGUucG9zID09PSBzZWxmLmN1cnJQb3MpIHtcbiAgICAgICAgICBzZWxmLmlzQW5pbWF0aW5nID0gZmFsc2U7XG5cbiAgICAgICAgICBzZWxmLnVwZGF0ZUN1cnNvcigpO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIHNsaWRlLiRzbGlkZS50cmlnZ2VyKFwicmVmcmVzaFwiKTtcblxuICAgICAgc2VsZi4kcmVmcy50b29sYmFyLnRvZ2dsZUNsYXNzKFwiY29tcGVuc2F0ZS1mb3Itc2Nyb2xsYmFyXCIsIHNsaWRlLiRzbGlkZS5nZXQoMCkuc2Nyb2xsSGVpZ2h0ID4gc2xpZGUuJHNsaWRlLmdldCgwKS5jbGllbnRIZWlnaHQpO1xuXG4gICAgICBzZWxmLnRyaWdnZXIoXCJvblVwZGF0ZVwiLCBzbGlkZSk7XG4gICAgfSxcblxuICAgIC8vIEhvcml6b250YWxseSBjZW50ZXIgc2xpZGVcbiAgICAvLyA9PT09PT09PT09PT09PT09PT09PT09PT09XG5cbiAgICBjZW50ZXJTbGlkZTogZnVuY3Rpb24oc2xpZGUsIGR1cmF0aW9uKSB7XG4gICAgICB2YXIgc2VsZiA9IHRoaXMsXG4gICAgICAgIGNhbnZhc1dpZHRoLFxuICAgICAgICBwb3M7XG5cbiAgICAgIGlmIChzZWxmLmN1cnJlbnQpIHtcbiAgICAgICAgY2FudmFzV2lkdGggPSBNYXRoLnJvdW5kKHNsaWRlLiRzbGlkZS53aWR0aCgpKTtcbiAgICAgICAgcG9zID0gc2xpZGUucG9zIC0gc2VsZi5jdXJyZW50LnBvcztcblxuICAgICAgICAkLmZhbmN5Ym94LmFuaW1hdGUoXG4gICAgICAgICAgc2xpZGUuJHNsaWRlLFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIHRvcDogMCxcbiAgICAgICAgICAgIGxlZnQ6IHBvcyAqIGNhbnZhc1dpZHRoICsgcG9zICogc2xpZGUub3B0cy5ndXR0ZXIsXG4gICAgICAgICAgICBvcGFjaXR5OiAxXG4gICAgICAgICAgfSxcbiAgICAgICAgICBkdXJhdGlvbiA9PT0gdW5kZWZpbmVkID8gMCA6IGR1cmF0aW9uLFxuICAgICAgICAgIG51bGwsXG4gICAgICAgICAgZmFsc2VcbiAgICAgICAgKTtcbiAgICAgIH1cbiAgICB9LFxuXG4gICAgLy8gVXBkYXRlIGN1cnNvciBzdHlsZSBkZXBlbmRpbmcgaWYgY29udGVudCBjYW4gYmUgem9vbWVkXG4gICAgLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG5cbiAgICB1cGRhdGVDdXJzb3I6IGZ1bmN0aW9uKG5leHRXaWR0aCwgbmV4dEhlaWdodCkge1xuICAgICAgdmFyIHNlbGYgPSB0aGlzLFxuICAgICAgICBjdXJyZW50ID0gc2VsZi5jdXJyZW50LFxuICAgICAgICAkY29udGFpbmVyID0gc2VsZi4kcmVmcy5jb250YWluZXIucmVtb3ZlQ2xhc3MoXCJmYW5jeWJveC1pcy16b29tYWJsZSBmYW5jeWJveC1jYW4tem9vbUluIGZhbmN5Ym94LWNhbi1kcmFnIGZhbmN5Ym94LWNhbi16b29tT3V0XCIpLFxuICAgICAgICBpc1pvb21hYmxlO1xuXG4gICAgICBpZiAoIWN1cnJlbnQgfHwgc2VsZi5pc0Nsb3NpbmcpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICBpc1pvb21hYmxlID0gc2VsZi5pc1pvb21hYmxlKCk7XG5cbiAgICAgICRjb250YWluZXIudG9nZ2xlQ2xhc3MoXCJmYW5jeWJveC1pcy16b29tYWJsZVwiLCBpc1pvb21hYmxlKTtcblxuICAgICAgJChcIltkYXRhLWZhbmN5Ym94LXpvb21dXCIpLnByb3AoXCJkaXNhYmxlZFwiLCAhaXNab29tYWJsZSk7XG5cbiAgICAgIC8vIFNldCBjdXJzb3IgdG8gem9vbSBpbi9vdXQgaWYgY2xpY2sgZXZlbnQgaXMgJ3pvb20nXG4gICAgICBpZiAoXG4gICAgICAgIGlzWm9vbWFibGUgJiZcbiAgICAgICAgKGN1cnJlbnQub3B0cy5jbGlja0NvbnRlbnQgPT09IFwiem9vbVwiIHx8ICgkLmlzRnVuY3Rpb24oY3VycmVudC5vcHRzLmNsaWNrQ29udGVudCkgJiYgY3VycmVudC5vcHRzLmNsaWNrQ29udGVudChjdXJyZW50KSA9PT0gXCJ6b29tXCIpKVxuICAgICAgKSB7XG4gICAgICAgIGlmIChzZWxmLmlzU2NhbGVkRG93bihuZXh0V2lkdGgsIG5leHRIZWlnaHQpKSB7XG4gICAgICAgICAgLy8gSWYgaW1hZ2UgaXMgc2NhbGVkIGRvd24sIHRoZW4sIG9idmlvdXNseSwgaXQgY2FuIGJlIHpvb21lZCB0byBmdWxsIHNpemVcbiAgICAgICAgICAkY29udGFpbmVyLmFkZENsYXNzKFwiZmFuY3lib3gtY2FuLXpvb21JblwiKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBpZiAoY3VycmVudC5vcHRzLnRvdWNoKSB7XG4gICAgICAgICAgICAvLyBJZiBpbWFnZSBzaXplIGlyIGxhcmdlbiB0aGFuIGF2YWlsYWJsZSBhdmFpbGFibGUgYW5kIHRvdWNoIG1vZHVsZSBpcyBub3QgZGlzYWJsZSxcbiAgICAgICAgICAgIC8vIHRoZW4gdXNlciBjYW4gZG8gcGFubmluZ1xuICAgICAgICAgICAgJGNvbnRhaW5lci5hZGRDbGFzcyhcImZhbmN5Ym94LWNhbi1kcmFnXCIpO1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAkY29udGFpbmVyLmFkZENsYXNzKFwiZmFuY3lib3gtY2FuLXpvb21PdXRcIik7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9IGVsc2UgaWYgKGN1cnJlbnQub3B0cy50b3VjaCAmJiBjdXJyZW50LmNvbnRlbnRUeXBlICE9PSBcInZpZGVvXCIpIHtcbiAgICAgICAgJGNvbnRhaW5lci5hZGRDbGFzcyhcImZhbmN5Ym94LWNhbi1kcmFnXCIpO1xuICAgICAgfVxuICAgIH0sXG5cbiAgICAvLyBDaGVjayBpZiBjdXJyZW50IHNsaWRlIGlzIHpvb21hYmxlXG4gICAgLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuXG4gICAgaXNab29tYWJsZTogZnVuY3Rpb24oKSB7XG4gICAgICB2YXIgc2VsZiA9IHRoaXMsXG4gICAgICAgIGN1cnJlbnQgPSBzZWxmLmN1cnJlbnQsXG4gICAgICAgIGZpdFBvcztcblxuICAgICAgLy8gQXNzdW1lIHRoYXQgc2xpZGUgaXMgem9vbWFibGUgaWY6XG4gICAgICAvLyAgIC0gaW1hZ2UgaXMgc3RpbGwgbG9hZGluZ1xuICAgICAgLy8gICAtIGFjdHVhbCBzaXplIG9mIHRoZSBpbWFnZSBpcyBzbWFsbGVyIHRoYW4gYXZhaWxhYmxlIGFyZWFcbiAgICAgIGlmIChjdXJyZW50ICYmICFzZWxmLmlzQ2xvc2luZyAmJiBjdXJyZW50LnR5cGUgPT09IFwiaW1hZ2VcIiAmJiAhY3VycmVudC5oYXNFcnJvcikge1xuICAgICAgICBpZiAoIWN1cnJlbnQuaXNMb2FkZWQpIHtcbiAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgfVxuXG4gICAgICAgIGZpdFBvcyA9IHNlbGYuZ2V0Rml0UG9zKGN1cnJlbnQpO1xuXG4gICAgICAgIGlmIChjdXJyZW50LndpZHRoID4gZml0UG9zLndpZHRoIHx8IGN1cnJlbnQuaGVpZ2h0ID4gZml0UG9zLmhlaWdodCkge1xuICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9LFxuXG4gICAgLy8gQ2hlY2sgaWYgY3VycmVudCBpbWFnZSBkaW1lbnNpb25zIGFyZSBzbWFsbGVyIHRoYW4gYWN0dWFsXG4gICAgLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG5cbiAgICBpc1NjYWxlZERvd246IGZ1bmN0aW9uKG5leHRXaWR0aCwgbmV4dEhlaWdodCkge1xuICAgICAgdmFyIHNlbGYgPSB0aGlzLFxuICAgICAgICByZXogPSBmYWxzZSxcbiAgICAgICAgY3VycmVudCA9IHNlbGYuY3VycmVudCxcbiAgICAgICAgJGNvbnRlbnQgPSBjdXJyZW50LiRjb250ZW50O1xuXG4gICAgICBpZiAobmV4dFdpZHRoICE9PSB1bmRlZmluZWQgJiYgbmV4dEhlaWdodCAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgIHJleiA9IG5leHRXaWR0aCA8IGN1cnJlbnQud2lkdGggJiYgbmV4dEhlaWdodCA8IGN1cnJlbnQuaGVpZ2h0O1xuICAgICAgfSBlbHNlIGlmICgkY29udGVudCkge1xuICAgICAgICByZXogPSAkLmZhbmN5Ym94LmdldFRyYW5zbGF0ZSgkY29udGVudCk7XG4gICAgICAgIHJleiA9IHJlei53aWR0aCA8IGN1cnJlbnQud2lkdGggJiYgcmV6LmhlaWdodCA8IGN1cnJlbnQuaGVpZ2h0O1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gcmV6O1xuICAgIH0sXG5cbiAgICAvLyBDaGVjayBpZiBpbWFnZSBkaW1lbnNpb25zIGV4Y2VlZCBwYXJlbnQgZWxlbWVudFxuICAgIC8vID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG5cbiAgICBjYW5QYW46IGZ1bmN0aW9uKCkge1xuICAgICAgdmFyIHNlbGYgPSB0aGlzLFxuICAgICAgICByZXogPSBmYWxzZSxcbiAgICAgICAgY3VycmVudCA9IHNlbGYuY3VycmVudCxcbiAgICAgICAgJGNvbnRlbnQ7XG5cbiAgICAgIGlmIChjdXJyZW50LnR5cGUgPT09IFwiaW1hZ2VcIiAmJiAoJGNvbnRlbnQgPSBjdXJyZW50LiRjb250ZW50KSAmJiAhY3VycmVudC5oYXNFcnJvcikge1xuICAgICAgICByZXogPSBzZWxmLmdldEZpdFBvcyhjdXJyZW50KTtcbiAgICAgICAgcmV6ID0gTWF0aC5hYnMoJGNvbnRlbnQud2lkdGgoKSAtIHJlei53aWR0aCkgPiAxIHx8IE1hdGguYWJzKCRjb250ZW50LmhlaWdodCgpIC0gcmV6LmhlaWdodCkgPiAxO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gcmV6O1xuICAgIH0sXG5cbiAgICAvLyBMb2FkIGNvbnRlbnQgaW50byB0aGUgc2xpZGVcbiAgICAvLyA9PT09PT09PT09PT09PT09PT09PT09PT09PT1cblxuICAgIGxvYWRTbGlkZTogZnVuY3Rpb24oc2xpZGUpIHtcbiAgICAgIHZhciBzZWxmID0gdGhpcyxcbiAgICAgICAgdHlwZSxcbiAgICAgICAgJHNsaWRlLFxuICAgICAgICBhamF4TG9hZDtcblxuICAgICAgaWYgKHNsaWRlLmlzTG9hZGluZyB8fCBzbGlkZS5pc0xvYWRlZCkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIHNsaWRlLmlzTG9hZGluZyA9IHRydWU7XG5cbiAgICAgIHNlbGYudHJpZ2dlcihcImJlZm9yZUxvYWRcIiwgc2xpZGUpO1xuXG4gICAgICB0eXBlID0gc2xpZGUudHlwZTtcbiAgICAgICRzbGlkZSA9IHNsaWRlLiRzbGlkZTtcblxuICAgICAgJHNsaWRlXG4gICAgICAgIC5vZmYoXCJyZWZyZXNoXCIpXG4gICAgICAgIC50cmlnZ2VyKFwib25SZXNldFwiKVxuICAgICAgICAuYWRkQ2xhc3Moc2xpZGUub3B0cy5zbGlkZUNsYXNzKTtcblxuICAgICAgLy8gQ3JlYXRlIGNvbnRlbnQgZGVwZW5kaW5nIG9uIHRoZSB0eXBlXG4gICAgICBzd2l0Y2ggKHR5cGUpIHtcbiAgICAgICAgY2FzZSBcImltYWdlXCI6XG4gICAgICAgICAgc2VsZi5zZXRJbWFnZShzbGlkZSk7XG5cbiAgICAgICAgICBicmVhaztcblxuICAgICAgICBjYXNlIFwiaWZyYW1lXCI6XG4gICAgICAgICAgc2VsZi5zZXRJZnJhbWUoc2xpZGUpO1xuXG4gICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgY2FzZSBcImh0bWxcIjpcbiAgICAgICAgICBzZWxmLnNldENvbnRlbnQoc2xpZGUsIHNsaWRlLnNyYyB8fCBzbGlkZS5jb250ZW50KTtcblxuICAgICAgICAgIGJyZWFrO1xuXG4gICAgICAgIGNhc2UgXCJ2aWRlb1wiOlxuICAgICAgICAgIHNlbGYuc2V0Q29udGVudChcbiAgICAgICAgICAgIHNsaWRlLFxuICAgICAgICAgICAgJzx2aWRlbyBjbGFzcz1cImZhbmN5Ym94LXZpZGVvXCIgY29udHJvbHMgY29udHJvbHNMaXN0PVwibm9kb3dubG9hZFwiPicgK1xuICAgICAgICAgICAgICAnPHNvdXJjZSBzcmM9XCInICtcbiAgICAgICAgICAgICAgc2xpZGUuc3JjICtcbiAgICAgICAgICAgICAgJ1wiIHR5cGU9XCInICtcbiAgICAgICAgICAgICAgc2xpZGUub3B0cy52aWRlb0Zvcm1hdCArXG4gICAgICAgICAgICAgICdcIj4nICtcbiAgICAgICAgICAgICAgXCJZb3VyIGJyb3dzZXIgZG9lc24ndCBzdXBwb3J0IEhUTUw1IHZpZGVvXCIgK1xuICAgICAgICAgICAgICBcIjwvdmlkZW9cIlxuICAgICAgICAgICk7XG5cbiAgICAgICAgICBicmVhaztcblxuICAgICAgICBjYXNlIFwiaW5saW5lXCI6XG4gICAgICAgICAgaWYgKCQoc2xpZGUuc3JjKS5sZW5ndGgpIHtcbiAgICAgICAgICAgIHNlbGYuc2V0Q29udGVudChzbGlkZSwgJChzbGlkZS5zcmMpKTtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgc2VsZi5zZXRFcnJvcihzbGlkZSk7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgY2FzZSBcImFqYXhcIjpcbiAgICAgICAgICBzZWxmLnNob3dMb2FkaW5nKHNsaWRlKTtcblxuICAgICAgICAgIGFqYXhMb2FkID0gJC5hamF4KFxuICAgICAgICAgICAgJC5leHRlbmQoe30sIHNsaWRlLm9wdHMuYWpheC5zZXR0aW5ncywge1xuICAgICAgICAgICAgICB1cmw6IHNsaWRlLnNyYyxcbiAgICAgICAgICAgICAgc3VjY2VzczogZnVuY3Rpb24oZGF0YSwgdGV4dFN0YXR1cykge1xuICAgICAgICAgICAgICAgIGlmICh0ZXh0U3RhdHVzID09PSBcInN1Y2Nlc3NcIikge1xuICAgICAgICAgICAgICAgICAgc2VsZi5zZXRDb250ZW50KHNsaWRlLCBkYXRhKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIGVycm9yOiBmdW5jdGlvbihqcVhIUiwgdGV4dFN0YXR1cykge1xuICAgICAgICAgICAgICAgIGlmIChqcVhIUiAmJiB0ZXh0U3RhdHVzICE9PSBcImFib3J0XCIpIHtcbiAgICAgICAgICAgICAgICAgIHNlbGYuc2V0RXJyb3Ioc2xpZGUpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSlcbiAgICAgICAgICApO1xuXG4gICAgICAgICAgJHNsaWRlLm9uZShcIm9uUmVzZXRcIiwgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBhamF4TG9hZC5hYm9ydCgpO1xuICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICBzZWxmLnNldEVycm9yKHNsaWRlKTtcblxuICAgICAgICAgIGJyZWFrO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gdHJ1ZTtcbiAgICB9LFxuXG4gICAgLy8gVXNlIHRodW1ibmFpbCBpbWFnZSwgaWYgcG9zc2libGVcbiAgICAvLyA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuXG4gICAgc2V0SW1hZ2U6IGZ1bmN0aW9uKHNsaWRlKSB7XG4gICAgICB2YXIgc2VsZiA9IHRoaXMsXG4gICAgICAgIHNyY3NldCA9IHNsaWRlLm9wdHMuc3Jjc2V0IHx8IHNsaWRlLm9wdHMuaW1hZ2Uuc3Jjc2V0LFxuICAgICAgICB0aHVtYlNyYyxcbiAgICAgICAgZm91bmQsXG4gICAgICAgIHRlbXAsXG4gICAgICAgIHB4UmF0aW8sXG4gICAgICAgIHdpbmRvd1dpZHRoO1xuXG4gICAgICAvLyBDaGVjayBpZiBuZWVkIHRvIHNob3cgbG9hZGluZyBpY29uXG4gICAgICBzbGlkZS50aW1vdXRzID0gc2V0VGltZW91dChmdW5jdGlvbigpIHtcbiAgICAgICAgdmFyICRpbWcgPSBzbGlkZS4kaW1hZ2U7XG5cbiAgICAgICAgaWYgKHNsaWRlLmlzTG9hZGluZyAmJiAoISRpbWcgfHwgISRpbWdbMF0uY29tcGxldGUpICYmICFzbGlkZS5oYXNFcnJvcikge1xuICAgICAgICAgIHNlbGYuc2hvd0xvYWRpbmcoc2xpZGUpO1xuICAgICAgICB9XG4gICAgICB9LCAzNTApO1xuXG4gICAgICAvLyBJZiB3ZSBoYXZlIFwic3Jjc2V0XCIsIHRoZW4gd2UgbmVlZCB0byBmaW5kIGZpcnN0IG1hdGNoaW5nIFwic3JjXCIgdmFsdWUuXG4gICAgICAvLyBUaGlzIGlzIG5lY2Vzc2FyeSwgYmVjYXVzZSB3aGVuIHlvdSBzZXQgYW4gc3JjIGF0dHJpYnV0ZSwgdGhlIGJyb3dzZXIgd2lsbCBwcmVsb2FkIHRoZSBpbWFnZVxuICAgICAgLy8gYmVmb3JlIGFueSBqYXZhc2NyaXB0IG9yIGV2ZW4gQ1NTIGlzIGFwcGxpZWQuXG4gICAgICBpZiAoc3Jjc2V0KSB7XG4gICAgICAgIHB4UmF0aW8gPSB3aW5kb3cuZGV2aWNlUGl4ZWxSYXRpbyB8fCAxO1xuICAgICAgICB3aW5kb3dXaWR0aCA9IHdpbmRvdy5pbm5lcldpZHRoICogcHhSYXRpbztcblxuICAgICAgICB0ZW1wID0gc3Jjc2V0LnNwbGl0KFwiLFwiKS5tYXAoZnVuY3Rpb24oZWwpIHtcbiAgICAgICAgICB2YXIgcmV0ID0ge307XG5cbiAgICAgICAgICBlbFxuICAgICAgICAgICAgLnRyaW0oKVxuICAgICAgICAgICAgLnNwbGl0KC9cXHMrLylcbiAgICAgICAgICAgIC5mb3JFYWNoKGZ1bmN0aW9uKGVsLCBpKSB7XG4gICAgICAgICAgICAgIHZhciB2YWx1ZSA9IHBhcnNlSW50KGVsLnN1YnN0cmluZygwLCBlbC5sZW5ndGggLSAxKSwgMTApO1xuXG4gICAgICAgICAgICAgIGlmIChpID09PSAwKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIChyZXQudXJsID0gZWwpO1xuICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgaWYgKHZhbHVlKSB7XG4gICAgICAgICAgICAgICAgcmV0LnZhbHVlID0gdmFsdWU7XG4gICAgICAgICAgICAgICAgcmV0LnBvc3RmaXggPSBlbFtlbC5sZW5ndGggLSAxXTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICByZXR1cm4gcmV0O1xuICAgICAgICB9KTtcblxuICAgICAgICAvLyBTb3J0IGJ5IHZhbHVlXG4gICAgICAgIHRlbXAuc29ydChmdW5jdGlvbihhLCBiKSB7XG4gICAgICAgICAgcmV0dXJuIGEudmFsdWUgLSBiLnZhbHVlO1xuICAgICAgICB9KTtcblxuICAgICAgICAvLyBPaywgbm93IHdlIGhhdmUgYW4gYXJyYXkgb2YgYWxsIHNyY3NldCB2YWx1ZXNcbiAgICAgICAgZm9yICh2YXIgaiA9IDA7IGogPCB0ZW1wLmxlbmd0aDsgaisrKSB7XG4gICAgICAgICAgdmFyIGVsID0gdGVtcFtqXTtcblxuICAgICAgICAgIGlmICgoZWwucG9zdGZpeCA9PT0gXCJ3XCIgJiYgZWwudmFsdWUgPj0gd2luZG93V2lkdGgpIHx8IChlbC5wb3N0Zml4ID09PSBcInhcIiAmJiBlbC52YWx1ZSA+PSBweFJhdGlvKSkge1xuICAgICAgICAgICAgZm91bmQgPSBlbDtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIC8vIElmIG5vdCBmb3VuZCwgdGFrZSB0aGUgbGFzdCBvbmVcbiAgICAgICAgaWYgKCFmb3VuZCAmJiB0ZW1wLmxlbmd0aCkge1xuICAgICAgICAgIGZvdW5kID0gdGVtcFt0ZW1wLmxlbmd0aCAtIDFdO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKGZvdW5kKSB7XG4gICAgICAgICAgc2xpZGUuc3JjID0gZm91bmQudXJsO1xuXG4gICAgICAgICAgLy8gSWYgd2UgaGF2ZSBkZWZhdWx0IHdpZHRoL2hlaWdodCB2YWx1ZXMsIHdlIGNhbiBjYWxjdWxhdGUgaGVpZ2h0IGZvciBtYXRjaGluZyBzb3VyY2VcbiAgICAgICAgICBpZiAoc2xpZGUud2lkdGggJiYgc2xpZGUuaGVpZ2h0ICYmIGZvdW5kLnBvc3RmaXggPT0gXCJ3XCIpIHtcbiAgICAgICAgICAgIHNsaWRlLmhlaWdodCA9IHNsaWRlLndpZHRoIC8gc2xpZGUuaGVpZ2h0ICogZm91bmQudmFsdWU7XG4gICAgICAgICAgICBzbGlkZS53aWR0aCA9IGZvdW5kLnZhbHVlO1xuICAgICAgICAgIH1cblxuICAgICAgICAgIHNsaWRlLm9wdHMuc3Jjc2V0ID0gc3Jjc2V0O1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIC8vIFRoaXMgd2lsbCBiZSB3cmFwcGVyIGNvbnRhaW5pbmcgYm90aCBnaG9zdCBhbmQgYWN0dWFsIGltYWdlXG4gICAgICBzbGlkZS4kY29udGVudCA9ICQoJzxkaXYgY2xhc3M9XCJmYW5jeWJveC1jb250ZW50XCI+PC9kaXY+JylcbiAgICAgICAgLmFkZENsYXNzKFwiZmFuY3lib3gtaXMtaGlkZGVuXCIpXG4gICAgICAgIC5hcHBlbmRUbyhzbGlkZS4kc2xpZGUuYWRkQ2xhc3MoXCJmYW5jeWJveC1zbGlkZS0taW1hZ2VcIikpO1xuXG4gICAgICAvLyBJZiB3ZSBoYXZlIGEgdGh1bWJuYWlsLCB3ZSBjYW4gZGlzcGxheSBpdCB3aGlsZSBhY3R1YWwgaW1hZ2UgaXMgbG9hZGluZ1xuICAgICAgLy8gVXNlcnMgd2lsbCBub3Qgc3RhcmUgYXQgYmxhY2sgc2NyZWVuIGFuZCBhY3R1YWwgaW1hZ2Ugd2lsbCBhcHBlYXIgZ3JhZHVhbGx5XG4gICAgICB0aHVtYlNyYyA9IHNsaWRlLm9wdHMudGh1bWIgfHwgKHNsaWRlLm9wdHMuJHRodW1iICYmIHNsaWRlLm9wdHMuJHRodW1iLmxlbmd0aCA/IHNsaWRlLm9wdHMuJHRodW1iLmF0dHIoXCJzcmNcIikgOiBmYWxzZSk7XG5cbiAgICAgIGlmIChzbGlkZS5vcHRzLnByZWxvYWQgIT09IGZhbHNlICYmIHNsaWRlLm9wdHMud2lkdGggJiYgc2xpZGUub3B0cy5oZWlnaHQgJiYgdGh1bWJTcmMpIHtcbiAgICAgICAgc2xpZGUud2lkdGggPSBzbGlkZS5vcHRzLndpZHRoO1xuICAgICAgICBzbGlkZS5oZWlnaHQgPSBzbGlkZS5vcHRzLmhlaWdodDtcblxuICAgICAgICBzbGlkZS4kZ2hvc3QgPSAkKFwiPGltZyAvPlwiKVxuICAgICAgICAgIC5vbmUoXCJlcnJvclwiLCBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICQodGhpcykucmVtb3ZlKCk7XG5cbiAgICAgICAgICAgIHNsaWRlLiRnaG9zdCA9IG51bGw7XG4gICAgICAgICAgfSlcbiAgICAgICAgICAub25lKFwibG9hZFwiLCBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHNlbGYuYWZ0ZXJMb2FkKHNsaWRlKTtcbiAgICAgICAgICB9KVxuICAgICAgICAgIC5hZGRDbGFzcyhcImZhbmN5Ym94LWltYWdlXCIpXG4gICAgICAgICAgLmFwcGVuZFRvKHNsaWRlLiRjb250ZW50KVxuICAgICAgICAgIC5hdHRyKFwic3JjXCIsIHRodW1iU3JjKTtcbiAgICAgIH1cblxuICAgICAgLy8gU3RhcnQgbG9hZGluZyBhY3R1YWwgaW1hZ2VcbiAgICAgIHNlbGYuc2V0QmlnSW1hZ2Uoc2xpZGUpO1xuICAgIH0sXG5cbiAgICAvLyBDcmVhdGUgZnVsbC1zaXplIGltYWdlXG4gICAgLy8gPT09PT09PT09PT09PT09PT09PT09PVxuXG4gICAgc2V0QmlnSW1hZ2U6IGZ1bmN0aW9uKHNsaWRlKSB7XG4gICAgICB2YXIgc2VsZiA9IHRoaXMsXG4gICAgICAgICRpbWcgPSAkKFwiPGltZyAvPlwiKTtcblxuICAgICAgc2xpZGUuJGltYWdlID0gJGltZ1xuICAgICAgICAub25lKFwiZXJyb3JcIiwgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgc2VsZi5zZXRFcnJvcihzbGlkZSk7XG4gICAgICAgIH0pXG4gICAgICAgIC5vbmUoXCJsb2FkXCIsIGZ1bmN0aW9uKCkge1xuICAgICAgICAgIHZhciBzaXplcztcblxuICAgICAgICAgIGlmICghc2xpZGUuJGdob3N0KSB7XG4gICAgICAgICAgICBzZWxmLnJlc29sdmVJbWFnZVNsaWRlU2l6ZShzbGlkZSwgdGhpcy5uYXR1cmFsV2lkdGgsIHRoaXMubmF0dXJhbEhlaWdodCk7XG5cbiAgICAgICAgICAgIHNlbGYuYWZ0ZXJMb2FkKHNsaWRlKTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICAvLyBDbGVhciB0aW1lb3V0IHRoYXQgY2hlY2tzIGlmIGxvYWRpbmcgaWNvbiBuZWVkcyB0byBiZSBkaXNwbGF5ZWRcbiAgICAgICAgICBpZiAoc2xpZGUudGltb3V0cykge1xuICAgICAgICAgICAgY2xlYXJUaW1lb3V0KHNsaWRlLnRpbW91dHMpO1xuICAgICAgICAgICAgc2xpZGUudGltb3V0cyA9IG51bGw7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgaWYgKHNlbGYuaXNDbG9zaW5nKSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgaWYgKHNsaWRlLm9wdHMuc3Jjc2V0KSB7XG4gICAgICAgICAgICBzaXplcyA9IHNsaWRlLm9wdHMuc2l6ZXM7XG5cbiAgICAgICAgICAgIGlmICghc2l6ZXMgfHwgc2l6ZXMgPT09IFwiYXV0b1wiKSB7XG4gICAgICAgICAgICAgIHNpemVzID1cbiAgICAgICAgICAgICAgICAoc2xpZGUud2lkdGggLyBzbGlkZS5oZWlnaHQgPiAxICYmICRXLndpZHRoKCkgLyAkVy5oZWlnaHQoKSA+IDEgPyBcIjEwMFwiIDogTWF0aC5yb3VuZChzbGlkZS53aWR0aCAvIHNsaWRlLmhlaWdodCAqIDEwMCkpICtcbiAgICAgICAgICAgICAgICBcInZ3XCI7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICRpbWcuYXR0cihcInNpemVzXCIsIHNpemVzKS5hdHRyKFwic3Jjc2V0XCIsIHNsaWRlLm9wdHMuc3Jjc2V0KTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICAvLyBIaWRlIHRlbXBvcmFyeSBpbWFnZSBhZnRlciBzb21lIGRlbGF5XG4gICAgICAgICAgaWYgKHNsaWRlLiRnaG9zdCkge1xuICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgaWYgKHNsaWRlLiRnaG9zdCAmJiAhc2VsZi5pc0Nsb3NpbmcpIHtcbiAgICAgICAgICAgICAgICBzbGlkZS4kZ2hvc3QuaGlkZSgpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LCBNYXRoLm1pbigzMDAsIE1hdGgubWF4KDEwMDAsIHNsaWRlLmhlaWdodCAvIDE2MDApKSk7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgc2VsZi5oaWRlTG9hZGluZyhzbGlkZSk7XG4gICAgICAgIH0pXG4gICAgICAgIC5hZGRDbGFzcyhcImZhbmN5Ym94LWltYWdlXCIpXG4gICAgICAgIC5hdHRyKFwic3JjXCIsIHNsaWRlLnNyYylcbiAgICAgICAgLmFwcGVuZFRvKHNsaWRlLiRjb250ZW50KTtcblxuICAgICAgaWYgKCgkaW1nWzBdLmNvbXBsZXRlIHx8ICRpbWdbMF0ucmVhZHlTdGF0ZSA9PSBcImNvbXBsZXRlXCIpICYmICRpbWdbMF0ubmF0dXJhbFdpZHRoICYmICRpbWdbMF0ubmF0dXJhbEhlaWdodCkge1xuICAgICAgICAkaW1nLnRyaWdnZXIoXCJsb2FkXCIpO1xuICAgICAgfSBlbHNlIGlmICgkaW1nWzBdLmVycm9yKSB7XG4gICAgICAgICRpbWcudHJpZ2dlcihcImVycm9yXCIpO1xuICAgICAgfVxuICAgIH0sXG5cbiAgICAvLyBDb21wdXRlcyB0aGUgc2xpZGUgc2l6ZSBmcm9tIGltYWdlIHNpemUgYW5kIG1heFdpZHRoL21heEhlaWdodFxuICAgIC8vID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG5cbiAgICByZXNvbHZlSW1hZ2VTbGlkZVNpemU6IGZ1bmN0aW9uKHNsaWRlLCBpbWdXaWR0aCwgaW1nSGVpZ2h0KSB7XG4gICAgICB2YXIgbWF4V2lkdGggPSBwYXJzZUludChzbGlkZS5vcHRzLndpZHRoLCAxMCksXG4gICAgICAgIG1heEhlaWdodCA9IHBhcnNlSW50KHNsaWRlLm9wdHMuaGVpZ2h0LCAxMCk7XG5cbiAgICAgIC8vIFNldHMgdGhlIGRlZmF1bHQgdmFsdWVzIGZyb20gdGhlIGltYWdlXG4gICAgICBzbGlkZS53aWR0aCA9IGltZ1dpZHRoO1xuICAgICAgc2xpZGUuaGVpZ2h0ID0gaW1nSGVpZ2h0O1xuXG4gICAgICBpZiAobWF4V2lkdGggPiAwKSB7XG4gICAgICAgIHNsaWRlLndpZHRoID0gbWF4V2lkdGg7XG4gICAgICAgIHNsaWRlLmhlaWdodCA9IE1hdGguZmxvb3IobWF4V2lkdGggKiBpbWdIZWlnaHQgLyBpbWdXaWR0aCk7XG4gICAgICB9XG5cbiAgICAgIGlmIChtYXhIZWlnaHQgPiAwKSB7XG4gICAgICAgIHNsaWRlLndpZHRoID0gTWF0aC5mbG9vcihtYXhIZWlnaHQgKiBpbWdXaWR0aCAvIGltZ0hlaWdodCk7XG4gICAgICAgIHNsaWRlLmhlaWdodCA9IG1heEhlaWdodDtcbiAgICAgIH1cbiAgICB9LFxuXG4gICAgLy8gQ3JlYXRlIGlmcmFtZSB3cmFwcGVyLCBpZnJhbWUgYW5kIGJpbmRpbmdzXG4gICAgLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG5cbiAgICBzZXRJZnJhbWU6IGZ1bmN0aW9uKHNsaWRlKSB7XG4gICAgICB2YXIgc2VsZiA9IHRoaXMsXG4gICAgICAgIG9wdHMgPSBzbGlkZS5vcHRzLmlmcmFtZSxcbiAgICAgICAgJHNsaWRlID0gc2xpZGUuJHNsaWRlLFxuICAgICAgICAkaWZyYW1lO1xuXG4gICAgICBzbGlkZS4kY29udGVudCA9ICQoJzxkaXYgY2xhc3M9XCJmYW5jeWJveC1jb250ZW50JyArIChvcHRzLnByZWxvYWQgPyBcIiBmYW5jeWJveC1pcy1oaWRkZW5cIiA6IFwiXCIpICsgJ1wiPjwvZGl2PicpXG4gICAgICAgIC5jc3Mob3B0cy5jc3MpXG4gICAgICAgIC5hcHBlbmRUbygkc2xpZGUpO1xuXG4gICAgICAkc2xpZGUuYWRkQ2xhc3MoXCJmYW5jeWJveC1zbGlkZS0tXCIgKyBzbGlkZS5jb250ZW50VHlwZSk7XG5cbiAgICAgIHNsaWRlLiRpZnJhbWUgPSAkaWZyYW1lID0gJChvcHRzLnRwbC5yZXBsYWNlKC9cXHtybmRcXH0vZywgbmV3IERhdGUoKS5nZXRUaW1lKCkpKVxuICAgICAgICAuYXR0cihvcHRzLmF0dHIpXG4gICAgICAgIC5hcHBlbmRUbyhzbGlkZS4kY29udGVudCk7XG5cbiAgICAgIGlmIChvcHRzLnByZWxvYWQpIHtcbiAgICAgICAgc2VsZi5zaG93TG9hZGluZyhzbGlkZSk7XG5cbiAgICAgICAgLy8gVW5mb3J0dW5hdGVseSwgaXQgaXMgbm90IGFsd2F5cyBwb3NzaWJsZSB0byBkZXRlcm1pbmUgaWYgaWZyYW1lIGlzIHN1Y2Nlc3NmdWxseSBsb2FkZWRcbiAgICAgICAgLy8gKGR1ZSB0byBicm93c2VyIHNlY3VyaXR5IHBvbGljeSlcblxuICAgICAgICAkaWZyYW1lLm9uKFwibG9hZC5mYiBlcnJvci5mYlwiLCBmdW5jdGlvbihlKSB7XG4gICAgICAgICAgdGhpcy5pc1JlYWR5ID0gMTtcblxuICAgICAgICAgIHNsaWRlLiRzbGlkZS50cmlnZ2VyKFwicmVmcmVzaFwiKTtcblxuICAgICAgICAgIHNlbGYuYWZ0ZXJMb2FkKHNsaWRlKTtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgLy8gUmVjYWxjdWxhdGUgaWZyYW1lIGNvbnRlbnQgc2l6ZVxuICAgICAgICAvLyA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG5cbiAgICAgICAgJHNsaWRlLm9uKFwicmVmcmVzaC5mYlwiLCBmdW5jdGlvbigpIHtcbiAgICAgICAgICB2YXIgJGNvbnRlbnQgPSBzbGlkZS4kY29udGVudCxcbiAgICAgICAgICAgIGZyYW1lV2lkdGggPSBvcHRzLmNzcy53aWR0aCxcbiAgICAgICAgICAgIGZyYW1lSGVpZ2h0ID0gb3B0cy5jc3MuaGVpZ2h0LFxuICAgICAgICAgICAgJGNvbnRlbnRzLFxuICAgICAgICAgICAgJGJvZHk7XG5cbiAgICAgICAgICBpZiAoJGlmcmFtZVswXS5pc1JlYWR5ICE9PSAxKSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgICRjb250ZW50cyA9ICRpZnJhbWUuY29udGVudHMoKTtcbiAgICAgICAgICAgICRib2R5ID0gJGNvbnRlbnRzLmZpbmQoXCJib2R5XCIpO1xuICAgICAgICAgIH0gY2F0Y2ggKGlnbm9yZSkge31cblxuICAgICAgICAgIC8vIENhbGN1bGF0ZSBjb250bmV0IGRpbWVuc2lvbnMgaWYgaXQgaXMgYWNjZXNzaWJsZVxuICAgICAgICAgIGlmICgkYm9keSAmJiAkYm9keS5sZW5ndGggJiYgJGJvZHkuY2hpbGRyZW4oKS5sZW5ndGgpIHtcbiAgICAgICAgICAgICRjb250ZW50LmNzcyh7XG4gICAgICAgICAgICAgIHdpZHRoOiBcIlwiLFxuICAgICAgICAgICAgICBoZWlnaHQ6IFwiXCJcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICBpZiAoZnJhbWVXaWR0aCA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgIGZyYW1lV2lkdGggPSBNYXRoLmNlaWwoTWF0aC5tYXgoJGJvZHlbMF0uY2xpZW50V2lkdGgsICRib2R5Lm91dGVyV2lkdGgodHJ1ZSkpKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKGZyYW1lV2lkdGgpIHtcbiAgICAgICAgICAgICAgJGNvbnRlbnQud2lkdGgoZnJhbWVXaWR0aCk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmIChmcmFtZUhlaWdodCA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgIGZyYW1lSGVpZ2h0ID0gTWF0aC5jZWlsKE1hdGgubWF4KCRib2R5WzBdLmNsaWVudEhlaWdodCwgJGJvZHkub3V0ZXJIZWlnaHQodHJ1ZSkpKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKGZyYW1lSGVpZ2h0KSB7XG4gICAgICAgICAgICAgICRjb250ZW50LmhlaWdodChmcmFtZUhlaWdodCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgJGNvbnRlbnQucmVtb3ZlQ2xhc3MoXCJmYW5jeWJveC1pcy1oaWRkZW5cIik7XG4gICAgICAgIH0pO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5hZnRlckxvYWQoc2xpZGUpO1xuICAgICAgfVxuXG4gICAgICAkaWZyYW1lLmF0dHIoXCJzcmNcIiwgc2xpZGUuc3JjKTtcblxuICAgICAgLy8gUmVtb3ZlIGlmcmFtZSBpZiBjbG9zaW5nIG9yIGNoYW5naW5nIGdhbGxlcnkgaXRlbVxuICAgICAgJHNsaWRlLm9uZShcIm9uUmVzZXRcIiwgZnVuY3Rpb24oKSB7XG4gICAgICAgIC8vIFRoaXMgaGVscHMgSUUgbm90IHRvIHRocm93IGVycm9ycyB3aGVuIGNsb3NpbmdcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAkKHRoaXMpXG4gICAgICAgICAgICAuZmluZChcImlmcmFtZVwiKVxuICAgICAgICAgICAgLmhpZGUoKVxuICAgICAgICAgICAgLnVuYmluZCgpXG4gICAgICAgICAgICAuYXR0cihcInNyY1wiLCBcIi8vYWJvdXQ6YmxhbmtcIik7XG4gICAgICAgIH0gY2F0Y2ggKGlnbm9yZSkge31cblxuICAgICAgICAkKHRoaXMpXG4gICAgICAgICAgLm9mZihcInJlZnJlc2guZmJcIilcbiAgICAgICAgICAuZW1wdHkoKTtcblxuICAgICAgICBzbGlkZS5pc0xvYWRlZCA9IGZhbHNlO1xuICAgICAgfSk7XG4gICAgfSxcblxuICAgIC8vIFdyYXAgYW5kIGFwcGVuZCBjb250ZW50IHRvIHRoZSBzbGlkZVxuICAgIC8vID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG5cbiAgICBzZXRDb250ZW50OiBmdW5jdGlvbihzbGlkZSwgY29udGVudCkge1xuICAgICAgdmFyIHNlbGYgPSB0aGlzO1xuXG4gICAgICBpZiAoc2VsZi5pc0Nsb3NpbmcpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICBzZWxmLmhpZGVMb2FkaW5nKHNsaWRlKTtcblxuICAgICAgaWYgKHNsaWRlLiRjb250ZW50KSB7XG4gICAgICAgICQuZmFuY3lib3guc3RvcChzbGlkZS4kY29udGVudCk7XG4gICAgICB9XG5cbiAgICAgIHNsaWRlLiRzbGlkZS5lbXB0eSgpO1xuXG4gICAgICAvLyBJZiBjb250ZW50IGlzIGEgalF1ZXJ5IG9iamVjdCwgdGhlbiBpdCB3aWxsIGJlIG1vdmVkIHRvIHRoZSBzbGlkZS5cbiAgICAgIC8vIFRoZSBwbGFjZWhvbGRlciBpcyBjcmVhdGVkIHNvIHdlIHdpbGwga25vdyB3aGVyZSB0byBwdXQgaXQgYmFjay5cbiAgICAgIGlmIChpc1F1ZXJ5KGNvbnRlbnQpICYmIGNvbnRlbnQucGFyZW50KCkubGVuZ3RoKSB7XG4gICAgICAgIC8vIE1ha2Ugc3VyZSBjb250ZW50IGlzIG5vdCBhbHJlYWR5IG1vdmVkIHRvIGZhbmN5Qm94XG4gICAgICAgIGNvbnRlbnRcbiAgICAgICAgICAucGFyZW50KClcbiAgICAgICAgICAucGFyZW50KFwiLmZhbmN5Ym94LXNsaWRlLS1pbmxpbmVcIilcbiAgICAgICAgICAudHJpZ2dlcihcIm9uUmVzZXRcIik7XG5cbiAgICAgICAgLy8gQ3JlYXRlIHRlbXBvcmFyeSBlbGVtZW50IG1hcmtpbmcgb3JpZ2luYWwgcGxhY2Ugb2YgdGhlIGNvbnRlbnRcbiAgICAgICAgc2xpZGUuJHBsYWNlaG9sZGVyID0gJChcIjxkaXY+XCIpXG4gICAgICAgICAgLmhpZGUoKVxuICAgICAgICAgIC5pbnNlcnRBZnRlcihjb250ZW50KTtcblxuICAgICAgICAvLyBNYWtlIHN1cmUgY29udGVudCBpcyB2aXNpYmxlXG4gICAgICAgIGNvbnRlbnQuY3NzKFwiZGlzcGxheVwiLCBcImlubGluZS1ibG9ja1wiKTtcbiAgICAgIH0gZWxzZSBpZiAoIXNsaWRlLmhhc0Vycm9yKSB7XG4gICAgICAgIC8vIElmIGNvbnRlbnQgaXMganVzdCBhIHBsYWluIHRleHQsIHRyeSB0byBjb252ZXJ0IGl0IHRvIGh0bWxcbiAgICAgICAgaWYgKCQudHlwZShjb250ZW50KSA9PT0gXCJzdHJpbmdcIikge1xuICAgICAgICAgIGNvbnRlbnQgPSAkKFwiPGRpdj5cIilcbiAgICAgICAgICAgIC5hcHBlbmQoJC50cmltKGNvbnRlbnQpKVxuICAgICAgICAgICAgLmNvbnRlbnRzKCk7XG5cbiAgICAgICAgICAvLyBJZiB3ZSBoYXZlIHRleHQgbm9kZSwgdGhlbiBhZGQgd3JhcHBpbmcgZWxlbWVudCB0byBtYWtlIHZlcnRpY2FsIGFsaWdubWVudCB3b3JrXG4gICAgICAgICAgaWYgKGNvbnRlbnRbMF0ubm9kZVR5cGUgPT09IDMpIHtcbiAgICAgICAgICAgIGNvbnRlbnQgPSAkKFwiPGRpdj5cIikuaHRtbChjb250ZW50KTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICAvLyBJZiBcImZpbHRlclwiIG9wdGlvbiBpcyBwcm92aWRlZCwgdGhlbiBmaWx0ZXIgY29udGVudFxuICAgICAgICBpZiAoc2xpZGUub3B0cy5maWx0ZXIpIHtcbiAgICAgICAgICBjb250ZW50ID0gJChcIjxkaXY+XCIpXG4gICAgICAgICAgICAuaHRtbChjb250ZW50KVxuICAgICAgICAgICAgLmZpbmQoc2xpZGUub3B0cy5maWx0ZXIpO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIHNsaWRlLiRzbGlkZS5vbmUoXCJvblJlc2V0XCIsIGZ1bmN0aW9uKCkge1xuICAgICAgICAvLyBQYXVzZSBhbGwgaHRtbDUgdmlkZW8vYXVkaW9cbiAgICAgICAgJCh0aGlzKVxuICAgICAgICAgIC5maW5kKFwidmlkZW8sYXVkaW9cIilcbiAgICAgICAgICAudHJpZ2dlcihcInBhdXNlXCIpO1xuXG4gICAgICAgIC8vIFB1dCBjb250ZW50IGJhY2tcbiAgICAgICAgaWYgKHNsaWRlLiRwbGFjZWhvbGRlcikge1xuICAgICAgICAgIHNsaWRlLiRwbGFjZWhvbGRlci5hZnRlcihjb250ZW50LmhpZGUoKSkucmVtb3ZlKCk7XG5cbiAgICAgICAgICBzbGlkZS4kcGxhY2Vob2xkZXIgPSBudWxsO1xuICAgICAgICB9XG5cbiAgICAgICAgLy8gUmVtb3ZlIGN1c3RvbSBjbG9zZSBidXR0b25cbiAgICAgICAgaWYgKHNsaWRlLiRzbWFsbEJ0bikge1xuICAgICAgICAgIHNsaWRlLiRzbWFsbEJ0bi5yZW1vdmUoKTtcblxuICAgICAgICAgIHNsaWRlLiRzbWFsbEJ0biA9IG51bGw7XG4gICAgICAgIH1cblxuICAgICAgICAvLyBSZW1vdmUgY29udGVudCBhbmQgbWFyayBzbGlkZSBhcyBub3QgbG9hZGVkXG4gICAgICAgIGlmICghc2xpZGUuaGFzRXJyb3IpIHtcbiAgICAgICAgICAkKHRoaXMpLmVtcHR5KCk7XG5cbiAgICAgICAgICBzbGlkZS5pc0xvYWRlZCA9IGZhbHNlO1xuICAgICAgICB9XG4gICAgICB9KTtcblxuICAgICAgJChjb250ZW50KS5hcHBlbmRUbyhzbGlkZS4kc2xpZGUpO1xuXG4gICAgICBpZiAoJChjb250ZW50KS5pcyhcInZpZGVvLGF1ZGlvXCIpKSB7XG4gICAgICAgICQoY29udGVudCkuYWRkQ2xhc3MoXCJmYW5jeWJveC12aWRlb1wiKTtcblxuICAgICAgICAkKGNvbnRlbnQpLndyYXAoXCI8ZGl2PjwvZGl2PlwiKTtcblxuICAgICAgICBzbGlkZS5jb250ZW50VHlwZSA9IFwidmlkZW9cIjtcblxuICAgICAgICBzbGlkZS5vcHRzLndpZHRoID0gc2xpZGUub3B0cy53aWR0aCB8fCAkKGNvbnRlbnQpLmF0dHIoXCJ3aWR0aFwiKTtcbiAgICAgICAgc2xpZGUub3B0cy5oZWlnaHQgPSBzbGlkZS5vcHRzLmhlaWdodCB8fCAkKGNvbnRlbnQpLmF0dHIoXCJoZWlnaHRcIik7XG4gICAgICB9XG5cbiAgICAgIHNsaWRlLiRjb250ZW50ID0gc2xpZGUuJHNsaWRlXG4gICAgICAgIC5jaGlsZHJlbigpXG4gICAgICAgIC5maWx0ZXIoXCJkaXYsZm9ybSxtYWluLHZpZGVvLGF1ZGlvXCIpXG4gICAgICAgIC5maXJzdCgpXG4gICAgICAgIC5hZGRDbGFzcyhcImZhbmN5Ym94LWNvbnRlbnRcIik7XG5cbiAgICAgIHNsaWRlLiRzbGlkZS5hZGRDbGFzcyhcImZhbmN5Ym94LXNsaWRlLS1cIiArIHNsaWRlLmNvbnRlbnRUeXBlKTtcblxuICAgICAgdGhpcy5hZnRlckxvYWQoc2xpZGUpO1xuICAgIH0sXG5cbiAgICAvLyBEaXNwbGF5IGVycm9yIG1lc3NhZ2VcbiAgICAvLyA9PT09PT09PT09PT09PT09PT09PT1cblxuICAgIHNldEVycm9yOiBmdW5jdGlvbihzbGlkZSkge1xuICAgICAgc2xpZGUuaGFzRXJyb3IgPSB0cnVlO1xuXG4gICAgICBzbGlkZS4kc2xpZGVcbiAgICAgICAgLnRyaWdnZXIoXCJvblJlc2V0XCIpXG4gICAgICAgIC5yZW1vdmVDbGFzcyhcImZhbmN5Ym94LXNsaWRlLS1cIiArIHNsaWRlLmNvbnRlbnRUeXBlKVxuICAgICAgICAuYWRkQ2xhc3MoXCJmYW5jeWJveC1zbGlkZS0tZXJyb3JcIik7XG5cbiAgICAgIHNsaWRlLmNvbnRlbnRUeXBlID0gXCJodG1sXCI7XG5cbiAgICAgIHRoaXMuc2V0Q29udGVudChzbGlkZSwgdGhpcy50cmFuc2xhdGUoc2xpZGUsIHNsaWRlLm9wdHMuZXJyb3JUcGwpKTtcblxuICAgICAgaWYgKHNsaWRlLnBvcyA9PT0gdGhpcy5jdXJyUG9zKSB7XG4gICAgICAgIHRoaXMuaXNBbmltYXRpbmcgPSBmYWxzZTtcbiAgICAgIH1cbiAgICB9LFxuXG4gICAgLy8gU2hvdyBsb2FkaW5nIGljb24gaW5zaWRlIHRoZSBzbGlkZVxuICAgIC8vID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cblxuICAgIHNob3dMb2FkaW5nOiBmdW5jdGlvbihzbGlkZSkge1xuICAgICAgdmFyIHNlbGYgPSB0aGlzO1xuXG4gICAgICBzbGlkZSA9IHNsaWRlIHx8IHNlbGYuY3VycmVudDtcblxuICAgICAgaWYgKHNsaWRlICYmICFzbGlkZS4kc3Bpbm5lcikge1xuICAgICAgICBzbGlkZS4kc3Bpbm5lciA9ICQoc2VsZi50cmFuc2xhdGUoc2VsZiwgc2VsZi5vcHRzLnNwaW5uZXJUcGwpKS5hcHBlbmRUbyhzbGlkZS4kc2xpZGUpO1xuICAgICAgfVxuICAgIH0sXG5cbiAgICAvLyBSZW1vdmUgbG9hZGluZyBpY29uIGZyb20gdGhlIHNsaWRlXG4gICAgLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuXG4gICAgaGlkZUxvYWRpbmc6IGZ1bmN0aW9uKHNsaWRlKSB7XG4gICAgICB2YXIgc2VsZiA9IHRoaXM7XG5cbiAgICAgIHNsaWRlID0gc2xpZGUgfHwgc2VsZi5jdXJyZW50O1xuXG4gICAgICBpZiAoc2xpZGUgJiYgc2xpZGUuJHNwaW5uZXIpIHtcbiAgICAgICAgc2xpZGUuJHNwaW5uZXIucmVtb3ZlKCk7XG5cbiAgICAgICAgZGVsZXRlIHNsaWRlLiRzcGlubmVyO1xuICAgICAgfVxuICAgIH0sXG5cbiAgICAvLyBBZGp1c3RtZW50cyBhZnRlciBzbGlkZSBjb250ZW50IGhhcyBiZWVuIGxvYWRlZFxuICAgIC8vID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG5cbiAgICBhZnRlckxvYWQ6IGZ1bmN0aW9uKHNsaWRlKSB7XG4gICAgICB2YXIgc2VsZiA9IHRoaXM7XG5cbiAgICAgIGlmIChzZWxmLmlzQ2xvc2luZykge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIHNsaWRlLmlzTG9hZGluZyA9IGZhbHNlO1xuICAgICAgc2xpZGUuaXNMb2FkZWQgPSB0cnVlO1xuXG4gICAgICBzZWxmLnRyaWdnZXIoXCJhZnRlckxvYWRcIiwgc2xpZGUpO1xuXG4gICAgICBzZWxmLmhpZGVMb2FkaW5nKHNsaWRlKTtcblxuICAgICAgaWYgKHNsaWRlLnBvcyA9PT0gc2VsZi5jdXJyUG9zKSB7XG4gICAgICAgIHNlbGYudXBkYXRlQ3Vyc29yKCk7XG4gICAgICB9XG5cbiAgICAgIGlmIChzbGlkZS5vcHRzLnNtYWxsQnRuICYmICghc2xpZGUuJHNtYWxsQnRuIHx8ICFzbGlkZS4kc21hbGxCdG4ubGVuZ3RoKSkge1xuICAgICAgICBzbGlkZS4kc21hbGxCdG4gPSAkKHNlbGYudHJhbnNsYXRlKHNsaWRlLCBzbGlkZS5vcHRzLmJ0blRwbC5zbWFsbEJ0bikpLnByZXBlbmRUbyhzbGlkZS4kY29udGVudCk7XG4gICAgICB9XG5cbiAgICAgIGlmIChzbGlkZS5vcHRzLnByb3RlY3QgJiYgc2xpZGUuJGNvbnRlbnQgJiYgIXNsaWRlLmhhc0Vycm9yKSB7XG4gICAgICAgIC8vIERpc2FibGUgcmlnaHQgY2xpY2tcbiAgICAgICAgc2xpZGUuJGNvbnRlbnQub24oXCJjb250ZXh0bWVudS5mYlwiLCBmdW5jdGlvbihlKSB7XG4gICAgICAgICAgaWYgKGUuYnV0dG9uID09IDIpIHtcbiAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgLy8gQWRkIGZha2UgZWxlbWVudCBvbiB0b3Agb2YgdGhlIGltYWdlXG4gICAgICAgIC8vIFRoaXMgbWFrZXMgYSBiaXQgaGFyZGVyIGZvciB1c2VyIHRvIHNlbGVjdCBpbWFnZVxuICAgICAgICBpZiAoc2xpZGUudHlwZSA9PT0gXCJpbWFnZVwiKSB7XG4gICAgICAgICAgJCgnPGRpdiBjbGFzcz1cImZhbmN5Ym94LXNwYWNlYmFsbFwiPjwvZGl2PicpLmFwcGVuZFRvKHNsaWRlLiRjb250ZW50KTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBzZWxmLnJldmVhbENvbnRlbnQoc2xpZGUpO1xuICAgIH0sXG5cbiAgICAvLyBNYWtlIGNvbnRlbnQgdmlzaWJsZVxuICAgIC8vIFRoaXMgbWV0aG9kIGlzIGNhbGxlZCByaWdodCBhZnRlciBjb250ZW50IGhhcyBiZWVuIGxvYWRlZCBvclxuICAgIC8vIHVzZXIgbmF2aWdhdGVzIGdhbGxlcnkgYW5kIHRyYW5zaXRpb24gc2hvdWxkIHN0YXJ0XG4gICAgLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG5cbiAgICByZXZlYWxDb250ZW50OiBmdW5jdGlvbihzbGlkZSkge1xuICAgICAgdmFyIHNlbGYgPSB0aGlzLFxuICAgICAgICAkc2xpZGUgPSBzbGlkZS4kc2xpZGUsXG4gICAgICAgIGVuZCA9IGZhbHNlLFxuICAgICAgICBzdGFydCA9IGZhbHNlLFxuICAgICAgICBlZmZlY3QsXG4gICAgICAgIGVmZmVjdENsYXNzTmFtZSxcbiAgICAgICAgZHVyYXRpb24sXG4gICAgICAgIG9wYWNpdHk7XG5cbiAgICAgIGVmZmVjdCA9IHNsaWRlLm9wdHNbc2VsZi5maXJzdFJ1biA/IFwiYW5pbWF0aW9uRWZmZWN0XCIgOiBcInRyYW5zaXRpb25FZmZlY3RcIl07XG4gICAgICBkdXJhdGlvbiA9IHNsaWRlLm9wdHNbc2VsZi5maXJzdFJ1biA/IFwiYW5pbWF0aW9uRHVyYXRpb25cIiA6IFwidHJhbnNpdGlvbkR1cmF0aW9uXCJdO1xuXG4gICAgICBkdXJhdGlvbiA9IHBhcnNlSW50KHNsaWRlLmZvcmNlZER1cmF0aW9uID09PSB1bmRlZmluZWQgPyBkdXJhdGlvbiA6IHNsaWRlLmZvcmNlZER1cmF0aW9uLCAxMCk7XG5cbiAgICAgIC8vIERvIG5vdCBhbmltYXRlIGlmIHJldmVhbGluZyB0aGUgc2FtZSBzbGlkZVxuICAgICAgaWYgKHNsaWRlLnBvcyA9PT0gc2VsZi5jdXJyUG9zKSB7XG4gICAgICAgIGlmIChzbGlkZS5pc0NvbXBsZXRlKSB7XG4gICAgICAgICAgZWZmZWN0ID0gZmFsc2U7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgc2VsZi5pc0FuaW1hdGluZyA9IHRydWU7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgaWYgKHNsaWRlLmlzTW92ZWQgfHwgc2xpZGUucG9zICE9PSBzZWxmLmN1cnJQb3MgfHwgIWR1cmF0aW9uKSB7XG4gICAgICAgIGVmZmVjdCA9IGZhbHNlO1xuICAgICAgfVxuXG4gICAgICAvLyBDaGVjayBpZiBjYW4gem9vbVxuICAgICAgaWYgKGVmZmVjdCA9PT0gXCJ6b29tXCIpIHtcbiAgICAgICAgaWYgKHNsaWRlLnBvcyA9PT0gc2VsZi5jdXJyUG9zICYmIGR1cmF0aW9uICYmIHNsaWRlLnR5cGUgPT09IFwiaW1hZ2VcIiAmJiAhc2xpZGUuaGFzRXJyb3IgJiYgKHN0YXJ0ID0gc2VsZi5nZXRUaHVtYlBvcyhzbGlkZSkpKSB7XG4gICAgICAgICAgZW5kID0gc2VsZi5nZXRGaXRQb3Moc2xpZGUpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGVmZmVjdCA9IFwiZmFkZVwiO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIC8vIFpvb20gYW5pbWF0aW9uXG4gICAgICAvLyA9PT09PT09PT09PT09PVxuICAgICAgaWYgKGVmZmVjdCA9PT0gXCJ6b29tXCIpIHtcbiAgICAgICAgZW5kLnNjYWxlWCA9IGVuZC53aWR0aCAvIHN0YXJ0LndpZHRoO1xuICAgICAgICBlbmQuc2NhbGVZID0gZW5kLmhlaWdodCAvIHN0YXJ0LmhlaWdodDtcblxuICAgICAgICAvLyBDaGVjayBpZiB3ZSBuZWVkIHRvIGFuaW1hdGUgb3BhY2l0eVxuICAgICAgICBvcGFjaXR5ID0gc2xpZGUub3B0cy56b29tT3BhY2l0eTtcblxuICAgICAgICBpZiAob3BhY2l0eSA9PSBcImF1dG9cIikge1xuICAgICAgICAgIG9wYWNpdHkgPSBNYXRoLmFicyhzbGlkZS53aWR0aCAvIHNsaWRlLmhlaWdodCAtIHN0YXJ0LndpZHRoIC8gc3RhcnQuaGVpZ2h0KSA+IDAuMTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChvcGFjaXR5KSB7XG4gICAgICAgICAgc3RhcnQub3BhY2l0eSA9IDAuMTtcbiAgICAgICAgICBlbmQub3BhY2l0eSA9IDE7XG4gICAgICAgIH1cblxuICAgICAgICAvLyBEcmF3IGltYWdlIGF0IHN0YXJ0IHBvc2l0aW9uXG4gICAgICAgICQuZmFuY3lib3guc2V0VHJhbnNsYXRlKHNsaWRlLiRjb250ZW50LnJlbW92ZUNsYXNzKFwiZmFuY3lib3gtaXMtaGlkZGVuXCIpLCBzdGFydCk7XG5cbiAgICAgICAgZm9yY2VSZWRyYXcoc2xpZGUuJGNvbnRlbnQpO1xuXG4gICAgICAgIC8vIFN0YXJ0IGFuaW1hdGlvblxuICAgICAgICAkLmZhbmN5Ym94LmFuaW1hdGUoc2xpZGUuJGNvbnRlbnQsIGVuZCwgZHVyYXRpb24sIGZ1bmN0aW9uKCkge1xuICAgICAgICAgIHNlbGYuaXNBbmltYXRpbmcgPSBmYWxzZTtcblxuICAgICAgICAgIHNlbGYuY29tcGxldGUoKTtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICBzZWxmLnVwZGF0ZVNsaWRlKHNsaWRlKTtcblxuICAgICAgLy8gU2ltcGx5IHNob3cgY29udGVudFxuICAgICAgLy8gPT09PT09PT09PT09PT09PT09PVxuXG4gICAgICBpZiAoIWVmZmVjdCkge1xuICAgICAgICBmb3JjZVJlZHJhdygkc2xpZGUpO1xuXG4gICAgICAgIHNsaWRlLiRjb250ZW50LnJlbW92ZUNsYXNzKFwiZmFuY3lib3gtaXMtaGlkZGVuXCIpO1xuXG4gICAgICAgIGlmIChzbGlkZS5wb3MgPT09IHNlbGYuY3VyclBvcykge1xuICAgICAgICAgIHNlbGYuY29tcGxldGUoKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgJC5mYW5jeWJveC5zdG9wKCRzbGlkZSk7XG5cbiAgICAgIGVmZmVjdENsYXNzTmFtZSA9IFwiZmFuY3lib3gtYW5pbWF0ZWQgZmFuY3lib3gtc2xpZGUtLVwiICsgKHNsaWRlLnBvcyA+PSBzZWxmLnByZXZQb3MgPyBcIm5leHRcIiA6IFwicHJldmlvdXNcIikgKyBcIiBmYW5jeWJveC1meC1cIiArIGVmZmVjdDtcblxuICAgICAgJHNsaWRlXG4gICAgICAgIC5yZW1vdmVBdHRyKFwic3R5bGVcIilcbiAgICAgICAgLnJlbW92ZUNsYXNzKFwiZmFuY3lib3gtc2xpZGUtLWN1cnJlbnQgZmFuY3lib3gtc2xpZGUtLW5leHQgZmFuY3lib3gtc2xpZGUtLXByZXZpb3VzXCIpXG4gICAgICAgIC5hZGRDbGFzcyhlZmZlY3RDbGFzc05hbWUpO1xuXG4gICAgICBzbGlkZS4kY29udGVudC5yZW1vdmVDbGFzcyhcImZhbmN5Ym94LWlzLWhpZGRlblwiKTtcblxuICAgICAgLy8gRm9yY2UgcmVmbG93IGZvciBDU1MzIHRyYW5zaXRpb25zXG4gICAgICBmb3JjZVJlZHJhdygkc2xpZGUpO1xuXG4gICAgICAkLmZhbmN5Ym94LmFuaW1hdGUoXG4gICAgICAgICRzbGlkZSxcbiAgICAgICAgXCJmYW5jeWJveC1zbGlkZS0tY3VycmVudFwiLFxuICAgICAgICBkdXJhdGlvbixcbiAgICAgICAgZnVuY3Rpb24oZSkge1xuICAgICAgICAgICRzbGlkZS5yZW1vdmVDbGFzcyhlZmZlY3RDbGFzc05hbWUpLnJlbW92ZUF0dHIoXCJzdHlsZVwiKTtcblxuICAgICAgICAgIGlmIChzbGlkZS5wb3MgPT09IHNlbGYuY3VyclBvcykge1xuICAgICAgICAgICAgc2VsZi5jb21wbGV0ZSgpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgdHJ1ZVxuICAgICAgKTtcbiAgICB9LFxuXG4gICAgLy8gQ2hlY2sgaWYgd2UgY2FuIGFuZCBoYXZlIHRvIHpvb20gZnJvbSB0aHVtYm5haWxcbiAgICAvLz09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuXG4gICAgZ2V0VGh1bWJQb3M6IGZ1bmN0aW9uKHNsaWRlKSB7XG4gICAgICB2YXIgc2VsZiA9IHRoaXMsXG4gICAgICAgIHJleiA9IGZhbHNlLFxuICAgICAgICAkdGh1bWIgPSBzbGlkZS5vcHRzLiR0aHVtYixcbiAgICAgICAgdGh1bWJQb3MgPSAkdGh1bWIgJiYgJHRodW1iLmxlbmd0aCAmJiAkdGh1bWJbMF0ub3duZXJEb2N1bWVudCA9PT0gZG9jdW1lbnQgPyAkdGh1bWIub2Zmc2V0KCkgOiAwLFxuICAgICAgICBzbGlkZVBvcztcblxuICAgICAgLy8gQ2hlY2sgaWYgZWxlbWVudCBpcyBpbnNpZGUgdGhlIHZpZXdwb3J0IGJ5IGF0IGxlYXN0IDEgcGl4ZWxcbiAgICAgIHZhciBpc0VsZW1lbnRWaXNpYmxlID0gZnVuY3Rpb24oJGVsKSB7XG4gICAgICAgIHZhciBlbGVtZW50ID0gJGVsWzBdLFxuICAgICAgICAgIGVsZW1lbnRSZWN0ID0gZWxlbWVudC5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKSxcbiAgICAgICAgICBwYXJlbnRSZWN0cyA9IFtdLFxuICAgICAgICAgIHZpc2libGVJbkFsbFBhcmVudHM7XG5cbiAgICAgICAgd2hpbGUgKGVsZW1lbnQucGFyZW50RWxlbWVudCAhPT0gbnVsbCkge1xuICAgICAgICAgIGlmICgkKGVsZW1lbnQucGFyZW50RWxlbWVudCkuY3NzKFwib3ZlcmZsb3dcIikgPT09IFwiaGlkZGVuXCIgfHwgJChlbGVtZW50LnBhcmVudEVsZW1lbnQpLmNzcyhcIm92ZXJmbG93XCIpID09PSBcImF1dG9cIikge1xuICAgICAgICAgICAgcGFyZW50UmVjdHMucHVzaChlbGVtZW50LnBhcmVudEVsZW1lbnQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkpO1xuICAgICAgICAgIH1cblxuICAgICAgICAgIGVsZW1lbnQgPSBlbGVtZW50LnBhcmVudEVsZW1lbnQ7XG4gICAgICAgIH1cblxuICAgICAgICB2aXNpYmxlSW5BbGxQYXJlbnRzID0gcGFyZW50UmVjdHMuZXZlcnkoZnVuY3Rpb24ocGFyZW50UmVjdCkge1xuICAgICAgICAgIHZhciB2aXNpYmxlUGl4ZWxYID0gTWF0aC5taW4oZWxlbWVudFJlY3QucmlnaHQsIHBhcmVudFJlY3QucmlnaHQpIC0gTWF0aC5tYXgoZWxlbWVudFJlY3QubGVmdCwgcGFyZW50UmVjdC5sZWZ0KTtcbiAgICAgICAgICB2YXIgdmlzaWJsZVBpeGVsWSA9IE1hdGgubWluKGVsZW1lbnRSZWN0LmJvdHRvbSwgcGFyZW50UmVjdC5ib3R0b20pIC0gTWF0aC5tYXgoZWxlbWVudFJlY3QudG9wLCBwYXJlbnRSZWN0LnRvcCk7XG5cbiAgICAgICAgICByZXR1cm4gdmlzaWJsZVBpeGVsWCA+IDAgJiYgdmlzaWJsZVBpeGVsWSA+IDA7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIHJldHVybiAoXG4gICAgICAgICAgdmlzaWJsZUluQWxsUGFyZW50cyAmJlxuICAgICAgICAgIGVsZW1lbnRSZWN0LmJvdHRvbSA+IDAgJiZcbiAgICAgICAgICBlbGVtZW50UmVjdC5yaWdodCA+IDAgJiZcbiAgICAgICAgICBlbGVtZW50UmVjdC5sZWZ0IDwgJCh3aW5kb3cpLndpZHRoKCkgJiZcbiAgICAgICAgICBlbGVtZW50UmVjdC50b3AgPCAkKHdpbmRvdykuaGVpZ2h0KClcbiAgICAgICAgKTtcbiAgICAgIH07XG5cbiAgICAgIGlmICh0aHVtYlBvcyAmJiBpc0VsZW1lbnRWaXNpYmxlKCR0aHVtYikpIHtcbiAgICAgICAgc2xpZGVQb3MgPSBzZWxmLiRyZWZzLnN0YWdlLm9mZnNldCgpO1xuXG4gICAgICAgIHJleiA9IHtcbiAgICAgICAgICB0b3A6IHRodW1iUG9zLnRvcCAtIHNsaWRlUG9zLnRvcCArIHBhcnNlRmxvYXQoJHRodW1iLmNzcyhcImJvcmRlci10b3Atd2lkdGhcIikgfHwgMCksXG4gICAgICAgICAgbGVmdDogdGh1bWJQb3MubGVmdCAtIHNsaWRlUG9zLmxlZnQgKyBwYXJzZUZsb2F0KCR0aHVtYi5jc3MoXCJib3JkZXItbGVmdC13aWR0aFwiKSB8fCAwKSxcbiAgICAgICAgICB3aWR0aDogJHRodW1iLndpZHRoKCksXG4gICAgICAgICAgaGVpZ2h0OiAkdGh1bWIuaGVpZ2h0KCksXG4gICAgICAgICAgc2NhbGVYOiAxLFxuICAgICAgICAgIHNjYWxlWTogMVxuICAgICAgICB9O1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gcmV6O1xuICAgIH0sXG5cbiAgICAvLyBGaW5hbCBhZGp1c3RtZW50cyBhZnRlciBjdXJyZW50IGdhbGxlcnkgaXRlbSBpcyBtb3ZlZCB0byBwb3NpdGlvblxuICAgIC8vIGFuZCBpdGBzIGNvbnRlbnQgaXMgbG9hZGVkXG4gICAgLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG5cbiAgICBjb21wbGV0ZTogZnVuY3Rpb24oKSB7XG4gICAgICB2YXIgc2VsZiA9IHRoaXMsXG4gICAgICAgIGN1cnJlbnQgPSBzZWxmLmN1cnJlbnQsXG4gICAgICAgIHNsaWRlcyA9IHt9O1xuXG4gICAgICBpZiAoY3VycmVudC5pc01vdmVkIHx8ICFjdXJyZW50LmlzTG9hZGVkKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgaWYgKCFjdXJyZW50LmlzQ29tcGxldGUpIHtcbiAgICAgICAgY3VycmVudC5pc0NvbXBsZXRlID0gdHJ1ZTtcblxuICAgICAgICBjdXJyZW50LiRzbGlkZS5zaWJsaW5ncygpLnRyaWdnZXIoXCJvblJlc2V0XCIpO1xuXG4gICAgICAgIHNlbGYucHJlbG9hZChcImlubGluZVwiKTtcblxuICAgICAgICAvLyBUcmlnZ2VyIGFueSBDU1MzIHRyYW5zaXRvbiBpbnNpZGUgdGhlIHNsaWRlXG4gICAgICAgIGZvcmNlUmVkcmF3KGN1cnJlbnQuJHNsaWRlKTtcblxuICAgICAgICBjdXJyZW50LiRzbGlkZS5hZGRDbGFzcyhcImZhbmN5Ym94LXNsaWRlLS1jb21wbGV0ZVwiKTtcblxuICAgICAgICAvLyBSZW1vdmUgdW5uZWNlc3Nhcnkgc2xpZGVzXG4gICAgICAgICQuZWFjaChzZWxmLnNsaWRlcywgZnVuY3Rpb24oa2V5LCBzbGlkZSkge1xuICAgICAgICAgIGlmIChzbGlkZS5wb3MgPj0gc2VsZi5jdXJyUG9zIC0gMSAmJiBzbGlkZS5wb3MgPD0gc2VsZi5jdXJyUG9zICsgMSkge1xuICAgICAgICAgICAgc2xpZGVzW3NsaWRlLnBvc10gPSBzbGlkZTtcbiAgICAgICAgICB9IGVsc2UgaWYgKHNsaWRlKSB7XG4gICAgICAgICAgICAkLmZhbmN5Ym94LnN0b3Aoc2xpZGUuJHNsaWRlKTtcblxuICAgICAgICAgICAgc2xpZGUuJHNsaWRlLm9mZigpLnJlbW92ZSgpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSk7XG5cbiAgICAgICAgc2VsZi5zbGlkZXMgPSBzbGlkZXM7XG4gICAgICB9XG5cbiAgICAgIHNlbGYuaXNBbmltYXRpbmcgPSBmYWxzZTtcblxuICAgICAgc2VsZi51cGRhdGVDdXJzb3IoKTtcblxuICAgICAgc2VsZi50cmlnZ2VyKFwiYWZ0ZXJTaG93XCIpO1xuXG4gICAgICAvLyBQbGF5IGZpcnN0IGh0bWw1IHZpZGVvL2F1ZGlvXG4gICAgICBjdXJyZW50LiRzbGlkZVxuICAgICAgICAuZmluZChcInZpZGVvLGF1ZGlvXCIpXG4gICAgICAgIC5maWx0ZXIoXCI6dmlzaWJsZTpmaXJzdFwiKVxuICAgICAgICAudHJpZ2dlcihcInBsYXlcIik7XG5cbiAgICAgIC8vIFRyeSB0byBmb2N1cyBvbiB0aGUgZmlyc3QgZm9jdXNhYmxlIGVsZW1lbnRcbiAgICAgIGlmIChcbiAgICAgICAgJChkb2N1bWVudC5hY3RpdmVFbGVtZW50KS5pcyhcIltkaXNhYmxlZF1cIikgfHxcbiAgICAgICAgKGN1cnJlbnQub3B0cy5hdXRvRm9jdXMgJiYgIShjdXJyZW50LnR5cGUgPT0gXCJpbWFnZVwiIHx8IGN1cnJlbnQudHlwZSA9PT0gXCJpZnJhbWVcIikpXG4gICAgICApIHtcbiAgICAgICAgc2VsZi5mb2N1cygpO1xuICAgICAgfVxuICAgIH0sXG5cbiAgICAvLyBQcmVsb2FkIG5leHQgYW5kIHByZXZpb3VzIHNsaWRlc1xuICAgIC8vID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG5cbiAgICBwcmVsb2FkOiBmdW5jdGlvbih0eXBlKSB7XG4gICAgICB2YXIgc2VsZiA9IHRoaXMsXG4gICAgICAgIG5leHQgPSBzZWxmLnNsaWRlc1tzZWxmLmN1cnJQb3MgKyAxXSxcbiAgICAgICAgcHJldiA9IHNlbGYuc2xpZGVzW3NlbGYuY3VyclBvcyAtIDFdO1xuXG4gICAgICBpZiAobmV4dCAmJiBuZXh0LnR5cGUgPT09IHR5cGUpIHtcbiAgICAgICAgc2VsZi5sb2FkU2xpZGUobmV4dCk7XG4gICAgICB9XG5cbiAgICAgIGlmIChwcmV2ICYmIHByZXYudHlwZSA9PT0gdHlwZSkge1xuICAgICAgICBzZWxmLmxvYWRTbGlkZShwcmV2KTtcbiAgICAgIH1cbiAgICB9LFxuXG4gICAgLy8gVHJ5IHRvIGZpbmQgYW5kIGZvY3VzIG9uIHRoZSBmaXJzdCBmb2N1c2FibGUgZWxlbWVudFxuICAgIC8vID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cblxuICAgIGZvY3VzOiBmdW5jdGlvbigpIHtcbiAgICAgIHZhciBjdXJyZW50ID0gdGhpcy5jdXJyZW50LFxuICAgICAgICAkZWw7XG5cbiAgICAgIGlmICh0aGlzLmlzQ2xvc2luZykge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIGlmIChjdXJyZW50ICYmIGN1cnJlbnQuaXNDb21wbGV0ZSAmJiBjdXJyZW50LiRjb250ZW50KSB7XG4gICAgICAgIC8vIExvb2sgZm9yIGZpcnN0IGlucHV0IHdpdGggYXV0b2ZvY3VzIGF0dHJpYnV0ZVxuICAgICAgICAkZWwgPSBjdXJyZW50LiRjb250ZW50LmZpbmQoXCJpbnB1dFthdXRvZm9jdXNdOmVuYWJsZWQ6dmlzaWJsZTpmaXJzdFwiKTtcblxuICAgICAgICBpZiAoISRlbC5sZW5ndGgpIHtcbiAgICAgICAgICAkZWwgPSBjdXJyZW50LiRjb250ZW50LmZpbmQoXCJidXR0b24sOmlucHV0LFt0YWJpbmRleF0sYVwiKS5maWx0ZXIoXCI6ZW5hYmxlZDp2aXNpYmxlOmZpcnN0XCIpO1xuICAgICAgICB9XG5cbiAgICAgICAgJGVsID0gJGVsICYmICRlbC5sZW5ndGggPyAkZWwgOiBjdXJyZW50LiRjb250ZW50O1xuXG4gICAgICAgICRlbC50cmlnZ2VyKFwiZm9jdXNcIik7XG4gICAgICB9XG4gICAgfSxcblxuICAgIC8vIEFjdGl2YXRlcyBjdXJyZW50IGluc3RhbmNlIC0gYnJpbmdzIGNvbnRhaW5lciB0byB0aGUgZnJvbnQgYW5kIGVuYWJsZXMga2V5Ym9hcmQsXG4gICAgLy8gbm90aWZpZXMgb3RoZXIgaW5zdGFuY2VzIGFib3V0IGRlYWN0aXZhdGluZ1xuICAgIC8vID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuXG4gICAgYWN0aXZhdGU6IGZ1bmN0aW9uKCkge1xuICAgICAgdmFyIHNlbGYgPSB0aGlzO1xuXG4gICAgICAvLyBEZWFjdGl2YXRlIGFsbCBpbnN0YW5jZXNcbiAgICAgICQoXCIuZmFuY3lib3gtY29udGFpbmVyXCIpLmVhY2goZnVuY3Rpb24oKSB7XG4gICAgICAgIHZhciBpbnN0YW5jZSA9ICQodGhpcykuZGF0YShcIkZhbmN5Qm94XCIpO1xuXG4gICAgICAgIC8vIFNraXAgc2VsZiBhbmQgY2xvc2luZyBpbnN0YW5jZXNcbiAgICAgICAgaWYgKGluc3RhbmNlICYmIGluc3RhbmNlLmlkICE9PSBzZWxmLmlkICYmICFpbnN0YW5jZS5pc0Nsb3NpbmcpIHtcbiAgICAgICAgICBpbnN0YW5jZS50cmlnZ2VyKFwib25EZWFjdGl2YXRlXCIpO1xuXG4gICAgICAgICAgaW5zdGFuY2UucmVtb3ZlRXZlbnRzKCk7XG5cbiAgICAgICAgICBpbnN0YW5jZS5pc1Zpc2libGUgPSBmYWxzZTtcbiAgICAgICAgfVxuICAgICAgfSk7XG5cbiAgICAgIHNlbGYuaXNWaXNpYmxlID0gdHJ1ZTtcblxuICAgICAgaWYgKHNlbGYuY3VycmVudCB8fCBzZWxmLmlzSWRsZSkge1xuICAgICAgICBzZWxmLnVwZGF0ZSgpO1xuXG4gICAgICAgIHNlbGYudXBkYXRlQ29udHJvbHMoKTtcbiAgICAgIH1cblxuICAgICAgc2VsZi50cmlnZ2VyKFwib25BY3RpdmF0ZVwiKTtcblxuICAgICAgc2VsZi5hZGRFdmVudHMoKTtcbiAgICB9LFxuXG4gICAgLy8gU3RhcnQgY2xvc2luZyBwcm9jZWR1cmVcbiAgICAvLyBUaGlzIHdpbGwgc3RhcnQgXCJ6b29tLW91dFwiIGFuaW1hdGlvbiBpZiBuZWVkZWQgYW5kIGNsZWFuIGV2ZXJ5dGhpbmcgdXAgYWZ0ZXJ3YXJkc1xuICAgIC8vID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuXG4gICAgY2xvc2U6IGZ1bmN0aW9uKGUsIGQpIHtcbiAgICAgIHZhciBzZWxmID0gdGhpcyxcbiAgICAgICAgY3VycmVudCA9IHNlbGYuY3VycmVudCxcbiAgICAgICAgZWZmZWN0LFxuICAgICAgICBkdXJhdGlvbixcbiAgICAgICAgJGNvbnRlbnQsXG4gICAgICAgIGRvbVJlY3QsXG4gICAgICAgIG9wYWNpdHksXG4gICAgICAgIHN0YXJ0LFxuICAgICAgICBlbmQ7XG5cbiAgICAgIHZhciBkb25lID0gZnVuY3Rpb24oKSB7XG4gICAgICAgIHNlbGYuY2xlYW5VcChlKTtcbiAgICAgIH07XG5cbiAgICAgIGlmIChzZWxmLmlzQ2xvc2luZykge1xuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICB9XG5cbiAgICAgIHNlbGYuaXNDbG9zaW5nID0gdHJ1ZTtcblxuICAgICAgLy8gSWYgYmVmb3JlQ2xvc2UgY2FsbGJhY2sgcHJldmVudHMgY2xvc2luZywgbWFrZSBzdXJlIGNvbnRlbnQgaXMgY2VudGVyZWRcbiAgICAgIGlmIChzZWxmLnRyaWdnZXIoXCJiZWZvcmVDbG9zZVwiLCBlKSA9PT0gZmFsc2UpIHtcbiAgICAgICAgc2VsZi5pc0Nsb3NpbmcgPSBmYWxzZTtcblxuICAgICAgICByZXF1ZXN0QUZyYW1lKGZ1bmN0aW9uKCkge1xuICAgICAgICAgIHNlbGYudXBkYXRlKCk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgIH1cblxuICAgICAgLy8gUmVtb3ZlIGFsbCBldmVudHNcbiAgICAgIC8vIElmIHRoZXJlIGFyZSBtdWx0aXBsZSBpbnN0YW5jZXMsIHRoZXkgd2lsbCBiZSBzZXQgYWdhaW4gYnkgXCJhY3RpdmF0ZVwiIG1ldGhvZFxuICAgICAgc2VsZi5yZW1vdmVFdmVudHMoKTtcblxuICAgICAgaWYgKGN1cnJlbnQudGltb3V0cykge1xuICAgICAgICBjbGVhclRpbWVvdXQoY3VycmVudC50aW1vdXRzKTtcbiAgICAgIH1cblxuICAgICAgJGNvbnRlbnQgPSBjdXJyZW50LiRjb250ZW50O1xuICAgICAgZWZmZWN0ID0gY3VycmVudC5vcHRzLmFuaW1hdGlvbkVmZmVjdDtcbiAgICAgIGR1cmF0aW9uID0gJC5pc051bWVyaWMoZCkgPyBkIDogZWZmZWN0ID8gY3VycmVudC5vcHRzLmFuaW1hdGlvbkR1cmF0aW9uIDogMDtcblxuICAgICAgLy8gUmVtb3ZlIG90aGVyIHNsaWRlc1xuICAgICAgY3VycmVudC4kc2xpZGVcbiAgICAgICAgLm9mZih0cmFuc2l0aW9uRW5kKVxuICAgICAgICAucmVtb3ZlQ2xhc3MoXCJmYW5jeWJveC1zbGlkZS0tY29tcGxldGUgZmFuY3lib3gtc2xpZGUtLW5leHQgZmFuY3lib3gtc2xpZGUtLXByZXZpb3VzIGZhbmN5Ym94LWFuaW1hdGVkXCIpO1xuXG4gICAgICBjdXJyZW50LiRzbGlkZVxuICAgICAgICAuc2libGluZ3MoKVxuICAgICAgICAudHJpZ2dlcihcIm9uUmVzZXRcIilcbiAgICAgICAgLnJlbW92ZSgpO1xuXG4gICAgICAvLyBUcmlnZ2VyIGFuaW1hdGlvbnNcbiAgICAgIGlmIChkdXJhdGlvbikge1xuICAgICAgICBzZWxmLiRyZWZzLmNvbnRhaW5lci5yZW1vdmVDbGFzcyhcImZhbmN5Ym94LWlzLW9wZW5cIikuYWRkQ2xhc3MoXCJmYW5jeWJveC1pcy1jbG9zaW5nXCIpO1xuICAgICAgfVxuXG4gICAgICAvLyBDbGVhbiB1cFxuICAgICAgc2VsZi5oaWRlTG9hZGluZyhjdXJyZW50KTtcblxuICAgICAgc2VsZi5oaWRlQ29udHJvbHMoKTtcblxuICAgICAgc2VsZi51cGRhdGVDdXJzb3IoKTtcblxuICAgICAgLy8gQ2hlY2sgaWYgcG9zc2libGUgdG8gem9vbS1vdXRcbiAgICAgIGlmIChcbiAgICAgICAgZWZmZWN0ID09PSBcInpvb21cIiAmJlxuICAgICAgICAhKGUgIT09IHRydWUgJiYgJGNvbnRlbnQgJiYgZHVyYXRpb24gJiYgY3VycmVudC50eXBlID09PSBcImltYWdlXCIgJiYgIWN1cnJlbnQuaGFzRXJyb3IgJiYgKGVuZCA9IHNlbGYuZ2V0VGh1bWJQb3MoY3VycmVudCkpKVxuICAgICAgKSB7XG4gICAgICAgIGVmZmVjdCA9IFwiZmFkZVwiO1xuICAgICAgfVxuXG4gICAgICBpZiAoZWZmZWN0ID09PSBcInpvb21cIikge1xuICAgICAgICAkLmZhbmN5Ym94LnN0b3AoJGNvbnRlbnQpO1xuXG4gICAgICAgIGRvbVJlY3QgPSAkLmZhbmN5Ym94LmdldFRyYW5zbGF0ZSgkY29udGVudCk7XG5cbiAgICAgICAgc3RhcnQgPSB7XG4gICAgICAgICAgdG9wOiBkb21SZWN0LnRvcCxcbiAgICAgICAgICBsZWZ0OiBkb21SZWN0LmxlZnQsXG4gICAgICAgICAgc2NhbGVYOiBkb21SZWN0LndpZHRoIC8gZW5kLndpZHRoLFxuICAgICAgICAgIHNjYWxlWTogZG9tUmVjdC5oZWlnaHQgLyBlbmQuaGVpZ2h0LFxuICAgICAgICAgIHdpZHRoOiBlbmQud2lkdGgsXG4gICAgICAgICAgaGVpZ2h0OiBlbmQuaGVpZ2h0XG4gICAgICAgIH07XG5cbiAgICAgICAgLy8gQ2hlY2sgaWYgd2UgbmVlZCB0byBhbmltYXRlIG9wYWNpdHlcbiAgICAgICAgb3BhY2l0eSA9IGN1cnJlbnQub3B0cy56b29tT3BhY2l0eTtcblxuICAgICAgICBpZiAob3BhY2l0eSA9PSBcImF1dG9cIikge1xuICAgICAgICAgIG9wYWNpdHkgPSBNYXRoLmFicyhjdXJyZW50LndpZHRoIC8gY3VycmVudC5oZWlnaHQgLSBlbmQud2lkdGggLyBlbmQuaGVpZ2h0KSA+IDAuMTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChvcGFjaXR5KSB7XG4gICAgICAgICAgZW5kLm9wYWNpdHkgPSAwO1xuICAgICAgICB9XG5cbiAgICAgICAgJC5mYW5jeWJveC5zZXRUcmFuc2xhdGUoJGNvbnRlbnQsIHN0YXJ0KTtcblxuICAgICAgICBmb3JjZVJlZHJhdygkY29udGVudCk7XG5cbiAgICAgICAgJC5mYW5jeWJveC5hbmltYXRlKCRjb250ZW50LCBlbmQsIGR1cmF0aW9uLCBkb25lKTtcblxuICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgIH1cblxuICAgICAgaWYgKGVmZmVjdCAmJiBkdXJhdGlvbikge1xuICAgICAgICAvLyBJZiBza2lwIGFuaW1hdGlvblxuICAgICAgICBpZiAoZSA9PT0gdHJ1ZSkge1xuICAgICAgICAgIHNldFRpbWVvdXQoZG9uZSwgZHVyYXRpb24pO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICQuZmFuY3lib3guYW5pbWF0ZShcbiAgICAgICAgICAgIGN1cnJlbnQuJHNsaWRlLnJlbW92ZUNsYXNzKFwiZmFuY3lib3gtc2xpZGUtLWN1cnJlbnRcIiksXG4gICAgICAgICAgICBcImZhbmN5Ym94LWFuaW1hdGVkIGZhbmN5Ym94LXNsaWRlLS1wcmV2aW91cyBmYW5jeWJveC1meC1cIiArIGVmZmVjdCxcbiAgICAgICAgICAgIGR1cmF0aW9uLFxuICAgICAgICAgICAgZG9uZVxuICAgICAgICAgICk7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGRvbmUoKTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHRydWU7XG4gICAgfSxcblxuICAgIC8vIEZpbmFsIGFkanVzdG1lbnRzIGFmdGVyIHJlbW92aW5nIHRoZSBpbnN0YW5jZVxuICAgIC8vID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuXG4gICAgY2xlYW5VcDogZnVuY3Rpb24oZSkge1xuICAgICAgdmFyIHNlbGYgPSB0aGlzLFxuICAgICAgICAkYm9keSA9ICQoXCJib2R5XCIpLFxuICAgICAgICBpbnN0YW5jZSxcbiAgICAgICAgc2Nyb2xsVG9wO1xuXG4gICAgICBzZWxmLmN1cnJlbnQuJHNsaWRlLnRyaWdnZXIoXCJvblJlc2V0XCIpO1xuXG4gICAgICBzZWxmLiRyZWZzLmNvbnRhaW5lci5lbXB0eSgpLnJlbW92ZSgpO1xuXG4gICAgICBzZWxmLnRyaWdnZXIoXCJhZnRlckNsb3NlXCIsIGUpO1xuXG4gICAgICAvLyBQbGFjZSBiYWNrIGZvY3VzXG4gICAgICBpZiAoc2VsZi4kbGFzdEZvY3VzICYmICEhc2VsZi5jdXJyZW50Lm9wdHMuYmFja0ZvY3VzKSB7XG4gICAgICAgIHNlbGYuJGxhc3RGb2N1cy50cmlnZ2VyKFwiZm9jdXNcIik7XG4gICAgICB9XG5cbiAgICAgIHNlbGYuY3VycmVudCA9IG51bGw7XG5cbiAgICAgIC8vIENoZWNrIGlmIHRoZXJlIGFyZSBvdGhlciBpbnN0YW5jZXNcbiAgICAgIGluc3RhbmNlID0gJC5mYW5jeWJveC5nZXRJbnN0YW5jZSgpO1xuXG4gICAgICBpZiAoaW5zdGFuY2UpIHtcbiAgICAgICAgaW5zdGFuY2UuYWN0aXZhdGUoKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgICRib2R5LnJlbW92ZUNsYXNzKFwiZmFuY3lib3gtYWN0aXZlIGNvbXBlbnNhdGUtZm9yLXNjcm9sbGJhclwiKTtcblxuICAgICAgICAkKFwiI2ZhbmN5Ym94LXN0eWxlLW5vc2Nyb2xsXCIpLnJlbW92ZSgpO1xuICAgICAgfVxuICAgIH0sXG5cbiAgICAvLyBDYWxsIGNhbGxiYWNrIGFuZCB0cmlnZ2VyIGFuIGV2ZW50XG4gICAgLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuXG4gICAgdHJpZ2dlcjogZnVuY3Rpb24obmFtZSwgc2xpZGUpIHtcbiAgICAgIHZhciBhcmdzID0gQXJyYXkucHJvdG90eXBlLnNsaWNlLmNhbGwoYXJndW1lbnRzLCAxKSxcbiAgICAgICAgc2VsZiA9IHRoaXMsXG4gICAgICAgIG9iaiA9IHNsaWRlICYmIHNsaWRlLm9wdHMgPyBzbGlkZSA6IHNlbGYuY3VycmVudCxcbiAgICAgICAgcmV6O1xuXG4gICAgICBpZiAob2JqKSB7XG4gICAgICAgIGFyZ3MudW5zaGlmdChvYmopO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgb2JqID0gc2VsZjtcbiAgICAgIH1cblxuICAgICAgYXJncy51bnNoaWZ0KHNlbGYpO1xuXG4gICAgICBpZiAoJC5pc0Z1bmN0aW9uKG9iai5vcHRzW25hbWVdKSkge1xuICAgICAgICByZXogPSBvYmoub3B0c1tuYW1lXS5hcHBseShvYmosIGFyZ3MpO1xuICAgICAgfVxuXG4gICAgICBpZiAocmV6ID09PSBmYWxzZSkge1xuICAgICAgICByZXR1cm4gcmV6O1xuICAgICAgfVxuXG4gICAgICBpZiAobmFtZSA9PT0gXCJhZnRlckNsb3NlXCIgfHwgIXNlbGYuJHJlZnMpIHtcbiAgICAgICAgJEQudHJpZ2dlcihuYW1lICsgXCIuZmJcIiwgYXJncyk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBzZWxmLiRyZWZzLmNvbnRhaW5lci50cmlnZ2VyKG5hbWUgKyBcIi5mYlwiLCBhcmdzKTtcbiAgICAgIH1cbiAgICB9LFxuXG4gICAgLy8gVXBkYXRlIGluZm9iYXIgdmFsdWVzLCBuYXZpZ2F0aW9uIGJ1dHRvbiBzdGF0ZXMgYW5kIHJldmVhbCBjYXB0aW9uXG4gICAgLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG5cbiAgICB1cGRhdGVDb250cm9sczogZnVuY3Rpb24oZm9yY2UpIHtcbiAgICAgIHZhciBzZWxmID0gdGhpcyxcbiAgICAgICAgY3VycmVudCA9IHNlbGYuY3VycmVudCxcbiAgICAgICAgaW5kZXggPSBjdXJyZW50LmluZGV4LFxuICAgICAgICBjYXB0aW9uID0gY3VycmVudC5vcHRzLmNhcHRpb24sXG4gICAgICAgICRjb250YWluZXIgPSBzZWxmLiRyZWZzLmNvbnRhaW5lcixcbiAgICAgICAgJGNhcHRpb24gPSBzZWxmLiRyZWZzLmNhcHRpb247XG5cbiAgICAgIC8vIFJlY2FsY3VsYXRlIGNvbnRlbnQgZGltZW5zaW9uc1xuICAgICAgY3VycmVudC4kc2xpZGUudHJpZ2dlcihcInJlZnJlc2hcIik7XG5cbiAgICAgIHNlbGYuJGNhcHRpb24gPSBjYXB0aW9uICYmIGNhcHRpb24ubGVuZ3RoID8gJGNhcHRpb24uaHRtbChjYXB0aW9uKSA6IG51bGw7XG5cbiAgICAgIGlmICghc2VsZi5pc0hpZGRlbkNvbnRyb2xzICYmICFzZWxmLmlzSWRsZSkge1xuICAgICAgICBzZWxmLnNob3dDb250cm9scygpO1xuICAgICAgfVxuXG4gICAgICAvLyBVcGRhdGUgaW5mbyBhbmQgbmF2aWdhdGlvbiBlbGVtZW50c1xuICAgICAgJGNvbnRhaW5lci5maW5kKFwiW2RhdGEtZmFuY3lib3gtY291bnRdXCIpLmh0bWwoc2VsZi5ncm91cC5sZW5ndGgpO1xuICAgICAgJGNvbnRhaW5lci5maW5kKFwiW2RhdGEtZmFuY3lib3gtaW5kZXhdXCIpLmh0bWwoaW5kZXggKyAxKTtcblxuICAgICAgJGNvbnRhaW5lci5maW5kKFwiW2RhdGEtZmFuY3lib3gtcHJldl1cIikudG9nZ2xlQ2xhc3MoXCJkaXNhYmxlZFwiLCAhY3VycmVudC5vcHRzLmxvb3AgJiYgaW5kZXggPD0gMCk7XG4gICAgICAkY29udGFpbmVyLmZpbmQoXCJbZGF0YS1mYW5jeWJveC1uZXh0XVwiKS50b2dnbGVDbGFzcyhcImRpc2FibGVkXCIsICFjdXJyZW50Lm9wdHMubG9vcCAmJiBpbmRleCA+PSBzZWxmLmdyb3VwLmxlbmd0aCAtIDEpO1xuXG4gICAgICBpZiAoY3VycmVudC50eXBlID09PSBcImltYWdlXCIpIHtcbiAgICAgICAgLy8gUmUtZW5hYmxlIGJ1dHRvbnM7IHVwZGF0ZSBkb3dubG9hZCBidXR0b24gc291cmNlXG4gICAgICAgICRjb250YWluZXJcbiAgICAgICAgICAuZmluZChcIltkYXRhLWZhbmN5Ym94LXpvb21dXCIpXG4gICAgICAgICAgLnNob3coKVxuICAgICAgICAgIC5lbmQoKVxuICAgICAgICAgIC5maW5kKFwiW2RhdGEtZmFuY3lib3gtZG93bmxvYWRdXCIpXG4gICAgICAgICAgLmF0dHIoXCJocmVmXCIsIGN1cnJlbnQub3B0cy5pbWFnZS5zcmMgfHwgY3VycmVudC5zcmMpXG4gICAgICAgICAgLnNob3coKTtcbiAgICAgIH0gZWxzZSBpZiAoY3VycmVudC5vcHRzLnRvb2xiYXIpIHtcbiAgICAgICAgJGNvbnRhaW5lci5maW5kKFwiW2RhdGEtZmFuY3lib3gtZG93bmxvYWRdLFtkYXRhLWZhbmN5Ym94LXpvb21dXCIpLmhpZGUoKTtcbiAgICAgIH1cbiAgICB9LFxuXG4gICAgLy8gSGlkZSB0b29sYmFyIGFuZCBjYXB0aW9uXG4gICAgLy8gPT09PT09PT09PT09PT09PT09PT09PT09XG5cbiAgICBoaWRlQ29udHJvbHM6IGZ1bmN0aW9uKCkge1xuICAgICAgdGhpcy5pc0hpZGRlbkNvbnRyb2xzID0gdHJ1ZTtcblxuICAgICAgdGhpcy4kcmVmcy5jb250YWluZXIucmVtb3ZlQ2xhc3MoXCJmYW5jeWJveC1zaG93LWluZm9iYXIgZmFuY3lib3gtc2hvdy10b29sYmFyIGZhbmN5Ym94LXNob3ctY2FwdGlvbiBmYW5jeWJveC1zaG93LW5hdlwiKTtcbiAgICB9LFxuXG4gICAgc2hvd0NvbnRyb2xzOiBmdW5jdGlvbigpIHtcbiAgICAgIHZhciBzZWxmID0gdGhpcyxcbiAgICAgICAgb3B0cyA9IHNlbGYuY3VycmVudCA/IHNlbGYuY3VycmVudC5vcHRzIDogc2VsZi5vcHRzLFxuICAgICAgICAkY29udGFpbmVyID0gc2VsZi4kcmVmcy5jb250YWluZXI7XG5cbiAgICAgIHNlbGYuaXNIaWRkZW5Db250cm9scyA9IGZhbHNlO1xuICAgICAgc2VsZi5pZGxlU2Vjb25kc0NvdW50ZXIgPSAwO1xuXG4gICAgICAkY29udGFpbmVyXG4gICAgICAgIC50b2dnbGVDbGFzcyhcImZhbmN5Ym94LXNob3ctdG9vbGJhclwiLCAhIShvcHRzLnRvb2xiYXIgJiYgb3B0cy5idXR0b25zKSlcbiAgICAgICAgLnRvZ2dsZUNsYXNzKFwiZmFuY3lib3gtc2hvdy1pbmZvYmFyXCIsICEhKG9wdHMuaW5mb2JhciAmJiBzZWxmLmdyb3VwLmxlbmd0aCA+IDEpKVxuICAgICAgICAudG9nZ2xlQ2xhc3MoXCJmYW5jeWJveC1zaG93LW5hdlwiLCAhIShvcHRzLmFycm93cyAmJiBzZWxmLmdyb3VwLmxlbmd0aCA+IDEpKVxuICAgICAgICAudG9nZ2xlQ2xhc3MoXCJmYW5jeWJveC1pcy1tb2RhbFwiLCAhIW9wdHMubW9kYWwpO1xuXG4gICAgICBpZiAoc2VsZi4kY2FwdGlvbikge1xuICAgICAgICAkY29udGFpbmVyLmFkZENsYXNzKFwiZmFuY3lib3gtc2hvdy1jYXB0aW9uIFwiKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgICRjb250YWluZXIucmVtb3ZlQ2xhc3MoXCJmYW5jeWJveC1zaG93LWNhcHRpb25cIik7XG4gICAgICB9XG4gICAgfSxcblxuICAgIC8vIFRvZ2dsZSB0b29sYmFyIGFuZCBjYXB0aW9uXG4gICAgLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT1cblxuICAgIHRvZ2dsZUNvbnRyb2xzOiBmdW5jdGlvbigpIHtcbiAgICAgIGlmICh0aGlzLmlzSGlkZGVuQ29udHJvbHMpIHtcbiAgICAgICAgdGhpcy5zaG93Q29udHJvbHMoKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRoaXMuaGlkZUNvbnRyb2xzKCk7XG4gICAgICB9XG4gICAgfVxuICB9KTtcblxuICAkLmZhbmN5Ym94ID0ge1xuICAgIHZlcnNpb246IFwie2ZhbmN5Ym94LXZlcnNpb259XCIsXG4gICAgZGVmYXVsdHM6IGRlZmF1bHRzLFxuXG4gICAgLy8gR2V0IGN1cnJlbnQgaW5zdGFuY2UgYW5kIGV4ZWN1dGUgYSBjb21tYW5kLlxuICAgIC8vXG4gICAgLy8gRXhhbXBsZXMgb2YgdXNhZ2U6XG4gICAgLy9cbiAgICAvLyAgICRpbnN0YW5jZSA9ICQuZmFuY3lib3guZ2V0SW5zdGFuY2UoKTtcbiAgICAvLyAgICQuZmFuY3lib3guZ2V0SW5zdGFuY2UoKS5qdW1wVG8oIDEgKTtcbiAgICAvLyAgICQuZmFuY3lib3guZ2V0SW5zdGFuY2UoICdqdW1wVG8nLCAxICk7XG4gICAgLy8gICAkLmZhbmN5Ym94LmdldEluc3RhbmNlKCBmdW5jdGlvbigpIHtcbiAgICAvLyAgICAgICBjb25zb2xlLmluZm8oIHRoaXMuY3VyckluZGV4ICk7XG4gICAgLy8gICB9KTtcbiAgICAvLyA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cblxuICAgIGdldEluc3RhbmNlOiBmdW5jdGlvbihjb21tYW5kKSB7XG4gICAgICB2YXIgaW5zdGFuY2UgPSAkKCcuZmFuY3lib3gtY29udGFpbmVyOm5vdChcIi5mYW5jeWJveC1pcy1jbG9zaW5nXCIpOmxhc3QnKS5kYXRhKFwiRmFuY3lCb3hcIiksXG4gICAgICAgIGFyZ3MgPSBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbChhcmd1bWVudHMsIDEpO1xuXG4gICAgICBpZiAoaW5zdGFuY2UgaW5zdGFuY2VvZiBGYW5jeUJveCkge1xuICAgICAgICBpZiAoJC50eXBlKGNvbW1hbmQpID09PSBcInN0cmluZ1wiKSB7XG4gICAgICAgICAgaW5zdGFuY2VbY29tbWFuZF0uYXBwbHkoaW5zdGFuY2UsIGFyZ3MpO1xuICAgICAgICB9IGVsc2UgaWYgKCQudHlwZShjb21tYW5kKSA9PT0gXCJmdW5jdGlvblwiKSB7XG4gICAgICAgICAgY29tbWFuZC5hcHBseShpbnN0YW5jZSwgYXJncyk7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gaW5zdGFuY2U7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9LFxuXG4gICAgLy8gQ3JlYXRlIG5ldyBpbnN0YW5jZVxuICAgIC8vID09PT09PT09PT09PT09PT09PT1cblxuICAgIG9wZW46IGZ1bmN0aW9uKGl0ZW1zLCBvcHRzLCBpbmRleCkge1xuICAgICAgcmV0dXJuIG5ldyBGYW5jeUJveChpdGVtcywgb3B0cywgaW5kZXgpO1xuICAgIH0sXG5cbiAgICAvLyBDbG9zZSBjdXJyZW50IG9yIGFsbCBpbnN0YW5jZXNcbiAgICAvLyA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cblxuICAgIGNsb3NlOiBmdW5jdGlvbihhbGwpIHtcbiAgICAgIHZhciBpbnN0YW5jZSA9IHRoaXMuZ2V0SW5zdGFuY2UoKTtcblxuICAgICAgaWYgKGluc3RhbmNlKSB7XG4gICAgICAgIGluc3RhbmNlLmNsb3NlKCk7XG5cbiAgICAgICAgLy8gVHJ5IHRvIGZpbmQgYW5kIGNsb3NlIG5leHQgaW5zdGFuY2VcblxuICAgICAgICBpZiAoYWxsID09PSB0cnVlKSB7XG4gICAgICAgICAgdGhpcy5jbG9zZSgpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfSxcblxuICAgIC8vIENsb3NlIGFsbCBpbnN0YW5jZXMgYW5kIHVuYmluZCBhbGwgZXZlbnRzXG4gICAgLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cblxuICAgIGRlc3Ryb3k6IGZ1bmN0aW9uKCkge1xuICAgICAgdGhpcy5jbG9zZSh0cnVlKTtcblxuICAgICAgJEQuYWRkKFwiYm9keVwiKS5vZmYoXCJjbGljay5mYi1zdGFydFwiLCBcIioqXCIpO1xuICAgIH0sXG5cbiAgICAvLyBUcnkgdG8gZGV0ZWN0IG1vYmlsZSBkZXZpY2VzXG4gICAgLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuXG4gICAgaXNNb2JpbGU6XG4gICAgICBkb2N1bWVudC5jcmVhdGVUb3VjaCAhPT0gdW5kZWZpbmVkICYmIC9BbmRyb2lkfHdlYk9TfGlQaG9uZXxpUGFkfGlQb2R8QmxhY2tCZXJyeXxJRU1vYmlsZXxPcGVyYSBNaW5pL2kudGVzdChuYXZpZ2F0b3IudXNlckFnZW50KSxcblxuICAgIC8vIERldGVjdCBpZiAndHJhbnNsYXRlM2QnIHN1cHBvcnQgaXMgYXZhaWxhYmxlXG4gICAgLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cblxuICAgIHVzZTNkOiAoZnVuY3Rpb24oKSB7XG4gICAgICB2YXIgZGl2ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImRpdlwiKTtcblxuICAgICAgcmV0dXJuIChcbiAgICAgICAgd2luZG93LmdldENvbXB1dGVkU3R5bGUgJiZcbiAgICAgICAgd2luZG93LmdldENvbXB1dGVkU3R5bGUoZGl2KSAmJlxuICAgICAgICB3aW5kb3cuZ2V0Q29tcHV0ZWRTdHlsZShkaXYpLmdldFByb3BlcnR5VmFsdWUoXCJ0cmFuc2Zvcm1cIikgJiZcbiAgICAgICAgIShkb2N1bWVudC5kb2N1bWVudE1vZGUgJiYgZG9jdW1lbnQuZG9jdW1lbnRNb2RlIDwgMTEpXG4gICAgICApO1xuICAgIH0pKCksXG5cbiAgICAvLyBIZWxwZXIgZnVuY3Rpb24gdG8gZ2V0IGN1cnJlbnQgdmlzdWFsIHN0YXRlIG9mIGFuIGVsZW1lbnRcbiAgICAvLyByZXR1cm5zIGFycmF5WyB0b3AsIGxlZnQsIGhvcml6b250YWwtc2NhbGUsIHZlcnRpY2FsLXNjYWxlLCBvcGFjaXR5IF1cbiAgICAvLyA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cblxuICAgIGdldFRyYW5zbGF0ZTogZnVuY3Rpb24oJGVsKSB7XG4gICAgICB2YXIgZG9tUmVjdDtcblxuICAgICAgaWYgKCEkZWwgfHwgISRlbC5sZW5ndGgpIHtcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgfVxuXG4gICAgICBkb21SZWN0ID0gJGVsWzBdLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpO1xuXG4gICAgICByZXR1cm4ge1xuICAgICAgICB0b3A6IGRvbVJlY3QudG9wIHx8IDAsXG4gICAgICAgIGxlZnQ6IGRvbVJlY3QubGVmdCB8fCAwLFxuICAgICAgICB3aWR0aDogZG9tUmVjdC53aWR0aCxcbiAgICAgICAgaGVpZ2h0OiBkb21SZWN0LmhlaWdodCxcbiAgICAgICAgb3BhY2l0eTogcGFyc2VGbG9hdCgkZWwuY3NzKFwib3BhY2l0eVwiKSlcbiAgICAgIH07XG4gICAgfSxcblxuICAgIC8vIFNob3J0Y3V0IGZvciBzZXR0aW5nIFwidHJhbnNsYXRlM2RcIiBwcm9wZXJ0aWVzIGZvciBlbGVtZW50XG4gICAgLy8gQ2FuIHNldCBiZSB1c2VkIHRvIHNldCBvcGFjaXR5LCB0b29cbiAgICAvLyA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuXG4gICAgc2V0VHJhbnNsYXRlOiBmdW5jdGlvbigkZWwsIHByb3BzKSB7XG4gICAgICB2YXIgc3RyID0gXCJcIixcbiAgICAgICAgY3NzID0ge307XG5cbiAgICAgIGlmICghJGVsIHx8ICFwcm9wcykge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIGlmIChwcm9wcy5sZWZ0ICE9PSB1bmRlZmluZWQgfHwgcHJvcHMudG9wICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgc3RyID1cbiAgICAgICAgICAocHJvcHMubGVmdCA9PT0gdW5kZWZpbmVkID8gJGVsLnBvc2l0aW9uKCkubGVmdCA6IHByb3BzLmxlZnQpICtcbiAgICAgICAgICBcInB4LCBcIiArXG4gICAgICAgICAgKHByb3BzLnRvcCA9PT0gdW5kZWZpbmVkID8gJGVsLnBvc2l0aW9uKCkudG9wIDogcHJvcHMudG9wKSArXG4gICAgICAgICAgXCJweFwiO1xuXG4gICAgICAgIGlmICh0aGlzLnVzZTNkKSB7XG4gICAgICAgICAgc3RyID0gXCJ0cmFuc2xhdGUzZChcIiArIHN0ciArIFwiLCAwcHgpXCI7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgc3RyID0gXCJ0cmFuc2xhdGUoXCIgKyBzdHIgKyBcIilcIjtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBpZiAocHJvcHMuc2NhbGVYICE9PSB1bmRlZmluZWQgJiYgcHJvcHMuc2NhbGVZICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgc3RyID0gKHN0ci5sZW5ndGggPyBzdHIgKyBcIiBcIiA6IFwiXCIpICsgXCJzY2FsZShcIiArIHByb3BzLnNjYWxlWCArIFwiLCBcIiArIHByb3BzLnNjYWxlWSArIFwiKVwiO1xuICAgICAgfVxuXG4gICAgICBpZiAoc3RyLmxlbmd0aCkge1xuICAgICAgICBjc3MudHJhbnNmb3JtID0gc3RyO1xuICAgICAgfVxuXG4gICAgICBpZiAocHJvcHMub3BhY2l0eSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgIGNzcy5vcGFjaXR5ID0gcHJvcHMub3BhY2l0eTtcbiAgICAgIH1cblxuICAgICAgaWYgKHByb3BzLndpZHRoICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgY3NzLndpZHRoID0gcHJvcHMud2lkdGg7XG4gICAgICB9XG5cbiAgICAgIGlmIChwcm9wcy5oZWlnaHQgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICBjc3MuaGVpZ2h0ID0gcHJvcHMuaGVpZ2h0O1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gJGVsLmNzcyhjc3MpO1xuICAgIH0sXG5cbiAgICAvLyBTaW1wbGUgQ1NTIHRyYW5zaXRpb24gaGFuZGxlclxuICAgIC8vID09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG5cbiAgICBhbmltYXRlOiBmdW5jdGlvbigkZWwsIHRvLCBkdXJhdGlvbiwgY2FsbGJhY2ssIGxlYXZlQW5pbWF0aW9uTmFtZSkge1xuICAgICAgdmFyIGZpbmFsID0gZmFsc2U7XG5cbiAgICAgIGlmICgkLmlzRnVuY3Rpb24oZHVyYXRpb24pKSB7XG4gICAgICAgIGNhbGxiYWNrID0gZHVyYXRpb247XG4gICAgICAgIGR1cmF0aW9uID0gbnVsbDtcbiAgICAgIH1cblxuICAgICAgaWYgKCEkLmlzUGxhaW5PYmplY3QodG8pKSB7XG4gICAgICAgICRlbC5yZW1vdmVBdHRyKFwic3R5bGVcIik7XG4gICAgICB9XG5cbiAgICAgICQuZmFuY3lib3guc3RvcCgkZWwpO1xuXG4gICAgICAkZWwub24odHJhbnNpdGlvbkVuZCwgZnVuY3Rpb24oZSkge1xuICAgICAgICAvLyBTa2lwIGV2ZW50cyBmcm9tIGNoaWxkIGVsZW1lbnRzIGFuZCB6LWluZGV4IGNoYW5nZVxuICAgICAgICBpZiAoZSAmJiBlLm9yaWdpbmFsRXZlbnQgJiYgKCEkZWwuaXMoZS5vcmlnaW5hbEV2ZW50LnRhcmdldCkgfHwgZS5vcmlnaW5hbEV2ZW50LnByb3BlcnR5TmFtZSA9PSBcInotaW5kZXhcIikpIHtcbiAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICAkLmZhbmN5Ym94LnN0b3AoJGVsKTtcblxuICAgICAgICBpZiAoZmluYWwpIHtcbiAgICAgICAgICAkLmZhbmN5Ym94LnNldFRyYW5zbGF0ZSgkZWwsIGZpbmFsKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmICgkLmlzUGxhaW5PYmplY3QodG8pKSB7XG4gICAgICAgICAgaWYgKGxlYXZlQW5pbWF0aW9uTmFtZSA9PT0gZmFsc2UpIHtcbiAgICAgICAgICAgICRlbC5yZW1vdmVBdHRyKFwic3R5bGVcIik7XG4gICAgICAgICAgfVxuICAgICAgICB9IGVsc2UgaWYgKGxlYXZlQW5pbWF0aW9uTmFtZSAhPT0gdHJ1ZSkge1xuICAgICAgICAgICRlbC5yZW1vdmVDbGFzcyh0byk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoJC5pc0Z1bmN0aW9uKGNhbGxiYWNrKSkge1xuICAgICAgICAgIGNhbGxiYWNrKGUpO1xuICAgICAgICB9XG4gICAgICB9KTtcblxuICAgICAgaWYgKCQuaXNOdW1lcmljKGR1cmF0aW9uKSkge1xuICAgICAgICAkZWwuY3NzKFwidHJhbnNpdGlvbi1kdXJhdGlvblwiLCBkdXJhdGlvbiArIFwibXNcIik7XG4gICAgICB9XG5cbiAgICAgIC8vIFN0YXJ0IGFuaW1hdGlvbiBieSBjaGFuZ2luZyBDU1MgcHJvcGVydGllcyBvciBjbGFzcyBuYW1lXG4gICAgICBpZiAoJC5pc1BsYWluT2JqZWN0KHRvKSkge1xuICAgICAgICBpZiAodG8uc2NhbGVYICE9PSB1bmRlZmluZWQgJiYgdG8uc2NhbGVZICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICBmaW5hbCA9ICQuZXh0ZW5kKHt9LCB0bywge1xuICAgICAgICAgICAgd2lkdGg6ICRlbC53aWR0aCgpICogdG8uc2NhbGVYLFxuICAgICAgICAgICAgaGVpZ2h0OiAkZWwuaGVpZ2h0KCkgKiB0by5zY2FsZVksXG4gICAgICAgICAgICBzY2FsZVg6IDEsXG4gICAgICAgICAgICBzY2FsZVk6IDFcbiAgICAgICAgICB9KTtcblxuICAgICAgICAgIGRlbGV0ZSB0by53aWR0aDtcbiAgICAgICAgICBkZWxldGUgdG8uaGVpZ2h0O1xuXG4gICAgICAgICAgaWYgKCRlbC5wYXJlbnQoKS5oYXNDbGFzcyhcImZhbmN5Ym94LXNsaWRlLS1pbWFnZVwiKSkge1xuICAgICAgICAgICAgJGVsLnBhcmVudCgpLmFkZENsYXNzKFwiZmFuY3lib3gtaXMtc2NhbGluZ1wiKTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICAkLmZhbmN5Ym94LnNldFRyYW5zbGF0ZSgkZWwsIHRvKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgICRlbC5hZGRDbGFzcyh0byk7XG4gICAgICB9XG5cbiAgICAgIC8vIE1ha2Ugc3VyZSB0aGF0IGB0cmFuc2l0aW9uZW5kYCBjYWxsYmFjayBnZXRzIGZpcmVkXG4gICAgICAkZWwuZGF0YShcbiAgICAgICAgXCJ0aW1lclwiLFxuICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuICAgICAgICAgICRlbC50cmlnZ2VyKFwidHJhbnNpdGlvbmVuZFwiKTtcbiAgICAgICAgfSwgZHVyYXRpb24gKyAxNilcbiAgICAgICk7XG4gICAgfSxcblxuICAgIHN0b3A6IGZ1bmN0aW9uKCRlbCkge1xuICAgICAgaWYgKCRlbCAmJiAkZWwubGVuZ3RoKSB7XG4gICAgICAgIGNsZWFyVGltZW91dCgkZWwuZGF0YShcInRpbWVyXCIpKTtcblxuICAgICAgICAkZWwub2ZmKFwidHJhbnNpdGlvbmVuZFwiKS5jc3MoXCJ0cmFuc2l0aW9uLWR1cmF0aW9uXCIsIFwiXCIpO1xuXG4gICAgICAgICRlbC5wYXJlbnQoKS5yZW1vdmVDbGFzcyhcImZhbmN5Ym94LWlzLXNjYWxpbmdcIik7XG4gICAgICB9XG4gICAgfVxuICB9O1xuXG4gIC8vIERlZmF1bHQgY2xpY2sgaGFuZGxlciBmb3IgXCJmYW5jeWJveGVkXCIgbGlua3NcbiAgLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cblxuICBmdW5jdGlvbiBfcnVuKGUsIG9wdHMpIHtcbiAgICB2YXIgaXRlbXMgPSBbXSxcbiAgICAgIGluZGV4ID0gMCxcbiAgICAgICR0YXJnZXQsXG4gICAgICB2YWx1ZTtcblxuICAgIC8vIEF2b2lkIG9wZW5pbmcgbXVsdGlwbGUgdGltZXNcbiAgICBpZiAoZSAmJiBlLmlzRGVmYXVsdFByZXZlbnRlZCgpKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgb3B0cyA9IGUgJiYgZS5kYXRhID8gZS5kYXRhLm9wdGlvbnMgOiBvcHRzIHx8IHt9O1xuXG4gICAgJHRhcmdldCA9IG9wdHMuJHRhcmdldCB8fCAkKGUuY3VycmVudFRhcmdldCk7XG4gICAgdmFsdWUgPSAkdGFyZ2V0LmF0dHIoXCJkYXRhLWZhbmN5Ym94XCIpIHx8IFwiXCI7XG5cbiAgICAvLyBHZXQgYWxsIHJlbGF0ZWQgaXRlbXMgYW5kIGZpbmQgaW5kZXggZm9yIGNsaWNrZWQgb25lXG4gICAgaWYgKHZhbHVlKSB7XG4gICAgICBpdGVtcyA9IG9wdHMuc2VsZWN0b3IgPyAkKG9wdHMuc2VsZWN0b3IpIDogZS5kYXRhID8gZS5kYXRhLml0ZW1zIDogW107XG4gICAgICBpdGVtcyA9IGl0ZW1zLmxlbmd0aCA/IGl0ZW1zLmZpbHRlcignW2RhdGEtZmFuY3lib3g9XCInICsgdmFsdWUgKyAnXCJdJykgOiAkKCdbZGF0YS1mYW5jeWJveD1cIicgKyB2YWx1ZSArICdcIl0nKTtcblxuICAgICAgaW5kZXggPSBpdGVtcy5pbmRleCgkdGFyZ2V0KTtcblxuICAgICAgLy8gU29tZXRpbWVzIGN1cnJlbnQgaXRlbSBjYW4gbm90IGJlIGZvdW5kIChmb3IgZXhhbXBsZSwgaWYgc29tZSBzY3JpcHQgY2xvbmVzIGl0ZW1zKVxuICAgICAgaWYgKGluZGV4IDwgMCkge1xuICAgICAgICBpbmRleCA9IDA7XG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIGl0ZW1zID0gWyR0YXJnZXRdO1xuICAgIH1cblxuICAgICQuZmFuY3lib3gub3BlbihpdGVtcywgb3B0cywgaW5kZXgpO1xuICB9XG5cbiAgLy8gQ3JlYXRlIGEgalF1ZXJ5IHBsdWdpblxuICAvLyA9PT09PT09PT09PT09PT09PT09PT09XG5cbiAgJC5mbi5mYW5jeWJveCA9IGZ1bmN0aW9uKG9wdGlvbnMpIHtcbiAgICB2YXIgc2VsZWN0b3I7XG5cbiAgICBvcHRpb25zID0gb3B0aW9ucyB8fCB7fTtcbiAgICBzZWxlY3RvciA9IG9wdGlvbnMuc2VsZWN0b3IgfHwgZmFsc2U7XG5cbiAgICBpZiAoc2VsZWN0b3IpIHtcbiAgICAgIC8vIFVzZSBib2R5IGVsZW1lbnQgaW5zdGVhZCBvZiBkb2N1bWVudCBzbyBpdCBleGVjdXRlcyBmaXJzdFxuICAgICAgJChcImJvZHlcIilcbiAgICAgICAgLm9mZihcImNsaWNrLmZiLXN0YXJ0XCIsIHNlbGVjdG9yKVxuICAgICAgICAub24oXCJjbGljay5mYi1zdGFydFwiLCBzZWxlY3Rvciwge29wdGlvbnM6IG9wdGlvbnN9LCBfcnVuKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5vZmYoXCJjbGljay5mYi1zdGFydFwiKS5vbihcbiAgICAgICAgXCJjbGljay5mYi1zdGFydFwiLFxuICAgICAgICB7XG4gICAgICAgICAgaXRlbXM6IHRoaXMsXG4gICAgICAgICAgb3B0aW9uczogb3B0aW9uc1xuICAgICAgICB9LFxuICAgICAgICBfcnVuXG4gICAgICApO1xuICAgIH1cblxuICAgIHJldHVybiB0aGlzO1xuICB9O1xuXG4gIC8vIFNlbGYgaW5pdGlhbGl6aW5nIHBsdWdpbiBmb3IgYWxsIGVsZW1lbnRzIGhhdmluZyBgZGF0YS1mYW5jeWJveGAgYXR0cmlidXRlXG4gIC8vID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG5cbiAgJEQub24oXCJjbGljay5mYi1zdGFydFwiLCBcIltkYXRhLWZhbmN5Ym94XVwiLCBfcnVuKTtcblxuICAvLyBFbmFibGUgXCJ0cmlnZ2VyIGVsZW1lbnRzXCJcbiAgLy8gPT09PT09PT09PT09PT09PT09PT09PT09PVxuXG4gICRELm9uKFwiY2xpY2suZmItc3RhcnRcIiwgXCJbZGF0YS10cmlnZ2VyXVwiLCBmdW5jdGlvbihlKSB7XG4gICAgX3J1bihlLCB7XG4gICAgICAkdGFyZ2V0OiAkKCdbZGF0YS1mYW5jeWJveD1cIicgKyAkKGUuY3VycmVudFRhcmdldCkuYXR0cihcImRhdGEtdHJpZ2dlclwiKSArICdcIl0nKS5lcSgkKGUuY3VycmVudFRhcmdldCkuYXR0cihcImRhdGEtaW5kZXhcIikgfHwgMCksXG4gICAgICAkdHJpZ2dlcjogJCh0aGlzKVxuICAgIH0pO1xuICB9KTtcbn0pKHdpbmRvdywgZG9jdW1lbnQsIHdpbmRvdy5qUXVlcnkgfHwgalF1ZXJ5KTtcbiJdLCJmaWxlIjoiY29yZS5qcyJ9
