// ==========================================================================
//
// SlideShow
// Enables slideshow functionality
//
// Example of usage:
// $.fancybox.getInstance().SlideShow.start()
//
// ==========================================================================
(function(document, $) {
  "use strict";

  $.extend(true, $.fancybox.defaults, {
    btnTpl: {
      slideShow:
        '<button data-fancybox-play class="fancybox-button fancybox-button--play" title="{{PLAY_START}}">' +
        '<svg viewBox="0 0 40 40">' +
        '<path d="M13,12 L27,20 L13,27 Z" />' +
        '<path d="M15,10 v19 M23,10 v19" />' +
        "</svg>" +
        "</button>"
    },
    slideShow: {
      autoStart: false,
      speed: 3000
    }
  });

  var SlideShow = function(instance) {
    this.instance = instance;
    this.init();
  };

  $.extend(SlideShow.prototype, {
    timer: null,
    isActive: false,
    $button: null,

    init: function() {
      var self = this;

      self.$button = self.instance.$refs.toolbar.find("[data-fancybox-play]").on("click", function() {
        self.toggle();
      });

      if (self.instance.group.length < 2 || !self.instance.group[self.instance.currIndex].opts.slideShow) {
        self.$button.hide();
      }
    },

    set: function(force) {
      var self = this;

      // Check if reached last element
      if (
        self.instance &&
        self.instance.current &&
        (force === true || self.instance.current.opts.loop || self.instance.currIndex < self.instance.group.length - 1)
      ) {
        self.timer = setTimeout(function() {
          if (self.isActive) {
            self.instance.jumpTo((self.instance.currIndex + 1) % self.instance.group.length);
          }
        }, self.instance.current.opts.slideShow.speed);
      } else {
        self.stop();
        self.instance.idleSecondsCounter = 0;
        self.instance.showControls();
      }
    },

    clear: function() {
      var self = this;

      clearTimeout(self.timer);

      self.timer = null;
    },

    start: function() {
      var self = this;
      var current = self.instance.current;

      if (current) {
        self.isActive = true;

        self.$button
          .attr("title", current.opts.i18n[current.opts.lang].PLAY_STOP)
          .removeClass("fancybox-button--play")
          .addClass("fancybox-button--pause");

        self.set(true);
      }
    },

    stop: function() {
      var self = this;
      var current = self.instance.current;

      self.clear();

      self.$button
        .attr("title", current.opts.i18n[current.opts.lang].PLAY_START)
        .removeClass("fancybox-button--pause")
        .addClass("fancybox-button--play");

      self.isActive = false;
    },

    toggle: function() {
      var self = this;

      if (self.isActive) {
        self.stop();
      } else {
        self.start();
      }
    }
  });

  $(document).on({
    "onInit.fb": function(e, instance) {
      if (instance && !instance.SlideShow) {
        instance.SlideShow = new SlideShow(instance);
      }
    },

    "beforeShow.fb": function(e, instance, current, firstRun) {
      var SlideShow = instance && instance.SlideShow;

      if (firstRun) {
        if (SlideShow && current.opts.slideShow.autoStart) {
          SlideShow.start();
        }
      } else if (SlideShow && SlideShow.isActive) {
        SlideShow.clear();
      }
    },

    "afterShow.fb": function(e, instance, current) {
      var SlideShow = instance && instance.SlideShow;

      if (SlideShow && SlideShow.isActive) {
        SlideShow.set();
      }
    },

    "afterKeydown.fb": function(e, instance, current, keypress, keycode) {
      var SlideShow = instance && instance.SlideShow;

      // "P" or Spacebar
      if (SlideShow && current.opts.slideShow && (keycode === 80 || keycode === 32) && !$(document.activeElement).is("button,a,input")) {
        keypress.preventDefault();

        SlideShow.toggle();
      }
    },

    "beforeClose.fb onDeactivate.fb": function(e, instance) {
      var SlideShow = instance && instance.SlideShow;

      if (SlideShow) {
        SlideShow.stop();
      }
    }
  });

  // Page Visibility API to pause slideshow when window is not active
  $(document).on("visibilitychange", function() {
    var instance = $.fancybox.getInstance();
    var SlideShow = instance && instance.SlideShow;

    if (SlideShow && SlideShow.isActive) {
      if (document.hidden) {
        SlideShow.clear();
      } else {
        SlideShow.set();
      }
    }
  });
})(document, window.jQuery || jQuery);

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJzbGlkZXNob3cuanMiXSwic291cmNlc0NvbnRlbnQiOlsiLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cbi8vXG4vLyBTbGlkZVNob3dcbi8vIEVuYWJsZXMgc2xpZGVzaG93IGZ1bmN0aW9uYWxpdHlcbi8vXG4vLyBFeGFtcGxlIG9mIHVzYWdlOlxuLy8gJC5mYW5jeWJveC5nZXRJbnN0YW5jZSgpLlNsaWRlU2hvdy5zdGFydCgpXG4vL1xuLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cbihmdW5jdGlvbihkb2N1bWVudCwgJCkge1xuICBcInVzZSBzdHJpY3RcIjtcblxuICAkLmV4dGVuZCh0cnVlLCAkLmZhbmN5Ym94LmRlZmF1bHRzLCB7XG4gICAgYnRuVHBsOiB7XG4gICAgICBzbGlkZVNob3c6XG4gICAgICAgICc8YnV0dG9uIGRhdGEtZmFuY3lib3gtcGxheSBjbGFzcz1cImZhbmN5Ym94LWJ1dHRvbiBmYW5jeWJveC1idXR0b24tLXBsYXlcIiB0aXRsZT1cInt7UExBWV9TVEFSVH19XCI+JyArXG4gICAgICAgICc8c3ZnIHZpZXdCb3g9XCIwIDAgNDAgNDBcIj4nICtcbiAgICAgICAgJzxwYXRoIGQ9XCJNMTMsMTIgTDI3LDIwIEwxMywyNyBaXCIgLz4nICtcbiAgICAgICAgJzxwYXRoIGQ9XCJNMTUsMTAgdjE5IE0yMywxMCB2MTlcIiAvPicgK1xuICAgICAgICBcIjwvc3ZnPlwiICtcbiAgICAgICAgXCI8L2J1dHRvbj5cIlxuICAgIH0sXG4gICAgc2xpZGVTaG93OiB7XG4gICAgICBhdXRvU3RhcnQ6IGZhbHNlLFxuICAgICAgc3BlZWQ6IDMwMDBcbiAgICB9XG4gIH0pO1xuXG4gIHZhciBTbGlkZVNob3cgPSBmdW5jdGlvbihpbnN0YW5jZSkge1xuICAgIHRoaXMuaW5zdGFuY2UgPSBpbnN0YW5jZTtcbiAgICB0aGlzLmluaXQoKTtcbiAgfTtcblxuICAkLmV4dGVuZChTbGlkZVNob3cucHJvdG90eXBlLCB7XG4gICAgdGltZXI6IG51bGwsXG4gICAgaXNBY3RpdmU6IGZhbHNlLFxuICAgICRidXR0b246IG51bGwsXG5cbiAgICBpbml0OiBmdW5jdGlvbigpIHtcbiAgICAgIHZhciBzZWxmID0gdGhpcztcblxuICAgICAgc2VsZi4kYnV0dG9uID0gc2VsZi5pbnN0YW5jZS4kcmVmcy50b29sYmFyLmZpbmQoXCJbZGF0YS1mYW5jeWJveC1wbGF5XVwiKS5vbihcImNsaWNrXCIsIGZ1bmN0aW9uKCkge1xuICAgICAgICBzZWxmLnRvZ2dsZSgpO1xuICAgICAgfSk7XG5cbiAgICAgIGlmIChzZWxmLmluc3RhbmNlLmdyb3VwLmxlbmd0aCA8IDIgfHwgIXNlbGYuaW5zdGFuY2UuZ3JvdXBbc2VsZi5pbnN0YW5jZS5jdXJySW5kZXhdLm9wdHMuc2xpZGVTaG93KSB7XG4gICAgICAgIHNlbGYuJGJ1dHRvbi5oaWRlKCk7XG4gICAgICB9XG4gICAgfSxcblxuICAgIHNldDogZnVuY3Rpb24oZm9yY2UpIHtcbiAgICAgIHZhciBzZWxmID0gdGhpcztcblxuICAgICAgLy8gQ2hlY2sgaWYgcmVhY2hlZCBsYXN0IGVsZW1lbnRcbiAgICAgIGlmIChcbiAgICAgICAgc2VsZi5pbnN0YW5jZSAmJlxuICAgICAgICBzZWxmLmluc3RhbmNlLmN1cnJlbnQgJiZcbiAgICAgICAgKGZvcmNlID09PSB0cnVlIHx8IHNlbGYuaW5zdGFuY2UuY3VycmVudC5vcHRzLmxvb3AgfHwgc2VsZi5pbnN0YW5jZS5jdXJySW5kZXggPCBzZWxmLmluc3RhbmNlLmdyb3VwLmxlbmd0aCAtIDEpXG4gICAgICApIHtcbiAgICAgICAgc2VsZi50aW1lciA9IHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XG4gICAgICAgICAgaWYgKHNlbGYuaXNBY3RpdmUpIHtcbiAgICAgICAgICAgIHNlbGYuaW5zdGFuY2UuanVtcFRvKChzZWxmLmluc3RhbmNlLmN1cnJJbmRleCArIDEpICUgc2VsZi5pbnN0YW5jZS5ncm91cC5sZW5ndGgpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSwgc2VsZi5pbnN0YW5jZS5jdXJyZW50Lm9wdHMuc2xpZGVTaG93LnNwZWVkKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHNlbGYuc3RvcCgpO1xuICAgICAgICBzZWxmLmluc3RhbmNlLmlkbGVTZWNvbmRzQ291bnRlciA9IDA7XG4gICAgICAgIHNlbGYuaW5zdGFuY2Uuc2hvd0NvbnRyb2xzKCk7XG4gICAgICB9XG4gICAgfSxcblxuICAgIGNsZWFyOiBmdW5jdGlvbigpIHtcbiAgICAgIHZhciBzZWxmID0gdGhpcztcblxuICAgICAgY2xlYXJUaW1lb3V0KHNlbGYudGltZXIpO1xuXG4gICAgICBzZWxmLnRpbWVyID0gbnVsbDtcbiAgICB9LFxuXG4gICAgc3RhcnQ6IGZ1bmN0aW9uKCkge1xuICAgICAgdmFyIHNlbGYgPSB0aGlzO1xuICAgICAgdmFyIGN1cnJlbnQgPSBzZWxmLmluc3RhbmNlLmN1cnJlbnQ7XG5cbiAgICAgIGlmIChjdXJyZW50KSB7XG4gICAgICAgIHNlbGYuaXNBY3RpdmUgPSB0cnVlO1xuXG4gICAgICAgIHNlbGYuJGJ1dHRvblxuICAgICAgICAgIC5hdHRyKFwidGl0bGVcIiwgY3VycmVudC5vcHRzLmkxOG5bY3VycmVudC5vcHRzLmxhbmddLlBMQVlfU1RPUClcbiAgICAgICAgICAucmVtb3ZlQ2xhc3MoXCJmYW5jeWJveC1idXR0b24tLXBsYXlcIilcbiAgICAgICAgICAuYWRkQ2xhc3MoXCJmYW5jeWJveC1idXR0b24tLXBhdXNlXCIpO1xuXG4gICAgICAgIHNlbGYuc2V0KHRydWUpO1xuICAgICAgfVxuICAgIH0sXG5cbiAgICBzdG9wOiBmdW5jdGlvbigpIHtcbiAgICAgIHZhciBzZWxmID0gdGhpcztcbiAgICAgIHZhciBjdXJyZW50ID0gc2VsZi5pbnN0YW5jZS5jdXJyZW50O1xuXG4gICAgICBzZWxmLmNsZWFyKCk7XG5cbiAgICAgIHNlbGYuJGJ1dHRvblxuICAgICAgICAuYXR0cihcInRpdGxlXCIsIGN1cnJlbnQub3B0cy5pMThuW2N1cnJlbnQub3B0cy5sYW5nXS5QTEFZX1NUQVJUKVxuICAgICAgICAucmVtb3ZlQ2xhc3MoXCJmYW5jeWJveC1idXR0b24tLXBhdXNlXCIpXG4gICAgICAgIC5hZGRDbGFzcyhcImZhbmN5Ym94LWJ1dHRvbi0tcGxheVwiKTtcblxuICAgICAgc2VsZi5pc0FjdGl2ZSA9IGZhbHNlO1xuICAgIH0sXG5cbiAgICB0b2dnbGU6IGZ1bmN0aW9uKCkge1xuICAgICAgdmFyIHNlbGYgPSB0aGlzO1xuXG4gICAgICBpZiAoc2VsZi5pc0FjdGl2ZSkge1xuICAgICAgICBzZWxmLnN0b3AoKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHNlbGYuc3RhcnQoKTtcbiAgICAgIH1cbiAgICB9XG4gIH0pO1xuXG4gICQoZG9jdW1lbnQpLm9uKHtcbiAgICBcIm9uSW5pdC5mYlwiOiBmdW5jdGlvbihlLCBpbnN0YW5jZSkge1xuICAgICAgaWYgKGluc3RhbmNlICYmICFpbnN0YW5jZS5TbGlkZVNob3cpIHtcbiAgICAgICAgaW5zdGFuY2UuU2xpZGVTaG93ID0gbmV3IFNsaWRlU2hvdyhpbnN0YW5jZSk7XG4gICAgICB9XG4gICAgfSxcblxuICAgIFwiYmVmb3JlU2hvdy5mYlwiOiBmdW5jdGlvbihlLCBpbnN0YW5jZSwgY3VycmVudCwgZmlyc3RSdW4pIHtcbiAgICAgIHZhciBTbGlkZVNob3cgPSBpbnN0YW5jZSAmJiBpbnN0YW5jZS5TbGlkZVNob3c7XG5cbiAgICAgIGlmIChmaXJzdFJ1bikge1xuICAgICAgICBpZiAoU2xpZGVTaG93ICYmIGN1cnJlbnQub3B0cy5zbGlkZVNob3cuYXV0b1N0YXJ0KSB7XG4gICAgICAgICAgU2xpZGVTaG93LnN0YXJ0KCk7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSBpZiAoU2xpZGVTaG93ICYmIFNsaWRlU2hvdy5pc0FjdGl2ZSkge1xuICAgICAgICBTbGlkZVNob3cuY2xlYXIoKTtcbiAgICAgIH1cbiAgICB9LFxuXG4gICAgXCJhZnRlclNob3cuZmJcIjogZnVuY3Rpb24oZSwgaW5zdGFuY2UsIGN1cnJlbnQpIHtcbiAgICAgIHZhciBTbGlkZVNob3cgPSBpbnN0YW5jZSAmJiBpbnN0YW5jZS5TbGlkZVNob3c7XG5cbiAgICAgIGlmIChTbGlkZVNob3cgJiYgU2xpZGVTaG93LmlzQWN0aXZlKSB7XG4gICAgICAgIFNsaWRlU2hvdy5zZXQoKTtcbiAgICAgIH1cbiAgICB9LFxuXG4gICAgXCJhZnRlcktleWRvd24uZmJcIjogZnVuY3Rpb24oZSwgaW5zdGFuY2UsIGN1cnJlbnQsIGtleXByZXNzLCBrZXljb2RlKSB7XG4gICAgICB2YXIgU2xpZGVTaG93ID0gaW5zdGFuY2UgJiYgaW5zdGFuY2UuU2xpZGVTaG93O1xuXG4gICAgICAvLyBcIlBcIiBvciBTcGFjZWJhclxuICAgICAgaWYgKFNsaWRlU2hvdyAmJiBjdXJyZW50Lm9wdHMuc2xpZGVTaG93ICYmIChrZXljb2RlID09PSA4MCB8fCBrZXljb2RlID09PSAzMikgJiYgISQoZG9jdW1lbnQuYWN0aXZlRWxlbWVudCkuaXMoXCJidXR0b24sYSxpbnB1dFwiKSkge1xuICAgICAgICBrZXlwcmVzcy5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgICAgIFNsaWRlU2hvdy50b2dnbGUoKTtcbiAgICAgIH1cbiAgICB9LFxuXG4gICAgXCJiZWZvcmVDbG9zZS5mYiBvbkRlYWN0aXZhdGUuZmJcIjogZnVuY3Rpb24oZSwgaW5zdGFuY2UpIHtcbiAgICAgIHZhciBTbGlkZVNob3cgPSBpbnN0YW5jZSAmJiBpbnN0YW5jZS5TbGlkZVNob3c7XG5cbiAgICAgIGlmIChTbGlkZVNob3cpIHtcbiAgICAgICAgU2xpZGVTaG93LnN0b3AoKTtcbiAgICAgIH1cbiAgICB9XG4gIH0pO1xuXG4gIC8vIFBhZ2UgVmlzaWJpbGl0eSBBUEkgdG8gcGF1c2Ugc2xpZGVzaG93IHdoZW4gd2luZG93IGlzIG5vdCBhY3RpdmVcbiAgJChkb2N1bWVudCkub24oXCJ2aXNpYmlsaXR5Y2hhbmdlXCIsIGZ1bmN0aW9uKCkge1xuICAgIHZhciBpbnN0YW5jZSA9ICQuZmFuY3lib3guZ2V0SW5zdGFuY2UoKTtcbiAgICB2YXIgU2xpZGVTaG93ID0gaW5zdGFuY2UgJiYgaW5zdGFuY2UuU2xpZGVTaG93O1xuXG4gICAgaWYgKFNsaWRlU2hvdyAmJiBTbGlkZVNob3cuaXNBY3RpdmUpIHtcbiAgICAgIGlmIChkb2N1bWVudC5oaWRkZW4pIHtcbiAgICAgICAgU2xpZGVTaG93LmNsZWFyKCk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBTbGlkZVNob3cuc2V0KCk7XG4gICAgICB9XG4gICAgfVxuICB9KTtcbn0pKGRvY3VtZW50LCB3aW5kb3cualF1ZXJ5IHx8IGpRdWVyeSk7XG4iXSwiZmlsZSI6InNsaWRlc2hvdy5qcyJ9
