

$(document).ready(function() {

    $(".center").slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 651,
                settings: "unslick"
            }
        ]

    });
});


/*   Slider Owl Carousel 2   */

$(function() {
    var owl = $('.owl-carousel'),
        owlOptions = {

            items: 1,
            singleItems: true,
            nav: false,
            dots: true,
            loop: true,
            startPosition: 0
        };


    /*  отключение слайдера при разрешении меньше 650px    */

    if ( $(window).width() > 650 ) {
        var owlActive = owl.owlCarousel(owlOptions);
    } else {
        owl.addClass('off');
    }

    $(window).resize(function() {
        if ( $(window).width() > 650 ) {
            if ( $('.owl-carousel').hasClass('off') ) {
                var owlActive = owl.owlCarousel(owlOptions);
                owl.removeClass('off');
            }
        } else {
            if ( !$('.owl-carousel').hasClass('off') ) {
                owl.addClass('off').trigger('destroy.owl.carousel');
                owl.find('.owl-stage-outer').children(':eq(0)').unwrap();
            }
        }
    });



       /*  переключение слайдов по наведению на dots   */

    $(".owl-dot").mouseenter(function(){
        $(this).click()
    });
});



$(document).ready(function () {
    $('.popup_link').magnificPopup({
        type: 'inline',
        focus: '#name',
        closeOnBgClick: true
    });
});




$('[data-fancybox="images"]').fancybox({
    buttons : [
        'share',
        'thumbs',
        'close'
    ]

});


$(document).ready(function () {
    $('.popup_link').magnificPopup({
        type: 'inline',
        focus: '#name',
        closeOnBgClick: true
    });
});


//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJtYWluLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIlxyXG5cclxuJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKSB7XHJcblxyXG4gICAgJChcIi5jZW50ZXJcIikuc2xpY2soe1xyXG4gICAgICAgIGluZmluaXRlOiB0cnVlLFxyXG4gICAgICAgIHNsaWRlc1RvU2hvdzogNCxcclxuICAgICAgICBzbGlkZXNUb1Njcm9sbDogMSxcclxuICAgICAgICByZXNwb25zaXZlOiBbXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIGJyZWFrcG9pbnQ6IDY1MSxcclxuICAgICAgICAgICAgICAgIHNldHRpbmdzOiBcInVuc2xpY2tcIlxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgXVxyXG5cclxuICAgIH0pO1xyXG59KTtcclxuXHJcblxyXG4vKiAgIFNsaWRlciBPd2wgQ2Fyb3VzZWwgMiAgICovXHJcblxyXG4kKGZ1bmN0aW9uKCkge1xyXG4gICAgdmFyIG93bCA9ICQoJy5vd2wtY2Fyb3VzZWwnKSxcclxuICAgICAgICBvd2xPcHRpb25zID0ge1xyXG5cclxuICAgICAgICAgICAgaXRlbXM6IDEsXHJcbiAgICAgICAgICAgIHNpbmdsZUl0ZW1zOiB0cnVlLFxyXG4gICAgICAgICAgICBuYXY6IGZhbHNlLFxyXG4gICAgICAgICAgICBkb3RzOiB0cnVlLFxyXG4gICAgICAgICAgICBsb29wOiB0cnVlLFxyXG4gICAgICAgICAgICBzdGFydFBvc2l0aW9uOiAwXHJcbiAgICAgICAgfTtcclxuXHJcblxyXG4gICAgLyogINC+0YLQutC70Y7Rh9C10L3QuNC1INGB0LvQsNC50LTQtdGA0LAg0L/RgNC4INGA0LDQt9GA0LXRiNC10L3QuNC4INC80LXQvdGM0YjQtSA2NTBweCAgICAqL1xyXG5cclxuICAgIGlmICggJCh3aW5kb3cpLndpZHRoKCkgPiA2NTAgKSB7XHJcbiAgICAgICAgdmFyIG93bEFjdGl2ZSA9IG93bC5vd2xDYXJvdXNlbChvd2xPcHRpb25zKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgICAgb3dsLmFkZENsYXNzKCdvZmYnKTtcclxuICAgIH1cclxuXHJcbiAgICAkKHdpbmRvdykucmVzaXplKGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGlmICggJCh3aW5kb3cpLndpZHRoKCkgPiA2NTAgKSB7XHJcbiAgICAgICAgICAgIGlmICggJCgnLm93bC1jYXJvdXNlbCcpLmhhc0NsYXNzKCdvZmYnKSApIHtcclxuICAgICAgICAgICAgICAgIHZhciBvd2xBY3RpdmUgPSBvd2wub3dsQ2Fyb3VzZWwob3dsT3B0aW9ucyk7XHJcbiAgICAgICAgICAgICAgICBvd2wucmVtb3ZlQ2xhc3MoJ29mZicpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgaWYgKCAhJCgnLm93bC1jYXJvdXNlbCcpLmhhc0NsYXNzKCdvZmYnKSApIHtcclxuICAgICAgICAgICAgICAgIG93bC5hZGRDbGFzcygnb2ZmJykudHJpZ2dlcignZGVzdHJveS5vd2wuY2Fyb3VzZWwnKTtcclxuICAgICAgICAgICAgICAgIG93bC5maW5kKCcub3dsLXN0YWdlLW91dGVyJykuY2hpbGRyZW4oJzplcSgwKScpLnVud3JhcCgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfSk7XHJcblxyXG5cclxuXHJcbiAgICAgICAvKiAg0L/QtdGA0LXQutC70Y7Rh9C10L3QuNC1INGB0LvQsNC50LTQvtCyINC/0L4g0L3QsNCy0LXQtNC10L3QuNGOINC90LAgZG90cyAgICovXHJcblxyXG4gICAgJChcIi5vd2wtZG90XCIpLm1vdXNlZW50ZXIoZnVuY3Rpb24oKXtcclxuICAgICAgICAkKHRoaXMpLmNsaWNrKClcclxuICAgIH0pO1xyXG59KTtcclxuXHJcblxyXG5cclxuJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24gKCkge1xyXG4gICAgJCgnLnBvcHVwX2xpbmsnKS5tYWduaWZpY1BvcHVwKHtcclxuICAgICAgICB0eXBlOiAnaW5saW5lJyxcclxuICAgICAgICBmb2N1czogJyNuYW1lJyxcclxuICAgICAgICBjbG9zZU9uQmdDbGljazogdHJ1ZVxyXG4gICAgfSk7XHJcbn0pO1xyXG5cclxuXHJcblxyXG5cclxuJCgnW2RhdGEtZmFuY3lib3g9XCJpbWFnZXNcIl0nKS5mYW5jeWJveCh7XHJcbiAgICBidXR0b25zIDogW1xyXG4gICAgICAgICdzaGFyZScsXHJcbiAgICAgICAgJ3RodW1icycsXHJcbiAgICAgICAgJ2Nsb3NlJ1xyXG4gICAgXVxyXG5cclxufSk7XHJcblxyXG5cclxuJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24gKCkge1xyXG4gICAgJCgnLnBvcHVwX2xpbmsnKS5tYWduaWZpY1BvcHVwKHtcclxuICAgICAgICB0eXBlOiAnaW5saW5lJyxcclxuICAgICAgICBmb2N1czogJyNuYW1lJyxcclxuICAgICAgICBjbG9zZU9uQmdDbGljazogdHJ1ZVxyXG4gICAgfSk7XHJcbn0pO1xyXG5cclxuIl0sImZpbGUiOiJtYWluLmpzIn0=
