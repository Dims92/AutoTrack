// ==========================================================================
//
// Thumbs
// Displays thumbnails in a grid
//
// ==========================================================================
(function(document, $) {
  "use strict";

  var CLASS = "fancybox-thumbs",
    CLASS_ACTIVE = CLASS + "-active",
    CLASS_LOAD = CLASS + "-loading";

  // Make sure there are default values
  $.fancybox.defaults = $.extend(
    true,
    {
      btnTpl: {
        thumbs:
          '<button data-fancybox-thumbs class="fancybox-button fancybox-button--thumbs" title="{{THUMBS}}">' +
          '<svg viewBox="0 0 120 120">' +
          '<path d="M30,30 h14 v14 h-14 Z M50,30 h14 v14 h-14 Z M70,30 h14 v14 h-14 Z M30,50 h14 v14 h-14 Z M50,50 h14 v14 h-14 Z M70,50 h14 v14 h-14 Z M30,70 h14 v14 h-14 Z M50,70 h14 v14 h-14 Z M70,70 h14 v14 h-14 Z" />' +
          "</svg>" +
          "</button>"
      },
      thumbs: {
        autoStart: false, // Display thumbnails on opening
        hideOnClose: true, // Hide thumbnail grid when closing animation starts
        parentEl: ".fancybox-container", // Container is injected into this element
        axis: "y" // Vertical (y) or horizontal (x) scrolling
      }
    },
    $.fancybox.defaults
  );

  var FancyThumbs = function(instance) {
    this.init(instance);
  };

  $.extend(FancyThumbs.prototype, {
    $button: null,
    $grid: null,
    $list: null,
    isVisible: false,
    isActive: false,

    init: function(instance) {
      var self = this,
        first,
        second;

      self.instance = instance;

      instance.Thumbs = self;

      self.opts = instance.group[instance.currIndex].opts.thumbs;

      // Enable thumbs if at least two group items have thumbnails
      first = instance.group[0];
      first = first.opts.thumb || (first.opts.$thumb && first.opts.$thumb.length ? first.opts.$thumb.attr("src") : false);

      if (instance.group.length > 1) {
        second = instance.group[1];
        second = second.opts.thumb || (second.opts.$thumb && second.opts.$thumb.length ? second.opts.$thumb.attr("src") : false);
      }

      self.$button = instance.$refs.toolbar.find("[data-fancybox-thumbs]");

      if (self.opts && first && second && first && second) {
        self.$button.show().on("click", function() {
          self.toggle();
        });

        self.isActive = true;
      } else {
        self.$button.hide();
      }
    },

    create: function() {
      var self = this,
        instance = self.instance,
        parentEl = self.opts.parentEl,
        list = [],
        src;

      if (!self.$grid) {
        // Create main element
        self.$grid = $('<div class="' + CLASS + " " + CLASS + "-" + self.opts.axis + '"></div>').appendTo(
          instance.$refs.container
            .find(parentEl)
            .addBack()
            .filter(parentEl)
        );

        // Add "click" event that performs gallery navigation
        self.$grid.on("click", "li", function() {
          instance.jumpTo($(this).attr("data-index"));
        });
      }

      // Build the list
      if (!self.$list) {
        self.$list = $("<ul>").appendTo(self.$grid);
      }

      $.each(instance.group, function(i, item) {
        src = item.opts.thumb || (item.opts.$thumb ? item.opts.$thumb.attr("src") : null);

        if (!src && item.type === "image") {
          src = item.src;
        }

        list.push(
          '<li data-index="' +
            i +
            '" tabindex="0" class="' +
            CLASS_LOAD +
            '"' +
            (src && src.length ? ' style="background-image:url(' + src + ')" />' : "") +
            "></li>"
        );
      });

      self.$list[0].innerHTML = list.join("");

      if (self.opts.axis === "x") {
        // Set fixed width for list element to enable horizontal scrolling
        self.$list.width(
          parseInt(self.$grid.css("padding-right"), 10) +
            instance.group.length *
              self.$list
                .children()
                .eq(0)
                .outerWidth(true)
        );
      }
    },

    focus: function(duration) {
      var self = this,
        $list = self.$list,
        $grid = self.$grid,
        thumb,
        thumbPos;

      if (!self.instance.current) {
        return;
      }

      thumb = $list
        .children()
        .removeClass(CLASS_ACTIVE)
        .filter('[data-index="' + self.instance.current.index + '"]')
        .addClass(CLASS_ACTIVE);

      thumbPos = thumb.position();

      // Check if need to scroll to make current thumb visible
      if (self.opts.axis === "y" && (thumbPos.top < 0 || thumbPos.top > $list.height() - thumb.outerHeight())) {
        $list.stop().animate(
          {
            scrollTop: $list.scrollTop() + thumbPos.top
          },
          duration
        );
      } else if (
        self.opts.axis === "x" &&
        (thumbPos.left < $grid.scrollLeft() || thumbPos.left > $grid.scrollLeft() + ($grid.width() - thumb.outerWidth()))
      ) {
        $list
          .parent()
          .stop()
          .animate(
            {
              scrollLeft: thumbPos.left
            },
            duration
          );
      }
    },

    update: function() {
      var that = this;
      that.instance.$refs.container.toggleClass("fancybox-show-thumbs", this.isVisible);

      if (that.isVisible) {
        if (!that.$grid) {
          that.create();
        }

        that.instance.trigger("onThumbsShow");

        that.focus(0);
      } else if (that.$grid) {
        that.instance.trigger("onThumbsHide");
      }

      // Update content position
      that.instance.update();
    },

    hide: function() {
      this.isVisible = false;
      this.update();
    },

    show: function() {
      this.isVisible = true;
      this.update();
    },

    toggle: function() {
      this.isVisible = !this.isVisible;
      this.update();
    }
  });

  $(document).on({
    "onInit.fb": function(e, instance) {
      var Thumbs;

      if (instance && !instance.Thumbs) {
        Thumbs = new FancyThumbs(instance);

        if (Thumbs.isActive && Thumbs.opts.autoStart === true) {
          Thumbs.show();
        }
      }
    },

    "beforeShow.fb": function(e, instance, item, firstRun) {
      var Thumbs = instance && instance.Thumbs;

      if (Thumbs && Thumbs.isVisible) {
        Thumbs.focus(firstRun ? 0 : 250);
      }
    },

    "afterKeydown.fb": function(e, instance, current, keypress, keycode) {
      var Thumbs = instance && instance.Thumbs;

      // "G"
      if (Thumbs && Thumbs.isActive && keycode === 71) {
        keypress.preventDefault();

        Thumbs.toggle();
      }
    },

    "beforeClose.fb": function(e, instance) {
      var Thumbs = instance && instance.Thumbs;

      if (Thumbs && Thumbs.isVisible && Thumbs.opts.hideOnClose !== false) {
        Thumbs.$grid.hide();
      }
    }
  });
})(document, window.jQuery || jQuery);

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJ0aHVtYnMuanMiXSwic291cmNlc0NvbnRlbnQiOlsiLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cbi8vXG4vLyBUaHVtYnNcbi8vIERpc3BsYXlzIHRodW1ibmFpbHMgaW4gYSBncmlkXG4vL1xuLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cbihmdW5jdGlvbihkb2N1bWVudCwgJCkge1xuICBcInVzZSBzdHJpY3RcIjtcblxuICB2YXIgQ0xBU1MgPSBcImZhbmN5Ym94LXRodW1ic1wiLFxuICAgIENMQVNTX0FDVElWRSA9IENMQVNTICsgXCItYWN0aXZlXCIsXG4gICAgQ0xBU1NfTE9BRCA9IENMQVNTICsgXCItbG9hZGluZ1wiO1xuXG4gIC8vIE1ha2Ugc3VyZSB0aGVyZSBhcmUgZGVmYXVsdCB2YWx1ZXNcbiAgJC5mYW5jeWJveC5kZWZhdWx0cyA9ICQuZXh0ZW5kKFxuICAgIHRydWUsXG4gICAge1xuICAgICAgYnRuVHBsOiB7XG4gICAgICAgIHRodW1iczpcbiAgICAgICAgICAnPGJ1dHRvbiBkYXRhLWZhbmN5Ym94LXRodW1icyBjbGFzcz1cImZhbmN5Ym94LWJ1dHRvbiBmYW5jeWJveC1idXR0b24tLXRodW1ic1wiIHRpdGxlPVwie3tUSFVNQlN9fVwiPicgK1xuICAgICAgICAgICc8c3ZnIHZpZXdCb3g9XCIwIDAgMTIwIDEyMFwiPicgK1xuICAgICAgICAgICc8cGF0aCBkPVwiTTMwLDMwIGgxNCB2MTQgaC0xNCBaIE01MCwzMCBoMTQgdjE0IGgtMTQgWiBNNzAsMzAgaDE0IHYxNCBoLTE0IFogTTMwLDUwIGgxNCB2MTQgaC0xNCBaIE01MCw1MCBoMTQgdjE0IGgtMTQgWiBNNzAsNTAgaDE0IHYxNCBoLTE0IFogTTMwLDcwIGgxNCB2MTQgaC0xNCBaIE01MCw3MCBoMTQgdjE0IGgtMTQgWiBNNzAsNzAgaDE0IHYxNCBoLTE0IFpcIiAvPicgK1xuICAgICAgICAgIFwiPC9zdmc+XCIgK1xuICAgICAgICAgIFwiPC9idXR0b24+XCJcbiAgICAgIH0sXG4gICAgICB0aHVtYnM6IHtcbiAgICAgICAgYXV0b1N0YXJ0OiBmYWxzZSwgLy8gRGlzcGxheSB0aHVtYm5haWxzIG9uIG9wZW5pbmdcbiAgICAgICAgaGlkZU9uQ2xvc2U6IHRydWUsIC8vIEhpZGUgdGh1bWJuYWlsIGdyaWQgd2hlbiBjbG9zaW5nIGFuaW1hdGlvbiBzdGFydHNcbiAgICAgICAgcGFyZW50RWw6IFwiLmZhbmN5Ym94LWNvbnRhaW5lclwiLCAvLyBDb250YWluZXIgaXMgaW5qZWN0ZWQgaW50byB0aGlzIGVsZW1lbnRcbiAgICAgICAgYXhpczogXCJ5XCIgLy8gVmVydGljYWwgKHkpIG9yIGhvcml6b250YWwgKHgpIHNjcm9sbGluZ1xuICAgICAgfVxuICAgIH0sXG4gICAgJC5mYW5jeWJveC5kZWZhdWx0c1xuICApO1xuXG4gIHZhciBGYW5jeVRodW1icyA9IGZ1bmN0aW9uKGluc3RhbmNlKSB7XG4gICAgdGhpcy5pbml0KGluc3RhbmNlKTtcbiAgfTtcblxuICAkLmV4dGVuZChGYW5jeVRodW1icy5wcm90b3R5cGUsIHtcbiAgICAkYnV0dG9uOiBudWxsLFxuICAgICRncmlkOiBudWxsLFxuICAgICRsaXN0OiBudWxsLFxuICAgIGlzVmlzaWJsZTogZmFsc2UsXG4gICAgaXNBY3RpdmU6IGZhbHNlLFxuXG4gICAgaW5pdDogZnVuY3Rpb24oaW5zdGFuY2UpIHtcbiAgICAgIHZhciBzZWxmID0gdGhpcyxcbiAgICAgICAgZmlyc3QsXG4gICAgICAgIHNlY29uZDtcblxuICAgICAgc2VsZi5pbnN0YW5jZSA9IGluc3RhbmNlO1xuXG4gICAgICBpbnN0YW5jZS5UaHVtYnMgPSBzZWxmO1xuXG4gICAgICBzZWxmLm9wdHMgPSBpbnN0YW5jZS5ncm91cFtpbnN0YW5jZS5jdXJySW5kZXhdLm9wdHMudGh1bWJzO1xuXG4gICAgICAvLyBFbmFibGUgdGh1bWJzIGlmIGF0IGxlYXN0IHR3byBncm91cCBpdGVtcyBoYXZlIHRodW1ibmFpbHNcbiAgICAgIGZpcnN0ID0gaW5zdGFuY2UuZ3JvdXBbMF07XG4gICAgICBmaXJzdCA9IGZpcnN0Lm9wdHMudGh1bWIgfHwgKGZpcnN0Lm9wdHMuJHRodW1iICYmIGZpcnN0Lm9wdHMuJHRodW1iLmxlbmd0aCA/IGZpcnN0Lm9wdHMuJHRodW1iLmF0dHIoXCJzcmNcIikgOiBmYWxzZSk7XG5cbiAgICAgIGlmIChpbnN0YW5jZS5ncm91cC5sZW5ndGggPiAxKSB7XG4gICAgICAgIHNlY29uZCA9IGluc3RhbmNlLmdyb3VwWzFdO1xuICAgICAgICBzZWNvbmQgPSBzZWNvbmQub3B0cy50aHVtYiB8fCAoc2Vjb25kLm9wdHMuJHRodW1iICYmIHNlY29uZC5vcHRzLiR0aHVtYi5sZW5ndGggPyBzZWNvbmQub3B0cy4kdGh1bWIuYXR0cihcInNyY1wiKSA6IGZhbHNlKTtcbiAgICAgIH1cblxuICAgICAgc2VsZi4kYnV0dG9uID0gaW5zdGFuY2UuJHJlZnMudG9vbGJhci5maW5kKFwiW2RhdGEtZmFuY3lib3gtdGh1bWJzXVwiKTtcblxuICAgICAgaWYgKHNlbGYub3B0cyAmJiBmaXJzdCAmJiBzZWNvbmQgJiYgZmlyc3QgJiYgc2Vjb25kKSB7XG4gICAgICAgIHNlbGYuJGJ1dHRvbi5zaG93KCkub24oXCJjbGlja1wiLCBmdW5jdGlvbigpIHtcbiAgICAgICAgICBzZWxmLnRvZ2dsZSgpO1xuICAgICAgICB9KTtcblxuICAgICAgICBzZWxmLmlzQWN0aXZlID0gdHJ1ZTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHNlbGYuJGJ1dHRvbi5oaWRlKCk7XG4gICAgICB9XG4gICAgfSxcblxuICAgIGNyZWF0ZTogZnVuY3Rpb24oKSB7XG4gICAgICB2YXIgc2VsZiA9IHRoaXMsXG4gICAgICAgIGluc3RhbmNlID0gc2VsZi5pbnN0YW5jZSxcbiAgICAgICAgcGFyZW50RWwgPSBzZWxmLm9wdHMucGFyZW50RWwsXG4gICAgICAgIGxpc3QgPSBbXSxcbiAgICAgICAgc3JjO1xuXG4gICAgICBpZiAoIXNlbGYuJGdyaWQpIHtcbiAgICAgICAgLy8gQ3JlYXRlIG1haW4gZWxlbWVudFxuICAgICAgICBzZWxmLiRncmlkID0gJCgnPGRpdiBjbGFzcz1cIicgKyBDTEFTUyArIFwiIFwiICsgQ0xBU1MgKyBcIi1cIiArIHNlbGYub3B0cy5heGlzICsgJ1wiPjwvZGl2PicpLmFwcGVuZFRvKFxuICAgICAgICAgIGluc3RhbmNlLiRyZWZzLmNvbnRhaW5lclxuICAgICAgICAgICAgLmZpbmQocGFyZW50RWwpXG4gICAgICAgICAgICAuYWRkQmFjaygpXG4gICAgICAgICAgICAuZmlsdGVyKHBhcmVudEVsKVxuICAgICAgICApO1xuXG4gICAgICAgIC8vIEFkZCBcImNsaWNrXCIgZXZlbnQgdGhhdCBwZXJmb3JtcyBnYWxsZXJ5IG5hdmlnYXRpb25cbiAgICAgICAgc2VsZi4kZ3JpZC5vbihcImNsaWNrXCIsIFwibGlcIiwgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgaW5zdGFuY2UuanVtcFRvKCQodGhpcykuYXR0cihcImRhdGEtaW5kZXhcIikpO1xuICAgICAgICB9KTtcbiAgICAgIH1cblxuICAgICAgLy8gQnVpbGQgdGhlIGxpc3RcbiAgICAgIGlmICghc2VsZi4kbGlzdCkge1xuICAgICAgICBzZWxmLiRsaXN0ID0gJChcIjx1bD5cIikuYXBwZW5kVG8oc2VsZi4kZ3JpZCk7XG4gICAgICB9XG5cbiAgICAgICQuZWFjaChpbnN0YW5jZS5ncm91cCwgZnVuY3Rpb24oaSwgaXRlbSkge1xuICAgICAgICBzcmMgPSBpdGVtLm9wdHMudGh1bWIgfHwgKGl0ZW0ub3B0cy4kdGh1bWIgPyBpdGVtLm9wdHMuJHRodW1iLmF0dHIoXCJzcmNcIikgOiBudWxsKTtcblxuICAgICAgICBpZiAoIXNyYyAmJiBpdGVtLnR5cGUgPT09IFwiaW1hZ2VcIikge1xuICAgICAgICAgIHNyYyA9IGl0ZW0uc3JjO1xuICAgICAgICB9XG5cbiAgICAgICAgbGlzdC5wdXNoKFxuICAgICAgICAgICc8bGkgZGF0YS1pbmRleD1cIicgK1xuICAgICAgICAgICAgaSArXG4gICAgICAgICAgICAnXCIgdGFiaW5kZXg9XCIwXCIgY2xhc3M9XCInICtcbiAgICAgICAgICAgIENMQVNTX0xPQUQgK1xuICAgICAgICAgICAgJ1wiJyArXG4gICAgICAgICAgICAoc3JjICYmIHNyYy5sZW5ndGggPyAnIHN0eWxlPVwiYmFja2dyb3VuZC1pbWFnZTp1cmwoJyArIHNyYyArICcpXCIgLz4nIDogXCJcIikgK1xuICAgICAgICAgICAgXCI+PC9saT5cIlxuICAgICAgICApO1xuICAgICAgfSk7XG5cbiAgICAgIHNlbGYuJGxpc3RbMF0uaW5uZXJIVE1MID0gbGlzdC5qb2luKFwiXCIpO1xuXG4gICAgICBpZiAoc2VsZi5vcHRzLmF4aXMgPT09IFwieFwiKSB7XG4gICAgICAgIC8vIFNldCBmaXhlZCB3aWR0aCBmb3IgbGlzdCBlbGVtZW50IHRvIGVuYWJsZSBob3Jpem9udGFsIHNjcm9sbGluZ1xuICAgICAgICBzZWxmLiRsaXN0LndpZHRoKFxuICAgICAgICAgIHBhcnNlSW50KHNlbGYuJGdyaWQuY3NzKFwicGFkZGluZy1yaWdodFwiKSwgMTApICtcbiAgICAgICAgICAgIGluc3RhbmNlLmdyb3VwLmxlbmd0aCAqXG4gICAgICAgICAgICAgIHNlbGYuJGxpc3RcbiAgICAgICAgICAgICAgICAuY2hpbGRyZW4oKVxuICAgICAgICAgICAgICAgIC5lcSgwKVxuICAgICAgICAgICAgICAgIC5vdXRlcldpZHRoKHRydWUpXG4gICAgICAgICk7XG4gICAgICB9XG4gICAgfSxcblxuICAgIGZvY3VzOiBmdW5jdGlvbihkdXJhdGlvbikge1xuICAgICAgdmFyIHNlbGYgPSB0aGlzLFxuICAgICAgICAkbGlzdCA9IHNlbGYuJGxpc3QsXG4gICAgICAgICRncmlkID0gc2VsZi4kZ3JpZCxcbiAgICAgICAgdGh1bWIsXG4gICAgICAgIHRodW1iUG9zO1xuXG4gICAgICBpZiAoIXNlbGYuaW5zdGFuY2UuY3VycmVudCkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIHRodW1iID0gJGxpc3RcbiAgICAgICAgLmNoaWxkcmVuKClcbiAgICAgICAgLnJlbW92ZUNsYXNzKENMQVNTX0FDVElWRSlcbiAgICAgICAgLmZpbHRlcignW2RhdGEtaW5kZXg9XCInICsgc2VsZi5pbnN0YW5jZS5jdXJyZW50LmluZGV4ICsgJ1wiXScpXG4gICAgICAgIC5hZGRDbGFzcyhDTEFTU19BQ1RJVkUpO1xuXG4gICAgICB0aHVtYlBvcyA9IHRodW1iLnBvc2l0aW9uKCk7XG5cbiAgICAgIC8vIENoZWNrIGlmIG5lZWQgdG8gc2Nyb2xsIHRvIG1ha2UgY3VycmVudCB0aHVtYiB2aXNpYmxlXG4gICAgICBpZiAoc2VsZi5vcHRzLmF4aXMgPT09IFwieVwiICYmICh0aHVtYlBvcy50b3AgPCAwIHx8IHRodW1iUG9zLnRvcCA+ICRsaXN0LmhlaWdodCgpIC0gdGh1bWIub3V0ZXJIZWlnaHQoKSkpIHtcbiAgICAgICAgJGxpc3Quc3RvcCgpLmFuaW1hdGUoXG4gICAgICAgICAge1xuICAgICAgICAgICAgc2Nyb2xsVG9wOiAkbGlzdC5zY3JvbGxUb3AoKSArIHRodW1iUG9zLnRvcFxuICAgICAgICAgIH0sXG4gICAgICAgICAgZHVyYXRpb25cbiAgICAgICAgKTtcbiAgICAgIH0gZWxzZSBpZiAoXG4gICAgICAgIHNlbGYub3B0cy5heGlzID09PSBcInhcIiAmJlxuICAgICAgICAodGh1bWJQb3MubGVmdCA8ICRncmlkLnNjcm9sbExlZnQoKSB8fCB0aHVtYlBvcy5sZWZ0ID4gJGdyaWQuc2Nyb2xsTGVmdCgpICsgKCRncmlkLndpZHRoKCkgLSB0aHVtYi5vdXRlcldpZHRoKCkpKVxuICAgICAgKSB7XG4gICAgICAgICRsaXN0XG4gICAgICAgICAgLnBhcmVudCgpXG4gICAgICAgICAgLnN0b3AoKVxuICAgICAgICAgIC5hbmltYXRlKFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBzY3JvbGxMZWZ0OiB0aHVtYlBvcy5sZWZ0XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgZHVyYXRpb25cbiAgICAgICAgICApO1xuICAgICAgfVxuICAgIH0sXG5cbiAgICB1cGRhdGU6IGZ1bmN0aW9uKCkge1xuICAgICAgdmFyIHRoYXQgPSB0aGlzO1xuICAgICAgdGhhdC5pbnN0YW5jZS4kcmVmcy5jb250YWluZXIudG9nZ2xlQ2xhc3MoXCJmYW5jeWJveC1zaG93LXRodW1ic1wiLCB0aGlzLmlzVmlzaWJsZSk7XG5cbiAgICAgIGlmICh0aGF0LmlzVmlzaWJsZSkge1xuICAgICAgICBpZiAoIXRoYXQuJGdyaWQpIHtcbiAgICAgICAgICB0aGF0LmNyZWF0ZSgpO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhhdC5pbnN0YW5jZS50cmlnZ2VyKFwib25UaHVtYnNTaG93XCIpO1xuXG4gICAgICAgIHRoYXQuZm9jdXMoMCk7XG4gICAgICB9IGVsc2UgaWYgKHRoYXQuJGdyaWQpIHtcbiAgICAgICAgdGhhdC5pbnN0YW5jZS50cmlnZ2VyKFwib25UaHVtYnNIaWRlXCIpO1xuICAgICAgfVxuXG4gICAgICAvLyBVcGRhdGUgY29udGVudCBwb3NpdGlvblxuICAgICAgdGhhdC5pbnN0YW5jZS51cGRhdGUoKTtcbiAgICB9LFxuXG4gICAgaGlkZTogZnVuY3Rpb24oKSB7XG4gICAgICB0aGlzLmlzVmlzaWJsZSA9IGZhbHNlO1xuICAgICAgdGhpcy51cGRhdGUoKTtcbiAgICB9LFxuXG4gICAgc2hvdzogZnVuY3Rpb24oKSB7XG4gICAgICB0aGlzLmlzVmlzaWJsZSA9IHRydWU7XG4gICAgICB0aGlzLnVwZGF0ZSgpO1xuICAgIH0sXG5cbiAgICB0b2dnbGU6IGZ1bmN0aW9uKCkge1xuICAgICAgdGhpcy5pc1Zpc2libGUgPSAhdGhpcy5pc1Zpc2libGU7XG4gICAgICB0aGlzLnVwZGF0ZSgpO1xuICAgIH1cbiAgfSk7XG5cbiAgJChkb2N1bWVudCkub24oe1xuICAgIFwib25Jbml0LmZiXCI6IGZ1bmN0aW9uKGUsIGluc3RhbmNlKSB7XG4gICAgICB2YXIgVGh1bWJzO1xuXG4gICAgICBpZiAoaW5zdGFuY2UgJiYgIWluc3RhbmNlLlRodW1icykge1xuICAgICAgICBUaHVtYnMgPSBuZXcgRmFuY3lUaHVtYnMoaW5zdGFuY2UpO1xuXG4gICAgICAgIGlmIChUaHVtYnMuaXNBY3RpdmUgJiYgVGh1bWJzLm9wdHMuYXV0b1N0YXJ0ID09PSB0cnVlKSB7XG4gICAgICAgICAgVGh1bWJzLnNob3coKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0sXG5cbiAgICBcImJlZm9yZVNob3cuZmJcIjogZnVuY3Rpb24oZSwgaW5zdGFuY2UsIGl0ZW0sIGZpcnN0UnVuKSB7XG4gICAgICB2YXIgVGh1bWJzID0gaW5zdGFuY2UgJiYgaW5zdGFuY2UuVGh1bWJzO1xuXG4gICAgICBpZiAoVGh1bWJzICYmIFRodW1icy5pc1Zpc2libGUpIHtcbiAgICAgICAgVGh1bWJzLmZvY3VzKGZpcnN0UnVuID8gMCA6IDI1MCk7XG4gICAgICB9XG4gICAgfSxcblxuICAgIFwiYWZ0ZXJLZXlkb3duLmZiXCI6IGZ1bmN0aW9uKGUsIGluc3RhbmNlLCBjdXJyZW50LCBrZXlwcmVzcywga2V5Y29kZSkge1xuICAgICAgdmFyIFRodW1icyA9IGluc3RhbmNlICYmIGluc3RhbmNlLlRodW1icztcblxuICAgICAgLy8gXCJHXCJcbiAgICAgIGlmIChUaHVtYnMgJiYgVGh1bWJzLmlzQWN0aXZlICYmIGtleWNvZGUgPT09IDcxKSB7XG4gICAgICAgIGtleXByZXNzLnByZXZlbnREZWZhdWx0KCk7XG5cbiAgICAgICAgVGh1bWJzLnRvZ2dsZSgpO1xuICAgICAgfVxuICAgIH0sXG5cbiAgICBcImJlZm9yZUNsb3NlLmZiXCI6IGZ1bmN0aW9uKGUsIGluc3RhbmNlKSB7XG4gICAgICB2YXIgVGh1bWJzID0gaW5zdGFuY2UgJiYgaW5zdGFuY2UuVGh1bWJzO1xuXG4gICAgICBpZiAoVGh1bWJzICYmIFRodW1icy5pc1Zpc2libGUgJiYgVGh1bWJzLm9wdHMuaGlkZU9uQ2xvc2UgIT09IGZhbHNlKSB7XG4gICAgICAgIFRodW1icy4kZ3JpZC5oaWRlKCk7XG4gICAgICB9XG4gICAgfVxuICB9KTtcbn0pKGRvY3VtZW50LCB3aW5kb3cualF1ZXJ5IHx8IGpRdWVyeSk7XG4iXSwiZmlsZSI6InRodW1icy5qcyJ9
