// ==========================================================================
//
// FullScreen
// Adds fullscreen functionality
//
// ==========================================================================
(function(document, $) {
  "use strict";

  // Collection of methods supported by user browser
  var fn = (function() {
    var fnMap = [
      ["requestFullscreen", "exitFullscreen", "fullscreenElement", "fullscreenEnabled", "fullscreenchange", "fullscreenerror"],
      // new WebKit
      [
        "webkitRequestFullscreen",
        "webkitExitFullscreen",
        "webkitFullscreenElement",
        "webkitFullscreenEnabled",
        "webkitfullscreenchange",
        "webkitfullscreenerror"
      ],
      // old WebKit (Safari 5.1)
      [
        "webkitRequestFullScreen",
        "webkitCancelFullScreen",
        "webkitCurrentFullScreenElement",
        "webkitCancelFullScreen",
        "webkitfullscreenchange",
        "webkitfullscreenerror"
      ],
      [
        "mozRequestFullScreen",
        "mozCancelFullScreen",
        "mozFullScreenElement",
        "mozFullScreenEnabled",
        "mozfullscreenchange",
        "mozfullscreenerror"
      ],
      ["msRequestFullscreen", "msExitFullscreen", "msFullscreenElement", "msFullscreenEnabled", "MSFullscreenChange", "MSFullscreenError"]
    ];

    var ret = {};

    for (var i = 0; i < fnMap.length; i++) {
      var val = fnMap[i];

      if (val && val[1] in document) {
        for (var j = 0; j < val.length; j++) {
          ret[fnMap[0][j]] = val[j];
        }

        return ret;
      }
    }

    return false;
  })();

  // If browser does not have Full Screen API, then simply unset default button template and stop
  if (!fn) {
    if ($ && $.fancybox) {
      $.fancybox.defaults.btnTpl.fullScreen = false;
    }

    return;
  }

  var FullScreen = {
    request: function(elem) {
      elem = elem || document.documentElement;

      elem[fn.requestFullscreen](elem.ALLOW_KEYBOARD_INPUT);
    },
    exit: function() {
      document[fn.exitFullscreen]();
    },
    toggle: function(elem) {
      elem = elem || document.documentElement;

      if (this.isFullscreen()) {
        this.exit();
      } else {
        this.request(elem);
      }
    },
    isFullscreen: function() {
      return Boolean(document[fn.fullscreenElement]);
    },
    enabled: function() {
      return Boolean(document[fn.fullscreenEnabled]);
    }
  };

  $.extend(true, $.fancybox.defaults, {
    btnTpl: {
      fullScreen:
        '<button data-fancybox-fullscreen class="fancybox-button fancybox-button--fullscreen" title="{{FULL_SCREEN}}">' +
        '<svg viewBox="0 0 40 40">' +
        '<path d="M9,12 v16 h22 v-16 h-22 v8" />' +
        "</svg>" +
        "</button>"
    },
    fullScreen: {
      autoStart: false
    }
  });

  $(document).on({
    "onInit.fb": function(e, instance) {
      var $container;

      if (instance && instance.group[instance.currIndex].opts.fullScreen) {
        $container = instance.$refs.container;

        $container.on("click.fb-fullscreen", "[data-fancybox-fullscreen]", function(e) {
          e.stopPropagation();
          e.preventDefault();

          FullScreen.toggle();
        });

        if (instance.opts.fullScreen && instance.opts.fullScreen.autoStart === true) {
          FullScreen.request();
        }

        // Expose API
        instance.FullScreen = FullScreen;
      } else if (instance) {
        instance.$refs.toolbar.find("[data-fancybox-fullscreen]").hide();
      }
    },

    "afterKeydown.fb": function(e, instance, current, keypress, keycode) {
      // "F"
      if (instance && instance.FullScreen && keycode === 70) {
        keypress.preventDefault();

        instance.FullScreen.toggle();
      }
    },

    "beforeClose.fb": function(e, instance) {
      if (instance && instance.FullScreen && instance.$refs.container.hasClass("fancybox-is-fullscreen")) {
        FullScreen.exit();
      }
    }
  });

  $(document).on(fn.fullscreenchange, function() {
    var isFullscreen = FullScreen.isFullscreen(),
      instance = $.fancybox.getInstance();

    if (instance) {
      // If image is zooming, then force to stop and reposition properly
      if (instance.current && instance.current.type === "image" && instance.isAnimating) {
        instance.current.$content.css("transition", "none");

        instance.isAnimating = false;

        instance.update(true, true, 0);
      }

      instance.trigger("onFullscreenChange", isFullscreen);

      instance.$refs.container.toggleClass("fancybox-is-fullscreen", isFullscreen);
    }
  });
})(document, window.jQuery || jQuery);

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJmdWxsc2NyZWVuLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8vID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG4vL1xuLy8gRnVsbFNjcmVlblxuLy8gQWRkcyBmdWxsc2NyZWVuIGZ1bmN0aW9uYWxpdHlcbi8vXG4vLyA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuKGZ1bmN0aW9uKGRvY3VtZW50LCAkKSB7XG4gIFwidXNlIHN0cmljdFwiO1xuXG4gIC8vIENvbGxlY3Rpb24gb2YgbWV0aG9kcyBzdXBwb3J0ZWQgYnkgdXNlciBicm93c2VyXG4gIHZhciBmbiA9IChmdW5jdGlvbigpIHtcbiAgICB2YXIgZm5NYXAgPSBbXG4gICAgICBbXCJyZXF1ZXN0RnVsbHNjcmVlblwiLCBcImV4aXRGdWxsc2NyZWVuXCIsIFwiZnVsbHNjcmVlbkVsZW1lbnRcIiwgXCJmdWxsc2NyZWVuRW5hYmxlZFwiLCBcImZ1bGxzY3JlZW5jaGFuZ2VcIiwgXCJmdWxsc2NyZWVuZXJyb3JcIl0sXG4gICAgICAvLyBuZXcgV2ViS2l0XG4gICAgICBbXG4gICAgICAgIFwid2Via2l0UmVxdWVzdEZ1bGxzY3JlZW5cIixcbiAgICAgICAgXCJ3ZWJraXRFeGl0RnVsbHNjcmVlblwiLFxuICAgICAgICBcIndlYmtpdEZ1bGxzY3JlZW5FbGVtZW50XCIsXG4gICAgICAgIFwid2Via2l0RnVsbHNjcmVlbkVuYWJsZWRcIixcbiAgICAgICAgXCJ3ZWJraXRmdWxsc2NyZWVuY2hhbmdlXCIsXG4gICAgICAgIFwid2Via2l0ZnVsbHNjcmVlbmVycm9yXCJcbiAgICAgIF0sXG4gICAgICAvLyBvbGQgV2ViS2l0IChTYWZhcmkgNS4xKVxuICAgICAgW1xuICAgICAgICBcIndlYmtpdFJlcXVlc3RGdWxsU2NyZWVuXCIsXG4gICAgICAgIFwid2Via2l0Q2FuY2VsRnVsbFNjcmVlblwiLFxuICAgICAgICBcIndlYmtpdEN1cnJlbnRGdWxsU2NyZWVuRWxlbWVudFwiLFxuICAgICAgICBcIndlYmtpdENhbmNlbEZ1bGxTY3JlZW5cIixcbiAgICAgICAgXCJ3ZWJraXRmdWxsc2NyZWVuY2hhbmdlXCIsXG4gICAgICAgIFwid2Via2l0ZnVsbHNjcmVlbmVycm9yXCJcbiAgICAgIF0sXG4gICAgICBbXG4gICAgICAgIFwibW96UmVxdWVzdEZ1bGxTY3JlZW5cIixcbiAgICAgICAgXCJtb3pDYW5jZWxGdWxsU2NyZWVuXCIsXG4gICAgICAgIFwibW96RnVsbFNjcmVlbkVsZW1lbnRcIixcbiAgICAgICAgXCJtb3pGdWxsU2NyZWVuRW5hYmxlZFwiLFxuICAgICAgICBcIm1vemZ1bGxzY3JlZW5jaGFuZ2VcIixcbiAgICAgICAgXCJtb3pmdWxsc2NyZWVuZXJyb3JcIlxuICAgICAgXSxcbiAgICAgIFtcIm1zUmVxdWVzdEZ1bGxzY3JlZW5cIiwgXCJtc0V4aXRGdWxsc2NyZWVuXCIsIFwibXNGdWxsc2NyZWVuRWxlbWVudFwiLCBcIm1zRnVsbHNjcmVlbkVuYWJsZWRcIiwgXCJNU0Z1bGxzY3JlZW5DaGFuZ2VcIiwgXCJNU0Z1bGxzY3JlZW5FcnJvclwiXVxuICAgIF07XG5cbiAgICB2YXIgcmV0ID0ge307XG5cbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IGZuTWFwLmxlbmd0aDsgaSsrKSB7XG4gICAgICB2YXIgdmFsID0gZm5NYXBbaV07XG5cbiAgICAgIGlmICh2YWwgJiYgdmFsWzFdIGluIGRvY3VtZW50KSB7XG4gICAgICAgIGZvciAodmFyIGogPSAwOyBqIDwgdmFsLmxlbmd0aDsgaisrKSB7XG4gICAgICAgICAgcmV0W2ZuTWFwWzBdW2pdXSA9IHZhbFtqXTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiByZXQ7XG4gICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuIGZhbHNlO1xuICB9KSgpO1xuXG4gIC8vIElmIGJyb3dzZXIgZG9lcyBub3QgaGF2ZSBGdWxsIFNjcmVlbiBBUEksIHRoZW4gc2ltcGx5IHVuc2V0IGRlZmF1bHQgYnV0dG9uIHRlbXBsYXRlIGFuZCBzdG9wXG4gIGlmICghZm4pIHtcbiAgICBpZiAoJCAmJiAkLmZhbmN5Ym94KSB7XG4gICAgICAkLmZhbmN5Ym94LmRlZmF1bHRzLmJ0blRwbC5mdWxsU2NyZWVuID0gZmFsc2U7XG4gICAgfVxuXG4gICAgcmV0dXJuO1xuICB9XG5cbiAgdmFyIEZ1bGxTY3JlZW4gPSB7XG4gICAgcmVxdWVzdDogZnVuY3Rpb24oZWxlbSkge1xuICAgICAgZWxlbSA9IGVsZW0gfHwgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50O1xuXG4gICAgICBlbGVtW2ZuLnJlcXVlc3RGdWxsc2NyZWVuXShlbGVtLkFMTE9XX0tFWUJPQVJEX0lOUFVUKTtcbiAgICB9LFxuICAgIGV4aXQ6IGZ1bmN0aW9uKCkge1xuICAgICAgZG9jdW1lbnRbZm4uZXhpdEZ1bGxzY3JlZW5dKCk7XG4gICAgfSxcbiAgICB0b2dnbGU6IGZ1bmN0aW9uKGVsZW0pIHtcbiAgICAgIGVsZW0gPSBlbGVtIHx8IGRvY3VtZW50LmRvY3VtZW50RWxlbWVudDtcblxuICAgICAgaWYgKHRoaXMuaXNGdWxsc2NyZWVuKCkpIHtcbiAgICAgICAgdGhpcy5leGl0KCk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLnJlcXVlc3QoZWxlbSk7XG4gICAgICB9XG4gICAgfSxcbiAgICBpc0Z1bGxzY3JlZW46IGZ1bmN0aW9uKCkge1xuICAgICAgcmV0dXJuIEJvb2xlYW4oZG9jdW1lbnRbZm4uZnVsbHNjcmVlbkVsZW1lbnRdKTtcbiAgICB9LFxuICAgIGVuYWJsZWQ6IGZ1bmN0aW9uKCkge1xuICAgICAgcmV0dXJuIEJvb2xlYW4oZG9jdW1lbnRbZm4uZnVsbHNjcmVlbkVuYWJsZWRdKTtcbiAgICB9XG4gIH07XG5cbiAgJC5leHRlbmQodHJ1ZSwgJC5mYW5jeWJveC5kZWZhdWx0cywge1xuICAgIGJ0blRwbDoge1xuICAgICAgZnVsbFNjcmVlbjpcbiAgICAgICAgJzxidXR0b24gZGF0YS1mYW5jeWJveC1mdWxsc2NyZWVuIGNsYXNzPVwiZmFuY3lib3gtYnV0dG9uIGZhbmN5Ym94LWJ1dHRvbi0tZnVsbHNjcmVlblwiIHRpdGxlPVwie3tGVUxMX1NDUkVFTn19XCI+JyArXG4gICAgICAgICc8c3ZnIHZpZXdCb3g9XCIwIDAgNDAgNDBcIj4nICtcbiAgICAgICAgJzxwYXRoIGQ9XCJNOSwxMiB2MTYgaDIyIHYtMTYgaC0yMiB2OFwiIC8+JyArXG4gICAgICAgIFwiPC9zdmc+XCIgK1xuICAgICAgICBcIjwvYnV0dG9uPlwiXG4gICAgfSxcbiAgICBmdWxsU2NyZWVuOiB7XG4gICAgICBhdXRvU3RhcnQ6IGZhbHNlXG4gICAgfVxuICB9KTtcblxuICAkKGRvY3VtZW50KS5vbih7XG4gICAgXCJvbkluaXQuZmJcIjogZnVuY3Rpb24oZSwgaW5zdGFuY2UpIHtcbiAgICAgIHZhciAkY29udGFpbmVyO1xuXG4gICAgICBpZiAoaW5zdGFuY2UgJiYgaW5zdGFuY2UuZ3JvdXBbaW5zdGFuY2UuY3VyckluZGV4XS5vcHRzLmZ1bGxTY3JlZW4pIHtcbiAgICAgICAgJGNvbnRhaW5lciA9IGluc3RhbmNlLiRyZWZzLmNvbnRhaW5lcjtcblxuICAgICAgICAkY29udGFpbmVyLm9uKFwiY2xpY2suZmItZnVsbHNjcmVlblwiLCBcIltkYXRhLWZhbmN5Ym94LWZ1bGxzY3JlZW5dXCIsIGZ1bmN0aW9uKGUpIHtcbiAgICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xuICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcblxuICAgICAgICAgIEZ1bGxTY3JlZW4udG9nZ2xlKCk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIGlmIChpbnN0YW5jZS5vcHRzLmZ1bGxTY3JlZW4gJiYgaW5zdGFuY2Uub3B0cy5mdWxsU2NyZWVuLmF1dG9TdGFydCA9PT0gdHJ1ZSkge1xuICAgICAgICAgIEZ1bGxTY3JlZW4ucmVxdWVzdCgpO1xuICAgICAgICB9XG5cbiAgICAgICAgLy8gRXhwb3NlIEFQSVxuICAgICAgICBpbnN0YW5jZS5GdWxsU2NyZWVuID0gRnVsbFNjcmVlbjtcbiAgICAgIH0gZWxzZSBpZiAoaW5zdGFuY2UpIHtcbiAgICAgICAgaW5zdGFuY2UuJHJlZnMudG9vbGJhci5maW5kKFwiW2RhdGEtZmFuY3lib3gtZnVsbHNjcmVlbl1cIikuaGlkZSgpO1xuICAgICAgfVxuICAgIH0sXG5cbiAgICBcImFmdGVyS2V5ZG93bi5mYlwiOiBmdW5jdGlvbihlLCBpbnN0YW5jZSwgY3VycmVudCwga2V5cHJlc3MsIGtleWNvZGUpIHtcbiAgICAgIC8vIFwiRlwiXG4gICAgICBpZiAoaW5zdGFuY2UgJiYgaW5zdGFuY2UuRnVsbFNjcmVlbiAmJiBrZXljb2RlID09PSA3MCkge1xuICAgICAgICBrZXlwcmVzcy5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgICAgIGluc3RhbmNlLkZ1bGxTY3JlZW4udG9nZ2xlKCk7XG4gICAgICB9XG4gICAgfSxcblxuICAgIFwiYmVmb3JlQ2xvc2UuZmJcIjogZnVuY3Rpb24oZSwgaW5zdGFuY2UpIHtcbiAgICAgIGlmIChpbnN0YW5jZSAmJiBpbnN0YW5jZS5GdWxsU2NyZWVuICYmIGluc3RhbmNlLiRyZWZzLmNvbnRhaW5lci5oYXNDbGFzcyhcImZhbmN5Ym94LWlzLWZ1bGxzY3JlZW5cIikpIHtcbiAgICAgICAgRnVsbFNjcmVlbi5leGl0KCk7XG4gICAgICB9XG4gICAgfVxuICB9KTtcblxuICAkKGRvY3VtZW50KS5vbihmbi5mdWxsc2NyZWVuY2hhbmdlLCBmdW5jdGlvbigpIHtcbiAgICB2YXIgaXNGdWxsc2NyZWVuID0gRnVsbFNjcmVlbi5pc0Z1bGxzY3JlZW4oKSxcbiAgICAgIGluc3RhbmNlID0gJC5mYW5jeWJveC5nZXRJbnN0YW5jZSgpO1xuXG4gICAgaWYgKGluc3RhbmNlKSB7XG4gICAgICAvLyBJZiBpbWFnZSBpcyB6b29taW5nLCB0aGVuIGZvcmNlIHRvIHN0b3AgYW5kIHJlcG9zaXRpb24gcHJvcGVybHlcbiAgICAgIGlmIChpbnN0YW5jZS5jdXJyZW50ICYmIGluc3RhbmNlLmN1cnJlbnQudHlwZSA9PT0gXCJpbWFnZVwiICYmIGluc3RhbmNlLmlzQW5pbWF0aW5nKSB7XG4gICAgICAgIGluc3RhbmNlLmN1cnJlbnQuJGNvbnRlbnQuY3NzKFwidHJhbnNpdGlvblwiLCBcIm5vbmVcIik7XG5cbiAgICAgICAgaW5zdGFuY2UuaXNBbmltYXRpbmcgPSBmYWxzZTtcblxuICAgICAgICBpbnN0YW5jZS51cGRhdGUodHJ1ZSwgdHJ1ZSwgMCk7XG4gICAgICB9XG5cbiAgICAgIGluc3RhbmNlLnRyaWdnZXIoXCJvbkZ1bGxzY3JlZW5DaGFuZ2VcIiwgaXNGdWxsc2NyZWVuKTtcblxuICAgICAgaW5zdGFuY2UuJHJlZnMuY29udGFpbmVyLnRvZ2dsZUNsYXNzKFwiZmFuY3lib3gtaXMtZnVsbHNjcmVlblwiLCBpc0Z1bGxzY3JlZW4pO1xuICAgIH1cbiAgfSk7XG59KShkb2N1bWVudCwgd2luZG93LmpRdWVyeSB8fCBqUXVlcnkpO1xuIl0sImZpbGUiOiJmdWxsc2NyZWVuLmpzIn0=
