// ==========================================================================
//
// Media
// Adds additional media type support
//
// ==========================================================================
(function($) {
  "use strict";

  // Formats matching url to final form

  var format = function(url, rez, params) {
    if (!url) {
      return;
    }

    params = params || "";

    if ($.type(params) === "object") {
      params = $.param(params, true);
    }

    $.each(rez, function(key, value) {
      url = url.replace("$" + key, value || "");
    });

    if (params.length) {
      url += (url.indexOf("?") > 0 ? "&" : "?") + params;
    }

    return url;
  };

  // Object containing properties for each media type

  var defaults = {
    youtube: {
      matcher: /(youtube\.com|youtu\.be|youtube\-nocookie\.com)\/(watch\?(.*&)?v=|v\/|u\/|embed\/?)?(videoseries\?list=(.*)|[\w-]{11}|\?listType=(.*)&list=(.*))(.*)/i,
      params: {
        autoplay: 1,
        autohide: 1,
        fs: 1,
        rel: 0,
        hd: 1,
        wmode: "transparent",
        enablejsapi: 1,
        html5: 1
      },
      paramPlace: 8,
      type: "iframe",
      url: "//www.youtube.com/embed/$4",
      thumb: "//img.youtube.com/vi/$4/hqdefault.jpg"
    },

    vimeo: {
      matcher: /^.+vimeo.com\/(.*\/)?([\d]+)(.*)?/,
      params: {
        autoplay: 1,
        hd: 1,
        show_title: 1,
        show_byline: 1,
        show_portrait: 0,
        fullscreen: 1,
        api: 1
      },
      paramPlace: 3,
      type: "iframe",
      url: "//player.vimeo.com/video/$2"
    },

    instagram: {
      matcher: /(instagr\.am|instagram\.com)\/p\/([a-zA-Z0-9_\-]+)\/?/i,
      type: "image",
      url: "//$1/p/$2/media/?size=l"
    },

    // Examples:
    // http://maps.google.com/?ll=48.857995,2.294297&spn=0.007666,0.021136&t=m&z=16
    // https://www.google.com/maps/@37.7852006,-122.4146355,14.65z
    // https://www.google.com/maps/@52.2111123,2.9237542,6.61z?hl=en
    // https://www.google.com/maps/place/Googleplex/@37.4220041,-122.0833494,17z/data=!4m5!3m4!1s0x0:0x6c296c66619367e0!8m2!3d37.4219998!4d-122.0840572
    gmap_place: {
      matcher: /(maps\.)?google\.([a-z]{2,3}(\.[a-z]{2})?)\/(((maps\/(place\/(.*)\/)?\@(.*),(\d+.?\d+?)z))|(\?ll=))(.*)?/i,
      type: "iframe",
      url: function(rez) {
        return (
          "//maps.google." +
          rez[2] +
          "/?ll=" +
          (rez[9] ? rez[9] + "&z=" + Math.floor(rez[10]) + (rez[12] ? rez[12].replace(/^\//, "&") : "") : rez[12] + "").replace(/\?/, "&") +
          "&output=" +
          (rez[12] && rez[12].indexOf("layer=c") > 0 ? "svembed" : "embed")
        );
      }
    },

    // Examples:
    // https://www.google.com/maps/search/Empire+State+Building/
    // https://www.google.com/maps/search/?api=1&query=centurylink+field
    // https://www.google.com/maps/search/?api=1&query=47.5951518,-122.3316393
    gmap_search: {
      matcher: /(maps\.)?google\.([a-z]{2,3}(\.[a-z]{2})?)\/(maps\/search\/)(.*)/i,
      type: "iframe",
      url: function(rez) {
        return "//maps.google." + rez[2] + "/maps?q=" + rez[5].replace("query=", "q=").replace("api=1", "") + "&output=embed";
      }
    }
  };

  $(document).on("objectNeedsType.fb", function(e, instance, item) {
    var url = item.src || "",
      type = false,
      media,
      thumb,
      rez,
      params,
      urlParams,
      paramObj,
      provider;

    media = $.extend(true, {}, defaults, item.opts.media);

    // Look for any matching media type
    $.each(media, function(providerName, providerOpts) {
      rez = url.match(providerOpts.matcher);

      if (!rez) {
        return;
      }

      type = providerOpts.type;
      provider = providerName;
      paramObj = {};

      if (providerOpts.paramPlace && rez[providerOpts.paramPlace]) {
        urlParams = rez[providerOpts.paramPlace];

        if (urlParams[0] == "?") {
          urlParams = urlParams.substring(1);
        }

        urlParams = urlParams.split("&");

        for (var m = 0; m < urlParams.length; ++m) {
          var p = urlParams[m].split("=", 2);

          if (p.length == 2) {
            paramObj[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
          }
        }
      }

      params = $.extend(true, {}, providerOpts.params, item.opts[providerName], paramObj);

      url =
        $.type(providerOpts.url) === "function" ? providerOpts.url.call(this, rez, params, item) : format(providerOpts.url, rez, params);

      thumb =
        $.type(providerOpts.thumb) === "function" ? providerOpts.thumb.call(this, rez, params, item) : format(providerOpts.thumb, rez);

      if (providerName === "youtube") {
        url = url.replace(/&t=((\d+)m)?(\d+)s/, function(match, p1, m, s) {
          return "&start=" + ((m ? parseInt(m, 10) * 60 : 0) + parseInt(s, 10));
        });
      } else if (providerName === "vimeo") {
        url = url.replace("&%23", "#");
      }

      return false;
    });

    // If it is found, then change content type and update the url

    if (type) {
      if (!item.opts.thumb && !(item.opts.$thumb && item.opts.$thumb.length)) {
        item.opts.thumb = thumb;
      }

      if (type === "iframe") {
        item.opts = $.extend(true, item.opts, {
          iframe: {
            preload: false,
            attr: {
              scrolling: "no"
            }
          }
        });
      }

      $.extend(item, {
        type: type,
        src: url,
        origSrc: item.src,
        contentSource: provider,
        contentType: type === "image" ? "image" : provider == "gmap_place" || provider == "gmap_search" ? "map" : "video"
      });
    } else if (url) {
      item.type = item.opts.defaultType;
    }
  });
})(window.jQuery || jQuery);

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJtZWRpYS5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvLyA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuLy9cbi8vIE1lZGlhXG4vLyBBZGRzIGFkZGl0aW9uYWwgbWVkaWEgdHlwZSBzdXBwb3J0XG4vL1xuLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cbihmdW5jdGlvbigkKSB7XG4gIFwidXNlIHN0cmljdFwiO1xuXG4gIC8vIEZvcm1hdHMgbWF0Y2hpbmcgdXJsIHRvIGZpbmFsIGZvcm1cblxuICB2YXIgZm9ybWF0ID0gZnVuY3Rpb24odXJsLCByZXosIHBhcmFtcykge1xuICAgIGlmICghdXJsKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgcGFyYW1zID0gcGFyYW1zIHx8IFwiXCI7XG5cbiAgICBpZiAoJC50eXBlKHBhcmFtcykgPT09IFwib2JqZWN0XCIpIHtcbiAgICAgIHBhcmFtcyA9ICQucGFyYW0ocGFyYW1zLCB0cnVlKTtcbiAgICB9XG5cbiAgICAkLmVhY2gocmV6LCBmdW5jdGlvbihrZXksIHZhbHVlKSB7XG4gICAgICB1cmwgPSB1cmwucmVwbGFjZShcIiRcIiArIGtleSwgdmFsdWUgfHwgXCJcIik7XG4gICAgfSk7XG5cbiAgICBpZiAocGFyYW1zLmxlbmd0aCkge1xuICAgICAgdXJsICs9ICh1cmwuaW5kZXhPZihcIj9cIikgPiAwID8gXCImXCIgOiBcIj9cIikgKyBwYXJhbXM7XG4gICAgfVxuXG4gICAgcmV0dXJuIHVybDtcbiAgfTtcblxuICAvLyBPYmplY3QgY29udGFpbmluZyBwcm9wZXJ0aWVzIGZvciBlYWNoIG1lZGlhIHR5cGVcblxuICB2YXIgZGVmYXVsdHMgPSB7XG4gICAgeW91dHViZToge1xuICAgICAgbWF0Y2hlcjogLyh5b3V0dWJlXFwuY29tfHlvdXR1XFwuYmV8eW91dHViZVxcLW5vY29va2llXFwuY29tKVxcLyh3YXRjaFxcPyguKiYpP3Y9fHZcXC98dVxcL3xlbWJlZFxcLz8pPyh2aWRlb3Nlcmllc1xcP2xpc3Q9KC4qKXxbXFx3LV17MTF9fFxcP2xpc3RUeXBlPSguKikmbGlzdD0oLiopKSguKikvaSxcbiAgICAgIHBhcmFtczoge1xuICAgICAgICBhdXRvcGxheTogMSxcbiAgICAgICAgYXV0b2hpZGU6IDEsXG4gICAgICAgIGZzOiAxLFxuICAgICAgICByZWw6IDAsXG4gICAgICAgIGhkOiAxLFxuICAgICAgICB3bW9kZTogXCJ0cmFuc3BhcmVudFwiLFxuICAgICAgICBlbmFibGVqc2FwaTogMSxcbiAgICAgICAgaHRtbDU6IDFcbiAgICAgIH0sXG4gICAgICBwYXJhbVBsYWNlOiA4LFxuICAgICAgdHlwZTogXCJpZnJhbWVcIixcbiAgICAgIHVybDogXCIvL3d3dy55b3V0dWJlLmNvbS9lbWJlZC8kNFwiLFxuICAgICAgdGh1bWI6IFwiLy9pbWcueW91dHViZS5jb20vdmkvJDQvaHFkZWZhdWx0LmpwZ1wiXG4gICAgfSxcblxuICAgIHZpbWVvOiB7XG4gICAgICBtYXRjaGVyOiAvXi4rdmltZW8uY29tXFwvKC4qXFwvKT8oW1xcZF0rKSguKik/LyxcbiAgICAgIHBhcmFtczoge1xuICAgICAgICBhdXRvcGxheTogMSxcbiAgICAgICAgaGQ6IDEsXG4gICAgICAgIHNob3dfdGl0bGU6IDEsXG4gICAgICAgIHNob3dfYnlsaW5lOiAxLFxuICAgICAgICBzaG93X3BvcnRyYWl0OiAwLFxuICAgICAgICBmdWxsc2NyZWVuOiAxLFxuICAgICAgICBhcGk6IDFcbiAgICAgIH0sXG4gICAgICBwYXJhbVBsYWNlOiAzLFxuICAgICAgdHlwZTogXCJpZnJhbWVcIixcbiAgICAgIHVybDogXCIvL3BsYXllci52aW1lby5jb20vdmlkZW8vJDJcIlxuICAgIH0sXG5cbiAgICBpbnN0YWdyYW06IHtcbiAgICAgIG1hdGNoZXI6IC8oaW5zdGFnclxcLmFtfGluc3RhZ3JhbVxcLmNvbSlcXC9wXFwvKFthLXpBLVowLTlfXFwtXSspXFwvPy9pLFxuICAgICAgdHlwZTogXCJpbWFnZVwiLFxuICAgICAgdXJsOiBcIi8vJDEvcC8kMi9tZWRpYS8/c2l6ZT1sXCJcbiAgICB9LFxuXG4gICAgLy8gRXhhbXBsZXM6XG4gICAgLy8gaHR0cDovL21hcHMuZ29vZ2xlLmNvbS8/bGw9NDguODU3OTk1LDIuMjk0Mjk3JnNwbj0wLjAwNzY2NiwwLjAyMTEzNiZ0PW0mej0xNlxuICAgIC8vIGh0dHBzOi8vd3d3Lmdvb2dsZS5jb20vbWFwcy9AMzcuNzg1MjAwNiwtMTIyLjQxNDYzNTUsMTQuNjV6XG4gICAgLy8gaHR0cHM6Ly93d3cuZ29vZ2xlLmNvbS9tYXBzL0A1Mi4yMTExMTIzLDIuOTIzNzU0Miw2LjYxej9obD1lblxuICAgIC8vIGh0dHBzOi8vd3d3Lmdvb2dsZS5jb20vbWFwcy9wbGFjZS9Hb29nbGVwbGV4L0AzNy40MjIwMDQxLC0xMjIuMDgzMzQ5NCwxN3ovZGF0YT0hNG01ITNtNCExczB4MDoweDZjMjk2YzY2NjE5MzY3ZTAhOG0yITNkMzcuNDIxOTk5OCE0ZC0xMjIuMDg0MDU3MlxuICAgIGdtYXBfcGxhY2U6IHtcbiAgICAgIG1hdGNoZXI6IC8obWFwc1xcLik/Z29vZ2xlXFwuKFthLXpdezIsM30oXFwuW2Etel17Mn0pPylcXC8oKChtYXBzXFwvKHBsYWNlXFwvKC4qKVxcLyk/XFxAKC4qKSwoXFxkKy4/XFxkKz8peikpfChcXD9sbD0pKSguKik/L2ksXG4gICAgICB0eXBlOiBcImlmcmFtZVwiLFxuICAgICAgdXJsOiBmdW5jdGlvbihyZXopIHtcbiAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICBcIi8vbWFwcy5nb29nbGUuXCIgK1xuICAgICAgICAgIHJlelsyXSArXG4gICAgICAgICAgXCIvP2xsPVwiICtcbiAgICAgICAgICAocmV6WzldID8gcmV6WzldICsgXCImej1cIiArIE1hdGguZmxvb3IocmV6WzEwXSkgKyAocmV6WzEyXSA/IHJlelsxMl0ucmVwbGFjZSgvXlxcLy8sIFwiJlwiKSA6IFwiXCIpIDogcmV6WzEyXSArIFwiXCIpLnJlcGxhY2UoL1xcPy8sIFwiJlwiKSArXG4gICAgICAgICAgXCImb3V0cHV0PVwiICtcbiAgICAgICAgICAocmV6WzEyXSAmJiByZXpbMTJdLmluZGV4T2YoXCJsYXllcj1jXCIpID4gMCA/IFwic3ZlbWJlZFwiIDogXCJlbWJlZFwiKVxuICAgICAgICApO1xuICAgICAgfVxuICAgIH0sXG5cbiAgICAvLyBFeGFtcGxlczpcbiAgICAvLyBodHRwczovL3d3dy5nb29nbGUuY29tL21hcHMvc2VhcmNoL0VtcGlyZStTdGF0ZStCdWlsZGluZy9cbiAgICAvLyBodHRwczovL3d3dy5nb29nbGUuY29tL21hcHMvc2VhcmNoLz9hcGk9MSZxdWVyeT1jZW50dXJ5bGluaytmaWVsZFxuICAgIC8vIGh0dHBzOi8vd3d3Lmdvb2dsZS5jb20vbWFwcy9zZWFyY2gvP2FwaT0xJnF1ZXJ5PTQ3LjU5NTE1MTgsLTEyMi4zMzE2MzkzXG4gICAgZ21hcF9zZWFyY2g6IHtcbiAgICAgIG1hdGNoZXI6IC8obWFwc1xcLik/Z29vZ2xlXFwuKFthLXpdezIsM30oXFwuW2Etel17Mn0pPylcXC8obWFwc1xcL3NlYXJjaFxcLykoLiopL2ksXG4gICAgICB0eXBlOiBcImlmcmFtZVwiLFxuICAgICAgdXJsOiBmdW5jdGlvbihyZXopIHtcbiAgICAgICAgcmV0dXJuIFwiLy9tYXBzLmdvb2dsZS5cIiArIHJlelsyXSArIFwiL21hcHM/cT1cIiArIHJlels1XS5yZXBsYWNlKFwicXVlcnk9XCIsIFwicT1cIikucmVwbGFjZShcImFwaT0xXCIsIFwiXCIpICsgXCImb3V0cHV0PWVtYmVkXCI7XG4gICAgICB9XG4gICAgfVxuICB9O1xuXG4gICQoZG9jdW1lbnQpLm9uKFwib2JqZWN0TmVlZHNUeXBlLmZiXCIsIGZ1bmN0aW9uKGUsIGluc3RhbmNlLCBpdGVtKSB7XG4gICAgdmFyIHVybCA9IGl0ZW0uc3JjIHx8IFwiXCIsXG4gICAgICB0eXBlID0gZmFsc2UsXG4gICAgICBtZWRpYSxcbiAgICAgIHRodW1iLFxuICAgICAgcmV6LFxuICAgICAgcGFyYW1zLFxuICAgICAgdXJsUGFyYW1zLFxuICAgICAgcGFyYW1PYmosXG4gICAgICBwcm92aWRlcjtcblxuICAgIG1lZGlhID0gJC5leHRlbmQodHJ1ZSwge30sIGRlZmF1bHRzLCBpdGVtLm9wdHMubWVkaWEpO1xuXG4gICAgLy8gTG9vayBmb3IgYW55IG1hdGNoaW5nIG1lZGlhIHR5cGVcbiAgICAkLmVhY2gobWVkaWEsIGZ1bmN0aW9uKHByb3ZpZGVyTmFtZSwgcHJvdmlkZXJPcHRzKSB7XG4gICAgICByZXogPSB1cmwubWF0Y2gocHJvdmlkZXJPcHRzLm1hdGNoZXIpO1xuXG4gICAgICBpZiAoIXJleikge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIHR5cGUgPSBwcm92aWRlck9wdHMudHlwZTtcbiAgICAgIHByb3ZpZGVyID0gcHJvdmlkZXJOYW1lO1xuICAgICAgcGFyYW1PYmogPSB7fTtcblxuICAgICAgaWYgKHByb3ZpZGVyT3B0cy5wYXJhbVBsYWNlICYmIHJleltwcm92aWRlck9wdHMucGFyYW1QbGFjZV0pIHtcbiAgICAgICAgdXJsUGFyYW1zID0gcmV6W3Byb3ZpZGVyT3B0cy5wYXJhbVBsYWNlXTtcblxuICAgICAgICBpZiAodXJsUGFyYW1zWzBdID09IFwiP1wiKSB7XG4gICAgICAgICAgdXJsUGFyYW1zID0gdXJsUGFyYW1zLnN1YnN0cmluZygxKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHVybFBhcmFtcyA9IHVybFBhcmFtcy5zcGxpdChcIiZcIik7XG5cbiAgICAgICAgZm9yICh2YXIgbSA9IDA7IG0gPCB1cmxQYXJhbXMubGVuZ3RoOyArK20pIHtcbiAgICAgICAgICB2YXIgcCA9IHVybFBhcmFtc1ttXS5zcGxpdChcIj1cIiwgMik7XG5cbiAgICAgICAgICBpZiAocC5sZW5ndGggPT0gMikge1xuICAgICAgICAgICAgcGFyYW1PYmpbcFswXV0gPSBkZWNvZGVVUklDb21wb25lbnQocFsxXS5yZXBsYWNlKC9cXCsvZywgXCIgXCIpKTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgcGFyYW1zID0gJC5leHRlbmQodHJ1ZSwge30sIHByb3ZpZGVyT3B0cy5wYXJhbXMsIGl0ZW0ub3B0c1twcm92aWRlck5hbWVdLCBwYXJhbU9iaik7XG5cbiAgICAgIHVybCA9XG4gICAgICAgICQudHlwZShwcm92aWRlck9wdHMudXJsKSA9PT0gXCJmdW5jdGlvblwiID8gcHJvdmlkZXJPcHRzLnVybC5jYWxsKHRoaXMsIHJleiwgcGFyYW1zLCBpdGVtKSA6IGZvcm1hdChwcm92aWRlck9wdHMudXJsLCByZXosIHBhcmFtcyk7XG5cbiAgICAgIHRodW1iID1cbiAgICAgICAgJC50eXBlKHByb3ZpZGVyT3B0cy50aHVtYikgPT09IFwiZnVuY3Rpb25cIiA/IHByb3ZpZGVyT3B0cy50aHVtYi5jYWxsKHRoaXMsIHJleiwgcGFyYW1zLCBpdGVtKSA6IGZvcm1hdChwcm92aWRlck9wdHMudGh1bWIsIHJleik7XG5cbiAgICAgIGlmIChwcm92aWRlck5hbWUgPT09IFwieW91dHViZVwiKSB7XG4gICAgICAgIHVybCA9IHVybC5yZXBsYWNlKC8mdD0oKFxcZCspbSk/KFxcZCspcy8sIGZ1bmN0aW9uKG1hdGNoLCBwMSwgbSwgcykge1xuICAgICAgICAgIHJldHVybiBcIiZzdGFydD1cIiArICgobSA/IHBhcnNlSW50KG0sIDEwKSAqIDYwIDogMCkgKyBwYXJzZUludChzLCAxMCkpO1xuICAgICAgICB9KTtcbiAgICAgIH0gZWxzZSBpZiAocHJvdmlkZXJOYW1lID09PSBcInZpbWVvXCIpIHtcbiAgICAgICAgdXJsID0gdXJsLnJlcGxhY2UoXCImJTIzXCIsIFwiI1wiKTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH0pO1xuXG4gICAgLy8gSWYgaXQgaXMgZm91bmQsIHRoZW4gY2hhbmdlIGNvbnRlbnQgdHlwZSBhbmQgdXBkYXRlIHRoZSB1cmxcblxuICAgIGlmICh0eXBlKSB7XG4gICAgICBpZiAoIWl0ZW0ub3B0cy50aHVtYiAmJiAhKGl0ZW0ub3B0cy4kdGh1bWIgJiYgaXRlbS5vcHRzLiR0aHVtYi5sZW5ndGgpKSB7XG4gICAgICAgIGl0ZW0ub3B0cy50aHVtYiA9IHRodW1iO1xuICAgICAgfVxuXG4gICAgICBpZiAodHlwZSA9PT0gXCJpZnJhbWVcIikge1xuICAgICAgICBpdGVtLm9wdHMgPSAkLmV4dGVuZCh0cnVlLCBpdGVtLm9wdHMsIHtcbiAgICAgICAgICBpZnJhbWU6IHtcbiAgICAgICAgICAgIHByZWxvYWQ6IGZhbHNlLFxuICAgICAgICAgICAgYXR0cjoge1xuICAgICAgICAgICAgICBzY3JvbGxpbmc6IFwibm9cIlxuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICB9XG5cbiAgICAgICQuZXh0ZW5kKGl0ZW0sIHtcbiAgICAgICAgdHlwZTogdHlwZSxcbiAgICAgICAgc3JjOiB1cmwsXG4gICAgICAgIG9yaWdTcmM6IGl0ZW0uc3JjLFxuICAgICAgICBjb250ZW50U291cmNlOiBwcm92aWRlcixcbiAgICAgICAgY29udGVudFR5cGU6IHR5cGUgPT09IFwiaW1hZ2VcIiA/IFwiaW1hZ2VcIiA6IHByb3ZpZGVyID09IFwiZ21hcF9wbGFjZVwiIHx8IHByb3ZpZGVyID09IFwiZ21hcF9zZWFyY2hcIiA/IFwibWFwXCIgOiBcInZpZGVvXCJcbiAgICAgIH0pO1xuICAgIH0gZWxzZSBpZiAodXJsKSB7XG4gICAgICBpdGVtLnR5cGUgPSBpdGVtLm9wdHMuZGVmYXVsdFR5cGU7XG4gICAgfVxuICB9KTtcbn0pKHdpbmRvdy5qUXVlcnkgfHwgalF1ZXJ5KTtcbiJdLCJmaWxlIjoibWVkaWEuanMifQ==
