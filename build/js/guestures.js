// ==========================================================================
//
// Guestures
// Adds touch guestures, handles click and tap events
//
// ==========================================================================
(function(window, document, $) {
  "use strict";

  var requestAFrame = (function() {
    return (
      window.requestAnimationFrame ||
      window.webkitRequestAnimationFrame ||
      window.mozRequestAnimationFrame ||
      window.oRequestAnimationFrame ||
      // if all else fails, use setTimeout
      function(callback) {
        return window.setTimeout(callback, 1000 / 60);
      }
    );
  })();

  var cancelAFrame = (function() {
    return (
      window.cancelAnimationFrame ||
      window.webkitCancelAnimationFrame ||
      window.mozCancelAnimationFrame ||
      window.oCancelAnimationFrame ||
      function(id) {
        window.clearTimeout(id);
      }
    );
  })();

  var getPointerXY = function(e) {
    var result = [];

    e = e.originalEvent || e || window.e;
    e = e.touches && e.touches.length ? e.touches : e.changedTouches && e.changedTouches.length ? e.changedTouches : [e];

    for (var key in e) {
      if (e[key].pageX) {
        result.push({
          x: e[key].pageX,
          y: e[key].pageY
        });
      } else if (e[key].clientX) {
        result.push({
          x: e[key].clientX,
          y: e[key].clientY
        });
      }
    }

    return result;
  };

  var distance = function(point2, point1, what) {
    if (!point1 || !point2) {
      return 0;
    }

    if (what === "x") {
      return point2.x - point1.x;
    } else if (what === "y") {
      return point2.y - point1.y;
    }

    return Math.sqrt(Math.pow(point2.x - point1.x, 2) + Math.pow(point2.y - point1.y, 2));
  };

  var isClickable = function($el) {
    if (
      $el.is('a,area,button,[role="button"],input,label,select,summary,textarea,video,audio') ||
      $.isFunction($el.get(0).onclick) ||
      $el.data("selectable")
    ) {
      return true;
    }

    // Check for attributes like data-fancybox-next or data-fancybox-close
    for (var i = 0, atts = $el[0].attributes, n = atts.length; i < n; i++) {
      if (atts[i].nodeName.substr(0, 14) === "data-fancybox-") {
        return true;
      }
    }

    return false;
  };

  var hasScrollbars = function(el) {
    var overflowY = window.getComputedStyle(el)["overflow-y"],
      overflowX = window.getComputedStyle(el)["overflow-x"],
      vertical = (overflowY === "scroll" || overflowY === "auto") && el.scrollHeight > el.clientHeight,
      horizontal = (overflowX === "scroll" || overflowX === "auto") && el.scrollWidth > el.clientWidth;

    return vertical || horizontal;
  };

  var isScrollable = function($el) {
    var rez = false;

    while (true) {
      rez = hasScrollbars($el.get(0));

      if (rez) {
        break;
      }

      $el = $el.parent();

      if (!$el.length || $el.hasClass("fancybox-stage") || $el.is("body")) {
        break;
      }
    }

    return rez;
  };

  var Guestures = function(instance) {
    var self = this;

    self.instance = instance;

    self.$bg = instance.$refs.bg;
    self.$stage = instance.$refs.stage;
    self.$container = instance.$refs.container;

    self.destroy();

    self.$container.on("touchstart.fb.touch mousedown.fb.touch", $.proxy(self, "ontouchstart"));
  };

  Guestures.prototype.destroy = function() {
    this.$container.off(".fb.touch");
  };

  Guestures.prototype.ontouchstart = function(e) {
    var self = this,
      $target = $(e.target),
      instance = self.instance,
      current = instance.current,
      $content = current.$content,
      isTouchDevice = e.type == "touchstart";

    // Do not respond to both (touch and mouse) events
    if (isTouchDevice) {
      self.$container.off("mousedown.fb.touch");
    }

    // Ignore right click
    if (e.originalEvent && e.originalEvent.button == 2) {
      return;
    }

    // Ignore taping on links, buttons, input elements
    if (!$target.length || isClickable($target) || isClickable($target.parent())) {
      return;
    }

    // Ignore clicks on the scrollbar
    if (!$target.is("img") && e.originalEvent.clientX > $target[0].clientWidth + $target.offset().left) {
      return;
    }

    // Ignore clicks while zooming or closing
    if (!current || instance.isAnimating || instance.isClosing) {
      e.stopPropagation();
      e.preventDefault();

      return;
    }

    self.realPoints = self.startPoints = getPointerXY(e);

    if (!self.startPoints.length) {
      return;
    }

    e.stopPropagation();

    self.startEvent = e;

    self.canTap = true;
    self.$target = $target;
    self.$content = $content;
    self.opts = current.opts.touch;

    self.isPanning = false;
    self.isSwiping = false;
    self.isZooming = false;
    self.isScrolling = false;

    self.startTime = new Date().getTime();
    self.distanceX = self.distanceY = self.distance = 0;

    self.canvasWidth = Math.round(current.$slide[0].clientWidth);
    self.canvasHeight = Math.round(current.$slide[0].clientHeight);

    self.contentLastPos = null;
    self.contentStartPos = $.fancybox.getTranslate(self.$content) || {top: 0, left: 0};
    self.sliderStartPos = self.sliderLastPos || $.fancybox.getTranslate(current.$slide);

    // Since position will be absolute, but we need to make it relative to the stage
    self.stagePos = $.fancybox.getTranslate(instance.$refs.stage);

    self.sliderStartPos.top -= self.stagePos.top;
    self.sliderStartPos.left -= self.stagePos.left;

    self.contentStartPos.top -= self.stagePos.top;
    self.contentStartPos.left -= self.stagePos.left;

    $(document)
      .off(".fb.touch")
      .on(isTouchDevice ? "touchend.fb.touch touchcancel.fb.touch" : "mouseup.fb.touch mouseleave.fb.touch", $.proxy(self, "ontouchend"))
      .on(isTouchDevice ? "touchmove.fb.touch" : "mousemove.fb.touch", $.proxy(self, "ontouchmove"));

    if ($.fancybox.isMobile) {
      document.addEventListener("scroll", self.onscroll, true);
    }

    if (!(self.opts || instance.canPan()) || !($target.is(self.$stage) || self.$stage.find($target).length)) {
      if ($target.is(".fancybox-image")) {
        e.preventDefault();
      }

      return;
    }

    if (!($.fancybox.isMobile && (isScrollable($target) || isScrollable($target.parent())))) {
      e.preventDefault();
    }

    if (self.startPoints.length === 1 || current.hasError) {
      if (self.instance.canPan()) {
        $.fancybox.stop(self.$content);

        self.$content.css("transition-duration", "");

        self.isPanning = true;
      } else {
        self.isSwiping = true;
      }

      self.$container.addClass("fancybox-controls--isGrabbing");
    }

    if (self.startPoints.length === 2 && current.type === "image" && (current.isLoaded || current.$ghost)) {
      self.canTap = false;
      self.isSwiping = false;
      self.isPanning = false;

      self.isZooming = true;

      $.fancybox.stop(self.$content);

      self.$content.css("transition-duration", "");

      self.centerPointStartX = (self.startPoints[0].x + self.startPoints[1].x) * 0.5 - $(window).scrollLeft();
      self.centerPointStartY = (self.startPoints[0].y + self.startPoints[1].y) * 0.5 - $(window).scrollTop();

      self.percentageOfImageAtPinchPointX = (self.centerPointStartX - self.contentStartPos.left) / self.contentStartPos.width;
      self.percentageOfImageAtPinchPointY = (self.centerPointStartY - self.contentStartPos.top) / self.contentStartPos.height;

      self.startDistanceBetweenFingers = distance(self.startPoints[0], self.startPoints[1]);
    }
  };

  Guestures.prototype.onscroll = function(e) {
    var self = this;

    self.isScrolling = true;

    document.removeEventListener("scroll", self.onscroll, true);
  };

  Guestures.prototype.ontouchmove = function(e) {
    var self = this,
      $target = $(e.target);

    // Make sure user has not released over iframe or disabled element
    if (e.originalEvent.buttons !== undefined && e.originalEvent.buttons === 0) {
      self.ontouchend(e);
      return;
    }

    if (self.isScrolling || !($target.is(self.$stage) || self.$stage.find($target).length)) {
      self.canTap = false;

      return;
    }

    self.newPoints = getPointerXY(e);

    if (!(self.opts || self.instance.canPan()) || !self.newPoints.length || !self.newPoints.length) {
      return;
    }

    if (!(self.isSwiping && self.isSwiping === true)) {
      e.preventDefault();
    }

    self.distanceX = distance(self.newPoints[0], self.startPoints[0], "x");
    self.distanceY = distance(self.newPoints[0], self.startPoints[0], "y");

    self.distance = distance(self.newPoints[0], self.startPoints[0]);

    // Skip false ontouchmove events (Chrome)
    if (self.distance > 0) {
      if (self.isSwiping) {
        self.onSwipe(e);
      } else if (self.isPanning) {
        self.onPan();
      } else if (self.isZooming) {
        self.onZoom();
      }
    }
  };

  Guestures.prototype.onSwipe = function(e) {
    var self = this,
      swiping = self.isSwiping,
      left = self.sliderStartPos.left || 0,
      angle;

    // If direction is not yet determined
    if (swiping === true) {
      // We need at least 10px distance to correctly calculate an angle
      if (Math.abs(self.distance) > 10) {
        self.canTap = false;

        if (self.instance.group.length < 2 && self.opts.vertical) {
          self.isSwiping = "y";
        } else if (self.instance.isDragging || self.opts.vertical === false || (self.opts.vertical === "auto" && $(window).width() > 800)) {
          self.isSwiping = "x";
        } else {
          angle = Math.abs(Math.atan2(self.distanceY, self.distanceX) * 180 / Math.PI);

          self.isSwiping = angle > 45 && angle < 135 ? "y" : "x";
        }

        self.canTap = false;

        if (self.isSwiping === "y" && $.fancybox.isMobile && (isScrollable(self.$target) || isScrollable(self.$target.parent()))) {
          self.isScrolling = true;

          return;
        }

        self.instance.isDragging = self.isSwiping;

        // Reset points to avoid jumping, because we dropped first swipes to calculate the angle
        self.startPoints = self.newPoints;

        $.each(self.instance.slides, function(index, slide) {
          $.fancybox.stop(slide.$slide);

          slide.$slide.css("transition-duration", "");

          slide.inTransition = false;

          if (slide.pos === self.instance.current.pos) {
            self.sliderStartPos.left = $.fancybox.getTranslate(slide.$slide).left - $.fancybox.getTranslate(self.instance.$refs.stage).left;
          }
        });

        // Stop slideshow
        if (self.instance.SlideShow && self.instance.SlideShow.isActive) {
          self.instance.SlideShow.stop();
        }
      }

      return;
    }

    // Sticky edges
    if (swiping == "x") {
      if (
        self.distanceX > 0 &&
        (self.instance.group.length < 2 || (self.instance.current.index === 0 && !self.instance.current.opts.loop))
      ) {
        left = left + Math.pow(self.distanceX, 0.8);
      } else if (
        self.distanceX < 0 &&
        (self.instance.group.length < 2 ||
          (self.instance.current.index === self.instance.group.length - 1 && !self.instance.current.opts.loop))
      ) {
        left = left - Math.pow(-self.distanceX, 0.8);
      } else {
        left = left + self.distanceX;
      }
    }

    self.sliderLastPos = {
      top: swiping == "x" ? 0 : self.sliderStartPos.top + self.distanceY,
      left: left
    };

    if (self.requestId) {
      cancelAFrame(self.requestId);

      self.requestId = null;
    }

    self.requestId = requestAFrame(function() {
      if (self.sliderLastPos) {
        $.each(self.instance.slides, function(index, slide) {
          var pos = slide.pos - self.instance.currPos;

          $.fancybox.setTranslate(slide.$slide, {
            top: self.sliderLastPos.top,
            left: self.sliderLastPos.left + pos * self.canvasWidth + pos * slide.opts.gutter
          });
        });

        self.$container.addClass("fancybox-is-sliding");
      }
    });
  };

  Guestures.prototype.onPan = function() {
    var self = this;

    // Prevent accidental movement (sometimes, when tapping casually, finger can move a bit)
    if (distance(self.newPoints[0], self.realPoints[0]) < ($.fancybox.isMobile ? 10 : 5)) {
      self.startPoints = self.newPoints;
      return;
    }

    self.canTap = false;

    self.contentLastPos = self.limitMovement();

    if (self.requestId) {
      cancelAFrame(self.requestId);

      self.requestId = null;
    }

    self.requestId = requestAFrame(function() {
      $.fancybox.setTranslate(self.$content, self.contentLastPos);
    });
  };

  // Make panning sticky to the edges
  Guestures.prototype.limitMovement = function() {
    var self = this;

    var canvasWidth = self.canvasWidth;
    var canvasHeight = self.canvasHeight;

    var distanceX = self.distanceX;
    var distanceY = self.distanceY;

    var contentStartPos = self.contentStartPos;

    var currentOffsetX = contentStartPos.left;
    var currentOffsetY = contentStartPos.top;

    var currentWidth = contentStartPos.width;
    var currentHeight = contentStartPos.height;

    var minTranslateX, minTranslateY, maxTranslateX, maxTranslateY, newOffsetX, newOffsetY;

    if (currentWidth > canvasWidth) {
      newOffsetX = currentOffsetX + distanceX;
    } else {
      newOffsetX = currentOffsetX;
    }

    newOffsetY = currentOffsetY + distanceY;

    // Slow down proportionally to traveled distance
    minTranslateX = Math.max(0, canvasWidth * 0.5 - currentWidth * 0.5);
    minTranslateY = Math.max(0, canvasHeight * 0.5 - currentHeight * 0.5);

    maxTranslateX = Math.min(canvasWidth - currentWidth, canvasWidth * 0.5 - currentWidth * 0.5);
    maxTranslateY = Math.min(canvasHeight - currentHeight, canvasHeight * 0.5 - currentHeight * 0.5);

    //   ->
    if (distanceX > 0 && newOffsetX > minTranslateX) {
      newOffsetX = minTranslateX - 1 + Math.pow(-minTranslateX + currentOffsetX + distanceX, 0.8) || 0;
    }

    //    <-
    if (distanceX < 0 && newOffsetX < maxTranslateX) {
      newOffsetX = maxTranslateX + 1 - Math.pow(maxTranslateX - currentOffsetX - distanceX, 0.8) || 0;
    }

    //   \/
    if (distanceY > 0 && newOffsetY > minTranslateY) {
      newOffsetY = minTranslateY - 1 + Math.pow(-minTranslateY + currentOffsetY + distanceY, 0.8) || 0;
    }

    //   /\
    if (distanceY < 0 && newOffsetY < maxTranslateY) {
      newOffsetY = maxTranslateY + 1 - Math.pow(maxTranslateY - currentOffsetY - distanceY, 0.8) || 0;
    }

    return {
      top: newOffsetY,
      left: newOffsetX
    };
  };

  Guestures.prototype.limitPosition = function(newOffsetX, newOffsetY, newWidth, newHeight) {
    var self = this;

    var canvasWidth = self.canvasWidth;
    var canvasHeight = self.canvasHeight;

    if (newWidth > canvasWidth) {
      newOffsetX = newOffsetX > 0 ? 0 : newOffsetX;
      newOffsetX = newOffsetX < canvasWidth - newWidth ? canvasWidth - newWidth : newOffsetX;
    } else {
      // Center horizontally
      newOffsetX = Math.max(0, canvasWidth / 2 - newWidth / 2);
    }

    if (newHeight > canvasHeight) {
      newOffsetY = newOffsetY > 0 ? 0 : newOffsetY;
      newOffsetY = newOffsetY < canvasHeight - newHeight ? canvasHeight - newHeight : newOffsetY;
    } else {
      // Center vertically
      newOffsetY = Math.max(0, canvasHeight / 2 - newHeight / 2);
    }

    return {
      top: newOffsetY,
      left: newOffsetX
    };
  };

  Guestures.prototype.onZoom = function() {
    var self = this;

    // Calculate current distance between points to get pinch ratio and new width and height
    var contentStartPos = self.contentStartPos;

    var currentWidth = contentStartPos.width;
    var currentHeight = contentStartPos.height;

    var currentOffsetX = contentStartPos.left;
    var currentOffsetY = contentStartPos.top;

    var endDistanceBetweenFingers = distance(self.newPoints[0], self.newPoints[1]);

    var pinchRatio = endDistanceBetweenFingers / self.startDistanceBetweenFingers;

    var newWidth = Math.floor(currentWidth * pinchRatio);
    var newHeight = Math.floor(currentHeight * pinchRatio);

    // This is the translation due to pinch-zooming
    var translateFromZoomingX = (currentWidth - newWidth) * self.percentageOfImageAtPinchPointX;
    var translateFromZoomingY = (currentHeight - newHeight) * self.percentageOfImageAtPinchPointY;

    // Point between the two touches
    var centerPointEndX = (self.newPoints[0].x + self.newPoints[1].x) / 2 - $(window).scrollLeft();
    var centerPointEndY = (self.newPoints[0].y + self.newPoints[1].y) / 2 - $(window).scrollTop();

    // And this is the translation due to translation of the centerpoint
    // between the two fingers
    var translateFromTranslatingX = centerPointEndX - self.centerPointStartX;
    var translateFromTranslatingY = centerPointEndY - self.centerPointStartY;

    // The new offset is the old/current one plus the total translation
    var newOffsetX = currentOffsetX + (translateFromZoomingX + translateFromTranslatingX);
    var newOffsetY = currentOffsetY + (translateFromZoomingY + translateFromTranslatingY);

    var newPos = {
      top: newOffsetY,
      left: newOffsetX,
      scaleX: pinchRatio,
      scaleY: pinchRatio
    };

    self.canTap = false;

    self.newWidth = newWidth;
    self.newHeight = newHeight;

    self.contentLastPos = newPos;

    if (self.requestId) {
      cancelAFrame(self.requestId);

      self.requestId = null;
    }

    self.requestId = requestAFrame(function() {
      $.fancybox.setTranslate(self.$content, self.contentLastPos);
    });
  };

  Guestures.prototype.ontouchend = function(e) {
    var self = this;
    var dMs = Math.max(new Date().getTime() - self.startTime, 1);

    var swiping = self.isSwiping;
    var panning = self.isPanning;
    var zooming = self.isZooming;
    var scrolling = self.isScrolling;

    self.endPoints = getPointerXY(e);

    self.$container.removeClass("fancybox-controls--isGrabbing");

    $(document).off(".fb.touch");

    document.removeEventListener("scroll", self.onscroll, true);

    if (self.requestId) {
      cancelAFrame(self.requestId);

      self.requestId = null;
    }

    self.isSwiping = false;
    self.isPanning = false;
    self.isZooming = false;
    self.isScrolling = false;

    self.instance.isDragging = false;

    if (self.canTap) {
      return self.onTap(e);
    }

    self.speed = 366;

    // Speed in px/ms
    self.velocityX = self.distanceX / dMs * 0.5;
    self.velocityY = self.distanceY / dMs * 0.5;

    self.speedX = Math.max(self.speed * 0.5, Math.min(self.speed * 1.5, 1 / Math.abs(self.velocityX) * self.speed));

    if (panning) {
      self.endPanning();
    } else if (zooming) {
      self.endZooming();
    } else {
      self.endSwiping(swiping, scrolling);
    }

    return;
  };

  Guestures.prototype.endSwiping = function(swiping, scrolling) {
    var self = this,
      ret = false,
      len = self.instance.group.length;

    self.sliderLastPos = null;

    // Close if swiped vertically / navigate if horizontally
    if (swiping == "y" && !scrolling && Math.abs(self.distanceY) > 50) {
      // Continue vertical movement
      $.fancybox.animate(
        self.instance.current.$slide,
        {
          top: self.sliderStartPos.top + self.distanceY + self.velocityY * 150,
          opacity: 0
        },
        200
      );

      ret = self.instance.close(true, 200);
    } else if (swiping == "x" && self.distanceX > 50 && len > 1) {
      ret = self.instance.previous(self.speedX);
    } else if (swiping == "x" && self.distanceX < -50 && len > 1) {
      ret = self.instance.next(self.speedX);
    }

    if (ret === false && (swiping == "x" || swiping == "y")) {
      if (scrolling || len < 2) {
        self.instance.centerSlide(self.instance.current, 150);
      } else {
        self.instance.jumpTo(self.instance.current.index);
      }
    }

    self.$container.removeClass("fancybox-is-sliding");
  };

  // Limit panning from edges
  // ========================
  Guestures.prototype.endPanning = function() {
    var self = this;
    var newOffsetX, newOffsetY, newPos;

    if (!self.contentLastPos) {
      return;
    }

    if (self.opts.momentum === false) {
      newOffsetX = self.contentLastPos.left;
      newOffsetY = self.contentLastPos.top;
    } else {
      // Continue movement
      newOffsetX = self.contentLastPos.left + self.velocityX * self.speed;
      newOffsetY = self.contentLastPos.top + self.velocityY * self.speed;
    }

    newPos = self.limitPosition(newOffsetX, newOffsetY, self.contentStartPos.width, self.contentStartPos.height);

    newPos.width = self.contentStartPos.width;
    newPos.height = self.contentStartPos.height;

    $.fancybox.animate(self.$content, newPos, 330);
  };

  Guestures.prototype.endZooming = function() {
    var self = this;

    var current = self.instance.current;

    var newOffsetX, newOffsetY, newPos, reset;

    var newWidth = self.newWidth;
    var newHeight = self.newHeight;

    if (!self.contentLastPos) {
      return;
    }

    newOffsetX = self.contentLastPos.left;
    newOffsetY = self.contentLastPos.top;

    reset = {
      top: newOffsetY,
      left: newOffsetX,
      width: newWidth,
      height: newHeight,
      scaleX: 1,
      scaleY: 1
    };

    // Reset scalex/scaleY values; this helps for perfomance and does not break animation
    $.fancybox.setTranslate(self.$content, reset);

    if (newWidth < self.canvasWidth && newHeight < self.canvasHeight) {
      self.instance.scaleToFit(150);
    } else if (newWidth > current.width || newHeight > current.height) {
      self.instance.scaleToActual(self.centerPointStartX, self.centerPointStartY, 150);
    } else {
      newPos = self.limitPosition(newOffsetX, newOffsetY, newWidth, newHeight);

      // Switch from scale() to width/height or animation will not work correctly
      $.fancybox.setTranslate(self.$content, $.fancybox.getTranslate(self.$content));

      $.fancybox.animate(self.$content, newPos, 150);
    }
  };

  Guestures.prototype.onTap = function(e) {
    var self = this;
    var $target = $(e.target);

    var instance = self.instance;
    var current = instance.current;

    var endPoints = (e && getPointerXY(e)) || self.startPoints;

    var tapX = endPoints[0] ? endPoints[0].x - $(window).scrollLeft() - self.stagePos.left : 0;
    var tapY = endPoints[0] ? endPoints[0].y - $(window).scrollTop() - self.stagePos.top : 0;

    var where;

    var process = function(prefix) {
      var action = current.opts[prefix];

      if ($.isFunction(action)) {
        action = action.apply(instance, [current, e]);
      }

      if (!action) {
        return;
      }

      switch (action) {
        case "close":
          instance.close(self.startEvent);

          break;

        case "toggleControls":
          instance.toggleControls(true);

          break;

        case "next":
          instance.next();

          break;

        case "nextOrClose":
          if (instance.group.length > 1) {
            instance.next();
          } else {
            instance.close(self.startEvent);
          }

          break;

        case "zoom":
          if (current.type == "image" && (current.isLoaded || current.$ghost)) {
            if (instance.canPan()) {
              instance.scaleToFit();
            } else if (instance.isScaledDown()) {
              instance.scaleToActual(tapX, tapY);
            } else if (instance.group.length < 2) {
              instance.close(self.startEvent);
            }
          }

          break;
      }
    };

    // Ignore right click
    if (e.originalEvent && e.originalEvent.button == 2) {
      return;
    }

    // Skip if clicked on the scrollbar
    if (!$target.is("img") && tapX > $target[0].clientWidth + $target.offset().left) {
      return;
    }

    // Check where is clicked
    if ($target.is(".fancybox-bg,.fancybox-inner,.fancybox-outer,.fancybox-container")) {
      where = "Outside";
    } else if ($target.is(".fancybox-slide")) {
      where = "Slide";
    } else if (
      instance.current.$content &&
      instance.current.$content
        .find($target)
        .addBack()
        .filter($target).length
    ) {
      where = "Content";
    } else {
      return;
    }

    // Check if this is a double tap
    if (self.tapped) {
      // Stop previously created single tap
      clearTimeout(self.tapped);
      self.tapped = null;

      // Skip if distance between taps is too big
      if (Math.abs(tapX - self.tapX) > 50 || Math.abs(tapY - self.tapY) > 50) {
        return this;
      }

      // OK, now we assume that this is a double-tap
      process("dblclick" + where);
    } else {
      // Single tap will be processed if user has not clicked second time within 300ms
      // or there is no need to wait for double-tap
      self.tapX = tapX;
      self.tapY = tapY;

      if (current.opts["dblclick" + where] && current.opts["dblclick" + where] !== current.opts["click" + where]) {
        self.tapped = setTimeout(function() {
          self.tapped = null;

          process("click" + where);
        }, 500);
      } else {
        process("click" + where);
      }
    }

    return this;
  };

  $(document).on("onActivate.fb", function(e, instance) {
    if (instance && !instance.Guestures) {
      instance.Guestures = new Guestures(instance);
    }
  });
})(window, document, window.jQuery || jQuery);

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJndWVzdHVyZXMuanMiXSwic291cmNlc0NvbnRlbnQiOlsiLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cbi8vXG4vLyBHdWVzdHVyZXNcbi8vIEFkZHMgdG91Y2ggZ3Vlc3R1cmVzLCBoYW5kbGVzIGNsaWNrIGFuZCB0YXAgZXZlbnRzXG4vL1xuLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cbihmdW5jdGlvbih3aW5kb3csIGRvY3VtZW50LCAkKSB7XG4gIFwidXNlIHN0cmljdFwiO1xuXG4gIHZhciByZXF1ZXN0QUZyYW1lID0gKGZ1bmN0aW9uKCkge1xuICAgIHJldHVybiAoXG4gICAgICB3aW5kb3cucmVxdWVzdEFuaW1hdGlvbkZyYW1lIHx8XG4gICAgICB3aW5kb3cud2Via2l0UmVxdWVzdEFuaW1hdGlvbkZyYW1lIHx8XG4gICAgICB3aW5kb3cubW96UmVxdWVzdEFuaW1hdGlvbkZyYW1lIHx8XG4gICAgICB3aW5kb3cub1JlcXVlc3RBbmltYXRpb25GcmFtZSB8fFxuICAgICAgLy8gaWYgYWxsIGVsc2UgZmFpbHMsIHVzZSBzZXRUaW1lb3V0XG4gICAgICBmdW5jdGlvbihjYWxsYmFjaykge1xuICAgICAgICByZXR1cm4gd2luZG93LnNldFRpbWVvdXQoY2FsbGJhY2ssIDEwMDAgLyA2MCk7XG4gICAgICB9XG4gICAgKTtcbiAgfSkoKTtcblxuICB2YXIgY2FuY2VsQUZyYW1lID0gKGZ1bmN0aW9uKCkge1xuICAgIHJldHVybiAoXG4gICAgICB3aW5kb3cuY2FuY2VsQW5pbWF0aW9uRnJhbWUgfHxcbiAgICAgIHdpbmRvdy53ZWJraXRDYW5jZWxBbmltYXRpb25GcmFtZSB8fFxuICAgICAgd2luZG93Lm1vekNhbmNlbEFuaW1hdGlvbkZyYW1lIHx8XG4gICAgICB3aW5kb3cub0NhbmNlbEFuaW1hdGlvbkZyYW1lIHx8XG4gICAgICBmdW5jdGlvbihpZCkge1xuICAgICAgICB3aW5kb3cuY2xlYXJUaW1lb3V0KGlkKTtcbiAgICAgIH1cbiAgICApO1xuICB9KSgpO1xuXG4gIHZhciBnZXRQb2ludGVyWFkgPSBmdW5jdGlvbihlKSB7XG4gICAgdmFyIHJlc3VsdCA9IFtdO1xuXG4gICAgZSA9IGUub3JpZ2luYWxFdmVudCB8fCBlIHx8IHdpbmRvdy5lO1xuICAgIGUgPSBlLnRvdWNoZXMgJiYgZS50b3VjaGVzLmxlbmd0aCA/IGUudG91Y2hlcyA6IGUuY2hhbmdlZFRvdWNoZXMgJiYgZS5jaGFuZ2VkVG91Y2hlcy5sZW5ndGggPyBlLmNoYW5nZWRUb3VjaGVzIDogW2VdO1xuXG4gICAgZm9yICh2YXIga2V5IGluIGUpIHtcbiAgICAgIGlmIChlW2tleV0ucGFnZVgpIHtcbiAgICAgICAgcmVzdWx0LnB1c2goe1xuICAgICAgICAgIHg6IGVba2V5XS5wYWdlWCxcbiAgICAgICAgICB5OiBlW2tleV0ucGFnZVlcbiAgICAgICAgfSk7XG4gICAgICB9IGVsc2UgaWYgKGVba2V5XS5jbGllbnRYKSB7XG4gICAgICAgIHJlc3VsdC5wdXNoKHtcbiAgICAgICAgICB4OiBlW2tleV0uY2xpZW50WCxcbiAgICAgICAgICB5OiBlW2tleV0uY2xpZW50WVxuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gcmVzdWx0O1xuICB9O1xuXG4gIHZhciBkaXN0YW5jZSA9IGZ1bmN0aW9uKHBvaW50MiwgcG9pbnQxLCB3aGF0KSB7XG4gICAgaWYgKCFwb2ludDEgfHwgIXBvaW50Mikge1xuICAgICAgcmV0dXJuIDA7XG4gICAgfVxuXG4gICAgaWYgKHdoYXQgPT09IFwieFwiKSB7XG4gICAgICByZXR1cm4gcG9pbnQyLnggLSBwb2ludDEueDtcbiAgICB9IGVsc2UgaWYgKHdoYXQgPT09IFwieVwiKSB7XG4gICAgICByZXR1cm4gcG9pbnQyLnkgLSBwb2ludDEueTtcbiAgICB9XG5cbiAgICByZXR1cm4gTWF0aC5zcXJ0KE1hdGgucG93KHBvaW50Mi54IC0gcG9pbnQxLngsIDIpICsgTWF0aC5wb3cocG9pbnQyLnkgLSBwb2ludDEueSwgMikpO1xuICB9O1xuXG4gIHZhciBpc0NsaWNrYWJsZSA9IGZ1bmN0aW9uKCRlbCkge1xuICAgIGlmIChcbiAgICAgICRlbC5pcygnYSxhcmVhLGJ1dHRvbixbcm9sZT1cImJ1dHRvblwiXSxpbnB1dCxsYWJlbCxzZWxlY3Qsc3VtbWFyeSx0ZXh0YXJlYSx2aWRlbyxhdWRpbycpIHx8XG4gICAgICAkLmlzRnVuY3Rpb24oJGVsLmdldCgwKS5vbmNsaWNrKSB8fFxuICAgICAgJGVsLmRhdGEoXCJzZWxlY3RhYmxlXCIpXG4gICAgKSB7XG4gICAgICByZXR1cm4gdHJ1ZTtcbiAgICB9XG5cbiAgICAvLyBDaGVjayBmb3IgYXR0cmlidXRlcyBsaWtlIGRhdGEtZmFuY3lib3gtbmV4dCBvciBkYXRhLWZhbmN5Ym94LWNsb3NlXG4gICAgZm9yICh2YXIgaSA9IDAsIGF0dHMgPSAkZWxbMF0uYXR0cmlidXRlcywgbiA9IGF0dHMubGVuZ3RoOyBpIDwgbjsgaSsrKSB7XG4gICAgICBpZiAoYXR0c1tpXS5ub2RlTmFtZS5zdWJzdHIoMCwgMTQpID09PSBcImRhdGEtZmFuY3lib3gtXCIpIHtcbiAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuIGZhbHNlO1xuICB9O1xuXG4gIHZhciBoYXNTY3JvbGxiYXJzID0gZnVuY3Rpb24oZWwpIHtcbiAgICB2YXIgb3ZlcmZsb3dZID0gd2luZG93LmdldENvbXB1dGVkU3R5bGUoZWwpW1wib3ZlcmZsb3cteVwiXSxcbiAgICAgIG92ZXJmbG93WCA9IHdpbmRvdy5nZXRDb21wdXRlZFN0eWxlKGVsKVtcIm92ZXJmbG93LXhcIl0sXG4gICAgICB2ZXJ0aWNhbCA9IChvdmVyZmxvd1kgPT09IFwic2Nyb2xsXCIgfHwgb3ZlcmZsb3dZID09PSBcImF1dG9cIikgJiYgZWwuc2Nyb2xsSGVpZ2h0ID4gZWwuY2xpZW50SGVpZ2h0LFxuICAgICAgaG9yaXpvbnRhbCA9IChvdmVyZmxvd1ggPT09IFwic2Nyb2xsXCIgfHwgb3ZlcmZsb3dYID09PSBcImF1dG9cIikgJiYgZWwuc2Nyb2xsV2lkdGggPiBlbC5jbGllbnRXaWR0aDtcblxuICAgIHJldHVybiB2ZXJ0aWNhbCB8fCBob3Jpem9udGFsO1xuICB9O1xuXG4gIHZhciBpc1Njcm9sbGFibGUgPSBmdW5jdGlvbigkZWwpIHtcbiAgICB2YXIgcmV6ID0gZmFsc2U7XG5cbiAgICB3aGlsZSAodHJ1ZSkge1xuICAgICAgcmV6ID0gaGFzU2Nyb2xsYmFycygkZWwuZ2V0KDApKTtcblxuICAgICAgaWYgKHJleikge1xuICAgICAgICBicmVhaztcbiAgICAgIH1cblxuICAgICAgJGVsID0gJGVsLnBhcmVudCgpO1xuXG4gICAgICBpZiAoISRlbC5sZW5ndGggfHwgJGVsLmhhc0NsYXNzKFwiZmFuY3lib3gtc3RhZ2VcIikgfHwgJGVsLmlzKFwiYm9keVwiKSkge1xuICAgICAgICBicmVhaztcbiAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gcmV6O1xuICB9O1xuXG4gIHZhciBHdWVzdHVyZXMgPSBmdW5jdGlvbihpbnN0YW5jZSkge1xuICAgIHZhciBzZWxmID0gdGhpcztcblxuICAgIHNlbGYuaW5zdGFuY2UgPSBpbnN0YW5jZTtcblxuICAgIHNlbGYuJGJnID0gaW5zdGFuY2UuJHJlZnMuYmc7XG4gICAgc2VsZi4kc3RhZ2UgPSBpbnN0YW5jZS4kcmVmcy5zdGFnZTtcbiAgICBzZWxmLiRjb250YWluZXIgPSBpbnN0YW5jZS4kcmVmcy5jb250YWluZXI7XG5cbiAgICBzZWxmLmRlc3Ryb3koKTtcblxuICAgIHNlbGYuJGNvbnRhaW5lci5vbihcInRvdWNoc3RhcnQuZmIudG91Y2ggbW91c2Vkb3duLmZiLnRvdWNoXCIsICQucHJveHkoc2VsZiwgXCJvbnRvdWNoc3RhcnRcIikpO1xuICB9O1xuXG4gIEd1ZXN0dXJlcy5wcm90b3R5cGUuZGVzdHJveSA9IGZ1bmN0aW9uKCkge1xuICAgIHRoaXMuJGNvbnRhaW5lci5vZmYoXCIuZmIudG91Y2hcIik7XG4gIH07XG5cbiAgR3Vlc3R1cmVzLnByb3RvdHlwZS5vbnRvdWNoc3RhcnQgPSBmdW5jdGlvbihlKSB7XG4gICAgdmFyIHNlbGYgPSB0aGlzLFxuICAgICAgJHRhcmdldCA9ICQoZS50YXJnZXQpLFxuICAgICAgaW5zdGFuY2UgPSBzZWxmLmluc3RhbmNlLFxuICAgICAgY3VycmVudCA9IGluc3RhbmNlLmN1cnJlbnQsXG4gICAgICAkY29udGVudCA9IGN1cnJlbnQuJGNvbnRlbnQsXG4gICAgICBpc1RvdWNoRGV2aWNlID0gZS50eXBlID09IFwidG91Y2hzdGFydFwiO1xuXG4gICAgLy8gRG8gbm90IHJlc3BvbmQgdG8gYm90aCAodG91Y2ggYW5kIG1vdXNlKSBldmVudHNcbiAgICBpZiAoaXNUb3VjaERldmljZSkge1xuICAgICAgc2VsZi4kY29udGFpbmVyLm9mZihcIm1vdXNlZG93bi5mYi50b3VjaFwiKTtcbiAgICB9XG5cbiAgICAvLyBJZ25vcmUgcmlnaHQgY2xpY2tcbiAgICBpZiAoZS5vcmlnaW5hbEV2ZW50ICYmIGUub3JpZ2luYWxFdmVudC5idXR0b24gPT0gMikge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIC8vIElnbm9yZSB0YXBpbmcgb24gbGlua3MsIGJ1dHRvbnMsIGlucHV0IGVsZW1lbnRzXG4gICAgaWYgKCEkdGFyZ2V0Lmxlbmd0aCB8fCBpc0NsaWNrYWJsZSgkdGFyZ2V0KSB8fCBpc0NsaWNrYWJsZSgkdGFyZ2V0LnBhcmVudCgpKSkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIC8vIElnbm9yZSBjbGlja3Mgb24gdGhlIHNjcm9sbGJhclxuICAgIGlmICghJHRhcmdldC5pcyhcImltZ1wiKSAmJiBlLm9yaWdpbmFsRXZlbnQuY2xpZW50WCA+ICR0YXJnZXRbMF0uY2xpZW50V2lkdGggKyAkdGFyZ2V0Lm9mZnNldCgpLmxlZnQpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICAvLyBJZ25vcmUgY2xpY2tzIHdoaWxlIHpvb21pbmcgb3IgY2xvc2luZ1xuICAgIGlmICghY3VycmVudCB8fCBpbnN0YW5jZS5pc0FuaW1hdGluZyB8fCBpbnN0YW5jZS5pc0Nsb3NpbmcpIHtcbiAgICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XG4gICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG5cbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBzZWxmLnJlYWxQb2ludHMgPSBzZWxmLnN0YXJ0UG9pbnRzID0gZ2V0UG9pbnRlclhZKGUpO1xuXG4gICAgaWYgKCFzZWxmLnN0YXJ0UG9pbnRzLmxlbmd0aCkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XG5cbiAgICBzZWxmLnN0YXJ0RXZlbnQgPSBlO1xuXG4gICAgc2VsZi5jYW5UYXAgPSB0cnVlO1xuICAgIHNlbGYuJHRhcmdldCA9ICR0YXJnZXQ7XG4gICAgc2VsZi4kY29udGVudCA9ICRjb250ZW50O1xuICAgIHNlbGYub3B0cyA9IGN1cnJlbnQub3B0cy50b3VjaDtcblxuICAgIHNlbGYuaXNQYW5uaW5nID0gZmFsc2U7XG4gICAgc2VsZi5pc1N3aXBpbmcgPSBmYWxzZTtcbiAgICBzZWxmLmlzWm9vbWluZyA9IGZhbHNlO1xuICAgIHNlbGYuaXNTY3JvbGxpbmcgPSBmYWxzZTtcblxuICAgIHNlbGYuc3RhcnRUaW1lID0gbmV3IERhdGUoKS5nZXRUaW1lKCk7XG4gICAgc2VsZi5kaXN0YW5jZVggPSBzZWxmLmRpc3RhbmNlWSA9IHNlbGYuZGlzdGFuY2UgPSAwO1xuXG4gICAgc2VsZi5jYW52YXNXaWR0aCA9IE1hdGgucm91bmQoY3VycmVudC4kc2xpZGVbMF0uY2xpZW50V2lkdGgpO1xuICAgIHNlbGYuY2FudmFzSGVpZ2h0ID0gTWF0aC5yb3VuZChjdXJyZW50LiRzbGlkZVswXS5jbGllbnRIZWlnaHQpO1xuXG4gICAgc2VsZi5jb250ZW50TGFzdFBvcyA9IG51bGw7XG4gICAgc2VsZi5jb250ZW50U3RhcnRQb3MgPSAkLmZhbmN5Ym94LmdldFRyYW5zbGF0ZShzZWxmLiRjb250ZW50KSB8fCB7dG9wOiAwLCBsZWZ0OiAwfTtcbiAgICBzZWxmLnNsaWRlclN0YXJ0UG9zID0gc2VsZi5zbGlkZXJMYXN0UG9zIHx8ICQuZmFuY3lib3guZ2V0VHJhbnNsYXRlKGN1cnJlbnQuJHNsaWRlKTtcblxuICAgIC8vIFNpbmNlIHBvc2l0aW9uIHdpbGwgYmUgYWJzb2x1dGUsIGJ1dCB3ZSBuZWVkIHRvIG1ha2UgaXQgcmVsYXRpdmUgdG8gdGhlIHN0YWdlXG4gICAgc2VsZi5zdGFnZVBvcyA9ICQuZmFuY3lib3guZ2V0VHJhbnNsYXRlKGluc3RhbmNlLiRyZWZzLnN0YWdlKTtcblxuICAgIHNlbGYuc2xpZGVyU3RhcnRQb3MudG9wIC09IHNlbGYuc3RhZ2VQb3MudG9wO1xuICAgIHNlbGYuc2xpZGVyU3RhcnRQb3MubGVmdCAtPSBzZWxmLnN0YWdlUG9zLmxlZnQ7XG5cbiAgICBzZWxmLmNvbnRlbnRTdGFydFBvcy50b3AgLT0gc2VsZi5zdGFnZVBvcy50b3A7XG4gICAgc2VsZi5jb250ZW50U3RhcnRQb3MubGVmdCAtPSBzZWxmLnN0YWdlUG9zLmxlZnQ7XG5cbiAgICAkKGRvY3VtZW50KVxuICAgICAgLm9mZihcIi5mYi50b3VjaFwiKVxuICAgICAgLm9uKGlzVG91Y2hEZXZpY2UgPyBcInRvdWNoZW5kLmZiLnRvdWNoIHRvdWNoY2FuY2VsLmZiLnRvdWNoXCIgOiBcIm1vdXNldXAuZmIudG91Y2ggbW91c2VsZWF2ZS5mYi50b3VjaFwiLCAkLnByb3h5KHNlbGYsIFwib250b3VjaGVuZFwiKSlcbiAgICAgIC5vbihpc1RvdWNoRGV2aWNlID8gXCJ0b3VjaG1vdmUuZmIudG91Y2hcIiA6IFwibW91c2Vtb3ZlLmZiLnRvdWNoXCIsICQucHJveHkoc2VsZiwgXCJvbnRvdWNobW92ZVwiKSk7XG5cbiAgICBpZiAoJC5mYW5jeWJveC5pc01vYmlsZSkge1xuICAgICAgZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihcInNjcm9sbFwiLCBzZWxmLm9uc2Nyb2xsLCB0cnVlKTtcbiAgICB9XG5cbiAgICBpZiAoIShzZWxmLm9wdHMgfHwgaW5zdGFuY2UuY2FuUGFuKCkpIHx8ICEoJHRhcmdldC5pcyhzZWxmLiRzdGFnZSkgfHwgc2VsZi4kc3RhZ2UuZmluZCgkdGFyZ2V0KS5sZW5ndGgpKSB7XG4gICAgICBpZiAoJHRhcmdldC5pcyhcIi5mYW5jeWJveC1pbWFnZVwiKSkge1xuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICB9XG5cbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBpZiAoISgkLmZhbmN5Ym94LmlzTW9iaWxlICYmIChpc1Njcm9sbGFibGUoJHRhcmdldCkgfHwgaXNTY3JvbGxhYmxlKCR0YXJnZXQucGFyZW50KCkpKSkpIHtcbiAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICB9XG5cbiAgICBpZiAoc2VsZi5zdGFydFBvaW50cy5sZW5ndGggPT09IDEgfHwgY3VycmVudC5oYXNFcnJvcikge1xuICAgICAgaWYgKHNlbGYuaW5zdGFuY2UuY2FuUGFuKCkpIHtcbiAgICAgICAgJC5mYW5jeWJveC5zdG9wKHNlbGYuJGNvbnRlbnQpO1xuXG4gICAgICAgIHNlbGYuJGNvbnRlbnQuY3NzKFwidHJhbnNpdGlvbi1kdXJhdGlvblwiLCBcIlwiKTtcblxuICAgICAgICBzZWxmLmlzUGFubmluZyA9IHRydWU7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBzZWxmLmlzU3dpcGluZyA9IHRydWU7XG4gICAgICB9XG5cbiAgICAgIHNlbGYuJGNvbnRhaW5lci5hZGRDbGFzcyhcImZhbmN5Ym94LWNvbnRyb2xzLS1pc0dyYWJiaW5nXCIpO1xuICAgIH1cblxuICAgIGlmIChzZWxmLnN0YXJ0UG9pbnRzLmxlbmd0aCA9PT0gMiAmJiBjdXJyZW50LnR5cGUgPT09IFwiaW1hZ2VcIiAmJiAoY3VycmVudC5pc0xvYWRlZCB8fCBjdXJyZW50LiRnaG9zdCkpIHtcbiAgICAgIHNlbGYuY2FuVGFwID0gZmFsc2U7XG4gICAgICBzZWxmLmlzU3dpcGluZyA9IGZhbHNlO1xuICAgICAgc2VsZi5pc1Bhbm5pbmcgPSBmYWxzZTtcblxuICAgICAgc2VsZi5pc1pvb21pbmcgPSB0cnVlO1xuXG4gICAgICAkLmZhbmN5Ym94LnN0b3Aoc2VsZi4kY29udGVudCk7XG5cbiAgICAgIHNlbGYuJGNvbnRlbnQuY3NzKFwidHJhbnNpdGlvbi1kdXJhdGlvblwiLCBcIlwiKTtcblxuICAgICAgc2VsZi5jZW50ZXJQb2ludFN0YXJ0WCA9IChzZWxmLnN0YXJ0UG9pbnRzWzBdLnggKyBzZWxmLnN0YXJ0UG9pbnRzWzFdLngpICogMC41IC0gJCh3aW5kb3cpLnNjcm9sbExlZnQoKTtcbiAgICAgIHNlbGYuY2VudGVyUG9pbnRTdGFydFkgPSAoc2VsZi5zdGFydFBvaW50c1swXS55ICsgc2VsZi5zdGFydFBvaW50c1sxXS55KSAqIDAuNSAtICQod2luZG93KS5zY3JvbGxUb3AoKTtcblxuICAgICAgc2VsZi5wZXJjZW50YWdlT2ZJbWFnZUF0UGluY2hQb2ludFggPSAoc2VsZi5jZW50ZXJQb2ludFN0YXJ0WCAtIHNlbGYuY29udGVudFN0YXJ0UG9zLmxlZnQpIC8gc2VsZi5jb250ZW50U3RhcnRQb3Mud2lkdGg7XG4gICAgICBzZWxmLnBlcmNlbnRhZ2VPZkltYWdlQXRQaW5jaFBvaW50WSA9IChzZWxmLmNlbnRlclBvaW50U3RhcnRZIC0gc2VsZi5jb250ZW50U3RhcnRQb3MudG9wKSAvIHNlbGYuY29udGVudFN0YXJ0UG9zLmhlaWdodDtcblxuICAgICAgc2VsZi5zdGFydERpc3RhbmNlQmV0d2VlbkZpbmdlcnMgPSBkaXN0YW5jZShzZWxmLnN0YXJ0UG9pbnRzWzBdLCBzZWxmLnN0YXJ0UG9pbnRzWzFdKTtcbiAgICB9XG4gIH07XG5cbiAgR3Vlc3R1cmVzLnByb3RvdHlwZS5vbnNjcm9sbCA9IGZ1bmN0aW9uKGUpIHtcbiAgICB2YXIgc2VsZiA9IHRoaXM7XG5cbiAgICBzZWxmLmlzU2Nyb2xsaW5nID0gdHJ1ZTtcblxuICAgIGRvY3VtZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJzY3JvbGxcIiwgc2VsZi5vbnNjcm9sbCwgdHJ1ZSk7XG4gIH07XG5cbiAgR3Vlc3R1cmVzLnByb3RvdHlwZS5vbnRvdWNobW92ZSA9IGZ1bmN0aW9uKGUpIHtcbiAgICB2YXIgc2VsZiA9IHRoaXMsXG4gICAgICAkdGFyZ2V0ID0gJChlLnRhcmdldCk7XG5cbiAgICAvLyBNYWtlIHN1cmUgdXNlciBoYXMgbm90IHJlbGVhc2VkIG92ZXIgaWZyYW1lIG9yIGRpc2FibGVkIGVsZW1lbnRcbiAgICBpZiAoZS5vcmlnaW5hbEV2ZW50LmJ1dHRvbnMgIT09IHVuZGVmaW5lZCAmJiBlLm9yaWdpbmFsRXZlbnQuYnV0dG9ucyA9PT0gMCkge1xuICAgICAgc2VsZi5vbnRvdWNoZW5kKGUpO1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIGlmIChzZWxmLmlzU2Nyb2xsaW5nIHx8ICEoJHRhcmdldC5pcyhzZWxmLiRzdGFnZSkgfHwgc2VsZi4kc3RhZ2UuZmluZCgkdGFyZ2V0KS5sZW5ndGgpKSB7XG4gICAgICBzZWxmLmNhblRhcCA9IGZhbHNlO1xuXG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgc2VsZi5uZXdQb2ludHMgPSBnZXRQb2ludGVyWFkoZSk7XG5cbiAgICBpZiAoIShzZWxmLm9wdHMgfHwgc2VsZi5pbnN0YW5jZS5jYW5QYW4oKSkgfHwgIXNlbGYubmV3UG9pbnRzLmxlbmd0aCB8fCAhc2VsZi5uZXdQb2ludHMubGVuZ3RoKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgaWYgKCEoc2VsZi5pc1N3aXBpbmcgJiYgc2VsZi5pc1N3aXBpbmcgPT09IHRydWUpKSB7XG4gICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgfVxuXG4gICAgc2VsZi5kaXN0YW5jZVggPSBkaXN0YW5jZShzZWxmLm5ld1BvaW50c1swXSwgc2VsZi5zdGFydFBvaW50c1swXSwgXCJ4XCIpO1xuICAgIHNlbGYuZGlzdGFuY2VZID0gZGlzdGFuY2Uoc2VsZi5uZXdQb2ludHNbMF0sIHNlbGYuc3RhcnRQb2ludHNbMF0sIFwieVwiKTtcblxuICAgIHNlbGYuZGlzdGFuY2UgPSBkaXN0YW5jZShzZWxmLm5ld1BvaW50c1swXSwgc2VsZi5zdGFydFBvaW50c1swXSk7XG5cbiAgICAvLyBTa2lwIGZhbHNlIG9udG91Y2htb3ZlIGV2ZW50cyAoQ2hyb21lKVxuICAgIGlmIChzZWxmLmRpc3RhbmNlID4gMCkge1xuICAgICAgaWYgKHNlbGYuaXNTd2lwaW5nKSB7XG4gICAgICAgIHNlbGYub25Td2lwZShlKTtcbiAgICAgIH0gZWxzZSBpZiAoc2VsZi5pc1Bhbm5pbmcpIHtcbiAgICAgICAgc2VsZi5vblBhbigpO1xuICAgICAgfSBlbHNlIGlmIChzZWxmLmlzWm9vbWluZykge1xuICAgICAgICBzZWxmLm9uWm9vbSgpO1xuICAgICAgfVxuICAgIH1cbiAgfTtcblxuICBHdWVzdHVyZXMucHJvdG90eXBlLm9uU3dpcGUgPSBmdW5jdGlvbihlKSB7XG4gICAgdmFyIHNlbGYgPSB0aGlzLFxuICAgICAgc3dpcGluZyA9IHNlbGYuaXNTd2lwaW5nLFxuICAgICAgbGVmdCA9IHNlbGYuc2xpZGVyU3RhcnRQb3MubGVmdCB8fCAwLFxuICAgICAgYW5nbGU7XG5cbiAgICAvLyBJZiBkaXJlY3Rpb24gaXMgbm90IHlldCBkZXRlcm1pbmVkXG4gICAgaWYgKHN3aXBpbmcgPT09IHRydWUpIHtcbiAgICAgIC8vIFdlIG5lZWQgYXQgbGVhc3QgMTBweCBkaXN0YW5jZSB0byBjb3JyZWN0bHkgY2FsY3VsYXRlIGFuIGFuZ2xlXG4gICAgICBpZiAoTWF0aC5hYnMoc2VsZi5kaXN0YW5jZSkgPiAxMCkge1xuICAgICAgICBzZWxmLmNhblRhcCA9IGZhbHNlO1xuXG4gICAgICAgIGlmIChzZWxmLmluc3RhbmNlLmdyb3VwLmxlbmd0aCA8IDIgJiYgc2VsZi5vcHRzLnZlcnRpY2FsKSB7XG4gICAgICAgICAgc2VsZi5pc1N3aXBpbmcgPSBcInlcIjtcbiAgICAgICAgfSBlbHNlIGlmIChzZWxmLmluc3RhbmNlLmlzRHJhZ2dpbmcgfHwgc2VsZi5vcHRzLnZlcnRpY2FsID09PSBmYWxzZSB8fCAoc2VsZi5vcHRzLnZlcnRpY2FsID09PSBcImF1dG9cIiAmJiAkKHdpbmRvdykud2lkdGgoKSA+IDgwMCkpIHtcbiAgICAgICAgICBzZWxmLmlzU3dpcGluZyA9IFwieFwiO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGFuZ2xlID0gTWF0aC5hYnMoTWF0aC5hdGFuMihzZWxmLmRpc3RhbmNlWSwgc2VsZi5kaXN0YW5jZVgpICogMTgwIC8gTWF0aC5QSSk7XG5cbiAgICAgICAgICBzZWxmLmlzU3dpcGluZyA9IGFuZ2xlID4gNDUgJiYgYW5nbGUgPCAxMzUgPyBcInlcIiA6IFwieFwiO1xuICAgICAgICB9XG5cbiAgICAgICAgc2VsZi5jYW5UYXAgPSBmYWxzZTtcblxuICAgICAgICBpZiAoc2VsZi5pc1N3aXBpbmcgPT09IFwieVwiICYmICQuZmFuY3lib3guaXNNb2JpbGUgJiYgKGlzU2Nyb2xsYWJsZShzZWxmLiR0YXJnZXQpIHx8IGlzU2Nyb2xsYWJsZShzZWxmLiR0YXJnZXQucGFyZW50KCkpKSkge1xuICAgICAgICAgIHNlbGYuaXNTY3JvbGxpbmcgPSB0cnVlO1xuXG4gICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgc2VsZi5pbnN0YW5jZS5pc0RyYWdnaW5nID0gc2VsZi5pc1N3aXBpbmc7XG5cbiAgICAgICAgLy8gUmVzZXQgcG9pbnRzIHRvIGF2b2lkIGp1bXBpbmcsIGJlY2F1c2Ugd2UgZHJvcHBlZCBmaXJzdCBzd2lwZXMgdG8gY2FsY3VsYXRlIHRoZSBhbmdsZVxuICAgICAgICBzZWxmLnN0YXJ0UG9pbnRzID0gc2VsZi5uZXdQb2ludHM7XG5cbiAgICAgICAgJC5lYWNoKHNlbGYuaW5zdGFuY2Uuc2xpZGVzLCBmdW5jdGlvbihpbmRleCwgc2xpZGUpIHtcbiAgICAgICAgICAkLmZhbmN5Ym94LnN0b3Aoc2xpZGUuJHNsaWRlKTtcblxuICAgICAgICAgIHNsaWRlLiRzbGlkZS5jc3MoXCJ0cmFuc2l0aW9uLWR1cmF0aW9uXCIsIFwiXCIpO1xuXG4gICAgICAgICAgc2xpZGUuaW5UcmFuc2l0aW9uID0gZmFsc2U7XG5cbiAgICAgICAgICBpZiAoc2xpZGUucG9zID09PSBzZWxmLmluc3RhbmNlLmN1cnJlbnQucG9zKSB7XG4gICAgICAgICAgICBzZWxmLnNsaWRlclN0YXJ0UG9zLmxlZnQgPSAkLmZhbmN5Ym94LmdldFRyYW5zbGF0ZShzbGlkZS4kc2xpZGUpLmxlZnQgLSAkLmZhbmN5Ym94LmdldFRyYW5zbGF0ZShzZWxmLmluc3RhbmNlLiRyZWZzLnN0YWdlKS5sZWZ0O1xuICAgICAgICAgIH1cbiAgICAgICAgfSk7XG5cbiAgICAgICAgLy8gU3RvcCBzbGlkZXNob3dcbiAgICAgICAgaWYgKHNlbGYuaW5zdGFuY2UuU2xpZGVTaG93ICYmIHNlbGYuaW5zdGFuY2UuU2xpZGVTaG93LmlzQWN0aXZlKSB7XG4gICAgICAgICAgc2VsZi5pbnN0YW5jZS5TbGlkZVNob3cuc3RvcCgpO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICAvLyBTdGlja3kgZWRnZXNcbiAgICBpZiAoc3dpcGluZyA9PSBcInhcIikge1xuICAgICAgaWYgKFxuICAgICAgICBzZWxmLmRpc3RhbmNlWCA+IDAgJiZcbiAgICAgICAgKHNlbGYuaW5zdGFuY2UuZ3JvdXAubGVuZ3RoIDwgMiB8fCAoc2VsZi5pbnN0YW5jZS5jdXJyZW50LmluZGV4ID09PSAwICYmICFzZWxmLmluc3RhbmNlLmN1cnJlbnQub3B0cy5sb29wKSlcbiAgICAgICkge1xuICAgICAgICBsZWZ0ID0gbGVmdCArIE1hdGgucG93KHNlbGYuZGlzdGFuY2VYLCAwLjgpO1xuICAgICAgfSBlbHNlIGlmIChcbiAgICAgICAgc2VsZi5kaXN0YW5jZVggPCAwICYmXG4gICAgICAgIChzZWxmLmluc3RhbmNlLmdyb3VwLmxlbmd0aCA8IDIgfHxcbiAgICAgICAgICAoc2VsZi5pbnN0YW5jZS5jdXJyZW50LmluZGV4ID09PSBzZWxmLmluc3RhbmNlLmdyb3VwLmxlbmd0aCAtIDEgJiYgIXNlbGYuaW5zdGFuY2UuY3VycmVudC5vcHRzLmxvb3ApKVxuICAgICAgKSB7XG4gICAgICAgIGxlZnQgPSBsZWZ0IC0gTWF0aC5wb3coLXNlbGYuZGlzdGFuY2VYLCAwLjgpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgbGVmdCA9IGxlZnQgKyBzZWxmLmRpc3RhbmNlWDtcbiAgICAgIH1cbiAgICB9XG5cbiAgICBzZWxmLnNsaWRlckxhc3RQb3MgPSB7XG4gICAgICB0b3A6IHN3aXBpbmcgPT0gXCJ4XCIgPyAwIDogc2VsZi5zbGlkZXJTdGFydFBvcy50b3AgKyBzZWxmLmRpc3RhbmNlWSxcbiAgICAgIGxlZnQ6IGxlZnRcbiAgICB9O1xuXG4gICAgaWYgKHNlbGYucmVxdWVzdElkKSB7XG4gICAgICBjYW5jZWxBRnJhbWUoc2VsZi5yZXF1ZXN0SWQpO1xuXG4gICAgICBzZWxmLnJlcXVlc3RJZCA9IG51bGw7XG4gICAgfVxuXG4gICAgc2VsZi5yZXF1ZXN0SWQgPSByZXF1ZXN0QUZyYW1lKGZ1bmN0aW9uKCkge1xuICAgICAgaWYgKHNlbGYuc2xpZGVyTGFzdFBvcykge1xuICAgICAgICAkLmVhY2goc2VsZi5pbnN0YW5jZS5zbGlkZXMsIGZ1bmN0aW9uKGluZGV4LCBzbGlkZSkge1xuICAgICAgICAgIHZhciBwb3MgPSBzbGlkZS5wb3MgLSBzZWxmLmluc3RhbmNlLmN1cnJQb3M7XG5cbiAgICAgICAgICAkLmZhbmN5Ym94LnNldFRyYW5zbGF0ZShzbGlkZS4kc2xpZGUsIHtcbiAgICAgICAgICAgIHRvcDogc2VsZi5zbGlkZXJMYXN0UG9zLnRvcCxcbiAgICAgICAgICAgIGxlZnQ6IHNlbGYuc2xpZGVyTGFzdFBvcy5sZWZ0ICsgcG9zICogc2VsZi5jYW52YXNXaWR0aCArIHBvcyAqIHNsaWRlLm9wdHMuZ3V0dGVyXG4gICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIHNlbGYuJGNvbnRhaW5lci5hZGRDbGFzcyhcImZhbmN5Ym94LWlzLXNsaWRpbmdcIik7XG4gICAgICB9XG4gICAgfSk7XG4gIH07XG5cbiAgR3Vlc3R1cmVzLnByb3RvdHlwZS5vblBhbiA9IGZ1bmN0aW9uKCkge1xuICAgIHZhciBzZWxmID0gdGhpcztcblxuICAgIC8vIFByZXZlbnQgYWNjaWRlbnRhbCBtb3ZlbWVudCAoc29tZXRpbWVzLCB3aGVuIHRhcHBpbmcgY2FzdWFsbHksIGZpbmdlciBjYW4gbW92ZSBhIGJpdClcbiAgICBpZiAoZGlzdGFuY2Uoc2VsZi5uZXdQb2ludHNbMF0sIHNlbGYucmVhbFBvaW50c1swXSkgPCAoJC5mYW5jeWJveC5pc01vYmlsZSA/IDEwIDogNSkpIHtcbiAgICAgIHNlbGYuc3RhcnRQb2ludHMgPSBzZWxmLm5ld1BvaW50cztcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBzZWxmLmNhblRhcCA9IGZhbHNlO1xuXG4gICAgc2VsZi5jb250ZW50TGFzdFBvcyA9IHNlbGYubGltaXRNb3ZlbWVudCgpO1xuXG4gICAgaWYgKHNlbGYucmVxdWVzdElkKSB7XG4gICAgICBjYW5jZWxBRnJhbWUoc2VsZi5yZXF1ZXN0SWQpO1xuXG4gICAgICBzZWxmLnJlcXVlc3RJZCA9IG51bGw7XG4gICAgfVxuXG4gICAgc2VsZi5yZXF1ZXN0SWQgPSByZXF1ZXN0QUZyYW1lKGZ1bmN0aW9uKCkge1xuICAgICAgJC5mYW5jeWJveC5zZXRUcmFuc2xhdGUoc2VsZi4kY29udGVudCwgc2VsZi5jb250ZW50TGFzdFBvcyk7XG4gICAgfSk7XG4gIH07XG5cbiAgLy8gTWFrZSBwYW5uaW5nIHN0aWNreSB0byB0aGUgZWRnZXNcbiAgR3Vlc3R1cmVzLnByb3RvdHlwZS5saW1pdE1vdmVtZW50ID0gZnVuY3Rpb24oKSB7XG4gICAgdmFyIHNlbGYgPSB0aGlzO1xuXG4gICAgdmFyIGNhbnZhc1dpZHRoID0gc2VsZi5jYW52YXNXaWR0aDtcbiAgICB2YXIgY2FudmFzSGVpZ2h0ID0gc2VsZi5jYW52YXNIZWlnaHQ7XG5cbiAgICB2YXIgZGlzdGFuY2VYID0gc2VsZi5kaXN0YW5jZVg7XG4gICAgdmFyIGRpc3RhbmNlWSA9IHNlbGYuZGlzdGFuY2VZO1xuXG4gICAgdmFyIGNvbnRlbnRTdGFydFBvcyA9IHNlbGYuY29udGVudFN0YXJ0UG9zO1xuXG4gICAgdmFyIGN1cnJlbnRPZmZzZXRYID0gY29udGVudFN0YXJ0UG9zLmxlZnQ7XG4gICAgdmFyIGN1cnJlbnRPZmZzZXRZID0gY29udGVudFN0YXJ0UG9zLnRvcDtcblxuICAgIHZhciBjdXJyZW50V2lkdGggPSBjb250ZW50U3RhcnRQb3Mud2lkdGg7XG4gICAgdmFyIGN1cnJlbnRIZWlnaHQgPSBjb250ZW50U3RhcnRQb3MuaGVpZ2h0O1xuXG4gICAgdmFyIG1pblRyYW5zbGF0ZVgsIG1pblRyYW5zbGF0ZVksIG1heFRyYW5zbGF0ZVgsIG1heFRyYW5zbGF0ZVksIG5ld09mZnNldFgsIG5ld09mZnNldFk7XG5cbiAgICBpZiAoY3VycmVudFdpZHRoID4gY2FudmFzV2lkdGgpIHtcbiAgICAgIG5ld09mZnNldFggPSBjdXJyZW50T2Zmc2V0WCArIGRpc3RhbmNlWDtcbiAgICB9IGVsc2Uge1xuICAgICAgbmV3T2Zmc2V0WCA9IGN1cnJlbnRPZmZzZXRYO1xuICAgIH1cblxuICAgIG5ld09mZnNldFkgPSBjdXJyZW50T2Zmc2V0WSArIGRpc3RhbmNlWTtcblxuICAgIC8vIFNsb3cgZG93biBwcm9wb3J0aW9uYWxseSB0byB0cmF2ZWxlZCBkaXN0YW5jZVxuICAgIG1pblRyYW5zbGF0ZVggPSBNYXRoLm1heCgwLCBjYW52YXNXaWR0aCAqIDAuNSAtIGN1cnJlbnRXaWR0aCAqIDAuNSk7XG4gICAgbWluVHJhbnNsYXRlWSA9IE1hdGgubWF4KDAsIGNhbnZhc0hlaWdodCAqIDAuNSAtIGN1cnJlbnRIZWlnaHQgKiAwLjUpO1xuXG4gICAgbWF4VHJhbnNsYXRlWCA9IE1hdGgubWluKGNhbnZhc1dpZHRoIC0gY3VycmVudFdpZHRoLCBjYW52YXNXaWR0aCAqIDAuNSAtIGN1cnJlbnRXaWR0aCAqIDAuNSk7XG4gICAgbWF4VHJhbnNsYXRlWSA9IE1hdGgubWluKGNhbnZhc0hlaWdodCAtIGN1cnJlbnRIZWlnaHQsIGNhbnZhc0hlaWdodCAqIDAuNSAtIGN1cnJlbnRIZWlnaHQgKiAwLjUpO1xuXG4gICAgLy8gICAtPlxuICAgIGlmIChkaXN0YW5jZVggPiAwICYmIG5ld09mZnNldFggPiBtaW5UcmFuc2xhdGVYKSB7XG4gICAgICBuZXdPZmZzZXRYID0gbWluVHJhbnNsYXRlWCAtIDEgKyBNYXRoLnBvdygtbWluVHJhbnNsYXRlWCArIGN1cnJlbnRPZmZzZXRYICsgZGlzdGFuY2VYLCAwLjgpIHx8IDA7XG4gICAgfVxuXG4gICAgLy8gICAgPC1cbiAgICBpZiAoZGlzdGFuY2VYIDwgMCAmJiBuZXdPZmZzZXRYIDwgbWF4VHJhbnNsYXRlWCkge1xuICAgICAgbmV3T2Zmc2V0WCA9IG1heFRyYW5zbGF0ZVggKyAxIC0gTWF0aC5wb3cobWF4VHJhbnNsYXRlWCAtIGN1cnJlbnRPZmZzZXRYIC0gZGlzdGFuY2VYLCAwLjgpIHx8IDA7XG4gICAgfVxuXG4gICAgLy8gICBcXC9cbiAgICBpZiAoZGlzdGFuY2VZID4gMCAmJiBuZXdPZmZzZXRZID4gbWluVHJhbnNsYXRlWSkge1xuICAgICAgbmV3T2Zmc2V0WSA9IG1pblRyYW5zbGF0ZVkgLSAxICsgTWF0aC5wb3coLW1pblRyYW5zbGF0ZVkgKyBjdXJyZW50T2Zmc2V0WSArIGRpc3RhbmNlWSwgMC44KSB8fCAwO1xuICAgIH1cblxuICAgIC8vICAgL1xcXG4gICAgaWYgKGRpc3RhbmNlWSA8IDAgJiYgbmV3T2Zmc2V0WSA8IG1heFRyYW5zbGF0ZVkpIHtcbiAgICAgIG5ld09mZnNldFkgPSBtYXhUcmFuc2xhdGVZICsgMSAtIE1hdGgucG93KG1heFRyYW5zbGF0ZVkgLSBjdXJyZW50T2Zmc2V0WSAtIGRpc3RhbmNlWSwgMC44KSB8fCAwO1xuICAgIH1cblxuICAgIHJldHVybiB7XG4gICAgICB0b3A6IG5ld09mZnNldFksXG4gICAgICBsZWZ0OiBuZXdPZmZzZXRYXG4gICAgfTtcbiAgfTtcblxuICBHdWVzdHVyZXMucHJvdG90eXBlLmxpbWl0UG9zaXRpb24gPSBmdW5jdGlvbihuZXdPZmZzZXRYLCBuZXdPZmZzZXRZLCBuZXdXaWR0aCwgbmV3SGVpZ2h0KSB7XG4gICAgdmFyIHNlbGYgPSB0aGlzO1xuXG4gICAgdmFyIGNhbnZhc1dpZHRoID0gc2VsZi5jYW52YXNXaWR0aDtcbiAgICB2YXIgY2FudmFzSGVpZ2h0ID0gc2VsZi5jYW52YXNIZWlnaHQ7XG5cbiAgICBpZiAobmV3V2lkdGggPiBjYW52YXNXaWR0aCkge1xuICAgICAgbmV3T2Zmc2V0WCA9IG5ld09mZnNldFggPiAwID8gMCA6IG5ld09mZnNldFg7XG4gICAgICBuZXdPZmZzZXRYID0gbmV3T2Zmc2V0WCA8IGNhbnZhc1dpZHRoIC0gbmV3V2lkdGggPyBjYW52YXNXaWR0aCAtIG5ld1dpZHRoIDogbmV3T2Zmc2V0WDtcbiAgICB9IGVsc2Uge1xuICAgICAgLy8gQ2VudGVyIGhvcml6b250YWxseVxuICAgICAgbmV3T2Zmc2V0WCA9IE1hdGgubWF4KDAsIGNhbnZhc1dpZHRoIC8gMiAtIG5ld1dpZHRoIC8gMik7XG4gICAgfVxuXG4gICAgaWYgKG5ld0hlaWdodCA+IGNhbnZhc0hlaWdodCkge1xuICAgICAgbmV3T2Zmc2V0WSA9IG5ld09mZnNldFkgPiAwID8gMCA6IG5ld09mZnNldFk7XG4gICAgICBuZXdPZmZzZXRZID0gbmV3T2Zmc2V0WSA8IGNhbnZhc0hlaWdodCAtIG5ld0hlaWdodCA/IGNhbnZhc0hlaWdodCAtIG5ld0hlaWdodCA6IG5ld09mZnNldFk7XG4gICAgfSBlbHNlIHtcbiAgICAgIC8vIENlbnRlciB2ZXJ0aWNhbGx5XG4gICAgICBuZXdPZmZzZXRZID0gTWF0aC5tYXgoMCwgY2FudmFzSGVpZ2h0IC8gMiAtIG5ld0hlaWdodCAvIDIpO1xuICAgIH1cblxuICAgIHJldHVybiB7XG4gICAgICB0b3A6IG5ld09mZnNldFksXG4gICAgICBsZWZ0OiBuZXdPZmZzZXRYXG4gICAgfTtcbiAgfTtcblxuICBHdWVzdHVyZXMucHJvdG90eXBlLm9uWm9vbSA9IGZ1bmN0aW9uKCkge1xuICAgIHZhciBzZWxmID0gdGhpcztcblxuICAgIC8vIENhbGN1bGF0ZSBjdXJyZW50IGRpc3RhbmNlIGJldHdlZW4gcG9pbnRzIHRvIGdldCBwaW5jaCByYXRpbyBhbmQgbmV3IHdpZHRoIGFuZCBoZWlnaHRcbiAgICB2YXIgY29udGVudFN0YXJ0UG9zID0gc2VsZi5jb250ZW50U3RhcnRQb3M7XG5cbiAgICB2YXIgY3VycmVudFdpZHRoID0gY29udGVudFN0YXJ0UG9zLndpZHRoO1xuICAgIHZhciBjdXJyZW50SGVpZ2h0ID0gY29udGVudFN0YXJ0UG9zLmhlaWdodDtcblxuICAgIHZhciBjdXJyZW50T2Zmc2V0WCA9IGNvbnRlbnRTdGFydFBvcy5sZWZ0O1xuICAgIHZhciBjdXJyZW50T2Zmc2V0WSA9IGNvbnRlbnRTdGFydFBvcy50b3A7XG5cbiAgICB2YXIgZW5kRGlzdGFuY2VCZXR3ZWVuRmluZ2VycyA9IGRpc3RhbmNlKHNlbGYubmV3UG9pbnRzWzBdLCBzZWxmLm5ld1BvaW50c1sxXSk7XG5cbiAgICB2YXIgcGluY2hSYXRpbyA9IGVuZERpc3RhbmNlQmV0d2VlbkZpbmdlcnMgLyBzZWxmLnN0YXJ0RGlzdGFuY2VCZXR3ZWVuRmluZ2VycztcblxuICAgIHZhciBuZXdXaWR0aCA9IE1hdGguZmxvb3IoY3VycmVudFdpZHRoICogcGluY2hSYXRpbyk7XG4gICAgdmFyIG5ld0hlaWdodCA9IE1hdGguZmxvb3IoY3VycmVudEhlaWdodCAqIHBpbmNoUmF0aW8pO1xuXG4gICAgLy8gVGhpcyBpcyB0aGUgdHJhbnNsYXRpb24gZHVlIHRvIHBpbmNoLXpvb21pbmdcbiAgICB2YXIgdHJhbnNsYXRlRnJvbVpvb21pbmdYID0gKGN1cnJlbnRXaWR0aCAtIG5ld1dpZHRoKSAqIHNlbGYucGVyY2VudGFnZU9mSW1hZ2VBdFBpbmNoUG9pbnRYO1xuICAgIHZhciB0cmFuc2xhdGVGcm9tWm9vbWluZ1kgPSAoY3VycmVudEhlaWdodCAtIG5ld0hlaWdodCkgKiBzZWxmLnBlcmNlbnRhZ2VPZkltYWdlQXRQaW5jaFBvaW50WTtcblxuICAgIC8vIFBvaW50IGJldHdlZW4gdGhlIHR3byB0b3VjaGVzXG4gICAgdmFyIGNlbnRlclBvaW50RW5kWCA9IChzZWxmLm5ld1BvaW50c1swXS54ICsgc2VsZi5uZXdQb2ludHNbMV0ueCkgLyAyIC0gJCh3aW5kb3cpLnNjcm9sbExlZnQoKTtcbiAgICB2YXIgY2VudGVyUG9pbnRFbmRZID0gKHNlbGYubmV3UG9pbnRzWzBdLnkgKyBzZWxmLm5ld1BvaW50c1sxXS55KSAvIDIgLSAkKHdpbmRvdykuc2Nyb2xsVG9wKCk7XG5cbiAgICAvLyBBbmQgdGhpcyBpcyB0aGUgdHJhbnNsYXRpb24gZHVlIHRvIHRyYW5zbGF0aW9uIG9mIHRoZSBjZW50ZXJwb2ludFxuICAgIC8vIGJldHdlZW4gdGhlIHR3byBmaW5nZXJzXG4gICAgdmFyIHRyYW5zbGF0ZUZyb21UcmFuc2xhdGluZ1ggPSBjZW50ZXJQb2ludEVuZFggLSBzZWxmLmNlbnRlclBvaW50U3RhcnRYO1xuICAgIHZhciB0cmFuc2xhdGVGcm9tVHJhbnNsYXRpbmdZID0gY2VudGVyUG9pbnRFbmRZIC0gc2VsZi5jZW50ZXJQb2ludFN0YXJ0WTtcblxuICAgIC8vIFRoZSBuZXcgb2Zmc2V0IGlzIHRoZSBvbGQvY3VycmVudCBvbmUgcGx1cyB0aGUgdG90YWwgdHJhbnNsYXRpb25cbiAgICB2YXIgbmV3T2Zmc2V0WCA9IGN1cnJlbnRPZmZzZXRYICsgKHRyYW5zbGF0ZUZyb21ab29taW5nWCArIHRyYW5zbGF0ZUZyb21UcmFuc2xhdGluZ1gpO1xuICAgIHZhciBuZXdPZmZzZXRZID0gY3VycmVudE9mZnNldFkgKyAodHJhbnNsYXRlRnJvbVpvb21pbmdZICsgdHJhbnNsYXRlRnJvbVRyYW5zbGF0aW5nWSk7XG5cbiAgICB2YXIgbmV3UG9zID0ge1xuICAgICAgdG9wOiBuZXdPZmZzZXRZLFxuICAgICAgbGVmdDogbmV3T2Zmc2V0WCxcbiAgICAgIHNjYWxlWDogcGluY2hSYXRpbyxcbiAgICAgIHNjYWxlWTogcGluY2hSYXRpb1xuICAgIH07XG5cbiAgICBzZWxmLmNhblRhcCA9IGZhbHNlO1xuXG4gICAgc2VsZi5uZXdXaWR0aCA9IG5ld1dpZHRoO1xuICAgIHNlbGYubmV3SGVpZ2h0ID0gbmV3SGVpZ2h0O1xuXG4gICAgc2VsZi5jb250ZW50TGFzdFBvcyA9IG5ld1BvcztcblxuICAgIGlmIChzZWxmLnJlcXVlc3RJZCkge1xuICAgICAgY2FuY2VsQUZyYW1lKHNlbGYucmVxdWVzdElkKTtcblxuICAgICAgc2VsZi5yZXF1ZXN0SWQgPSBudWxsO1xuICAgIH1cblxuICAgIHNlbGYucmVxdWVzdElkID0gcmVxdWVzdEFGcmFtZShmdW5jdGlvbigpIHtcbiAgICAgICQuZmFuY3lib3guc2V0VHJhbnNsYXRlKHNlbGYuJGNvbnRlbnQsIHNlbGYuY29udGVudExhc3RQb3MpO1xuICAgIH0pO1xuICB9O1xuXG4gIEd1ZXN0dXJlcy5wcm90b3R5cGUub250b3VjaGVuZCA9IGZ1bmN0aW9uKGUpIHtcbiAgICB2YXIgc2VsZiA9IHRoaXM7XG4gICAgdmFyIGRNcyA9IE1hdGgubWF4KG5ldyBEYXRlKCkuZ2V0VGltZSgpIC0gc2VsZi5zdGFydFRpbWUsIDEpO1xuXG4gICAgdmFyIHN3aXBpbmcgPSBzZWxmLmlzU3dpcGluZztcbiAgICB2YXIgcGFubmluZyA9IHNlbGYuaXNQYW5uaW5nO1xuICAgIHZhciB6b29taW5nID0gc2VsZi5pc1pvb21pbmc7XG4gICAgdmFyIHNjcm9sbGluZyA9IHNlbGYuaXNTY3JvbGxpbmc7XG5cbiAgICBzZWxmLmVuZFBvaW50cyA9IGdldFBvaW50ZXJYWShlKTtcblxuICAgIHNlbGYuJGNvbnRhaW5lci5yZW1vdmVDbGFzcyhcImZhbmN5Ym94LWNvbnRyb2xzLS1pc0dyYWJiaW5nXCIpO1xuXG4gICAgJChkb2N1bWVudCkub2ZmKFwiLmZiLnRvdWNoXCIpO1xuXG4gICAgZG9jdW1lbnQucmVtb3ZlRXZlbnRMaXN0ZW5lcihcInNjcm9sbFwiLCBzZWxmLm9uc2Nyb2xsLCB0cnVlKTtcblxuICAgIGlmIChzZWxmLnJlcXVlc3RJZCkge1xuICAgICAgY2FuY2VsQUZyYW1lKHNlbGYucmVxdWVzdElkKTtcblxuICAgICAgc2VsZi5yZXF1ZXN0SWQgPSBudWxsO1xuICAgIH1cblxuICAgIHNlbGYuaXNTd2lwaW5nID0gZmFsc2U7XG4gICAgc2VsZi5pc1Bhbm5pbmcgPSBmYWxzZTtcbiAgICBzZWxmLmlzWm9vbWluZyA9IGZhbHNlO1xuICAgIHNlbGYuaXNTY3JvbGxpbmcgPSBmYWxzZTtcblxuICAgIHNlbGYuaW5zdGFuY2UuaXNEcmFnZ2luZyA9IGZhbHNlO1xuXG4gICAgaWYgKHNlbGYuY2FuVGFwKSB7XG4gICAgICByZXR1cm4gc2VsZi5vblRhcChlKTtcbiAgICB9XG5cbiAgICBzZWxmLnNwZWVkID0gMzY2O1xuXG4gICAgLy8gU3BlZWQgaW4gcHgvbXNcbiAgICBzZWxmLnZlbG9jaXR5WCA9IHNlbGYuZGlzdGFuY2VYIC8gZE1zICogMC41O1xuICAgIHNlbGYudmVsb2NpdHlZID0gc2VsZi5kaXN0YW5jZVkgLyBkTXMgKiAwLjU7XG5cbiAgICBzZWxmLnNwZWVkWCA9IE1hdGgubWF4KHNlbGYuc3BlZWQgKiAwLjUsIE1hdGgubWluKHNlbGYuc3BlZWQgKiAxLjUsIDEgLyBNYXRoLmFicyhzZWxmLnZlbG9jaXR5WCkgKiBzZWxmLnNwZWVkKSk7XG5cbiAgICBpZiAocGFubmluZykge1xuICAgICAgc2VsZi5lbmRQYW5uaW5nKCk7XG4gICAgfSBlbHNlIGlmICh6b29taW5nKSB7XG4gICAgICBzZWxmLmVuZFpvb21pbmcoKTtcbiAgICB9IGVsc2Uge1xuICAgICAgc2VsZi5lbmRTd2lwaW5nKHN3aXBpbmcsIHNjcm9sbGluZyk7XG4gICAgfVxuXG4gICAgcmV0dXJuO1xuICB9O1xuXG4gIEd1ZXN0dXJlcy5wcm90b3R5cGUuZW5kU3dpcGluZyA9IGZ1bmN0aW9uKHN3aXBpbmcsIHNjcm9sbGluZykge1xuICAgIHZhciBzZWxmID0gdGhpcyxcbiAgICAgIHJldCA9IGZhbHNlLFxuICAgICAgbGVuID0gc2VsZi5pbnN0YW5jZS5ncm91cC5sZW5ndGg7XG5cbiAgICBzZWxmLnNsaWRlckxhc3RQb3MgPSBudWxsO1xuXG4gICAgLy8gQ2xvc2UgaWYgc3dpcGVkIHZlcnRpY2FsbHkgLyBuYXZpZ2F0ZSBpZiBob3Jpem9udGFsbHlcbiAgICBpZiAoc3dpcGluZyA9PSBcInlcIiAmJiAhc2Nyb2xsaW5nICYmIE1hdGguYWJzKHNlbGYuZGlzdGFuY2VZKSA+IDUwKSB7XG4gICAgICAvLyBDb250aW51ZSB2ZXJ0aWNhbCBtb3ZlbWVudFxuICAgICAgJC5mYW5jeWJveC5hbmltYXRlKFxuICAgICAgICBzZWxmLmluc3RhbmNlLmN1cnJlbnQuJHNsaWRlLFxuICAgICAgICB7XG4gICAgICAgICAgdG9wOiBzZWxmLnNsaWRlclN0YXJ0UG9zLnRvcCArIHNlbGYuZGlzdGFuY2VZICsgc2VsZi52ZWxvY2l0eVkgKiAxNTAsXG4gICAgICAgICAgb3BhY2l0eTogMFxuICAgICAgICB9LFxuICAgICAgICAyMDBcbiAgICAgICk7XG5cbiAgICAgIHJldCA9IHNlbGYuaW5zdGFuY2UuY2xvc2UodHJ1ZSwgMjAwKTtcbiAgICB9IGVsc2UgaWYgKHN3aXBpbmcgPT0gXCJ4XCIgJiYgc2VsZi5kaXN0YW5jZVggPiA1MCAmJiBsZW4gPiAxKSB7XG4gICAgICByZXQgPSBzZWxmLmluc3RhbmNlLnByZXZpb3VzKHNlbGYuc3BlZWRYKTtcbiAgICB9IGVsc2UgaWYgKHN3aXBpbmcgPT0gXCJ4XCIgJiYgc2VsZi5kaXN0YW5jZVggPCAtNTAgJiYgbGVuID4gMSkge1xuICAgICAgcmV0ID0gc2VsZi5pbnN0YW5jZS5uZXh0KHNlbGYuc3BlZWRYKTtcbiAgICB9XG5cbiAgICBpZiAocmV0ID09PSBmYWxzZSAmJiAoc3dpcGluZyA9PSBcInhcIiB8fCBzd2lwaW5nID09IFwieVwiKSkge1xuICAgICAgaWYgKHNjcm9sbGluZyB8fCBsZW4gPCAyKSB7XG4gICAgICAgIHNlbGYuaW5zdGFuY2UuY2VudGVyU2xpZGUoc2VsZi5pbnN0YW5jZS5jdXJyZW50LCAxNTApO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgc2VsZi5pbnN0YW5jZS5qdW1wVG8oc2VsZi5pbnN0YW5jZS5jdXJyZW50LmluZGV4KTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICBzZWxmLiRjb250YWluZXIucmVtb3ZlQ2xhc3MoXCJmYW5jeWJveC1pcy1zbGlkaW5nXCIpO1xuICB9O1xuXG4gIC8vIExpbWl0IHBhbm5pbmcgZnJvbSBlZGdlc1xuICAvLyA9PT09PT09PT09PT09PT09PT09PT09PT1cbiAgR3Vlc3R1cmVzLnByb3RvdHlwZS5lbmRQYW5uaW5nID0gZnVuY3Rpb24oKSB7XG4gICAgdmFyIHNlbGYgPSB0aGlzO1xuICAgIHZhciBuZXdPZmZzZXRYLCBuZXdPZmZzZXRZLCBuZXdQb3M7XG5cbiAgICBpZiAoIXNlbGYuY29udGVudExhc3RQb3MpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBpZiAoc2VsZi5vcHRzLm1vbWVudHVtID09PSBmYWxzZSkge1xuICAgICAgbmV3T2Zmc2V0WCA9IHNlbGYuY29udGVudExhc3RQb3MubGVmdDtcbiAgICAgIG5ld09mZnNldFkgPSBzZWxmLmNvbnRlbnRMYXN0UG9zLnRvcDtcbiAgICB9IGVsc2Uge1xuICAgICAgLy8gQ29udGludWUgbW92ZW1lbnRcbiAgICAgIG5ld09mZnNldFggPSBzZWxmLmNvbnRlbnRMYXN0UG9zLmxlZnQgKyBzZWxmLnZlbG9jaXR5WCAqIHNlbGYuc3BlZWQ7XG4gICAgICBuZXdPZmZzZXRZID0gc2VsZi5jb250ZW50TGFzdFBvcy50b3AgKyBzZWxmLnZlbG9jaXR5WSAqIHNlbGYuc3BlZWQ7XG4gICAgfVxuXG4gICAgbmV3UG9zID0gc2VsZi5saW1pdFBvc2l0aW9uKG5ld09mZnNldFgsIG5ld09mZnNldFksIHNlbGYuY29udGVudFN0YXJ0UG9zLndpZHRoLCBzZWxmLmNvbnRlbnRTdGFydFBvcy5oZWlnaHQpO1xuXG4gICAgbmV3UG9zLndpZHRoID0gc2VsZi5jb250ZW50U3RhcnRQb3Mud2lkdGg7XG4gICAgbmV3UG9zLmhlaWdodCA9IHNlbGYuY29udGVudFN0YXJ0UG9zLmhlaWdodDtcblxuICAgICQuZmFuY3lib3guYW5pbWF0ZShzZWxmLiRjb250ZW50LCBuZXdQb3MsIDMzMCk7XG4gIH07XG5cbiAgR3Vlc3R1cmVzLnByb3RvdHlwZS5lbmRab29taW5nID0gZnVuY3Rpb24oKSB7XG4gICAgdmFyIHNlbGYgPSB0aGlzO1xuXG4gICAgdmFyIGN1cnJlbnQgPSBzZWxmLmluc3RhbmNlLmN1cnJlbnQ7XG5cbiAgICB2YXIgbmV3T2Zmc2V0WCwgbmV3T2Zmc2V0WSwgbmV3UG9zLCByZXNldDtcblxuICAgIHZhciBuZXdXaWR0aCA9IHNlbGYubmV3V2lkdGg7XG4gICAgdmFyIG5ld0hlaWdodCA9IHNlbGYubmV3SGVpZ2h0O1xuXG4gICAgaWYgKCFzZWxmLmNvbnRlbnRMYXN0UG9zKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgbmV3T2Zmc2V0WCA9IHNlbGYuY29udGVudExhc3RQb3MubGVmdDtcbiAgICBuZXdPZmZzZXRZID0gc2VsZi5jb250ZW50TGFzdFBvcy50b3A7XG5cbiAgICByZXNldCA9IHtcbiAgICAgIHRvcDogbmV3T2Zmc2V0WSxcbiAgICAgIGxlZnQ6IG5ld09mZnNldFgsXG4gICAgICB3aWR0aDogbmV3V2lkdGgsXG4gICAgICBoZWlnaHQ6IG5ld0hlaWdodCxcbiAgICAgIHNjYWxlWDogMSxcbiAgICAgIHNjYWxlWTogMVxuICAgIH07XG5cbiAgICAvLyBSZXNldCBzY2FsZXgvc2NhbGVZIHZhbHVlczsgdGhpcyBoZWxwcyBmb3IgcGVyZm9tYW5jZSBhbmQgZG9lcyBub3QgYnJlYWsgYW5pbWF0aW9uXG4gICAgJC5mYW5jeWJveC5zZXRUcmFuc2xhdGUoc2VsZi4kY29udGVudCwgcmVzZXQpO1xuXG4gICAgaWYgKG5ld1dpZHRoIDwgc2VsZi5jYW52YXNXaWR0aCAmJiBuZXdIZWlnaHQgPCBzZWxmLmNhbnZhc0hlaWdodCkge1xuICAgICAgc2VsZi5pbnN0YW5jZS5zY2FsZVRvRml0KDE1MCk7XG4gICAgfSBlbHNlIGlmIChuZXdXaWR0aCA+IGN1cnJlbnQud2lkdGggfHwgbmV3SGVpZ2h0ID4gY3VycmVudC5oZWlnaHQpIHtcbiAgICAgIHNlbGYuaW5zdGFuY2Uuc2NhbGVUb0FjdHVhbChzZWxmLmNlbnRlclBvaW50U3RhcnRYLCBzZWxmLmNlbnRlclBvaW50U3RhcnRZLCAxNTApO1xuICAgIH0gZWxzZSB7XG4gICAgICBuZXdQb3MgPSBzZWxmLmxpbWl0UG9zaXRpb24obmV3T2Zmc2V0WCwgbmV3T2Zmc2V0WSwgbmV3V2lkdGgsIG5ld0hlaWdodCk7XG5cbiAgICAgIC8vIFN3aXRjaCBmcm9tIHNjYWxlKCkgdG8gd2lkdGgvaGVpZ2h0IG9yIGFuaW1hdGlvbiB3aWxsIG5vdCB3b3JrIGNvcnJlY3RseVxuICAgICAgJC5mYW5jeWJveC5zZXRUcmFuc2xhdGUoc2VsZi4kY29udGVudCwgJC5mYW5jeWJveC5nZXRUcmFuc2xhdGUoc2VsZi4kY29udGVudCkpO1xuXG4gICAgICAkLmZhbmN5Ym94LmFuaW1hdGUoc2VsZi4kY29udGVudCwgbmV3UG9zLCAxNTApO1xuICAgIH1cbiAgfTtcblxuICBHdWVzdHVyZXMucHJvdG90eXBlLm9uVGFwID0gZnVuY3Rpb24oZSkge1xuICAgIHZhciBzZWxmID0gdGhpcztcbiAgICB2YXIgJHRhcmdldCA9ICQoZS50YXJnZXQpO1xuXG4gICAgdmFyIGluc3RhbmNlID0gc2VsZi5pbnN0YW5jZTtcbiAgICB2YXIgY3VycmVudCA9IGluc3RhbmNlLmN1cnJlbnQ7XG5cbiAgICB2YXIgZW5kUG9pbnRzID0gKGUgJiYgZ2V0UG9pbnRlclhZKGUpKSB8fCBzZWxmLnN0YXJ0UG9pbnRzO1xuXG4gICAgdmFyIHRhcFggPSBlbmRQb2ludHNbMF0gPyBlbmRQb2ludHNbMF0ueCAtICQod2luZG93KS5zY3JvbGxMZWZ0KCkgLSBzZWxmLnN0YWdlUG9zLmxlZnQgOiAwO1xuICAgIHZhciB0YXBZID0gZW5kUG9pbnRzWzBdID8gZW5kUG9pbnRzWzBdLnkgLSAkKHdpbmRvdykuc2Nyb2xsVG9wKCkgLSBzZWxmLnN0YWdlUG9zLnRvcCA6IDA7XG5cbiAgICB2YXIgd2hlcmU7XG5cbiAgICB2YXIgcHJvY2VzcyA9IGZ1bmN0aW9uKHByZWZpeCkge1xuICAgICAgdmFyIGFjdGlvbiA9IGN1cnJlbnQub3B0c1twcmVmaXhdO1xuXG4gICAgICBpZiAoJC5pc0Z1bmN0aW9uKGFjdGlvbikpIHtcbiAgICAgICAgYWN0aW9uID0gYWN0aW9uLmFwcGx5KGluc3RhbmNlLCBbY3VycmVudCwgZV0pO1xuICAgICAgfVxuXG4gICAgICBpZiAoIWFjdGlvbikge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIHN3aXRjaCAoYWN0aW9uKSB7XG4gICAgICAgIGNhc2UgXCJjbG9zZVwiOlxuICAgICAgICAgIGluc3RhbmNlLmNsb3NlKHNlbGYuc3RhcnRFdmVudCk7XG5cbiAgICAgICAgICBicmVhaztcblxuICAgICAgICBjYXNlIFwidG9nZ2xlQ29udHJvbHNcIjpcbiAgICAgICAgICBpbnN0YW5jZS50b2dnbGVDb250cm9scyh0cnVlKTtcblxuICAgICAgICAgIGJyZWFrO1xuXG4gICAgICAgIGNhc2UgXCJuZXh0XCI6XG4gICAgICAgICAgaW5zdGFuY2UubmV4dCgpO1xuXG4gICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgY2FzZSBcIm5leHRPckNsb3NlXCI6XG4gICAgICAgICAgaWYgKGluc3RhbmNlLmdyb3VwLmxlbmd0aCA+IDEpIHtcbiAgICAgICAgICAgIGluc3RhbmNlLm5leHQoKTtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgaW5zdGFuY2UuY2xvc2Uoc2VsZi5zdGFydEV2ZW50KTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICBicmVhaztcblxuICAgICAgICBjYXNlIFwiem9vbVwiOlxuICAgICAgICAgIGlmIChjdXJyZW50LnR5cGUgPT0gXCJpbWFnZVwiICYmIChjdXJyZW50LmlzTG9hZGVkIHx8IGN1cnJlbnQuJGdob3N0KSkge1xuICAgICAgICAgICAgaWYgKGluc3RhbmNlLmNhblBhbigpKSB7XG4gICAgICAgICAgICAgIGluc3RhbmNlLnNjYWxlVG9GaXQoKTtcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoaW5zdGFuY2UuaXNTY2FsZWREb3duKCkpIHtcbiAgICAgICAgICAgICAgaW5zdGFuY2Uuc2NhbGVUb0FjdHVhbCh0YXBYLCB0YXBZKTtcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoaW5zdGFuY2UuZ3JvdXAubGVuZ3RoIDwgMikge1xuICAgICAgICAgICAgICBpbnN0YW5jZS5jbG9zZShzZWxmLnN0YXJ0RXZlbnQpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cblxuICAgICAgICAgIGJyZWFrO1xuICAgICAgfVxuICAgIH07XG5cbiAgICAvLyBJZ25vcmUgcmlnaHQgY2xpY2tcbiAgICBpZiAoZS5vcmlnaW5hbEV2ZW50ICYmIGUub3JpZ2luYWxFdmVudC5idXR0b24gPT0gMikge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIC8vIFNraXAgaWYgY2xpY2tlZCBvbiB0aGUgc2Nyb2xsYmFyXG4gICAgaWYgKCEkdGFyZ2V0LmlzKFwiaW1nXCIpICYmIHRhcFggPiAkdGFyZ2V0WzBdLmNsaWVudFdpZHRoICsgJHRhcmdldC5vZmZzZXQoKS5sZWZ0KSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgLy8gQ2hlY2sgd2hlcmUgaXMgY2xpY2tlZFxuICAgIGlmICgkdGFyZ2V0LmlzKFwiLmZhbmN5Ym94LWJnLC5mYW5jeWJveC1pbm5lciwuZmFuY3lib3gtb3V0ZXIsLmZhbmN5Ym94LWNvbnRhaW5lclwiKSkge1xuICAgICAgd2hlcmUgPSBcIk91dHNpZGVcIjtcbiAgICB9IGVsc2UgaWYgKCR0YXJnZXQuaXMoXCIuZmFuY3lib3gtc2xpZGVcIikpIHtcbiAgICAgIHdoZXJlID0gXCJTbGlkZVwiO1xuICAgIH0gZWxzZSBpZiAoXG4gICAgICBpbnN0YW5jZS5jdXJyZW50LiRjb250ZW50ICYmXG4gICAgICBpbnN0YW5jZS5jdXJyZW50LiRjb250ZW50XG4gICAgICAgIC5maW5kKCR0YXJnZXQpXG4gICAgICAgIC5hZGRCYWNrKClcbiAgICAgICAgLmZpbHRlcigkdGFyZ2V0KS5sZW5ndGhcbiAgICApIHtcbiAgICAgIHdoZXJlID0gXCJDb250ZW50XCI7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICAvLyBDaGVjayBpZiB0aGlzIGlzIGEgZG91YmxlIHRhcFxuICAgIGlmIChzZWxmLnRhcHBlZCkge1xuICAgICAgLy8gU3RvcCBwcmV2aW91c2x5IGNyZWF0ZWQgc2luZ2xlIHRhcFxuICAgICAgY2xlYXJUaW1lb3V0KHNlbGYudGFwcGVkKTtcbiAgICAgIHNlbGYudGFwcGVkID0gbnVsbDtcblxuICAgICAgLy8gU2tpcCBpZiBkaXN0YW5jZSBiZXR3ZWVuIHRhcHMgaXMgdG9vIGJpZ1xuICAgICAgaWYgKE1hdGguYWJzKHRhcFggLSBzZWxmLnRhcFgpID4gNTAgfHwgTWF0aC5hYnModGFwWSAtIHNlbGYudGFwWSkgPiA1MCkge1xuICAgICAgICByZXR1cm4gdGhpcztcbiAgICAgIH1cblxuICAgICAgLy8gT0ssIG5vdyB3ZSBhc3N1bWUgdGhhdCB0aGlzIGlzIGEgZG91YmxlLXRhcFxuICAgICAgcHJvY2VzcyhcImRibGNsaWNrXCIgKyB3aGVyZSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIC8vIFNpbmdsZSB0YXAgd2lsbCBiZSBwcm9jZXNzZWQgaWYgdXNlciBoYXMgbm90IGNsaWNrZWQgc2Vjb25kIHRpbWUgd2l0aGluIDMwMG1zXG4gICAgICAvLyBvciB0aGVyZSBpcyBubyBuZWVkIHRvIHdhaXQgZm9yIGRvdWJsZS10YXBcbiAgICAgIHNlbGYudGFwWCA9IHRhcFg7XG4gICAgICBzZWxmLnRhcFkgPSB0YXBZO1xuXG4gICAgICBpZiAoY3VycmVudC5vcHRzW1wiZGJsY2xpY2tcIiArIHdoZXJlXSAmJiBjdXJyZW50Lm9wdHNbXCJkYmxjbGlja1wiICsgd2hlcmVdICE9PSBjdXJyZW50Lm9wdHNbXCJjbGlja1wiICsgd2hlcmVdKSB7XG4gICAgICAgIHNlbGYudGFwcGVkID0gc2V0VGltZW91dChmdW5jdGlvbigpIHtcbiAgICAgICAgICBzZWxmLnRhcHBlZCA9IG51bGw7XG5cbiAgICAgICAgICBwcm9jZXNzKFwiY2xpY2tcIiArIHdoZXJlKTtcbiAgICAgICAgfSwgNTAwKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHByb2Nlc3MoXCJjbGlja1wiICsgd2hlcmUpO1xuICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybiB0aGlzO1xuICB9O1xuXG4gICQoZG9jdW1lbnQpLm9uKFwib25BY3RpdmF0ZS5mYlwiLCBmdW5jdGlvbihlLCBpbnN0YW5jZSkge1xuICAgIGlmIChpbnN0YW5jZSAmJiAhaW5zdGFuY2UuR3Vlc3R1cmVzKSB7XG4gICAgICBpbnN0YW5jZS5HdWVzdHVyZXMgPSBuZXcgR3Vlc3R1cmVzKGluc3RhbmNlKTtcbiAgICB9XG4gIH0pO1xufSkod2luZG93LCBkb2N1bWVudCwgd2luZG93LmpRdWVyeSB8fCBqUXVlcnkpO1xuIl0sImZpbGUiOiJndWVzdHVyZXMuanMifQ==
