//// ==========================================================================
//
// Share
// Displays simple form for sharing current url
//
// ==========================================================================
(function(document, $) {
  "use strict";

  $.extend(true, $.fancybox.defaults, {
    btnTpl: {
      share:
        '<button data-fancybox-share class="fancybox-button fancybox-button--share" title="{{SHARE}}">' +
        '<svg viewBox="0 0 40 40">' +
        '<path d="M6,30 C8,18 19,16 23,16 L23,16 L23,10 L33,20 L23,29 L23,24 C19,24 8,27 6,30 Z">' +
        "</svg>" +
        "</button>"
    },
    share: {
      url: function(instance, item) {
        return (
          (!instance.currentHash && !(item.type === "inline" || item.type === "html") ? item.origSrc || item.src : false) || window.location
        );
      },
      tpl:
        '<div class="fancybox-share">' +
        "<h1>{{SHARE}}</h1>" +
        "<p>" +
        '<a class="fancybox-share__button fancybox-share__button--fb" href="https://www.facebook.com/sharer/sharer.php?u={{url}}">' +
        '<svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="m287 456v-299c0-21 6-35 35-35h38v-63c-7-1-29-3-55-3-54 0-91 33-91 94v306m143-254h-205v72h196" /></svg>' +
        "<span>Facebook</span>" +
        "</a>" +
        '<a class="fancybox-share__button fancybox-share__button--tw" href="https://twitter.com/intent/tweet?url={{url}}&text={{descr}}">' +
        '<svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="m456 133c-14 7-31 11-47 13 17-10 30-27 37-46-15 10-34 16-52 20-61-62-157-7-141 75-68-3-129-35-169-85-22 37-11 86 26 109-13 0-26-4-37-9 0 39 28 72 65 80-12 3-25 4-37 2 10 33 41 57 77 57-42 30-77 38-122 34 170 111 378-32 359-208 16-11 30-25 41-42z" /></svg>' +
        "<span>Twitter</span>" +
        "</a>" +
        '<a class="fancybox-share__button fancybox-share__button--pt" href="https://www.pinterest.com/pin/create/button/?url={{url}}&description={{descr}}&media={{media}}">' +
        '<svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="m265 56c-109 0-164 78-164 144 0 39 15 74 47 87 5 2 10 0 12-5l4-19c2-6 1-8-3-13-9-11-15-25-15-45 0-58 43-110 113-110 62 0 96 38 96 88 0 67-30 122-73 122-24 0-42-19-36-44 6-29 20-60 20-81 0-19-10-35-31-35-25 0-44 26-44 60 0 21 7 36 7 36l-30 125c-8 37-1 83 0 87 0 3 4 4 5 2 2-3 32-39 42-75l16-64c8 16 31 29 56 29 74 0 124-67 124-157 0-69-58-132-146-132z" fill="#fff"/></svg>' +
        "<span>Pinterest</span>" +
        "</a>" +
        "</p>" +
        '<p><input class="fancybox-share__input" type="text" value="{{url_raw}}" /></p>' +
        "</div>"
    }
  });

  function escapeHtml(string) {
    var entityMap = {
      "&": "&amp;",
      "<": "&lt;",
      ">": "&gt;",
      '"': "&quot;",
      "'": "&#39;",
      "/": "&#x2F;",
      "`": "&#x60;",
      "=": "&#x3D;"
    };

    return String(string).replace(/[&<>"'`=\/]/g, function(s) {
      return entityMap[s];
    });
  }

  $(document).on("click", "[data-fancybox-share]", function() {
    var instance = $.fancybox.getInstance(),
      current = instance.current || null,
      url,
      tpl;

    if (!current) {
      return;
    }

    if ($.type(current.opts.share.url) === "function") {
      url = current.opts.share.url.apply(current, [instance, current]);
    }

    tpl = current.opts.share.tpl
      .replace(/\{\{media\}\}/g, current.type === "image" ? encodeURIComponent(current.src) : "")
      .replace(/\{\{url\}\}/g, encodeURIComponent(url))
      .replace(/\{\{url_raw\}\}/g, escapeHtml(url))
      .replace(/\{\{descr\}\}/g, instance.$caption ? encodeURIComponent(instance.$caption.text()) : "");

    $.fancybox.open({
      src: instance.translate(instance, tpl),
      type: "html",
      opts: {
        animationEffect: false,
        afterLoad: function(shareInstance, shareCurrent) {
          // Close self if parent instance is closing
          instance.$refs.container.one("beforeClose.fb", function() {
            shareInstance.close(null, 0);
          });

          // Opening links in a popup window
          shareCurrent.$content.find(".fancybox-share__links a").click(function() {
            window.open(this.href, "Share", "width=550, height=450");
            return false;
          });
        }
      }
    });
  });
})(document, window.jQuery || jQuery);

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJzaGFyZS5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvLy8vID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG4vL1xuLy8gU2hhcmVcbi8vIERpc3BsYXlzIHNpbXBsZSBmb3JtIGZvciBzaGFyaW5nIGN1cnJlbnQgdXJsXG4vL1xuLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cbihmdW5jdGlvbihkb2N1bWVudCwgJCkge1xuICBcInVzZSBzdHJpY3RcIjtcblxuICAkLmV4dGVuZCh0cnVlLCAkLmZhbmN5Ym94LmRlZmF1bHRzLCB7XG4gICAgYnRuVHBsOiB7XG4gICAgICBzaGFyZTpcbiAgICAgICAgJzxidXR0b24gZGF0YS1mYW5jeWJveC1zaGFyZSBjbGFzcz1cImZhbmN5Ym94LWJ1dHRvbiBmYW5jeWJveC1idXR0b24tLXNoYXJlXCIgdGl0bGU9XCJ7e1NIQVJFfX1cIj4nICtcbiAgICAgICAgJzxzdmcgdmlld0JveD1cIjAgMCA0MCA0MFwiPicgK1xuICAgICAgICAnPHBhdGggZD1cIk02LDMwIEM4LDE4IDE5LDE2IDIzLDE2IEwyMywxNiBMMjMsMTAgTDMzLDIwIEwyMywyOSBMMjMsMjQgQzE5LDI0IDgsMjcgNiwzMCBaXCI+JyArXG4gICAgICAgIFwiPC9zdmc+XCIgK1xuICAgICAgICBcIjwvYnV0dG9uPlwiXG4gICAgfSxcbiAgICBzaGFyZToge1xuICAgICAgdXJsOiBmdW5jdGlvbihpbnN0YW5jZSwgaXRlbSkge1xuICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICghaW5zdGFuY2UuY3VycmVudEhhc2ggJiYgIShpdGVtLnR5cGUgPT09IFwiaW5saW5lXCIgfHwgaXRlbS50eXBlID09PSBcImh0bWxcIikgPyBpdGVtLm9yaWdTcmMgfHwgaXRlbS5zcmMgOiBmYWxzZSkgfHwgd2luZG93LmxvY2F0aW9uXG4gICAgICAgICk7XG4gICAgICB9LFxuICAgICAgdHBsOlxuICAgICAgICAnPGRpdiBjbGFzcz1cImZhbmN5Ym94LXNoYXJlXCI+JyArXG4gICAgICAgIFwiPGgxPnt7U0hBUkV9fTwvaDE+XCIgK1xuICAgICAgICBcIjxwPlwiICtcbiAgICAgICAgJzxhIGNsYXNzPVwiZmFuY3lib3gtc2hhcmVfX2J1dHRvbiBmYW5jeWJveC1zaGFyZV9fYnV0dG9uLS1mYlwiIGhyZWY9XCJodHRwczovL3d3dy5mYWNlYm9vay5jb20vc2hhcmVyL3NoYXJlci5waHA/dT17e3VybH19XCI+JyArXG4gICAgICAgICc8c3ZnIHZpZXdCb3g9XCIwIDAgNTEyIDUxMlwiIHhtbG5zPVwiaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmdcIj48cGF0aCBkPVwibTI4NyA0NTZ2LTI5OWMwLTIxIDYtMzUgMzUtMzVoMzh2LTYzYy03LTEtMjktMy01NS0zLTU0IDAtOTEgMzMtOTEgOTR2MzA2bTE0My0yNTRoLTIwNXY3MmgxOTZcIiAvPjwvc3ZnPicgK1xuICAgICAgICBcIjxzcGFuPkZhY2Vib29rPC9zcGFuPlwiICtcbiAgICAgICAgXCI8L2E+XCIgK1xuICAgICAgICAnPGEgY2xhc3M9XCJmYW5jeWJveC1zaGFyZV9fYnV0dG9uIGZhbmN5Ym94LXNoYXJlX19idXR0b24tLXR3XCIgaHJlZj1cImh0dHBzOi8vdHdpdHRlci5jb20vaW50ZW50L3R3ZWV0P3VybD17e3VybH19JnRleHQ9e3tkZXNjcn19XCI+JyArXG4gICAgICAgICc8c3ZnIHZpZXdCb3g9XCIwIDAgNTEyIDUxMlwiIHhtbG5zPVwiaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmdcIj48cGF0aCBkPVwibTQ1NiAxMzNjLTE0IDctMzEgMTEtNDcgMTMgMTctMTAgMzAtMjcgMzctNDYtMTUgMTAtMzQgMTYtNTIgMjAtNjEtNjItMTU3LTctMTQxIDc1LTY4LTMtMTI5LTM1LTE2OS04NS0yMiAzNy0xMSA4NiAyNiAxMDktMTMgMC0yNi00LTM3LTkgMCAzOSAyOCA3MiA2NSA4MC0xMiAzLTI1IDQtMzcgMiAxMCAzMyA0MSA1NyA3NyA1Ny00MiAzMC03NyAzOC0xMjIgMzQgMTcwIDExMSAzNzgtMzIgMzU5LTIwOCAxNi0xMSAzMC0yNSA0MS00MnpcIiAvPjwvc3ZnPicgK1xuICAgICAgICBcIjxzcGFuPlR3aXR0ZXI8L3NwYW4+XCIgK1xuICAgICAgICBcIjwvYT5cIiArXG4gICAgICAgICc8YSBjbGFzcz1cImZhbmN5Ym94LXNoYXJlX19idXR0b24gZmFuY3lib3gtc2hhcmVfX2J1dHRvbi0tcHRcIiBocmVmPVwiaHR0cHM6Ly93d3cucGludGVyZXN0LmNvbS9waW4vY3JlYXRlL2J1dHRvbi8/dXJsPXt7dXJsfX0mZGVzY3JpcHRpb249e3tkZXNjcn19Jm1lZGlhPXt7bWVkaWF9fVwiPicgK1xuICAgICAgICAnPHN2ZyB2aWV3Qm94PVwiMCAwIDUxMiA1MTJcIiB4bWxucz1cImh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnXCI+PHBhdGggZD1cIm0yNjUgNTZjLTEwOSAwLTE2NCA3OC0xNjQgMTQ0IDAgMzkgMTUgNzQgNDcgODcgNSAyIDEwIDAgMTItNWw0LTE5YzItNiAxLTgtMy0xMy05LTExLTE1LTI1LTE1LTQ1IDAtNTggNDMtMTEwIDExMy0xMTAgNjIgMCA5NiAzOCA5NiA4OCAwIDY3LTMwIDEyMi03MyAxMjItMjQgMC00Mi0xOS0zNi00NCA2LTI5IDIwLTYwIDIwLTgxIDAtMTktMTAtMzUtMzEtMzUtMjUgMC00NCAyNi00NCA2MCAwIDIxIDcgMzYgNyAzNmwtMzAgMTI1Yy04IDM3LTEgODMgMCA4NyAwIDMgNCA0IDUgMiAyLTMgMzItMzkgNDItNzVsMTYtNjRjOCAxNiAzMSAyOSA1NiAyOSA3NCAwIDEyNC02NyAxMjQtMTU3IDAtNjktNTgtMTMyLTE0Ni0xMzJ6XCIgZmlsbD1cIiNmZmZcIi8+PC9zdmc+JyArXG4gICAgICAgIFwiPHNwYW4+UGludGVyZXN0PC9zcGFuPlwiICtcbiAgICAgICAgXCI8L2E+XCIgK1xuICAgICAgICBcIjwvcD5cIiArXG4gICAgICAgICc8cD48aW5wdXQgY2xhc3M9XCJmYW5jeWJveC1zaGFyZV9faW5wdXRcIiB0eXBlPVwidGV4dFwiIHZhbHVlPVwie3t1cmxfcmF3fX1cIiAvPjwvcD4nICtcbiAgICAgICAgXCI8L2Rpdj5cIlxuICAgIH1cbiAgfSk7XG5cbiAgZnVuY3Rpb24gZXNjYXBlSHRtbChzdHJpbmcpIHtcbiAgICB2YXIgZW50aXR5TWFwID0ge1xuICAgICAgXCImXCI6IFwiJmFtcDtcIixcbiAgICAgIFwiPFwiOiBcIiZsdDtcIixcbiAgICAgIFwiPlwiOiBcIiZndDtcIixcbiAgICAgICdcIic6IFwiJnF1b3Q7XCIsXG4gICAgICBcIidcIjogXCImIzM5O1wiLFxuICAgICAgXCIvXCI6IFwiJiN4MkY7XCIsXG4gICAgICBcImBcIjogXCImI3g2MDtcIixcbiAgICAgIFwiPVwiOiBcIiYjeDNEO1wiXG4gICAgfTtcblxuICAgIHJldHVybiBTdHJpbmcoc3RyaW5nKS5yZXBsYWNlKC9bJjw+XCInYD1cXC9dL2csIGZ1bmN0aW9uKHMpIHtcbiAgICAgIHJldHVybiBlbnRpdHlNYXBbc107XG4gICAgfSk7XG4gIH1cblxuICAkKGRvY3VtZW50KS5vbihcImNsaWNrXCIsIFwiW2RhdGEtZmFuY3lib3gtc2hhcmVdXCIsIGZ1bmN0aW9uKCkge1xuICAgIHZhciBpbnN0YW5jZSA9ICQuZmFuY3lib3guZ2V0SW5zdGFuY2UoKSxcbiAgICAgIGN1cnJlbnQgPSBpbnN0YW5jZS5jdXJyZW50IHx8IG51bGwsXG4gICAgICB1cmwsXG4gICAgICB0cGw7XG5cbiAgICBpZiAoIWN1cnJlbnQpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBpZiAoJC50eXBlKGN1cnJlbnQub3B0cy5zaGFyZS51cmwpID09PSBcImZ1bmN0aW9uXCIpIHtcbiAgICAgIHVybCA9IGN1cnJlbnQub3B0cy5zaGFyZS51cmwuYXBwbHkoY3VycmVudCwgW2luc3RhbmNlLCBjdXJyZW50XSk7XG4gICAgfVxuXG4gICAgdHBsID0gY3VycmVudC5vcHRzLnNoYXJlLnRwbFxuICAgICAgLnJlcGxhY2UoL1xce1xce21lZGlhXFx9XFx9L2csIGN1cnJlbnQudHlwZSA9PT0gXCJpbWFnZVwiID8gZW5jb2RlVVJJQ29tcG9uZW50KGN1cnJlbnQuc3JjKSA6IFwiXCIpXG4gICAgICAucmVwbGFjZSgvXFx7XFx7dXJsXFx9XFx9L2csIGVuY29kZVVSSUNvbXBvbmVudCh1cmwpKVxuICAgICAgLnJlcGxhY2UoL1xce1xce3VybF9yYXdcXH1cXH0vZywgZXNjYXBlSHRtbCh1cmwpKVxuICAgICAgLnJlcGxhY2UoL1xce1xce2Rlc2NyXFx9XFx9L2csIGluc3RhbmNlLiRjYXB0aW9uID8gZW5jb2RlVVJJQ29tcG9uZW50KGluc3RhbmNlLiRjYXB0aW9uLnRleHQoKSkgOiBcIlwiKTtcblxuICAgICQuZmFuY3lib3gub3Blbih7XG4gICAgICBzcmM6IGluc3RhbmNlLnRyYW5zbGF0ZShpbnN0YW5jZSwgdHBsKSxcbiAgICAgIHR5cGU6IFwiaHRtbFwiLFxuICAgICAgb3B0czoge1xuICAgICAgICBhbmltYXRpb25FZmZlY3Q6IGZhbHNlLFxuICAgICAgICBhZnRlckxvYWQ6IGZ1bmN0aW9uKHNoYXJlSW5zdGFuY2UsIHNoYXJlQ3VycmVudCkge1xuICAgICAgICAgIC8vIENsb3NlIHNlbGYgaWYgcGFyZW50IGluc3RhbmNlIGlzIGNsb3NpbmdcbiAgICAgICAgICBpbnN0YW5jZS4kcmVmcy5jb250YWluZXIub25lKFwiYmVmb3JlQ2xvc2UuZmJcIiwgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBzaGFyZUluc3RhbmNlLmNsb3NlKG51bGwsIDApO1xuICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgLy8gT3BlbmluZyBsaW5rcyBpbiBhIHBvcHVwIHdpbmRvd1xuICAgICAgICAgIHNoYXJlQ3VycmVudC4kY29udGVudC5maW5kKFwiLmZhbmN5Ym94LXNoYXJlX19saW5rcyBhXCIpLmNsaWNrKGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgd2luZG93Lm9wZW4odGhpcy5ocmVmLCBcIlNoYXJlXCIsIFwid2lkdGg9NTUwLCBoZWlnaHQ9NDUwXCIpO1xuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfSk7XG4gIH0pO1xufSkoZG9jdW1lbnQsIHdpbmRvdy5qUXVlcnkgfHwgalF1ZXJ5KTtcbiJdLCJmaWxlIjoic2hhcmUuanMifQ==
