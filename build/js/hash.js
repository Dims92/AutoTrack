// ==========================================================================
//
// Hash
// Enables linking to each modal
//
// ==========================================================================
(function(document, window, $) {
  "use strict";

  // Simple $.escapeSelector polyfill (for jQuery prior v3)
  if (!$.escapeSelector) {
    $.escapeSelector = function(sel) {
      var rcssescape = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\x80-\uFFFF\w-]/g;
      var fcssescape = function(ch, asCodePoint) {
        if (asCodePoint) {
          // U+0000 NULL becomes U+FFFD REPLACEMENT CHARACTER
          if (ch === "\0") {
            return "\uFFFD";
          }

          // Control characters and (dependent upon position) numbers get escaped as code points
          return ch.slice(0, -1) + "\\" + ch.charCodeAt(ch.length - 1).toString(16) + " ";
        }

        // Other potentially-special ASCII characters get backslash-escaped
        return "\\" + ch;
      };

      return (sel + "").replace(rcssescape, fcssescape);
    };
  }

  // Get info about gallery name and current index from url
  function parseUrl() {
    var hash = window.location.hash.substr(1),
      rez = hash.split("-"),
      index = rez.length > 1 && /^\+?\d+$/.test(rez[rez.length - 1]) ? parseInt(rez.pop(-1), 10) || 1 : 1,
      gallery = rez.join("-");

    return {
      hash: hash,
      /* Index is starting from 1 */
      index: index < 1 ? 1 : index,
      gallery: gallery
    };
  }

  // Trigger click evnt on links to open new fancyBox instance
  function triggerFromUrl(url) {
    var $el;

    if (url.gallery !== "") {
      // If we can find element matching 'data-fancybox' atribute, then trigger click event for that.
      // It should start fancyBox
      $el = $("[data-fancybox='" + $.escapeSelector(url.gallery) + "']")
        .eq(url.index - 1)
        .trigger("click.fb-start");
    }
  }

  // Get gallery name from current instance
  function getGalleryID(instance) {
    var opts, ret;

    if (!instance) {
      return false;
    }

    opts = instance.current ? instance.current.opts : instance.opts;
    ret = opts.hash || (opts.$orig ? opts.$orig.data("fancybox") : "");

    return ret === "" ? false : ret;
  }

  // Start when DOM becomes ready
  $(function() {
    // Check if user has disabled this module
    if ($.fancybox.defaults.hash === false) {
      return;
    }

    // Update hash when opening/closing fancyBox
    $(document).on({
      "onInit.fb": function(e, instance) {
        var url, gallery;

        if (instance.group[instance.currIndex].opts.hash === false) {
          return;
        }

        url = parseUrl();
        gallery = getGalleryID(instance);

        // Make sure gallery start index matches index from hash
        if (gallery && url.gallery && gallery == url.gallery) {
          instance.currIndex = url.index - 1;
        }
      },

      "beforeShow.fb": function(e, instance, current, firstRun) {
        var gallery;

        if (!current || current.opts.hash === false) {
          return;
        }

        // Check if need to update window hash
        gallery = getGalleryID(instance);

        if (!gallery) {
          return;
        }

        // Variable containing last hash value set by fancyBox
        // It will be used to determine if fancyBox needs to close after hash change is detected
        instance.currentHash = gallery + (instance.group.length > 1 ? "-" + (current.index + 1) : "");

        // If current hash is the same (this instance most likely is opened by hashchange), then do nothing
        if (window.location.hash === "#" + instance.currentHash) {
          return;
        }

        if (!instance.origHash) {
          instance.origHash = window.location.hash;
        }

        if (instance.hashTimer) {
          clearTimeout(instance.hashTimer);
        }

        // Update hash
        instance.hashTimer = setTimeout(function() {
          if ("replaceState" in window.history) {
            window.history[firstRun ? "pushState" : "replaceState"](
              {},
              document.title,
              window.location.pathname + window.location.search + "#" + instance.currentHash
            );

            if (firstRun) {
              instance.hasCreatedHistory = true;
            }
          } else {
            window.location.hash = instance.currentHash;
          }

          instance.hashTimer = null;
        }, 300);
      },

      "beforeClose.fb": function(e, instance, current) {
        var gallery;

        if (current.opts.hash === false) {
          return;
        }

        gallery = getGalleryID(instance);

        // Goto previous history entry
        if (instance.currentHash && instance.hasCreatedHistory) {
          window.history.back();
        } else if (instance.currentHash) {
          if ("replaceState" in window.history) {
            window.history.replaceState({}, document.title, window.location.pathname + window.location.search + (instance.origHash || ""));
          } else {
            window.location.hash = instance.origHash;
          }
        }

        instance.currentHash = null;

        clearTimeout(instance.hashTimer);
      }
    });

    // Check if need to start/close after url has changed
    $(window).on("hashchange.fb", function() {
      var url = parseUrl(),
        fb;

      // Find last fancyBox instance that has "hash"
      $.each(
        $(".fancybox-container")
          .get()
          .reverse(),
        function(index, value) {
          var tmp = $(value).data("FancyBox");
          //isClosing
          if (tmp.currentHash) {
            fb = tmp;
            return false;
          }
        }
      );

      if (fb) {
        // Now, compare hash values
        if (fb.currentHash && fb.currentHash !== url.gallery + "-" + url.index && !(url.index === 1 && fb.currentHash == url.gallery)) {
          fb.currentHash = null;

          fb.close();
        }
      } else if (url.gallery !== "") {
        triggerFromUrl(url);
      }
    });

    // Check current hash and trigger click event on matching element to start fancyBox, if needed
    setTimeout(function() {
      if (!$.fancybox.getInstance()) {
        triggerFromUrl(parseUrl());
      }
    }, 50);
  });
})(document, window, window.jQuery || jQuery);

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJoYXNoLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8vID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG4vL1xuLy8gSGFzaFxuLy8gRW5hYmxlcyBsaW5raW5nIHRvIGVhY2ggbW9kYWxcbi8vXG4vLyA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuKGZ1bmN0aW9uKGRvY3VtZW50LCB3aW5kb3csICQpIHtcbiAgXCJ1c2Ugc3RyaWN0XCI7XG5cbiAgLy8gU2ltcGxlICQuZXNjYXBlU2VsZWN0b3IgcG9seWZpbGwgKGZvciBqUXVlcnkgcHJpb3IgdjMpXG4gIGlmICghJC5lc2NhcGVTZWxlY3Rvcikge1xuICAgICQuZXNjYXBlU2VsZWN0b3IgPSBmdW5jdGlvbihzZWwpIHtcbiAgICAgIHZhciByY3NzZXNjYXBlID0gLyhbXFwwLVxceDFmXFx4N2ZdfF4tP1xcZCl8Xi0kfFteXFx4ODAtXFx1RkZGRlxcdy1dL2c7XG4gICAgICB2YXIgZmNzc2VzY2FwZSA9IGZ1bmN0aW9uKGNoLCBhc0NvZGVQb2ludCkge1xuICAgICAgICBpZiAoYXNDb2RlUG9pbnQpIHtcbiAgICAgICAgICAvLyBVKzAwMDAgTlVMTCBiZWNvbWVzIFUrRkZGRCBSRVBMQUNFTUVOVCBDSEFSQUNURVJcbiAgICAgICAgICBpZiAoY2ggPT09IFwiXFwwXCIpIHtcbiAgICAgICAgICAgIHJldHVybiBcIlxcdUZGRkRcIjtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICAvLyBDb250cm9sIGNoYXJhY3RlcnMgYW5kIChkZXBlbmRlbnQgdXBvbiBwb3NpdGlvbikgbnVtYmVycyBnZXQgZXNjYXBlZCBhcyBjb2RlIHBvaW50c1xuICAgICAgICAgIHJldHVybiBjaC5zbGljZSgwLCAtMSkgKyBcIlxcXFxcIiArIGNoLmNoYXJDb2RlQXQoY2gubGVuZ3RoIC0gMSkudG9TdHJpbmcoMTYpICsgXCIgXCI7XG4gICAgICAgIH1cblxuICAgICAgICAvLyBPdGhlciBwb3RlbnRpYWxseS1zcGVjaWFsIEFTQ0lJIGNoYXJhY3RlcnMgZ2V0IGJhY2tzbGFzaC1lc2NhcGVkXG4gICAgICAgIHJldHVybiBcIlxcXFxcIiArIGNoO1xuICAgICAgfTtcblxuICAgICAgcmV0dXJuIChzZWwgKyBcIlwiKS5yZXBsYWNlKHJjc3Nlc2NhcGUsIGZjc3Nlc2NhcGUpO1xuICAgIH07XG4gIH1cblxuICAvLyBHZXQgaW5mbyBhYm91dCBnYWxsZXJ5IG5hbWUgYW5kIGN1cnJlbnQgaW5kZXggZnJvbSB1cmxcbiAgZnVuY3Rpb24gcGFyc2VVcmwoKSB7XG4gICAgdmFyIGhhc2ggPSB3aW5kb3cubG9jYXRpb24uaGFzaC5zdWJzdHIoMSksXG4gICAgICByZXogPSBoYXNoLnNwbGl0KFwiLVwiKSxcbiAgICAgIGluZGV4ID0gcmV6Lmxlbmd0aCA+IDEgJiYgL15cXCs/XFxkKyQvLnRlc3QocmV6W3Jlei5sZW5ndGggLSAxXSkgPyBwYXJzZUludChyZXoucG9wKC0xKSwgMTApIHx8IDEgOiAxLFxuICAgICAgZ2FsbGVyeSA9IHJlei5qb2luKFwiLVwiKTtcblxuICAgIHJldHVybiB7XG4gICAgICBoYXNoOiBoYXNoLFxuICAgICAgLyogSW5kZXggaXMgc3RhcnRpbmcgZnJvbSAxICovXG4gICAgICBpbmRleDogaW5kZXggPCAxID8gMSA6IGluZGV4LFxuICAgICAgZ2FsbGVyeTogZ2FsbGVyeVxuICAgIH07XG4gIH1cblxuICAvLyBUcmlnZ2VyIGNsaWNrIGV2bnQgb24gbGlua3MgdG8gb3BlbiBuZXcgZmFuY3lCb3ggaW5zdGFuY2VcbiAgZnVuY3Rpb24gdHJpZ2dlckZyb21VcmwodXJsKSB7XG4gICAgdmFyICRlbDtcblxuICAgIGlmICh1cmwuZ2FsbGVyeSAhPT0gXCJcIikge1xuICAgICAgLy8gSWYgd2UgY2FuIGZpbmQgZWxlbWVudCBtYXRjaGluZyAnZGF0YS1mYW5jeWJveCcgYXRyaWJ1dGUsIHRoZW4gdHJpZ2dlciBjbGljayBldmVudCBmb3IgdGhhdC5cbiAgICAgIC8vIEl0IHNob3VsZCBzdGFydCBmYW5jeUJveFxuICAgICAgJGVsID0gJChcIltkYXRhLWZhbmN5Ym94PSdcIiArICQuZXNjYXBlU2VsZWN0b3IodXJsLmdhbGxlcnkpICsgXCInXVwiKVxuICAgICAgICAuZXEodXJsLmluZGV4IC0gMSlcbiAgICAgICAgLnRyaWdnZXIoXCJjbGljay5mYi1zdGFydFwiKTtcbiAgICB9XG4gIH1cblxuICAvLyBHZXQgZ2FsbGVyeSBuYW1lIGZyb20gY3VycmVudCBpbnN0YW5jZVxuICBmdW5jdGlvbiBnZXRHYWxsZXJ5SUQoaW5zdGFuY2UpIHtcbiAgICB2YXIgb3B0cywgcmV0O1xuXG4gICAgaWYgKCFpbnN0YW5jZSkge1xuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cblxuICAgIG9wdHMgPSBpbnN0YW5jZS5jdXJyZW50ID8gaW5zdGFuY2UuY3VycmVudC5vcHRzIDogaW5zdGFuY2Uub3B0cztcbiAgICByZXQgPSBvcHRzLmhhc2ggfHwgKG9wdHMuJG9yaWcgPyBvcHRzLiRvcmlnLmRhdGEoXCJmYW5jeWJveFwiKSA6IFwiXCIpO1xuXG4gICAgcmV0dXJuIHJldCA9PT0gXCJcIiA/IGZhbHNlIDogcmV0O1xuICB9XG5cbiAgLy8gU3RhcnQgd2hlbiBET00gYmVjb21lcyByZWFkeVxuICAkKGZ1bmN0aW9uKCkge1xuICAgIC8vIENoZWNrIGlmIHVzZXIgaGFzIGRpc2FibGVkIHRoaXMgbW9kdWxlXG4gICAgaWYgKCQuZmFuY3lib3guZGVmYXVsdHMuaGFzaCA9PT0gZmFsc2UpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICAvLyBVcGRhdGUgaGFzaCB3aGVuIG9wZW5pbmcvY2xvc2luZyBmYW5jeUJveFxuICAgICQoZG9jdW1lbnQpLm9uKHtcbiAgICAgIFwib25Jbml0LmZiXCI6IGZ1bmN0aW9uKGUsIGluc3RhbmNlKSB7XG4gICAgICAgIHZhciB1cmwsIGdhbGxlcnk7XG5cbiAgICAgICAgaWYgKGluc3RhbmNlLmdyb3VwW2luc3RhbmNlLmN1cnJJbmRleF0ub3B0cy5oYXNoID09PSBmYWxzZSkge1xuICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIHVybCA9IHBhcnNlVXJsKCk7XG4gICAgICAgIGdhbGxlcnkgPSBnZXRHYWxsZXJ5SUQoaW5zdGFuY2UpO1xuXG4gICAgICAgIC8vIE1ha2Ugc3VyZSBnYWxsZXJ5IHN0YXJ0IGluZGV4IG1hdGNoZXMgaW5kZXggZnJvbSBoYXNoXG4gICAgICAgIGlmIChnYWxsZXJ5ICYmIHVybC5nYWxsZXJ5ICYmIGdhbGxlcnkgPT0gdXJsLmdhbGxlcnkpIHtcbiAgICAgICAgICBpbnN0YW5jZS5jdXJySW5kZXggPSB1cmwuaW5kZXggLSAxO1xuICAgICAgICB9XG4gICAgICB9LFxuXG4gICAgICBcImJlZm9yZVNob3cuZmJcIjogZnVuY3Rpb24oZSwgaW5zdGFuY2UsIGN1cnJlbnQsIGZpcnN0UnVuKSB7XG4gICAgICAgIHZhciBnYWxsZXJ5O1xuXG4gICAgICAgIGlmICghY3VycmVudCB8fCBjdXJyZW50Lm9wdHMuaGFzaCA9PT0gZmFsc2UpIHtcbiAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICAvLyBDaGVjayBpZiBuZWVkIHRvIHVwZGF0ZSB3aW5kb3cgaGFzaFxuICAgICAgICBnYWxsZXJ5ID0gZ2V0R2FsbGVyeUlEKGluc3RhbmNlKTtcblxuICAgICAgICBpZiAoIWdhbGxlcnkpIHtcbiAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICAvLyBWYXJpYWJsZSBjb250YWluaW5nIGxhc3QgaGFzaCB2YWx1ZSBzZXQgYnkgZmFuY3lCb3hcbiAgICAgICAgLy8gSXQgd2lsbCBiZSB1c2VkIHRvIGRldGVybWluZSBpZiBmYW5jeUJveCBuZWVkcyB0byBjbG9zZSBhZnRlciBoYXNoIGNoYW5nZSBpcyBkZXRlY3RlZFxuICAgICAgICBpbnN0YW5jZS5jdXJyZW50SGFzaCA9IGdhbGxlcnkgKyAoaW5zdGFuY2UuZ3JvdXAubGVuZ3RoID4gMSA/IFwiLVwiICsgKGN1cnJlbnQuaW5kZXggKyAxKSA6IFwiXCIpO1xuXG4gICAgICAgIC8vIElmIGN1cnJlbnQgaGFzaCBpcyB0aGUgc2FtZSAodGhpcyBpbnN0YW5jZSBtb3N0IGxpa2VseSBpcyBvcGVuZWQgYnkgaGFzaGNoYW5nZSksIHRoZW4gZG8gbm90aGluZ1xuICAgICAgICBpZiAod2luZG93LmxvY2F0aW9uLmhhc2ggPT09IFwiI1wiICsgaW5zdGFuY2UuY3VycmVudEhhc2gpIHtcbiAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoIWluc3RhbmNlLm9yaWdIYXNoKSB7XG4gICAgICAgICAgaW5zdGFuY2Uub3JpZ0hhc2ggPSB3aW5kb3cubG9jYXRpb24uaGFzaDtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChpbnN0YW5jZS5oYXNoVGltZXIpIHtcbiAgICAgICAgICBjbGVhclRpbWVvdXQoaW5zdGFuY2UuaGFzaFRpbWVyKTtcbiAgICAgICAgfVxuXG4gICAgICAgIC8vIFVwZGF0ZSBoYXNoXG4gICAgICAgIGluc3RhbmNlLmhhc2hUaW1lciA9IHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XG4gICAgICAgICAgaWYgKFwicmVwbGFjZVN0YXRlXCIgaW4gd2luZG93Lmhpc3RvcnkpIHtcbiAgICAgICAgICAgIHdpbmRvdy5oaXN0b3J5W2ZpcnN0UnVuID8gXCJwdXNoU3RhdGVcIiA6IFwicmVwbGFjZVN0YXRlXCJdKFxuICAgICAgICAgICAgICB7fSxcbiAgICAgICAgICAgICAgZG9jdW1lbnQudGl0bGUsXG4gICAgICAgICAgICAgIHdpbmRvdy5sb2NhdGlvbi5wYXRobmFtZSArIHdpbmRvdy5sb2NhdGlvbi5zZWFyY2ggKyBcIiNcIiArIGluc3RhbmNlLmN1cnJlbnRIYXNoXG4gICAgICAgICAgICApO1xuXG4gICAgICAgICAgICBpZiAoZmlyc3RSdW4pIHtcbiAgICAgICAgICAgICAgaW5zdGFuY2UuaGFzQ3JlYXRlZEhpc3RvcnkgPSB0cnVlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB3aW5kb3cubG9jYXRpb24uaGFzaCA9IGluc3RhbmNlLmN1cnJlbnRIYXNoO1xuICAgICAgICAgIH1cblxuICAgICAgICAgIGluc3RhbmNlLmhhc2hUaW1lciA9IG51bGw7XG4gICAgICAgIH0sIDMwMCk7XG4gICAgICB9LFxuXG4gICAgICBcImJlZm9yZUNsb3NlLmZiXCI6IGZ1bmN0aW9uKGUsIGluc3RhbmNlLCBjdXJyZW50KSB7XG4gICAgICAgIHZhciBnYWxsZXJ5O1xuXG4gICAgICAgIGlmIChjdXJyZW50Lm9wdHMuaGFzaCA9PT0gZmFsc2UpIHtcbiAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICBnYWxsZXJ5ID0gZ2V0R2FsbGVyeUlEKGluc3RhbmNlKTtcblxuICAgICAgICAvLyBHb3RvIHByZXZpb3VzIGhpc3RvcnkgZW50cnlcbiAgICAgICAgaWYgKGluc3RhbmNlLmN1cnJlbnRIYXNoICYmIGluc3RhbmNlLmhhc0NyZWF0ZWRIaXN0b3J5KSB7XG4gICAgICAgICAgd2luZG93Lmhpc3RvcnkuYmFjaygpO1xuICAgICAgICB9IGVsc2UgaWYgKGluc3RhbmNlLmN1cnJlbnRIYXNoKSB7XG4gICAgICAgICAgaWYgKFwicmVwbGFjZVN0YXRlXCIgaW4gd2luZG93Lmhpc3RvcnkpIHtcbiAgICAgICAgICAgIHdpbmRvdy5oaXN0b3J5LnJlcGxhY2VTdGF0ZSh7fSwgZG9jdW1lbnQudGl0bGUsIHdpbmRvdy5sb2NhdGlvbi5wYXRobmFtZSArIHdpbmRvdy5sb2NhdGlvbi5zZWFyY2ggKyAoaW5zdGFuY2Uub3JpZ0hhc2ggfHwgXCJcIikpO1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB3aW5kb3cubG9jYXRpb24uaGFzaCA9IGluc3RhbmNlLm9yaWdIYXNoO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIGluc3RhbmNlLmN1cnJlbnRIYXNoID0gbnVsbDtcblxuICAgICAgICBjbGVhclRpbWVvdXQoaW5zdGFuY2UuaGFzaFRpbWVyKTtcbiAgICAgIH1cbiAgICB9KTtcblxuICAgIC8vIENoZWNrIGlmIG5lZWQgdG8gc3RhcnQvY2xvc2UgYWZ0ZXIgdXJsIGhhcyBjaGFuZ2VkXG4gICAgJCh3aW5kb3cpLm9uKFwiaGFzaGNoYW5nZS5mYlwiLCBmdW5jdGlvbigpIHtcbiAgICAgIHZhciB1cmwgPSBwYXJzZVVybCgpLFxuICAgICAgICBmYjtcblxuICAgICAgLy8gRmluZCBsYXN0IGZhbmN5Qm94IGluc3RhbmNlIHRoYXQgaGFzIFwiaGFzaFwiXG4gICAgICAkLmVhY2goXG4gICAgICAgICQoXCIuZmFuY3lib3gtY29udGFpbmVyXCIpXG4gICAgICAgICAgLmdldCgpXG4gICAgICAgICAgLnJldmVyc2UoKSxcbiAgICAgICAgZnVuY3Rpb24oaW5kZXgsIHZhbHVlKSB7XG4gICAgICAgICAgdmFyIHRtcCA9ICQodmFsdWUpLmRhdGEoXCJGYW5jeUJveFwiKTtcbiAgICAgICAgICAvL2lzQ2xvc2luZ1xuICAgICAgICAgIGlmICh0bXAuY3VycmVudEhhc2gpIHtcbiAgICAgICAgICAgIGZiID0gdG1wO1xuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgKTtcblxuICAgICAgaWYgKGZiKSB7XG4gICAgICAgIC8vIE5vdywgY29tcGFyZSBoYXNoIHZhbHVlc1xuICAgICAgICBpZiAoZmIuY3VycmVudEhhc2ggJiYgZmIuY3VycmVudEhhc2ggIT09IHVybC5nYWxsZXJ5ICsgXCItXCIgKyB1cmwuaW5kZXggJiYgISh1cmwuaW5kZXggPT09IDEgJiYgZmIuY3VycmVudEhhc2ggPT0gdXJsLmdhbGxlcnkpKSB7XG4gICAgICAgICAgZmIuY3VycmVudEhhc2ggPSBudWxsO1xuXG4gICAgICAgICAgZmIuY2xvc2UoKTtcbiAgICAgICAgfVxuICAgICAgfSBlbHNlIGlmICh1cmwuZ2FsbGVyeSAhPT0gXCJcIikge1xuICAgICAgICB0cmlnZ2VyRnJvbVVybCh1cmwpO1xuICAgICAgfVxuICAgIH0pO1xuXG4gICAgLy8gQ2hlY2sgY3VycmVudCBoYXNoIGFuZCB0cmlnZ2VyIGNsaWNrIGV2ZW50IG9uIG1hdGNoaW5nIGVsZW1lbnQgdG8gc3RhcnQgZmFuY3lCb3gsIGlmIG5lZWRlZFxuICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XG4gICAgICBpZiAoISQuZmFuY3lib3guZ2V0SW5zdGFuY2UoKSkge1xuICAgICAgICB0cmlnZ2VyRnJvbVVybChwYXJzZVVybCgpKTtcbiAgICAgIH1cbiAgICB9LCA1MCk7XG4gIH0pO1xufSkoZG9jdW1lbnQsIHdpbmRvdywgd2luZG93LmpRdWVyeSB8fCBqUXVlcnkpO1xuIl0sImZpbGUiOiJoYXNoLmpzIn0=
