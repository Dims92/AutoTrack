/*
 * FancyBox - jQuery Plugin
 * Simple and fancy lightbox alternative
 *
 * Examples and documentation at: http://fancybox.net
 *
 * Copyright (c) 2008 - 2010 Janis Skarnelis
 * That said, it is hardly a one-person project. Many people have submitted bugs, code, and offered their advice freely. Their support is greatly appreciated.
 *
 * Version: 1.3.4 (11/11/2010)
 * Requires: jQuery v1.3+
 *
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 */

;(function($) {
	var tmp, loading, overlay, wrap, outer, content, close, title, nav_left, nav_right,

		selectedIndex = 0, selectedOpts = {}, selectedArray = [], currentIndex = 0, currentOpts = {}, currentArray = [],

		ajaxLoader = null, imgPreloader = new Image(), imgRegExp = /\.(jpg|gif|png|bmp|jpeg)(.*)?$/i, swfRegExp = /[^\.]\.(swf)\s*$/i,

		loadingTimer, loadingFrame = 1,

		titleHeight = 0, titleStr = '', start_pos, final_pos, busy = false, fx = $.extend($('<div/>')[0], { prop: 0 }),

		isIE6 = $.browser.msie && $.browser.version < 7 && !window.XMLHttpRequest,

		/*
		 * Private methods 
		 */

		_abort = function() {
			loading.hide();

			imgPreloader.onerror = imgPreloader.onload = null;

			if (ajaxLoader) {
				ajaxLoader.abort();
			}

			tmp.empty();
		},

		_error = function() {
			if (false === selectedOpts.onError(selectedArray, selectedIndex, selectedOpts)) {
				loading.hide();
				busy = false;
				return;
			}

			selectedOpts.titleShow = false;

			selectedOpts.width = 'auto';
			selectedOpts.height = 'auto';

			tmp.html( '<p id="fancybox-error">The requested content cannot be loaded.<br />Please try again later.</p>' );

			_process_inline();
		},

		_start = function() {
			var obj = selectedArray[ selectedIndex ],
				href, 
				type, 
				title,
				str,
				emb,
				ret;

			_abort();

			selectedOpts = $.extend({}, $.fn.fancybox.defaults, (typeof $(obj).data('fancybox') == 'undefined' ? selectedOpts : $(obj).data('fancybox')));

			ret = selectedOpts.onStart(selectedArray, selectedIndex, selectedOpts);

			if (ret === false) {
				busy = false;
				return;
			} else if (typeof ret == 'object') {
				selectedOpts = $.extend(selectedOpts, ret);
			}

			title = selectedOpts.title || (obj.nodeName ? $(obj).attr('title') : obj.title) || '';

			if (obj.nodeName && !selectedOpts.orig) {
				selectedOpts.orig = $(obj).children("img:first").length ? $(obj).children("img:first") : $(obj);
			}

			if (title === '' && selectedOpts.orig && selectedOpts.titleFromAlt) {
				title = selectedOpts.orig.attr('alt');
			}

			href = selectedOpts.href || (obj.nodeName ? $(obj).attr('href') : obj.href) || null;

			if ((/^(?:javascript)/i).test(href) || href == '#') {
				href = null;
			}

			if (selectedOpts.type) {
				type = selectedOpts.type;

				if (!href) {
					href = selectedOpts.content;
				}

			} else if (selectedOpts.content) {
				type = 'html';

			} else if (href) {
				if (href.match(imgRegExp)) {
					type = 'image';

				} else if (href.match(swfRegExp)) {
					type = 'swf';

				} else if ($(obj).hasClass("iframe")) {
					type = 'iframe';

				} else if (href.indexOf("#") === 0) {
					type = 'inline';

				} else {
					type = 'ajax';
				}
			}

			if (!type) {
				_error();
				return;
			}

			if (type == 'inline') {
				obj	= href.substr(href.indexOf("#"));
				type = $(obj).length > 0 ? 'inline' : 'ajax';
			}

			selectedOpts.type = type;
			selectedOpts.href = href;
			selectedOpts.title = title;

			if (selectedOpts.autoDimensions) {
				if (selectedOpts.type == 'html' || selectedOpts.type == 'inline' || selectedOpts.type == 'ajax') {
					selectedOpts.width = 'auto';
					selectedOpts.height = 'auto';
				} else {
					selectedOpts.autoDimensions = false;	
				}
			}

			if (selectedOpts.modal) {
				selectedOpts.overlayShow = true;
				selectedOpts.hideOnOverlayClick = false;
				selectedOpts.hideOnContentClick = false;
				selectedOpts.enableEscapeButton = false;
				selectedOpts.showCloseButton = false;
			}

			selectedOpts.padding = parseInt(selectedOpts.padding, 10);
			selectedOpts.margin = parseInt(selectedOpts.margin, 10);

			tmp.css('padding', (selectedOpts.padding + selectedOpts.margin));

			$('.fancybox-inline-tmp').unbind('fancybox-cancel').bind('fancybox-change', function() {
				$(this).replaceWith(content.children());				
			});

			switch (type) {
				case 'html' :
					tmp.html( selectedOpts.content );
					_process_inline();
				break;

				case 'inline' :
					if ( $(obj).parent().is('#fancybox-content') === true) {
						busy = false;
						return;
					}

					$('<div class="fancybox-inline-tmp" />')
						.hide()
						.insertBefore( $(obj) )
						.bind('fancybox-cleanup', function() {
							$(this).replaceWith(content.children());
						}).bind('fancybox-cancel', function() {
							$(this).replaceWith(tmp.children());
						});

					$(obj).appendTo(tmp);

					_process_inline();
				break;

				case 'image':
					busy = false;

					$.fancybox.showActivity();

					imgPreloader = new Image();

					imgPreloader.onerror = function() {
						_error();
					};

					imgPreloader.onload = function() {
						busy = true;

						imgPreloader.onerror = imgPreloader.onload = null;

						_process_image();
					};

					imgPreloader.src = href;
				break;

				case 'swf':
					selectedOpts.scrolling = 'no';

					str = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="' + selectedOpts.width + '" height="' + selectedOpts.height + '"><param name="movie" value="' + href + '"></param>';
					emb = '';

					$.each(selectedOpts.swf, function(name, val) {
						str += '<param name="' + name + '" value="' + val + '"></param>';
						emb += ' ' + name + '="' + val + '"';
					});

					str += '<embed src="' + href + '" type="application/x-shockwave-flash" width="' + selectedOpts.width + '" height="' + selectedOpts.height + '"' + emb + '></embed></object>';

					tmp.html(str);

					_process_inline();
				break;

				case 'ajax':
					busy = false;

					$.fancybox.showActivity();

					selectedOpts.ajax.win = selectedOpts.ajax.success;

					ajaxLoader = $.ajax($.extend({}, selectedOpts.ajax, {
						url	: href,
						data : selectedOpts.ajax.data || {},
						error : function(XMLHttpRequest, textStatus, errorThrown) {
							if ( XMLHttpRequest.status > 0 ) {
								_error();
							}
						},
						success : function(data, textStatus, XMLHttpRequest) {
							var o = typeof XMLHttpRequest == 'object' ? XMLHttpRequest : ajaxLoader;
							if (o.status == 200) {
								if ( typeof selectedOpts.ajax.win == 'function' ) {
									ret = selectedOpts.ajax.win(href, data, textStatus, XMLHttpRequest);

									if (ret === false) {
										loading.hide();
										return;
									} else if (typeof ret == 'string' || typeof ret == 'object') {
										data = ret;
									}
								}

								tmp.html( data );
								_process_inline();
							}
						}
					}));

				break;

				case 'iframe':
					_show();
				break;
			}
		},

		_process_inline = function() {
			var
				w = selectedOpts.width,
				h = selectedOpts.height;

			if (w.toString().indexOf('%') > -1) {
				w = parseInt( ($(window).width() - (selectedOpts.margin * 2)) * parseFloat(w) / 100, 10) + 'px';

			} else {
				w = w == 'auto' ? 'auto' : w + 'px';	
			}

			if (h.toString().indexOf('%') > -1) {
				h = parseInt( ($(window).height() - (selectedOpts.margin * 2)) * parseFloat(h) / 100, 10) + 'px';

			} else {
				h = h == 'auto' ? 'auto' : h + 'px';	
			}

			tmp.wrapInner('<div style="width:' + w + ';height:' + h + ';overflow: ' + (selectedOpts.scrolling == 'auto' ? 'auto' : (selectedOpts.scrolling == 'yes' ? 'scroll' : 'hidden')) + ';position:relative;"></div>');

			selectedOpts.width = tmp.width();
			selectedOpts.height = tmp.height();

			_show();
		},

		_process_image = function() {
			selectedOpts.width = imgPreloader.width;
			selectedOpts.height = imgPreloader.height;

			$("<img />").attr({
				'id' : 'fancybox-img',
				'src' : imgPreloader.src,
				'alt' : selectedOpts.title
			}).appendTo( tmp );

			_show();
		},

		_show = function() {
			var pos, equal;

			loading.hide();

			if (wrap.is(":visible") && false === currentOpts.onCleanup(currentArray, currentIndex, currentOpts)) {
				$.event.trigger('fancybox-cancel');

				busy = false;
				return;
			}

			busy = true;

			$(content.add( overlay )).unbind();

			$(window).unbind("resize.fb scroll.fb");
			$(document).unbind('keydown.fb');

			if (wrap.is(":visible") && currentOpts.titlePosition !== 'outside') {
				wrap.css('height', wrap.height());
			}

			currentArray = selectedArray;
			currentIndex = selectedIndex;
			currentOpts = selectedOpts;

			if (currentOpts.overlayShow) {
				overlay.css({
					'background-color' : currentOpts.overlayColor,
					'opacity' : currentOpts.overlayOpacity,
					'cursor' : currentOpts.hideOnOverlayClick ? 'pointer' : 'auto',
					'height' : $(document).height()
				});

				if (!overlay.is(':visible')) {
					if (isIE6) {
						$('select:not(#fancybox-tmp select)').filter(function() {
							return this.style.visibility !== 'hidden';
						}).css({'visibility' : 'hidden'}).one('fancybox-cleanup', function() {
							this.style.visibility = 'inherit';
						});
					}

					overlay.show();
				}
			} else {
				overlay.hide();
			}

			final_pos = _get_zoom_to();

			_process_title();

			if (wrap.is(":visible")) {
				$( close.add( nav_left ).add( nav_right ) ).hide();

				pos = wrap.position(),

				start_pos = {
					top	 : pos.top,
					left : pos.left,
					width : wrap.width(),
					height : wrap.height()
				};

				equal = (start_pos.width == final_pos.width && start_pos.height == final_pos.height);

				content.fadeTo(currentOpts.changeFade, 0.3, function() {
					var finish_resizing = function() {
						content.html( tmp.contents() ).fadeTo(currentOpts.changeFade, 1, _finish);
					};

					$.event.trigger('fancybox-change');

					content
						.empty()
						.removeAttr('filter')
						.css({
							'border-width' : currentOpts.padding,
							'width'	: final_pos.width - currentOpts.padding * 2,
							'height' : selectedOpts.autoDimensions ? 'auto' : final_pos.height - titleHeight - currentOpts.padding * 2
						});

					if (equal) {
						finish_resizing();

					} else {
						fx.prop = 0;

						$(fx).animate({prop: 1}, {
							 duration : currentOpts.changeSpeed,
							 easing : currentOpts.easingChange,
							 step : _draw,
							 complete : finish_resizing
						});
					}
				});

				return;
			}

			wrap.removeAttr("style");

			content.css('border-width', currentOpts.padding);

			if (currentOpts.transitionIn == 'elastic') {
				start_pos = _get_zoom_from();

				content.html( tmp.contents() );

				wrap.show();

				if (currentOpts.opacity) {
					final_pos.opacity = 0;
				}

				fx.prop = 0;

				$(fx).animate({prop: 1}, {
					 duration : currentOpts.speedIn,
					 easing : currentOpts.easingIn,
					 step : _draw,
					 complete : _finish
				});

				return;
			}

			if (currentOpts.titlePosition == 'inside' && titleHeight > 0) {	
				title.show();	
			}

			content
				.css({
					'width' : final_pos.width - currentOpts.padding * 2,
					'height' : selectedOpts.autoDimensions ? 'auto' : final_pos.height - titleHeight - currentOpts.padding * 2
				})
				.html( tmp.contents() );

			wrap
				.css(final_pos)
				.fadeIn( currentOpts.transitionIn == 'none' ? 0 : currentOpts.speedIn, _finish );
		},

		_format_title = function(title) {
			if (title && title.length) {
				if (currentOpts.titlePosition == 'float') {
					return '<table id="fancybox-title-float-wrap" cellpadding="0" cellspacing="0"><tr><td id="fancybox-title-float-left"></td><td id="fancybox-title-float-main">' + title + '</td><td id="fancybox-title-float-right"></td></tr></table>';
				}

				return '<div id="fancybox-title-' + currentOpts.titlePosition + '">' + title + '</div>';
			}

			return false;
		},

		_process_title = function() {
			titleStr = currentOpts.title || '';
			titleHeight = 0;

			title
				.empty()
				.removeAttr('style')
				.removeClass();

			if (currentOpts.titleShow === false) {
				title.hide();
				return;
			}

			titleStr = $.isFunction(currentOpts.titleFormat) ? currentOpts.titleFormat(titleStr, currentArray, currentIndex, currentOpts) : _format_title(titleStr);

			if (!titleStr || titleStr === '') {
				title.hide();
				return;
			}

			title
				.addClass('fancybox-title-' + currentOpts.titlePosition)
				.html( titleStr )
				.appendTo( 'body' )
				.show();

			switch (currentOpts.titlePosition) {
				case 'inside':
					title
						.css({
							'width' : final_pos.width - (currentOpts.padding * 2),
							'marginLeft' : currentOpts.padding,
							'marginRight' : currentOpts.padding
						});

					titleHeight = title.outerHeight(true);

					title.appendTo( outer );

					final_pos.height += titleHeight;
				break;

				case 'over':
					title
						.css({
							'marginLeft' : currentOpts.padding,
							'width'	: final_pos.width - (currentOpts.padding * 2),
							'bottom' : currentOpts.padding
						})
						.appendTo( outer );
				break;

				case 'float':
					title
						.css('left', parseInt((title.width() - final_pos.width - 40)/ 2, 10) * -1)
						.appendTo( wrap );
				break;

				default:
					title
						.css({
							'width' : final_pos.width - (currentOpts.padding * 2),
							'paddingLeft' : currentOpts.padding,
							'paddingRight' : currentOpts.padding
						})
						.appendTo( wrap );
				break;
			}

			title.hide();
		},

		_set_navigation = function() {
			if (currentOpts.enableEscapeButton || currentOpts.enableKeyboardNav) {
				$(document).bind('keydown.fb', function(e) {
					if (e.keyCode == 27 && currentOpts.enableEscapeButton) {
						e.preventDefault();
						$.fancybox.close();

					} else if ((e.keyCode == 37 || e.keyCode == 39) && currentOpts.enableKeyboardNav && e.target.tagName !== 'INPUT' && e.target.tagName !== 'TEXTAREA' && e.target.tagName !== 'SELECT') {
						e.preventDefault();
						$.fancybox[ e.keyCode == 37 ? 'prev' : 'next']();
					}
				});
			}

			if (!currentOpts.showNavArrows) { 
				nav_left.hide();
				nav_right.hide();
				return;
			}

			if ((currentOpts.cyclic && currentArray.length > 1) || currentIndex !== 0) {
				nav_left.show();
			}

			if ((currentOpts.cyclic && currentArray.length > 1) || currentIndex != (currentArray.length -1)) {
				nav_right.show();
			}
		},

		_finish = function () {
			if (!$.support.opacity) {
				content.get(0).style.removeAttribute('filter');
				wrap.get(0).style.removeAttribute('filter');
			}

			if (selectedOpts.autoDimensions) {
				content.css('height', 'auto');
			}

			wrap.css('height', 'auto');

			if (titleStr && titleStr.length) {
				title.show();
			}

			if (currentOpts.showCloseButton) {
				close.show();
			}

			_set_navigation();
	
			if (currentOpts.hideOnContentClick)	{
				content.bind('click', $.fancybox.close);
			}

			if (currentOpts.hideOnOverlayClick)	{
				overlay.bind('click', $.fancybox.close);
			}

			$(window).bind("resize.fb", $.fancybox.resize);

			if (currentOpts.centerOnScroll) {
				$(window).bind("scroll.fb", $.fancybox.center);
			}

			if (currentOpts.type == 'iframe') {
				$('<iframe id="fancybox-frame" name="fancybox-frame' + new Date().getTime() + '" frameborder="0" hspace="0" ' + ($.browser.msie ? 'allowtransparency="true""' : '') + ' scrolling="' + selectedOpts.scrolling + '" src="' + currentOpts.href + '"></iframe>').appendTo(content);
			}

			wrap.show();

			busy = false;

			$.fancybox.center();

			currentOpts.onComplete(currentArray, currentIndex, currentOpts);

			_preload_images();
		},

		_preload_images = function() {
			var href, 
				objNext;

			if ((currentArray.length -1) > currentIndex) {
				href = currentArray[ currentIndex + 1 ].href;

				if (typeof href !== 'undefined' && href.match(imgRegExp)) {
					objNext = new Image();
					objNext.src = href;
				}
			}

			if (currentIndex > 0) {
				href = currentArray[ currentIndex - 1 ].href;

				if (typeof href !== 'undefined' && href.match(imgRegExp)) {
					objNext = new Image();
					objNext.src = href;
				}
			}
		},

		_draw = function(pos) {
			var dim = {
				width : parseInt(start_pos.width + (final_pos.width - start_pos.width) * pos, 10),
				height : parseInt(start_pos.height + (final_pos.height - start_pos.height) * pos, 10),

				top : parseInt(start_pos.top + (final_pos.top - start_pos.top) * pos, 10),
				left : parseInt(start_pos.left + (final_pos.left - start_pos.left) * pos, 10)
			};

			if (typeof final_pos.opacity !== 'undefined') {
				dim.opacity = pos < 0.5 ? 0.5 : pos;
			}

			wrap.css(dim);

			content.css({
				'width' : dim.width - currentOpts.padding * 2,
				'height' : dim.height - (titleHeight * pos) - currentOpts.padding * 2
			});
		},

		_get_viewport = function() {
			return [
				$(window).width() - (currentOpts.margin * 2),
				$(window).height() - (currentOpts.margin * 2),
				$(document).scrollLeft() + currentOpts.margin,
				$(document).scrollTop() + currentOpts.margin
			];
		},

		_get_zoom_to = function () {
			var view = _get_viewport(),
				to = {},
				resize = currentOpts.autoScale,
				double_padding = currentOpts.padding * 2,
				ratio;

			if (currentOpts.width.toString().indexOf('%') > -1) {
				to.width = parseInt((view[0] * parseFloat(currentOpts.width)) / 100, 10);
			} else {
				to.width = currentOpts.width + double_padding;
			}

			if (currentOpts.height.toString().indexOf('%') > -1) {
				to.height = parseInt((view[1] * parseFloat(currentOpts.height)) / 100, 10);
			} else {
				to.height = currentOpts.height + double_padding;
			}

			if (resize && (to.width > view[0] || to.height > view[1])) {
				if (selectedOpts.type == 'image' || selectedOpts.type == 'swf') {
					ratio = (currentOpts.width ) / (currentOpts.height );

					if ((to.width ) > view[0]) {
						to.width = view[0];
						to.height = parseInt(((to.width - double_padding) / ratio) + double_padding, 10);
					}

					if ((to.height) > view[1]) {
						to.height = view[1];
						to.width = parseInt(((to.height - double_padding) * ratio) + double_padding, 10);
					}

				} else {
					to.width = Math.min(to.width, view[0]);
					to.height = Math.min(to.height, view[1]);
				}
			}

			to.top = parseInt(Math.max(view[3] - 20, view[3] + ((view[1] - to.height - 40) * 0.5)), 10);
			to.left = parseInt(Math.max(view[2] - 20, view[2] + ((view[0] - to.width - 40) * 0.5)), 10);

			return to;
		},

		_get_obj_pos = function(obj) {
			var pos = obj.offset();

			pos.top += parseInt( obj.css('paddingTop'), 10 ) || 0;
			pos.left += parseInt( obj.css('paddingLeft'), 10 ) || 0;

			pos.top += parseInt( obj.css('border-top-width'), 10 ) || 0;
			pos.left += parseInt( obj.css('border-left-width'), 10 ) || 0;

			pos.width = obj.width();
			pos.height = obj.height();

			return pos;
		},

		_get_zoom_from = function() {
			var orig = selectedOpts.orig ? $(selectedOpts.orig) : false,
				from = {},
				pos,
				view;

			if (orig && orig.length) {
				pos = _get_obj_pos(orig);

				from = {
					width : pos.width + (currentOpts.padding * 2),
					height : pos.height + (currentOpts.padding * 2),
					top	: pos.top - currentOpts.padding - 20,
					left : pos.left - currentOpts.padding - 20
				};

			} else {
				view = _get_viewport();

				from = {
					width : currentOpts.padding * 2,
					height : currentOpts.padding * 2,
					top	: parseInt(view[3] + view[1] * 0.5, 10),
					left : parseInt(view[2] + view[0] * 0.5, 10)
				};
			}

			return from;
		},

		_animate_loading = function() {
			if (!loading.is(':visible')){
				clearInterval(loadingTimer);
				return;
			}

			$('div', loading).css('top', (loadingFrame * -40) + 'px');

			loadingFrame = (loadingFrame + 1) % 12;
		};

	/*
	 * Public methods 
	 */

	$.fn.fancybox = function(options) {
		if (!$(this).length) {
			return this;
		}

		$(this)
			.data('fancybox', $.extend({}, options, ($.metadata ? $(this).metadata() : {})))
			.unbind('click.fb')
			.bind('click.fb', function(e) {
				e.preventDefault();

				if (busy) {
					return;
				}

				busy = true;

				$(this).blur();

				selectedArray = [];
				selectedIndex = 0;

				var rel = $(this).attr('rel') || '';

				if (!rel || rel == '' || rel === 'nofollow') {
					selectedArray.push(this);

				} else {
					selectedArray = $("a[rel=" + rel + "], area[rel=" + rel + "]");
					selectedIndex = selectedArray.index( this );
				}

				_start();

				return;
			});

		return this;
	};

	$.fancybox = function(obj) {
		var opts;

		if (busy) {
			return;
		}

		busy = true;
		opts = typeof arguments[1] !== 'undefined' ? arguments[1] : {};

		selectedArray = [];
		selectedIndex = parseInt(opts.index, 10) || 0;

		if ($.isArray(obj)) {
			for (var i = 0, j = obj.length; i < j; i++) {
				if (typeof obj[i] == 'object') {
					$(obj[i]).data('fancybox', $.extend({}, opts, obj[i]));
				} else {
					obj[i] = $({}).data('fancybox', $.extend({content : obj[i]}, opts));
				}
			}

			selectedArray = jQuery.merge(selectedArray, obj);

		} else {
			if (typeof obj == 'object') {
				$(obj).data('fancybox', $.extend({}, opts, obj));
			} else {
				obj = $({}).data('fancybox', $.extend({content : obj}, opts));
			}

			selectedArray.push(obj);
		}

		if (selectedIndex > selectedArray.length || selectedIndex < 0) {
			selectedIndex = 0;
		}

		_start();
	};

	$.fancybox.showActivity = function() {
		clearInterval(loadingTimer);

		loading.show();
		loadingTimer = setInterval(_animate_loading, 66);
	};

	$.fancybox.hideActivity = function() {
		loading.hide();
	};

	$.fancybox.next = function() {
		return $.fancybox.pos( currentIndex + 1);
	};

	$.fancybox.prev = function() {
		return $.fancybox.pos( currentIndex - 1);
	};

	$.fancybox.pos = function(pos) {
		if (busy) {
			return;
		}

		pos = parseInt(pos);

		selectedArray = currentArray;

		if (pos > -1 && pos < currentArray.length) {
			selectedIndex = pos;
			_start();

		} else if (currentOpts.cyclic && currentArray.length > 1) {
			selectedIndex = pos >= currentArray.length ? 0 : currentArray.length - 1;
			_start();
		}

		return;
	};

	$.fancybox.cancel = function() {
		if (busy) {
			return;
		}

		busy = true;

		$.event.trigger('fancybox-cancel');

		_abort();

		selectedOpts.onCancel(selectedArray, selectedIndex, selectedOpts);

		busy = false;
	};

	// Note: within an iframe use - parent.$.fancybox.close();
	$.fancybox.close = function() {
		if (busy || wrap.is(':hidden')) {
			return;
		}

		busy = true;

		if (currentOpts && false === currentOpts.onCleanup(currentArray, currentIndex, currentOpts)) {
			busy = false;
			return;
		}

		_abort();

		$(close.add( nav_left ).add( nav_right )).hide();

		$(content.add( overlay )).unbind();

		$(window).unbind("resize.fb scroll.fb");
		$(document).unbind('keydown.fb');

		content.find('iframe').attr('src', isIE6 && /^https/i.test(window.location.href || '') ? 'javascript:void(false)' : 'about:blank');

		if (currentOpts.titlePosition !== 'inside') {
			title.empty();
		}

		wrap.stop();

		function _cleanup() {
			overlay.fadeOut('fast');

			title.empty().hide();
			wrap.hide();

			$.event.trigger('fancybox-cleanup');

			content.empty();

			currentOpts.onClosed(currentArray, currentIndex, currentOpts);

			currentArray = selectedOpts	= [];
			currentIndex = selectedIndex = 0;
			currentOpts = selectedOpts	= {};

			busy = false;
		}

		if (currentOpts.transitionOut == 'elastic') {
			start_pos = _get_zoom_from();

			var pos = wrap.position();

			final_pos = {
				top	 : pos.top ,
				left : pos.left,
				width :	wrap.width(),
				height : wrap.height()
			};

			if (currentOpts.opacity) {
				final_pos.opacity = 1;
			}

			title.empty().hide();

			fx.prop = 1;

			$(fx).animate({ prop: 0 }, {
				 duration : currentOpts.speedOut,
				 easing : currentOpts.easingOut,
				 step : _draw,
				 complete : _cleanup
			});

		} else {
			wrap.fadeOut( currentOpts.transitionOut == 'none' ? 0 : currentOpts.speedOut, _cleanup);
		}
	};

	$.fancybox.resize = function() {
		if (overlay.is(':visible')) {
			overlay.css('height', $(document).height());
		}

		$.fancybox.center(true);
	};

	$.fancybox.center = function() {
		var view, align;

		if (busy) {
			return;	
		}

		align = arguments[0] === true ? 1 : 0;
		view = _get_viewport();

		if (!align && (wrap.width() > view[0] || wrap.height() > view[1])) {
			return;	
		}

		wrap
			.stop()
			.animate({
				'top' : parseInt(Math.max(view[3] - 20, view[3] + ((view[1] - content.height() - 40) * 0.5) - currentOpts.padding)),
				'left' : parseInt(Math.max(view[2] - 20, view[2] + ((view[0] - content.width() - 40) * 0.5) - currentOpts.padding))
			}, typeof arguments[0] == 'number' ? arguments[0] : 200);
	};

	$.fancybox.init = function() {
		if ($("#fancybox-wrap").length) {
			return;
		}

		$('body').append(
			tmp	= $('<div id="fancybox-tmp"></div>'),
			loading	= $('<div id="fancybox-loading"><div></div></div>'),
			overlay	= $('<div id="fancybox-overlay"></div>'),
			wrap = $('<div id="fancybox-wrap"></div>')
		);

		outer = $('<div id="fancybox-outer"></div>')
			.append('<div class="fancybox-bg" id="fancybox-bg-n"></div><div class="fancybox-bg" id="fancybox-bg-ne"></div><div class="fancybox-bg" id="fancybox-bg-e"></div><div class="fancybox-bg" id="fancybox-bg-se"></div><div class="fancybox-bg" id="fancybox-bg-s"></div><div class="fancybox-bg" id="fancybox-bg-sw"></div><div class="fancybox-bg" id="fancybox-bg-w"></div><div class="fancybox-bg" id="fancybox-bg-nw"></div>')
			.appendTo( wrap );

		outer.append(
			content = $('<div id="fancybox-content"></div>'),
			close = $('<a id="fancybox-close"></a>'),
			title = $('<div id="fancybox-title"></div>'),

			nav_left = $('<a href="javascript:;" id="fancybox-left"><span class="fancy-ico" id="fancybox-left-ico"></span></a>'),
			nav_right = $('<a href="javascript:;" id="fancybox-right"><span class="fancy-ico" id="fancybox-right-ico"></span></a>')
		);

		close.click($.fancybox.close);
		loading.click($.fancybox.cancel);

		nav_left.click(function(e) {
			e.preventDefault();
			$.fancybox.prev();
		});

		nav_right.click(function(e) {
			e.preventDefault();
			$.fancybox.next();
		});

		if ($.fn.mousewheel) {
			wrap.bind('mousewheel.fb', function(e, delta) {
				if (busy) {
					e.preventDefault();

				} else if ($(e.target).get(0).clientHeight == 0 || $(e.target).get(0).scrollHeight === $(e.target).get(0).clientHeight) {
					e.preventDefault();
					$.fancybox[ delta > 0 ? 'prev' : 'next']();
				}
			});
		}

		if (!$.support.opacity) {
			wrap.addClass('fancybox-ie');
		}

		if (isIE6) {
			loading.addClass('fancybox-ie6');
			wrap.addClass('fancybox-ie6');

			$('<iframe id="fancybox-hide-sel-frame" src="' + (/^https/i.test(window.location.href || '') ? 'javascript:void(false)' : 'about:blank' ) + '" scrolling="no" border="0" frameborder="0" tabindex="-1"></iframe>').prependTo(outer);
		}
	};

	$.fn.fancybox.defaults = {
		padding : 10,
		margin : 40,
		opacity : false,
		modal : false,
		cyclic : false,
		scrolling : 'auto',	// 'auto', 'yes' or 'no'

		width : 560,
		height : 340,

		autoScale : true,
		autoDimensions : true,
		centerOnScroll : false,

		ajax : {},
		swf : { wmode: 'transparent' },

		hideOnOverlayClick : true,
		hideOnContentClick : false,

		overlayShow : true,
		overlayOpacity : 0.7,
		overlayColor : '#777',

		titleShow : true,
		titlePosition : 'float', // 'float', 'outside', 'inside' or 'over'
		titleFormat : null,
		titleFromAlt : false,

		transitionIn : 'fade', // 'elastic', 'fade' or 'none'
		transitionOut : 'fade', // 'elastic', 'fade' or 'none'

		speedIn : 300,
		speedOut : 300,

		changeSpeed : 300,
		changeFade : 'fast',

		easingIn : 'swing',
		easingOut : 'swing',

		showCloseButton	 : true,
		showNavArrows : true,
		enableEscapeButton : true,
		enableKeyboardNav : true,

		onStart : function(){},
		onCancel : function(){},
		onComplete : function(){},
		onCleanup : function(){},
		onClosed : function(){},
		onError : function(){}
	};

	$(document).ready(function() {
		$.fancybox.init();
	});

})(jQuery);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJqcXVlcnkuZmFuY3lib3gtMS4zLjQuanMiXSwic291cmNlc0NvbnRlbnQiOlsiLypcclxuICogRmFuY3lCb3ggLSBqUXVlcnkgUGx1Z2luXHJcbiAqIFNpbXBsZSBhbmQgZmFuY3kgbGlnaHRib3ggYWx0ZXJuYXRpdmVcclxuICpcclxuICogRXhhbXBsZXMgYW5kIGRvY3VtZW50YXRpb24gYXQ6IGh0dHA6Ly9mYW5jeWJveC5uZXRcclxuICpcclxuICogQ29weXJpZ2h0IChjKSAyMDA4IC0gMjAxMCBKYW5pcyBTa2FybmVsaXNcclxuICogVGhhdCBzYWlkLCBpdCBpcyBoYXJkbHkgYSBvbmUtcGVyc29uIHByb2plY3QuIE1hbnkgcGVvcGxlIGhhdmUgc3VibWl0dGVkIGJ1Z3MsIGNvZGUsIGFuZCBvZmZlcmVkIHRoZWlyIGFkdmljZSBmcmVlbHkuIFRoZWlyIHN1cHBvcnQgaXMgZ3JlYXRseSBhcHByZWNpYXRlZC5cclxuICpcclxuICogVmVyc2lvbjogMS4zLjQgKDExLzExLzIwMTApXHJcbiAqIFJlcXVpcmVzOiBqUXVlcnkgdjEuMytcclxuICpcclxuICogRHVhbCBsaWNlbnNlZCB1bmRlciB0aGUgTUlUIGFuZCBHUEwgbGljZW5zZXM6XHJcbiAqICAgaHR0cDovL3d3dy5vcGVuc291cmNlLm9yZy9saWNlbnNlcy9taXQtbGljZW5zZS5waHBcclxuICogICBodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLmh0bWxcclxuICovXHJcblxyXG47KGZ1bmN0aW9uKCQpIHtcclxuXHR2YXIgdG1wLCBsb2FkaW5nLCBvdmVybGF5LCB3cmFwLCBvdXRlciwgY29udGVudCwgY2xvc2UsIHRpdGxlLCBuYXZfbGVmdCwgbmF2X3JpZ2h0LFxyXG5cclxuXHRcdHNlbGVjdGVkSW5kZXggPSAwLCBzZWxlY3RlZE9wdHMgPSB7fSwgc2VsZWN0ZWRBcnJheSA9IFtdLCBjdXJyZW50SW5kZXggPSAwLCBjdXJyZW50T3B0cyA9IHt9LCBjdXJyZW50QXJyYXkgPSBbXSxcclxuXHJcblx0XHRhamF4TG9hZGVyID0gbnVsbCwgaW1nUHJlbG9hZGVyID0gbmV3IEltYWdlKCksIGltZ1JlZ0V4cCA9IC9cXC4oanBnfGdpZnxwbmd8Ym1wfGpwZWcpKC4qKT8kL2ksIHN3ZlJlZ0V4cCA9IC9bXlxcLl1cXC4oc3dmKVxccyokL2ksXHJcblxyXG5cdFx0bG9hZGluZ1RpbWVyLCBsb2FkaW5nRnJhbWUgPSAxLFxyXG5cclxuXHRcdHRpdGxlSGVpZ2h0ID0gMCwgdGl0bGVTdHIgPSAnJywgc3RhcnRfcG9zLCBmaW5hbF9wb3MsIGJ1c3kgPSBmYWxzZSwgZnggPSAkLmV4dGVuZCgkKCc8ZGl2Lz4nKVswXSwgeyBwcm9wOiAwIH0pLFxyXG5cclxuXHRcdGlzSUU2ID0gJC5icm93c2VyLm1zaWUgJiYgJC5icm93c2VyLnZlcnNpb24gPCA3ICYmICF3aW5kb3cuWE1MSHR0cFJlcXVlc3QsXHJcblxyXG5cdFx0LypcclxuXHRcdCAqIFByaXZhdGUgbWV0aG9kcyBcclxuXHRcdCAqL1xyXG5cclxuXHRcdF9hYm9ydCA9IGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRsb2FkaW5nLmhpZGUoKTtcclxuXHJcblx0XHRcdGltZ1ByZWxvYWRlci5vbmVycm9yID0gaW1nUHJlbG9hZGVyLm9ubG9hZCA9IG51bGw7XHJcblxyXG5cdFx0XHRpZiAoYWpheExvYWRlcikge1xyXG5cdFx0XHRcdGFqYXhMb2FkZXIuYWJvcnQoKTtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0dG1wLmVtcHR5KCk7XHJcblx0XHR9LFxyXG5cclxuXHRcdF9lcnJvciA9IGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRpZiAoZmFsc2UgPT09IHNlbGVjdGVkT3B0cy5vbkVycm9yKHNlbGVjdGVkQXJyYXksIHNlbGVjdGVkSW5kZXgsIHNlbGVjdGVkT3B0cykpIHtcclxuXHRcdFx0XHRsb2FkaW5nLmhpZGUoKTtcclxuXHRcdFx0XHRidXN5ID0gZmFsc2U7XHJcblx0XHRcdFx0cmV0dXJuO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRzZWxlY3RlZE9wdHMudGl0bGVTaG93ID0gZmFsc2U7XHJcblxyXG5cdFx0XHRzZWxlY3RlZE9wdHMud2lkdGggPSAnYXV0byc7XHJcblx0XHRcdHNlbGVjdGVkT3B0cy5oZWlnaHQgPSAnYXV0byc7XHJcblxyXG5cdFx0XHR0bXAuaHRtbCggJzxwIGlkPVwiZmFuY3lib3gtZXJyb3JcIj5UaGUgcmVxdWVzdGVkIGNvbnRlbnQgY2Fubm90IGJlIGxvYWRlZC48YnIgLz5QbGVhc2UgdHJ5IGFnYWluIGxhdGVyLjwvcD4nICk7XHJcblxyXG5cdFx0XHRfcHJvY2Vzc19pbmxpbmUoKTtcclxuXHRcdH0sXHJcblxyXG5cdFx0X3N0YXJ0ID0gZnVuY3Rpb24oKSB7XHJcblx0XHRcdHZhciBvYmogPSBzZWxlY3RlZEFycmF5WyBzZWxlY3RlZEluZGV4IF0sXHJcblx0XHRcdFx0aHJlZiwgXHJcblx0XHRcdFx0dHlwZSwgXHJcblx0XHRcdFx0dGl0bGUsXHJcblx0XHRcdFx0c3RyLFxyXG5cdFx0XHRcdGVtYixcclxuXHRcdFx0XHRyZXQ7XHJcblxyXG5cdFx0XHRfYWJvcnQoKTtcclxuXHJcblx0XHRcdHNlbGVjdGVkT3B0cyA9ICQuZXh0ZW5kKHt9LCAkLmZuLmZhbmN5Ym94LmRlZmF1bHRzLCAodHlwZW9mICQob2JqKS5kYXRhKCdmYW5jeWJveCcpID09ICd1bmRlZmluZWQnID8gc2VsZWN0ZWRPcHRzIDogJChvYmopLmRhdGEoJ2ZhbmN5Ym94JykpKTtcclxuXHJcblx0XHRcdHJldCA9IHNlbGVjdGVkT3B0cy5vblN0YXJ0KHNlbGVjdGVkQXJyYXksIHNlbGVjdGVkSW5kZXgsIHNlbGVjdGVkT3B0cyk7XHJcblxyXG5cdFx0XHRpZiAocmV0ID09PSBmYWxzZSkge1xyXG5cdFx0XHRcdGJ1c3kgPSBmYWxzZTtcclxuXHRcdFx0XHRyZXR1cm47XHJcblx0XHRcdH0gZWxzZSBpZiAodHlwZW9mIHJldCA9PSAnb2JqZWN0Jykge1xyXG5cdFx0XHRcdHNlbGVjdGVkT3B0cyA9ICQuZXh0ZW5kKHNlbGVjdGVkT3B0cywgcmV0KTtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0dGl0bGUgPSBzZWxlY3RlZE9wdHMudGl0bGUgfHwgKG9iai5ub2RlTmFtZSA/ICQob2JqKS5hdHRyKCd0aXRsZScpIDogb2JqLnRpdGxlKSB8fCAnJztcclxuXHJcblx0XHRcdGlmIChvYmoubm9kZU5hbWUgJiYgIXNlbGVjdGVkT3B0cy5vcmlnKSB7XHJcblx0XHRcdFx0c2VsZWN0ZWRPcHRzLm9yaWcgPSAkKG9iaikuY2hpbGRyZW4oXCJpbWc6Zmlyc3RcIikubGVuZ3RoID8gJChvYmopLmNoaWxkcmVuKFwiaW1nOmZpcnN0XCIpIDogJChvYmopO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRpZiAodGl0bGUgPT09ICcnICYmIHNlbGVjdGVkT3B0cy5vcmlnICYmIHNlbGVjdGVkT3B0cy50aXRsZUZyb21BbHQpIHtcclxuXHRcdFx0XHR0aXRsZSA9IHNlbGVjdGVkT3B0cy5vcmlnLmF0dHIoJ2FsdCcpO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRocmVmID0gc2VsZWN0ZWRPcHRzLmhyZWYgfHwgKG9iai5ub2RlTmFtZSA/ICQob2JqKS5hdHRyKCdocmVmJykgOiBvYmouaHJlZikgfHwgbnVsbDtcclxuXHJcblx0XHRcdGlmICgoL14oPzpqYXZhc2NyaXB0KS9pKS50ZXN0KGhyZWYpIHx8IGhyZWYgPT0gJyMnKSB7XHJcblx0XHRcdFx0aHJlZiA9IG51bGw7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdGlmIChzZWxlY3RlZE9wdHMudHlwZSkge1xyXG5cdFx0XHRcdHR5cGUgPSBzZWxlY3RlZE9wdHMudHlwZTtcclxuXHJcblx0XHRcdFx0aWYgKCFocmVmKSB7XHJcblx0XHRcdFx0XHRocmVmID0gc2VsZWN0ZWRPcHRzLmNvbnRlbnQ7XHJcblx0XHRcdFx0fVxyXG5cclxuXHRcdFx0fSBlbHNlIGlmIChzZWxlY3RlZE9wdHMuY29udGVudCkge1xyXG5cdFx0XHRcdHR5cGUgPSAnaHRtbCc7XHJcblxyXG5cdFx0XHR9IGVsc2UgaWYgKGhyZWYpIHtcclxuXHRcdFx0XHRpZiAoaHJlZi5tYXRjaChpbWdSZWdFeHApKSB7XHJcblx0XHRcdFx0XHR0eXBlID0gJ2ltYWdlJztcclxuXHJcblx0XHRcdFx0fSBlbHNlIGlmIChocmVmLm1hdGNoKHN3ZlJlZ0V4cCkpIHtcclxuXHRcdFx0XHRcdHR5cGUgPSAnc3dmJztcclxuXHJcblx0XHRcdFx0fSBlbHNlIGlmICgkKG9iaikuaGFzQ2xhc3MoXCJpZnJhbWVcIikpIHtcclxuXHRcdFx0XHRcdHR5cGUgPSAnaWZyYW1lJztcclxuXHJcblx0XHRcdFx0fSBlbHNlIGlmIChocmVmLmluZGV4T2YoXCIjXCIpID09PSAwKSB7XHJcblx0XHRcdFx0XHR0eXBlID0gJ2lubGluZSc7XHJcblxyXG5cdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHR0eXBlID0gJ2FqYXgnO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0aWYgKCF0eXBlKSB7XHJcblx0XHRcdFx0X2Vycm9yKCk7XHJcblx0XHRcdFx0cmV0dXJuO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRpZiAodHlwZSA9PSAnaW5saW5lJykge1xyXG5cdFx0XHRcdG9ialx0PSBocmVmLnN1YnN0cihocmVmLmluZGV4T2YoXCIjXCIpKTtcclxuXHRcdFx0XHR0eXBlID0gJChvYmopLmxlbmd0aCA+IDAgPyAnaW5saW5lJyA6ICdhamF4JztcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0c2VsZWN0ZWRPcHRzLnR5cGUgPSB0eXBlO1xyXG5cdFx0XHRzZWxlY3RlZE9wdHMuaHJlZiA9IGhyZWY7XHJcblx0XHRcdHNlbGVjdGVkT3B0cy50aXRsZSA9IHRpdGxlO1xyXG5cclxuXHRcdFx0aWYgKHNlbGVjdGVkT3B0cy5hdXRvRGltZW5zaW9ucykge1xyXG5cdFx0XHRcdGlmIChzZWxlY3RlZE9wdHMudHlwZSA9PSAnaHRtbCcgfHwgc2VsZWN0ZWRPcHRzLnR5cGUgPT0gJ2lubGluZScgfHwgc2VsZWN0ZWRPcHRzLnR5cGUgPT0gJ2FqYXgnKSB7XHJcblx0XHRcdFx0XHRzZWxlY3RlZE9wdHMud2lkdGggPSAnYXV0byc7XHJcblx0XHRcdFx0XHRzZWxlY3RlZE9wdHMuaGVpZ2h0ID0gJ2F1dG8nO1xyXG5cdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHRzZWxlY3RlZE9wdHMuYXV0b0RpbWVuc2lvbnMgPSBmYWxzZTtcdFxyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0aWYgKHNlbGVjdGVkT3B0cy5tb2RhbCkge1xyXG5cdFx0XHRcdHNlbGVjdGVkT3B0cy5vdmVybGF5U2hvdyA9IHRydWU7XHJcblx0XHRcdFx0c2VsZWN0ZWRPcHRzLmhpZGVPbk92ZXJsYXlDbGljayA9IGZhbHNlO1xyXG5cdFx0XHRcdHNlbGVjdGVkT3B0cy5oaWRlT25Db250ZW50Q2xpY2sgPSBmYWxzZTtcclxuXHRcdFx0XHRzZWxlY3RlZE9wdHMuZW5hYmxlRXNjYXBlQnV0dG9uID0gZmFsc2U7XHJcblx0XHRcdFx0c2VsZWN0ZWRPcHRzLnNob3dDbG9zZUJ1dHRvbiA9IGZhbHNlO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRzZWxlY3RlZE9wdHMucGFkZGluZyA9IHBhcnNlSW50KHNlbGVjdGVkT3B0cy5wYWRkaW5nLCAxMCk7XHJcblx0XHRcdHNlbGVjdGVkT3B0cy5tYXJnaW4gPSBwYXJzZUludChzZWxlY3RlZE9wdHMubWFyZ2luLCAxMCk7XHJcblxyXG5cdFx0XHR0bXAuY3NzKCdwYWRkaW5nJywgKHNlbGVjdGVkT3B0cy5wYWRkaW5nICsgc2VsZWN0ZWRPcHRzLm1hcmdpbikpO1xyXG5cclxuXHRcdFx0JCgnLmZhbmN5Ym94LWlubGluZS10bXAnKS51bmJpbmQoJ2ZhbmN5Ym94LWNhbmNlbCcpLmJpbmQoJ2ZhbmN5Ym94LWNoYW5nZScsIGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRcdCQodGhpcykucmVwbGFjZVdpdGgoY29udGVudC5jaGlsZHJlbigpKTtcdFx0XHRcdFxyXG5cdFx0XHR9KTtcclxuXHJcblx0XHRcdHN3aXRjaCAodHlwZSkge1xyXG5cdFx0XHRcdGNhc2UgJ2h0bWwnIDpcclxuXHRcdFx0XHRcdHRtcC5odG1sKCBzZWxlY3RlZE9wdHMuY29udGVudCApO1xyXG5cdFx0XHRcdFx0X3Byb2Nlc3NfaW5saW5lKCk7XHJcblx0XHRcdFx0YnJlYWs7XHJcblxyXG5cdFx0XHRcdGNhc2UgJ2lubGluZScgOlxyXG5cdFx0XHRcdFx0aWYgKCAkKG9iaikucGFyZW50KCkuaXMoJyNmYW5jeWJveC1jb250ZW50JykgPT09IHRydWUpIHtcclxuXHRcdFx0XHRcdFx0YnVzeSA9IGZhbHNlO1xyXG5cdFx0XHRcdFx0XHRyZXR1cm47XHJcblx0XHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdFx0JCgnPGRpdiBjbGFzcz1cImZhbmN5Ym94LWlubGluZS10bXBcIiAvPicpXHJcblx0XHRcdFx0XHRcdC5oaWRlKClcclxuXHRcdFx0XHRcdFx0Lmluc2VydEJlZm9yZSggJChvYmopIClcclxuXHRcdFx0XHRcdFx0LmJpbmQoJ2ZhbmN5Ym94LWNsZWFudXAnLCBmdW5jdGlvbigpIHtcclxuXHRcdFx0XHRcdFx0XHQkKHRoaXMpLnJlcGxhY2VXaXRoKGNvbnRlbnQuY2hpbGRyZW4oKSk7XHJcblx0XHRcdFx0XHRcdH0pLmJpbmQoJ2ZhbmN5Ym94LWNhbmNlbCcsIGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRcdFx0XHRcdCQodGhpcykucmVwbGFjZVdpdGgodG1wLmNoaWxkcmVuKCkpO1xyXG5cdFx0XHRcdFx0XHR9KTtcclxuXHJcblx0XHRcdFx0XHQkKG9iaikuYXBwZW5kVG8odG1wKTtcclxuXHJcblx0XHRcdFx0XHRfcHJvY2Vzc19pbmxpbmUoKTtcclxuXHRcdFx0XHRicmVhaztcclxuXHJcblx0XHRcdFx0Y2FzZSAnaW1hZ2UnOlxyXG5cdFx0XHRcdFx0YnVzeSA9IGZhbHNlO1xyXG5cclxuXHRcdFx0XHRcdCQuZmFuY3lib3guc2hvd0FjdGl2aXR5KCk7XHJcblxyXG5cdFx0XHRcdFx0aW1nUHJlbG9hZGVyID0gbmV3IEltYWdlKCk7XHJcblxyXG5cdFx0XHRcdFx0aW1nUHJlbG9hZGVyLm9uZXJyb3IgPSBmdW5jdGlvbigpIHtcclxuXHRcdFx0XHRcdFx0X2Vycm9yKCk7XHJcblx0XHRcdFx0XHR9O1xyXG5cclxuXHRcdFx0XHRcdGltZ1ByZWxvYWRlci5vbmxvYWQgPSBmdW5jdGlvbigpIHtcclxuXHRcdFx0XHRcdFx0YnVzeSA9IHRydWU7XHJcblxyXG5cdFx0XHRcdFx0XHRpbWdQcmVsb2FkZXIub25lcnJvciA9IGltZ1ByZWxvYWRlci5vbmxvYWQgPSBudWxsO1xyXG5cclxuXHRcdFx0XHRcdFx0X3Byb2Nlc3NfaW1hZ2UoKTtcclxuXHRcdFx0XHRcdH07XHJcblxyXG5cdFx0XHRcdFx0aW1nUHJlbG9hZGVyLnNyYyA9IGhyZWY7XHJcblx0XHRcdFx0YnJlYWs7XHJcblxyXG5cdFx0XHRcdGNhc2UgJ3N3Zic6XHJcblx0XHRcdFx0XHRzZWxlY3RlZE9wdHMuc2Nyb2xsaW5nID0gJ25vJztcclxuXHJcblx0XHRcdFx0XHRzdHIgPSAnPG9iamVjdCBjbGFzc2lkPVwiY2xzaWQ6RDI3Q0RCNkUtQUU2RC0xMWNmLTk2QjgtNDQ0NTUzNTQwMDAwXCIgd2lkdGg9XCInICsgc2VsZWN0ZWRPcHRzLndpZHRoICsgJ1wiIGhlaWdodD1cIicgKyBzZWxlY3RlZE9wdHMuaGVpZ2h0ICsgJ1wiPjxwYXJhbSBuYW1lPVwibW92aWVcIiB2YWx1ZT1cIicgKyBocmVmICsgJ1wiPjwvcGFyYW0+JztcclxuXHRcdFx0XHRcdGVtYiA9ICcnO1xyXG5cclxuXHRcdFx0XHRcdCQuZWFjaChzZWxlY3RlZE9wdHMuc3dmLCBmdW5jdGlvbihuYW1lLCB2YWwpIHtcclxuXHRcdFx0XHRcdFx0c3RyICs9ICc8cGFyYW0gbmFtZT1cIicgKyBuYW1lICsgJ1wiIHZhbHVlPVwiJyArIHZhbCArICdcIj48L3BhcmFtPic7XHJcblx0XHRcdFx0XHRcdGVtYiArPSAnICcgKyBuYW1lICsgJz1cIicgKyB2YWwgKyAnXCInO1xyXG5cdFx0XHRcdFx0fSk7XHJcblxyXG5cdFx0XHRcdFx0c3RyICs9ICc8ZW1iZWQgc3JjPVwiJyArIGhyZWYgKyAnXCIgdHlwZT1cImFwcGxpY2F0aW9uL3gtc2hvY2t3YXZlLWZsYXNoXCIgd2lkdGg9XCInICsgc2VsZWN0ZWRPcHRzLndpZHRoICsgJ1wiIGhlaWdodD1cIicgKyBzZWxlY3RlZE9wdHMuaGVpZ2h0ICsgJ1wiJyArIGVtYiArICc+PC9lbWJlZD48L29iamVjdD4nO1xyXG5cclxuXHRcdFx0XHRcdHRtcC5odG1sKHN0cik7XHJcblxyXG5cdFx0XHRcdFx0X3Byb2Nlc3NfaW5saW5lKCk7XHJcblx0XHRcdFx0YnJlYWs7XHJcblxyXG5cdFx0XHRcdGNhc2UgJ2FqYXgnOlxyXG5cdFx0XHRcdFx0YnVzeSA9IGZhbHNlO1xyXG5cclxuXHRcdFx0XHRcdCQuZmFuY3lib3guc2hvd0FjdGl2aXR5KCk7XHJcblxyXG5cdFx0XHRcdFx0c2VsZWN0ZWRPcHRzLmFqYXgud2luID0gc2VsZWN0ZWRPcHRzLmFqYXguc3VjY2VzcztcclxuXHJcblx0XHRcdFx0XHRhamF4TG9hZGVyID0gJC5hamF4KCQuZXh0ZW5kKHt9LCBzZWxlY3RlZE9wdHMuYWpheCwge1xyXG5cdFx0XHRcdFx0XHR1cmxcdDogaHJlZixcclxuXHRcdFx0XHRcdFx0ZGF0YSA6IHNlbGVjdGVkT3B0cy5hamF4LmRhdGEgfHwge30sXHJcblx0XHRcdFx0XHRcdGVycm9yIDogZnVuY3Rpb24oWE1MSHR0cFJlcXVlc3QsIHRleHRTdGF0dXMsIGVycm9yVGhyb3duKSB7XHJcblx0XHRcdFx0XHRcdFx0aWYgKCBYTUxIdHRwUmVxdWVzdC5zdGF0dXMgPiAwICkge1xyXG5cdFx0XHRcdFx0XHRcdFx0X2Vycm9yKCk7XHJcblx0XHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHR9LFxyXG5cdFx0XHRcdFx0XHRzdWNjZXNzIDogZnVuY3Rpb24oZGF0YSwgdGV4dFN0YXR1cywgWE1MSHR0cFJlcXVlc3QpIHtcclxuXHRcdFx0XHRcdFx0XHR2YXIgbyA9IHR5cGVvZiBYTUxIdHRwUmVxdWVzdCA9PSAnb2JqZWN0JyA/IFhNTEh0dHBSZXF1ZXN0IDogYWpheExvYWRlcjtcclxuXHRcdFx0XHRcdFx0XHRpZiAoby5zdGF0dXMgPT0gMjAwKSB7XHJcblx0XHRcdFx0XHRcdFx0XHRpZiAoIHR5cGVvZiBzZWxlY3RlZE9wdHMuYWpheC53aW4gPT0gJ2Z1bmN0aW9uJyApIHtcclxuXHRcdFx0XHRcdFx0XHRcdFx0cmV0ID0gc2VsZWN0ZWRPcHRzLmFqYXgud2luKGhyZWYsIGRhdGEsIHRleHRTdGF0dXMsIFhNTEh0dHBSZXF1ZXN0KTtcclxuXHJcblx0XHRcdFx0XHRcdFx0XHRcdGlmIChyZXQgPT09IGZhbHNlKSB7XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0bG9hZGluZy5oaWRlKCk7XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0cmV0dXJuO1xyXG5cdFx0XHRcdFx0XHRcdFx0XHR9IGVsc2UgaWYgKHR5cGVvZiByZXQgPT0gJ3N0cmluZycgfHwgdHlwZW9mIHJldCA9PSAnb2JqZWN0Jykge1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdGRhdGEgPSByZXQ7XHJcblx0XHRcdFx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0XHRcdFx0XHR0bXAuaHRtbCggZGF0YSApO1xyXG5cdFx0XHRcdFx0XHRcdFx0X3Byb2Nlc3NfaW5saW5lKCk7XHJcblx0XHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHR9KSk7XHJcblxyXG5cdFx0XHRcdGJyZWFrO1xyXG5cclxuXHRcdFx0XHRjYXNlICdpZnJhbWUnOlxyXG5cdFx0XHRcdFx0X3Nob3coKTtcclxuXHRcdFx0XHRicmVhaztcclxuXHRcdFx0fVxyXG5cdFx0fSxcclxuXHJcblx0XHRfcHJvY2Vzc19pbmxpbmUgPSBmdW5jdGlvbigpIHtcclxuXHRcdFx0dmFyXHJcblx0XHRcdFx0dyA9IHNlbGVjdGVkT3B0cy53aWR0aCxcclxuXHRcdFx0XHRoID0gc2VsZWN0ZWRPcHRzLmhlaWdodDtcclxuXHJcblx0XHRcdGlmICh3LnRvU3RyaW5nKCkuaW5kZXhPZignJScpID4gLTEpIHtcclxuXHRcdFx0XHR3ID0gcGFyc2VJbnQoICgkKHdpbmRvdykud2lkdGgoKSAtIChzZWxlY3RlZE9wdHMubWFyZ2luICogMikpICogcGFyc2VGbG9hdCh3KSAvIDEwMCwgMTApICsgJ3B4JztcclxuXHJcblx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0dyA9IHcgPT0gJ2F1dG8nID8gJ2F1dG8nIDogdyArICdweCc7XHRcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0aWYgKGgudG9TdHJpbmcoKS5pbmRleE9mKCclJykgPiAtMSkge1xyXG5cdFx0XHRcdGggPSBwYXJzZUludCggKCQod2luZG93KS5oZWlnaHQoKSAtIChzZWxlY3RlZE9wdHMubWFyZ2luICogMikpICogcGFyc2VGbG9hdChoKSAvIDEwMCwgMTApICsgJ3B4JztcclxuXHJcblx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0aCA9IGggPT0gJ2F1dG8nID8gJ2F1dG8nIDogaCArICdweCc7XHRcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0dG1wLndyYXBJbm5lcignPGRpdiBzdHlsZT1cIndpZHRoOicgKyB3ICsgJztoZWlnaHQ6JyArIGggKyAnO292ZXJmbG93OiAnICsgKHNlbGVjdGVkT3B0cy5zY3JvbGxpbmcgPT0gJ2F1dG8nID8gJ2F1dG8nIDogKHNlbGVjdGVkT3B0cy5zY3JvbGxpbmcgPT0gJ3llcycgPyAnc2Nyb2xsJyA6ICdoaWRkZW4nKSkgKyAnO3Bvc2l0aW9uOnJlbGF0aXZlO1wiPjwvZGl2PicpO1xyXG5cclxuXHRcdFx0c2VsZWN0ZWRPcHRzLndpZHRoID0gdG1wLndpZHRoKCk7XHJcblx0XHRcdHNlbGVjdGVkT3B0cy5oZWlnaHQgPSB0bXAuaGVpZ2h0KCk7XHJcblxyXG5cdFx0XHRfc2hvdygpO1xyXG5cdFx0fSxcclxuXHJcblx0XHRfcHJvY2Vzc19pbWFnZSA9IGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRzZWxlY3RlZE9wdHMud2lkdGggPSBpbWdQcmVsb2FkZXIud2lkdGg7XHJcblx0XHRcdHNlbGVjdGVkT3B0cy5oZWlnaHQgPSBpbWdQcmVsb2FkZXIuaGVpZ2h0O1xyXG5cclxuXHRcdFx0JChcIjxpbWcgLz5cIikuYXR0cih7XHJcblx0XHRcdFx0J2lkJyA6ICdmYW5jeWJveC1pbWcnLFxyXG5cdFx0XHRcdCdzcmMnIDogaW1nUHJlbG9hZGVyLnNyYyxcclxuXHRcdFx0XHQnYWx0JyA6IHNlbGVjdGVkT3B0cy50aXRsZVxyXG5cdFx0XHR9KS5hcHBlbmRUbyggdG1wICk7XHJcblxyXG5cdFx0XHRfc2hvdygpO1xyXG5cdFx0fSxcclxuXHJcblx0XHRfc2hvdyA9IGZ1bmN0aW9uKCkge1xyXG5cdFx0XHR2YXIgcG9zLCBlcXVhbDtcclxuXHJcblx0XHRcdGxvYWRpbmcuaGlkZSgpO1xyXG5cclxuXHRcdFx0aWYgKHdyYXAuaXMoXCI6dmlzaWJsZVwiKSAmJiBmYWxzZSA9PT0gY3VycmVudE9wdHMub25DbGVhbnVwKGN1cnJlbnRBcnJheSwgY3VycmVudEluZGV4LCBjdXJyZW50T3B0cykpIHtcclxuXHRcdFx0XHQkLmV2ZW50LnRyaWdnZXIoJ2ZhbmN5Ym94LWNhbmNlbCcpO1xyXG5cclxuXHRcdFx0XHRidXN5ID0gZmFsc2U7XHJcblx0XHRcdFx0cmV0dXJuO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRidXN5ID0gdHJ1ZTtcclxuXHJcblx0XHRcdCQoY29udGVudC5hZGQoIG92ZXJsYXkgKSkudW5iaW5kKCk7XHJcblxyXG5cdFx0XHQkKHdpbmRvdykudW5iaW5kKFwicmVzaXplLmZiIHNjcm9sbC5mYlwiKTtcclxuXHRcdFx0JChkb2N1bWVudCkudW5iaW5kKCdrZXlkb3duLmZiJyk7XHJcblxyXG5cdFx0XHRpZiAod3JhcC5pcyhcIjp2aXNpYmxlXCIpICYmIGN1cnJlbnRPcHRzLnRpdGxlUG9zaXRpb24gIT09ICdvdXRzaWRlJykge1xyXG5cdFx0XHRcdHdyYXAuY3NzKCdoZWlnaHQnLCB3cmFwLmhlaWdodCgpKTtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0Y3VycmVudEFycmF5ID0gc2VsZWN0ZWRBcnJheTtcclxuXHRcdFx0Y3VycmVudEluZGV4ID0gc2VsZWN0ZWRJbmRleDtcclxuXHRcdFx0Y3VycmVudE9wdHMgPSBzZWxlY3RlZE9wdHM7XHJcblxyXG5cdFx0XHRpZiAoY3VycmVudE9wdHMub3ZlcmxheVNob3cpIHtcclxuXHRcdFx0XHRvdmVybGF5LmNzcyh7XHJcblx0XHRcdFx0XHQnYmFja2dyb3VuZC1jb2xvcicgOiBjdXJyZW50T3B0cy5vdmVybGF5Q29sb3IsXHJcblx0XHRcdFx0XHQnb3BhY2l0eScgOiBjdXJyZW50T3B0cy5vdmVybGF5T3BhY2l0eSxcclxuXHRcdFx0XHRcdCdjdXJzb3InIDogY3VycmVudE9wdHMuaGlkZU9uT3ZlcmxheUNsaWNrID8gJ3BvaW50ZXInIDogJ2F1dG8nLFxyXG5cdFx0XHRcdFx0J2hlaWdodCcgOiAkKGRvY3VtZW50KS5oZWlnaHQoKVxyXG5cdFx0XHRcdH0pO1xyXG5cclxuXHRcdFx0XHRpZiAoIW92ZXJsYXkuaXMoJzp2aXNpYmxlJykpIHtcclxuXHRcdFx0XHRcdGlmIChpc0lFNikge1xyXG5cdFx0XHRcdFx0XHQkKCdzZWxlY3Q6bm90KCNmYW5jeWJveC10bXAgc2VsZWN0KScpLmZpbHRlcihmdW5jdGlvbigpIHtcclxuXHRcdFx0XHRcdFx0XHRyZXR1cm4gdGhpcy5zdHlsZS52aXNpYmlsaXR5ICE9PSAnaGlkZGVuJztcclxuXHRcdFx0XHRcdFx0fSkuY3NzKHsndmlzaWJpbGl0eScgOiAnaGlkZGVuJ30pLm9uZSgnZmFuY3lib3gtY2xlYW51cCcsIGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRcdFx0XHRcdHRoaXMuc3R5bGUudmlzaWJpbGl0eSA9ICdpbmhlcml0JztcclxuXHRcdFx0XHRcdFx0fSk7XHJcblx0XHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdFx0b3ZlcmxheS5zaG93KCk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdG92ZXJsYXkuaGlkZSgpO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRmaW5hbF9wb3MgPSBfZ2V0X3pvb21fdG8oKTtcclxuXHJcblx0XHRcdF9wcm9jZXNzX3RpdGxlKCk7XHJcblxyXG5cdFx0XHRpZiAod3JhcC5pcyhcIjp2aXNpYmxlXCIpKSB7XHJcblx0XHRcdFx0JCggY2xvc2UuYWRkKCBuYXZfbGVmdCApLmFkZCggbmF2X3JpZ2h0ICkgKS5oaWRlKCk7XHJcblxyXG5cdFx0XHRcdHBvcyA9IHdyYXAucG9zaXRpb24oKSxcclxuXHJcblx0XHRcdFx0c3RhcnRfcG9zID0ge1xyXG5cdFx0XHRcdFx0dG9wXHQgOiBwb3MudG9wLFxyXG5cdFx0XHRcdFx0bGVmdCA6IHBvcy5sZWZ0LFxyXG5cdFx0XHRcdFx0d2lkdGggOiB3cmFwLndpZHRoKCksXHJcblx0XHRcdFx0XHRoZWlnaHQgOiB3cmFwLmhlaWdodCgpXHJcblx0XHRcdFx0fTtcclxuXHJcblx0XHRcdFx0ZXF1YWwgPSAoc3RhcnRfcG9zLndpZHRoID09IGZpbmFsX3Bvcy53aWR0aCAmJiBzdGFydF9wb3MuaGVpZ2h0ID09IGZpbmFsX3Bvcy5oZWlnaHQpO1xyXG5cclxuXHRcdFx0XHRjb250ZW50LmZhZGVUbyhjdXJyZW50T3B0cy5jaGFuZ2VGYWRlLCAwLjMsIGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRcdFx0dmFyIGZpbmlzaF9yZXNpemluZyA9IGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRcdFx0XHRjb250ZW50Lmh0bWwoIHRtcC5jb250ZW50cygpICkuZmFkZVRvKGN1cnJlbnRPcHRzLmNoYW5nZUZhZGUsIDEsIF9maW5pc2gpO1xyXG5cdFx0XHRcdFx0fTtcclxuXHJcblx0XHRcdFx0XHQkLmV2ZW50LnRyaWdnZXIoJ2ZhbmN5Ym94LWNoYW5nZScpO1xyXG5cclxuXHRcdFx0XHRcdGNvbnRlbnRcclxuXHRcdFx0XHRcdFx0LmVtcHR5KClcclxuXHRcdFx0XHRcdFx0LnJlbW92ZUF0dHIoJ2ZpbHRlcicpXHJcblx0XHRcdFx0XHRcdC5jc3Moe1xyXG5cdFx0XHRcdFx0XHRcdCdib3JkZXItd2lkdGgnIDogY3VycmVudE9wdHMucGFkZGluZyxcclxuXHRcdFx0XHRcdFx0XHQnd2lkdGgnXHQ6IGZpbmFsX3Bvcy53aWR0aCAtIGN1cnJlbnRPcHRzLnBhZGRpbmcgKiAyLFxyXG5cdFx0XHRcdFx0XHRcdCdoZWlnaHQnIDogc2VsZWN0ZWRPcHRzLmF1dG9EaW1lbnNpb25zID8gJ2F1dG8nIDogZmluYWxfcG9zLmhlaWdodCAtIHRpdGxlSGVpZ2h0IC0gY3VycmVudE9wdHMucGFkZGluZyAqIDJcclxuXHRcdFx0XHRcdFx0fSk7XHJcblxyXG5cdFx0XHRcdFx0aWYgKGVxdWFsKSB7XHJcblx0XHRcdFx0XHRcdGZpbmlzaF9yZXNpemluZygpO1xyXG5cclxuXHRcdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHRcdGZ4LnByb3AgPSAwO1xyXG5cclxuXHRcdFx0XHRcdFx0JChmeCkuYW5pbWF0ZSh7cHJvcDogMX0sIHtcclxuXHRcdFx0XHRcdFx0XHQgZHVyYXRpb24gOiBjdXJyZW50T3B0cy5jaGFuZ2VTcGVlZCxcclxuXHRcdFx0XHRcdFx0XHQgZWFzaW5nIDogY3VycmVudE9wdHMuZWFzaW5nQ2hhbmdlLFxyXG5cdFx0XHRcdFx0XHRcdCBzdGVwIDogX2RyYXcsXHJcblx0XHRcdFx0XHRcdFx0IGNvbXBsZXRlIDogZmluaXNoX3Jlc2l6aW5nXHJcblx0XHRcdFx0XHRcdH0pO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH0pO1xyXG5cclxuXHRcdFx0XHRyZXR1cm47XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdHdyYXAucmVtb3ZlQXR0cihcInN0eWxlXCIpO1xyXG5cclxuXHRcdFx0Y29udGVudC5jc3MoJ2JvcmRlci13aWR0aCcsIGN1cnJlbnRPcHRzLnBhZGRpbmcpO1xyXG5cclxuXHRcdFx0aWYgKGN1cnJlbnRPcHRzLnRyYW5zaXRpb25JbiA9PSAnZWxhc3RpYycpIHtcclxuXHRcdFx0XHRzdGFydF9wb3MgPSBfZ2V0X3pvb21fZnJvbSgpO1xyXG5cclxuXHRcdFx0XHRjb250ZW50Lmh0bWwoIHRtcC5jb250ZW50cygpICk7XHJcblxyXG5cdFx0XHRcdHdyYXAuc2hvdygpO1xyXG5cclxuXHRcdFx0XHRpZiAoY3VycmVudE9wdHMub3BhY2l0eSkge1xyXG5cdFx0XHRcdFx0ZmluYWxfcG9zLm9wYWNpdHkgPSAwO1xyXG5cdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0ZngucHJvcCA9IDA7XHJcblxyXG5cdFx0XHRcdCQoZngpLmFuaW1hdGUoe3Byb3A6IDF9LCB7XHJcblx0XHRcdFx0XHQgZHVyYXRpb24gOiBjdXJyZW50T3B0cy5zcGVlZEluLFxyXG5cdFx0XHRcdFx0IGVhc2luZyA6IGN1cnJlbnRPcHRzLmVhc2luZ0luLFxyXG5cdFx0XHRcdFx0IHN0ZXAgOiBfZHJhdyxcclxuXHRcdFx0XHRcdCBjb21wbGV0ZSA6IF9maW5pc2hcclxuXHRcdFx0XHR9KTtcclxuXHJcblx0XHRcdFx0cmV0dXJuO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRpZiAoY3VycmVudE9wdHMudGl0bGVQb3NpdGlvbiA9PSAnaW5zaWRlJyAmJiB0aXRsZUhlaWdodCA+IDApIHtcdFxyXG5cdFx0XHRcdHRpdGxlLnNob3coKTtcdFxyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRjb250ZW50XHJcblx0XHRcdFx0LmNzcyh7XHJcblx0XHRcdFx0XHQnd2lkdGgnIDogZmluYWxfcG9zLndpZHRoIC0gY3VycmVudE9wdHMucGFkZGluZyAqIDIsXHJcblx0XHRcdFx0XHQnaGVpZ2h0JyA6IHNlbGVjdGVkT3B0cy5hdXRvRGltZW5zaW9ucyA/ICdhdXRvJyA6IGZpbmFsX3Bvcy5oZWlnaHQgLSB0aXRsZUhlaWdodCAtIGN1cnJlbnRPcHRzLnBhZGRpbmcgKiAyXHJcblx0XHRcdFx0fSlcclxuXHRcdFx0XHQuaHRtbCggdG1wLmNvbnRlbnRzKCkgKTtcclxuXHJcblx0XHRcdHdyYXBcclxuXHRcdFx0XHQuY3NzKGZpbmFsX3BvcylcclxuXHRcdFx0XHQuZmFkZUluKCBjdXJyZW50T3B0cy50cmFuc2l0aW9uSW4gPT0gJ25vbmUnID8gMCA6IGN1cnJlbnRPcHRzLnNwZWVkSW4sIF9maW5pc2ggKTtcclxuXHRcdH0sXHJcblxyXG5cdFx0X2Zvcm1hdF90aXRsZSA9IGZ1bmN0aW9uKHRpdGxlKSB7XHJcblx0XHRcdGlmICh0aXRsZSAmJiB0aXRsZS5sZW5ndGgpIHtcclxuXHRcdFx0XHRpZiAoY3VycmVudE9wdHMudGl0bGVQb3NpdGlvbiA9PSAnZmxvYXQnKSB7XHJcblx0XHRcdFx0XHRyZXR1cm4gJzx0YWJsZSBpZD1cImZhbmN5Ym94LXRpdGxlLWZsb2F0LXdyYXBcIiBjZWxscGFkZGluZz1cIjBcIiBjZWxsc3BhY2luZz1cIjBcIj48dHI+PHRkIGlkPVwiZmFuY3lib3gtdGl0bGUtZmxvYXQtbGVmdFwiPjwvdGQ+PHRkIGlkPVwiZmFuY3lib3gtdGl0bGUtZmxvYXQtbWFpblwiPicgKyB0aXRsZSArICc8L3RkPjx0ZCBpZD1cImZhbmN5Ym94LXRpdGxlLWZsb2F0LXJpZ2h0XCI+PC90ZD48L3RyPjwvdGFibGU+JztcclxuXHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdHJldHVybiAnPGRpdiBpZD1cImZhbmN5Ym94LXRpdGxlLScgKyBjdXJyZW50T3B0cy50aXRsZVBvc2l0aW9uICsgJ1wiPicgKyB0aXRsZSArICc8L2Rpdj4nO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRyZXR1cm4gZmFsc2U7XHJcblx0XHR9LFxyXG5cclxuXHRcdF9wcm9jZXNzX3RpdGxlID0gZnVuY3Rpb24oKSB7XHJcblx0XHRcdHRpdGxlU3RyID0gY3VycmVudE9wdHMudGl0bGUgfHwgJyc7XHJcblx0XHRcdHRpdGxlSGVpZ2h0ID0gMDtcclxuXHJcblx0XHRcdHRpdGxlXHJcblx0XHRcdFx0LmVtcHR5KClcclxuXHRcdFx0XHQucmVtb3ZlQXR0cignc3R5bGUnKVxyXG5cdFx0XHRcdC5yZW1vdmVDbGFzcygpO1xyXG5cclxuXHRcdFx0aWYgKGN1cnJlbnRPcHRzLnRpdGxlU2hvdyA9PT0gZmFsc2UpIHtcclxuXHRcdFx0XHR0aXRsZS5oaWRlKCk7XHJcblx0XHRcdFx0cmV0dXJuO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHR0aXRsZVN0ciA9ICQuaXNGdW5jdGlvbihjdXJyZW50T3B0cy50aXRsZUZvcm1hdCkgPyBjdXJyZW50T3B0cy50aXRsZUZvcm1hdCh0aXRsZVN0ciwgY3VycmVudEFycmF5LCBjdXJyZW50SW5kZXgsIGN1cnJlbnRPcHRzKSA6IF9mb3JtYXRfdGl0bGUodGl0bGVTdHIpO1xyXG5cclxuXHRcdFx0aWYgKCF0aXRsZVN0ciB8fCB0aXRsZVN0ciA9PT0gJycpIHtcclxuXHRcdFx0XHR0aXRsZS5oaWRlKCk7XHJcblx0XHRcdFx0cmV0dXJuO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHR0aXRsZVxyXG5cdFx0XHRcdC5hZGRDbGFzcygnZmFuY3lib3gtdGl0bGUtJyArIGN1cnJlbnRPcHRzLnRpdGxlUG9zaXRpb24pXHJcblx0XHRcdFx0Lmh0bWwoIHRpdGxlU3RyIClcclxuXHRcdFx0XHQuYXBwZW5kVG8oICdib2R5JyApXHJcblx0XHRcdFx0LnNob3coKTtcclxuXHJcblx0XHRcdHN3aXRjaCAoY3VycmVudE9wdHMudGl0bGVQb3NpdGlvbikge1xyXG5cdFx0XHRcdGNhc2UgJ2luc2lkZSc6XHJcblx0XHRcdFx0XHR0aXRsZVxyXG5cdFx0XHRcdFx0XHQuY3NzKHtcclxuXHRcdFx0XHRcdFx0XHQnd2lkdGgnIDogZmluYWxfcG9zLndpZHRoIC0gKGN1cnJlbnRPcHRzLnBhZGRpbmcgKiAyKSxcclxuXHRcdFx0XHRcdFx0XHQnbWFyZ2luTGVmdCcgOiBjdXJyZW50T3B0cy5wYWRkaW5nLFxyXG5cdFx0XHRcdFx0XHRcdCdtYXJnaW5SaWdodCcgOiBjdXJyZW50T3B0cy5wYWRkaW5nXHJcblx0XHRcdFx0XHRcdH0pO1xyXG5cclxuXHRcdFx0XHRcdHRpdGxlSGVpZ2h0ID0gdGl0bGUub3V0ZXJIZWlnaHQodHJ1ZSk7XHJcblxyXG5cdFx0XHRcdFx0dGl0bGUuYXBwZW5kVG8oIG91dGVyICk7XHJcblxyXG5cdFx0XHRcdFx0ZmluYWxfcG9zLmhlaWdodCArPSB0aXRsZUhlaWdodDtcclxuXHRcdFx0XHRicmVhaztcclxuXHJcblx0XHRcdFx0Y2FzZSAnb3Zlcic6XHJcblx0XHRcdFx0XHR0aXRsZVxyXG5cdFx0XHRcdFx0XHQuY3NzKHtcclxuXHRcdFx0XHRcdFx0XHQnbWFyZ2luTGVmdCcgOiBjdXJyZW50T3B0cy5wYWRkaW5nLFxyXG5cdFx0XHRcdFx0XHRcdCd3aWR0aCdcdDogZmluYWxfcG9zLndpZHRoIC0gKGN1cnJlbnRPcHRzLnBhZGRpbmcgKiAyKSxcclxuXHRcdFx0XHRcdFx0XHQnYm90dG9tJyA6IGN1cnJlbnRPcHRzLnBhZGRpbmdcclxuXHRcdFx0XHRcdFx0fSlcclxuXHRcdFx0XHRcdFx0LmFwcGVuZFRvKCBvdXRlciApO1xyXG5cdFx0XHRcdGJyZWFrO1xyXG5cclxuXHRcdFx0XHRjYXNlICdmbG9hdCc6XHJcblx0XHRcdFx0XHR0aXRsZVxyXG5cdFx0XHRcdFx0XHQuY3NzKCdsZWZ0JywgcGFyc2VJbnQoKHRpdGxlLndpZHRoKCkgLSBmaW5hbF9wb3Mud2lkdGggLSA0MCkvIDIsIDEwKSAqIC0xKVxyXG5cdFx0XHRcdFx0XHQuYXBwZW5kVG8oIHdyYXAgKTtcclxuXHRcdFx0XHRicmVhaztcclxuXHJcblx0XHRcdFx0ZGVmYXVsdDpcclxuXHRcdFx0XHRcdHRpdGxlXHJcblx0XHRcdFx0XHRcdC5jc3Moe1xyXG5cdFx0XHRcdFx0XHRcdCd3aWR0aCcgOiBmaW5hbF9wb3Mud2lkdGggLSAoY3VycmVudE9wdHMucGFkZGluZyAqIDIpLFxyXG5cdFx0XHRcdFx0XHRcdCdwYWRkaW5nTGVmdCcgOiBjdXJyZW50T3B0cy5wYWRkaW5nLFxyXG5cdFx0XHRcdFx0XHRcdCdwYWRkaW5nUmlnaHQnIDogY3VycmVudE9wdHMucGFkZGluZ1xyXG5cdFx0XHRcdFx0XHR9KVxyXG5cdFx0XHRcdFx0XHQuYXBwZW5kVG8oIHdyYXAgKTtcclxuXHRcdFx0XHRicmVhaztcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0dGl0bGUuaGlkZSgpO1xyXG5cdFx0fSxcclxuXHJcblx0XHRfc2V0X25hdmlnYXRpb24gPSBmdW5jdGlvbigpIHtcclxuXHRcdFx0aWYgKGN1cnJlbnRPcHRzLmVuYWJsZUVzY2FwZUJ1dHRvbiB8fCBjdXJyZW50T3B0cy5lbmFibGVLZXlib2FyZE5hdikge1xyXG5cdFx0XHRcdCQoZG9jdW1lbnQpLmJpbmQoJ2tleWRvd24uZmInLCBmdW5jdGlvbihlKSB7XHJcblx0XHRcdFx0XHRpZiAoZS5rZXlDb2RlID09IDI3ICYmIGN1cnJlbnRPcHRzLmVuYWJsZUVzY2FwZUJ1dHRvbikge1xyXG5cdFx0XHRcdFx0XHRlLnByZXZlbnREZWZhdWx0KCk7XHJcblx0XHRcdFx0XHRcdCQuZmFuY3lib3guY2xvc2UoKTtcclxuXHJcblx0XHRcdFx0XHR9IGVsc2UgaWYgKChlLmtleUNvZGUgPT0gMzcgfHwgZS5rZXlDb2RlID09IDM5KSAmJiBjdXJyZW50T3B0cy5lbmFibGVLZXlib2FyZE5hdiAmJiBlLnRhcmdldC50YWdOYW1lICE9PSAnSU5QVVQnICYmIGUudGFyZ2V0LnRhZ05hbWUgIT09ICdURVhUQVJFQScgJiYgZS50YXJnZXQudGFnTmFtZSAhPT0gJ1NFTEVDVCcpIHtcclxuXHRcdFx0XHRcdFx0ZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cdFx0XHRcdFx0XHQkLmZhbmN5Ym94WyBlLmtleUNvZGUgPT0gMzcgPyAncHJldicgOiAnbmV4dCddKCk7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdGlmICghY3VycmVudE9wdHMuc2hvd05hdkFycm93cykgeyBcclxuXHRcdFx0XHRuYXZfbGVmdC5oaWRlKCk7XHJcblx0XHRcdFx0bmF2X3JpZ2h0LmhpZGUoKTtcclxuXHRcdFx0XHRyZXR1cm47XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdGlmICgoY3VycmVudE9wdHMuY3ljbGljICYmIGN1cnJlbnRBcnJheS5sZW5ndGggPiAxKSB8fCBjdXJyZW50SW5kZXggIT09IDApIHtcclxuXHRcdFx0XHRuYXZfbGVmdC5zaG93KCk7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdGlmICgoY3VycmVudE9wdHMuY3ljbGljICYmIGN1cnJlbnRBcnJheS5sZW5ndGggPiAxKSB8fCBjdXJyZW50SW5kZXggIT0gKGN1cnJlbnRBcnJheS5sZW5ndGggLTEpKSB7XHJcblx0XHRcdFx0bmF2X3JpZ2h0LnNob3coKTtcclxuXHRcdFx0fVxyXG5cdFx0fSxcclxuXHJcblx0XHRfZmluaXNoID0gZnVuY3Rpb24gKCkge1xyXG5cdFx0XHRpZiAoISQuc3VwcG9ydC5vcGFjaXR5KSB7XHJcblx0XHRcdFx0Y29udGVudC5nZXQoMCkuc3R5bGUucmVtb3ZlQXR0cmlidXRlKCdmaWx0ZXInKTtcclxuXHRcdFx0XHR3cmFwLmdldCgwKS5zdHlsZS5yZW1vdmVBdHRyaWJ1dGUoJ2ZpbHRlcicpO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRpZiAoc2VsZWN0ZWRPcHRzLmF1dG9EaW1lbnNpb25zKSB7XHJcblx0XHRcdFx0Y29udGVudC5jc3MoJ2hlaWdodCcsICdhdXRvJyk7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdHdyYXAuY3NzKCdoZWlnaHQnLCAnYXV0bycpO1xyXG5cclxuXHRcdFx0aWYgKHRpdGxlU3RyICYmIHRpdGxlU3RyLmxlbmd0aCkge1xyXG5cdFx0XHRcdHRpdGxlLnNob3coKTtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0aWYgKGN1cnJlbnRPcHRzLnNob3dDbG9zZUJ1dHRvbikge1xyXG5cdFx0XHRcdGNsb3NlLnNob3coKTtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0X3NldF9uYXZpZ2F0aW9uKCk7XHJcblx0XHJcblx0XHRcdGlmIChjdXJyZW50T3B0cy5oaWRlT25Db250ZW50Q2xpY2spXHR7XHJcblx0XHRcdFx0Y29udGVudC5iaW5kKCdjbGljaycsICQuZmFuY3lib3guY2xvc2UpO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRpZiAoY3VycmVudE9wdHMuaGlkZU9uT3ZlcmxheUNsaWNrKVx0e1xyXG5cdFx0XHRcdG92ZXJsYXkuYmluZCgnY2xpY2snLCAkLmZhbmN5Ym94LmNsb3NlKTtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0JCh3aW5kb3cpLmJpbmQoXCJyZXNpemUuZmJcIiwgJC5mYW5jeWJveC5yZXNpemUpO1xyXG5cclxuXHRcdFx0aWYgKGN1cnJlbnRPcHRzLmNlbnRlck9uU2Nyb2xsKSB7XHJcblx0XHRcdFx0JCh3aW5kb3cpLmJpbmQoXCJzY3JvbGwuZmJcIiwgJC5mYW5jeWJveC5jZW50ZXIpO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRpZiAoY3VycmVudE9wdHMudHlwZSA9PSAnaWZyYW1lJykge1xyXG5cdFx0XHRcdCQoJzxpZnJhbWUgaWQ9XCJmYW5jeWJveC1mcmFtZVwiIG5hbWU9XCJmYW5jeWJveC1mcmFtZScgKyBuZXcgRGF0ZSgpLmdldFRpbWUoKSArICdcIiBmcmFtZWJvcmRlcj1cIjBcIiBoc3BhY2U9XCIwXCIgJyArICgkLmJyb3dzZXIubXNpZSA/ICdhbGxvd3RyYW5zcGFyZW5jeT1cInRydWVcIlwiJyA6ICcnKSArICcgc2Nyb2xsaW5nPVwiJyArIHNlbGVjdGVkT3B0cy5zY3JvbGxpbmcgKyAnXCIgc3JjPVwiJyArIGN1cnJlbnRPcHRzLmhyZWYgKyAnXCI+PC9pZnJhbWU+JykuYXBwZW5kVG8oY29udGVudCk7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdHdyYXAuc2hvdygpO1xyXG5cclxuXHRcdFx0YnVzeSA9IGZhbHNlO1xyXG5cclxuXHRcdFx0JC5mYW5jeWJveC5jZW50ZXIoKTtcclxuXHJcblx0XHRcdGN1cnJlbnRPcHRzLm9uQ29tcGxldGUoY3VycmVudEFycmF5LCBjdXJyZW50SW5kZXgsIGN1cnJlbnRPcHRzKTtcclxuXHJcblx0XHRcdF9wcmVsb2FkX2ltYWdlcygpO1xyXG5cdFx0fSxcclxuXHJcblx0XHRfcHJlbG9hZF9pbWFnZXMgPSBmdW5jdGlvbigpIHtcclxuXHRcdFx0dmFyIGhyZWYsIFxyXG5cdFx0XHRcdG9iak5leHQ7XHJcblxyXG5cdFx0XHRpZiAoKGN1cnJlbnRBcnJheS5sZW5ndGggLTEpID4gY3VycmVudEluZGV4KSB7XHJcblx0XHRcdFx0aHJlZiA9IGN1cnJlbnRBcnJheVsgY3VycmVudEluZGV4ICsgMSBdLmhyZWY7XHJcblxyXG5cdFx0XHRcdGlmICh0eXBlb2YgaHJlZiAhPT0gJ3VuZGVmaW5lZCcgJiYgaHJlZi5tYXRjaChpbWdSZWdFeHApKSB7XHJcblx0XHRcdFx0XHRvYmpOZXh0ID0gbmV3IEltYWdlKCk7XHJcblx0XHRcdFx0XHRvYmpOZXh0LnNyYyA9IGhyZWY7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRpZiAoY3VycmVudEluZGV4ID4gMCkge1xyXG5cdFx0XHRcdGhyZWYgPSBjdXJyZW50QXJyYXlbIGN1cnJlbnRJbmRleCAtIDEgXS5ocmVmO1xyXG5cclxuXHRcdFx0XHRpZiAodHlwZW9mIGhyZWYgIT09ICd1bmRlZmluZWQnICYmIGhyZWYubWF0Y2goaW1nUmVnRXhwKSkge1xyXG5cdFx0XHRcdFx0b2JqTmV4dCA9IG5ldyBJbWFnZSgpO1xyXG5cdFx0XHRcdFx0b2JqTmV4dC5zcmMgPSBocmVmO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0fSxcclxuXHJcblx0XHRfZHJhdyA9IGZ1bmN0aW9uKHBvcykge1xyXG5cdFx0XHR2YXIgZGltID0ge1xyXG5cdFx0XHRcdHdpZHRoIDogcGFyc2VJbnQoc3RhcnRfcG9zLndpZHRoICsgKGZpbmFsX3Bvcy53aWR0aCAtIHN0YXJ0X3Bvcy53aWR0aCkgKiBwb3MsIDEwKSxcclxuXHRcdFx0XHRoZWlnaHQgOiBwYXJzZUludChzdGFydF9wb3MuaGVpZ2h0ICsgKGZpbmFsX3Bvcy5oZWlnaHQgLSBzdGFydF9wb3MuaGVpZ2h0KSAqIHBvcywgMTApLFxyXG5cclxuXHRcdFx0XHR0b3AgOiBwYXJzZUludChzdGFydF9wb3MudG9wICsgKGZpbmFsX3Bvcy50b3AgLSBzdGFydF9wb3MudG9wKSAqIHBvcywgMTApLFxyXG5cdFx0XHRcdGxlZnQgOiBwYXJzZUludChzdGFydF9wb3MubGVmdCArIChmaW5hbF9wb3MubGVmdCAtIHN0YXJ0X3Bvcy5sZWZ0KSAqIHBvcywgMTApXHJcblx0XHRcdH07XHJcblxyXG5cdFx0XHRpZiAodHlwZW9mIGZpbmFsX3Bvcy5vcGFjaXR5ICE9PSAndW5kZWZpbmVkJykge1xyXG5cdFx0XHRcdGRpbS5vcGFjaXR5ID0gcG9zIDwgMC41ID8gMC41IDogcG9zO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHR3cmFwLmNzcyhkaW0pO1xyXG5cclxuXHRcdFx0Y29udGVudC5jc3Moe1xyXG5cdFx0XHRcdCd3aWR0aCcgOiBkaW0ud2lkdGggLSBjdXJyZW50T3B0cy5wYWRkaW5nICogMixcclxuXHRcdFx0XHQnaGVpZ2h0JyA6IGRpbS5oZWlnaHQgLSAodGl0bGVIZWlnaHQgKiBwb3MpIC0gY3VycmVudE9wdHMucGFkZGluZyAqIDJcclxuXHRcdFx0fSk7XHJcblx0XHR9LFxyXG5cclxuXHRcdF9nZXRfdmlld3BvcnQgPSBmdW5jdGlvbigpIHtcclxuXHRcdFx0cmV0dXJuIFtcclxuXHRcdFx0XHQkKHdpbmRvdykud2lkdGgoKSAtIChjdXJyZW50T3B0cy5tYXJnaW4gKiAyKSxcclxuXHRcdFx0XHQkKHdpbmRvdykuaGVpZ2h0KCkgLSAoY3VycmVudE9wdHMubWFyZ2luICogMiksXHJcblx0XHRcdFx0JChkb2N1bWVudCkuc2Nyb2xsTGVmdCgpICsgY3VycmVudE9wdHMubWFyZ2luLFxyXG5cdFx0XHRcdCQoZG9jdW1lbnQpLnNjcm9sbFRvcCgpICsgY3VycmVudE9wdHMubWFyZ2luXHJcblx0XHRcdF07XHJcblx0XHR9LFxyXG5cclxuXHRcdF9nZXRfem9vbV90byA9IGZ1bmN0aW9uICgpIHtcclxuXHRcdFx0dmFyIHZpZXcgPSBfZ2V0X3ZpZXdwb3J0KCksXHJcblx0XHRcdFx0dG8gPSB7fSxcclxuXHRcdFx0XHRyZXNpemUgPSBjdXJyZW50T3B0cy5hdXRvU2NhbGUsXHJcblx0XHRcdFx0ZG91YmxlX3BhZGRpbmcgPSBjdXJyZW50T3B0cy5wYWRkaW5nICogMixcclxuXHRcdFx0XHRyYXRpbztcclxuXHJcblx0XHRcdGlmIChjdXJyZW50T3B0cy53aWR0aC50b1N0cmluZygpLmluZGV4T2YoJyUnKSA+IC0xKSB7XHJcblx0XHRcdFx0dG8ud2lkdGggPSBwYXJzZUludCgodmlld1swXSAqIHBhcnNlRmxvYXQoY3VycmVudE9wdHMud2lkdGgpKSAvIDEwMCwgMTApO1xyXG5cdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdHRvLndpZHRoID0gY3VycmVudE9wdHMud2lkdGggKyBkb3VibGVfcGFkZGluZztcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0aWYgKGN1cnJlbnRPcHRzLmhlaWdodC50b1N0cmluZygpLmluZGV4T2YoJyUnKSA+IC0xKSB7XHJcblx0XHRcdFx0dG8uaGVpZ2h0ID0gcGFyc2VJbnQoKHZpZXdbMV0gKiBwYXJzZUZsb2F0KGN1cnJlbnRPcHRzLmhlaWdodCkpIC8gMTAwLCAxMCk7XHJcblx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0dG8uaGVpZ2h0ID0gY3VycmVudE9wdHMuaGVpZ2h0ICsgZG91YmxlX3BhZGRpbmc7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdGlmIChyZXNpemUgJiYgKHRvLndpZHRoID4gdmlld1swXSB8fCB0by5oZWlnaHQgPiB2aWV3WzFdKSkge1xyXG5cdFx0XHRcdGlmIChzZWxlY3RlZE9wdHMudHlwZSA9PSAnaW1hZ2UnIHx8IHNlbGVjdGVkT3B0cy50eXBlID09ICdzd2YnKSB7XHJcblx0XHRcdFx0XHRyYXRpbyA9IChjdXJyZW50T3B0cy53aWR0aCApIC8gKGN1cnJlbnRPcHRzLmhlaWdodCApO1xyXG5cclxuXHRcdFx0XHRcdGlmICgodG8ud2lkdGggKSA+IHZpZXdbMF0pIHtcclxuXHRcdFx0XHRcdFx0dG8ud2lkdGggPSB2aWV3WzBdO1xyXG5cdFx0XHRcdFx0XHR0by5oZWlnaHQgPSBwYXJzZUludCgoKHRvLndpZHRoIC0gZG91YmxlX3BhZGRpbmcpIC8gcmF0aW8pICsgZG91YmxlX3BhZGRpbmcsIDEwKTtcclxuXHRcdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0XHRpZiAoKHRvLmhlaWdodCkgPiB2aWV3WzFdKSB7XHJcblx0XHRcdFx0XHRcdHRvLmhlaWdodCA9IHZpZXdbMV07XHJcblx0XHRcdFx0XHRcdHRvLndpZHRoID0gcGFyc2VJbnQoKCh0by5oZWlnaHQgLSBkb3VibGVfcGFkZGluZykgKiByYXRpbykgKyBkb3VibGVfcGFkZGluZywgMTApO1xyXG5cdFx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0dG8ud2lkdGggPSBNYXRoLm1pbih0by53aWR0aCwgdmlld1swXSk7XHJcblx0XHRcdFx0XHR0by5oZWlnaHQgPSBNYXRoLm1pbih0by5oZWlnaHQsIHZpZXdbMV0pO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0dG8udG9wID0gcGFyc2VJbnQoTWF0aC5tYXgodmlld1szXSAtIDIwLCB2aWV3WzNdICsgKCh2aWV3WzFdIC0gdG8uaGVpZ2h0IC0gNDApICogMC41KSksIDEwKTtcclxuXHRcdFx0dG8ubGVmdCA9IHBhcnNlSW50KE1hdGgubWF4KHZpZXdbMl0gLSAyMCwgdmlld1syXSArICgodmlld1swXSAtIHRvLndpZHRoIC0gNDApICogMC41KSksIDEwKTtcclxuXHJcblx0XHRcdHJldHVybiB0bztcclxuXHRcdH0sXHJcblxyXG5cdFx0X2dldF9vYmpfcG9zID0gZnVuY3Rpb24ob2JqKSB7XHJcblx0XHRcdHZhciBwb3MgPSBvYmoub2Zmc2V0KCk7XHJcblxyXG5cdFx0XHRwb3MudG9wICs9IHBhcnNlSW50KCBvYmouY3NzKCdwYWRkaW5nVG9wJyksIDEwICkgfHwgMDtcclxuXHRcdFx0cG9zLmxlZnQgKz0gcGFyc2VJbnQoIG9iai5jc3MoJ3BhZGRpbmdMZWZ0JyksIDEwICkgfHwgMDtcclxuXHJcblx0XHRcdHBvcy50b3AgKz0gcGFyc2VJbnQoIG9iai5jc3MoJ2JvcmRlci10b3Atd2lkdGgnKSwgMTAgKSB8fCAwO1xyXG5cdFx0XHRwb3MubGVmdCArPSBwYXJzZUludCggb2JqLmNzcygnYm9yZGVyLWxlZnQtd2lkdGgnKSwgMTAgKSB8fCAwO1xyXG5cclxuXHRcdFx0cG9zLndpZHRoID0gb2JqLndpZHRoKCk7XHJcblx0XHRcdHBvcy5oZWlnaHQgPSBvYmouaGVpZ2h0KCk7XHJcblxyXG5cdFx0XHRyZXR1cm4gcG9zO1xyXG5cdFx0fSxcclxuXHJcblx0XHRfZ2V0X3pvb21fZnJvbSA9IGZ1bmN0aW9uKCkge1xyXG5cdFx0XHR2YXIgb3JpZyA9IHNlbGVjdGVkT3B0cy5vcmlnID8gJChzZWxlY3RlZE9wdHMub3JpZykgOiBmYWxzZSxcclxuXHRcdFx0XHRmcm9tID0ge30sXHJcblx0XHRcdFx0cG9zLFxyXG5cdFx0XHRcdHZpZXc7XHJcblxyXG5cdFx0XHRpZiAob3JpZyAmJiBvcmlnLmxlbmd0aCkge1xyXG5cdFx0XHRcdHBvcyA9IF9nZXRfb2JqX3BvcyhvcmlnKTtcclxuXHJcblx0XHRcdFx0ZnJvbSA9IHtcclxuXHRcdFx0XHRcdHdpZHRoIDogcG9zLndpZHRoICsgKGN1cnJlbnRPcHRzLnBhZGRpbmcgKiAyKSxcclxuXHRcdFx0XHRcdGhlaWdodCA6IHBvcy5oZWlnaHQgKyAoY3VycmVudE9wdHMucGFkZGluZyAqIDIpLFxyXG5cdFx0XHRcdFx0dG9wXHQ6IHBvcy50b3AgLSBjdXJyZW50T3B0cy5wYWRkaW5nIC0gMjAsXHJcblx0XHRcdFx0XHRsZWZ0IDogcG9zLmxlZnQgLSBjdXJyZW50T3B0cy5wYWRkaW5nIC0gMjBcclxuXHRcdFx0XHR9O1xyXG5cclxuXHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHR2aWV3ID0gX2dldF92aWV3cG9ydCgpO1xyXG5cclxuXHRcdFx0XHRmcm9tID0ge1xyXG5cdFx0XHRcdFx0d2lkdGggOiBjdXJyZW50T3B0cy5wYWRkaW5nICogMixcclxuXHRcdFx0XHRcdGhlaWdodCA6IGN1cnJlbnRPcHRzLnBhZGRpbmcgKiAyLFxyXG5cdFx0XHRcdFx0dG9wXHQ6IHBhcnNlSW50KHZpZXdbM10gKyB2aWV3WzFdICogMC41LCAxMCksXHJcblx0XHRcdFx0XHRsZWZ0IDogcGFyc2VJbnQodmlld1syXSArIHZpZXdbMF0gKiAwLjUsIDEwKVxyXG5cdFx0XHRcdH07XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdHJldHVybiBmcm9tO1xyXG5cdFx0fSxcclxuXHJcblx0XHRfYW5pbWF0ZV9sb2FkaW5nID0gZnVuY3Rpb24oKSB7XHJcblx0XHRcdGlmICghbG9hZGluZy5pcygnOnZpc2libGUnKSl7XHJcblx0XHRcdFx0Y2xlYXJJbnRlcnZhbChsb2FkaW5nVGltZXIpO1xyXG5cdFx0XHRcdHJldHVybjtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0JCgnZGl2JywgbG9hZGluZykuY3NzKCd0b3AnLCAobG9hZGluZ0ZyYW1lICogLTQwKSArICdweCcpO1xyXG5cclxuXHRcdFx0bG9hZGluZ0ZyYW1lID0gKGxvYWRpbmdGcmFtZSArIDEpICUgMTI7XHJcblx0XHR9O1xyXG5cclxuXHQvKlxyXG5cdCAqIFB1YmxpYyBtZXRob2RzIFxyXG5cdCAqL1xyXG5cclxuXHQkLmZuLmZhbmN5Ym94ID0gZnVuY3Rpb24ob3B0aW9ucykge1xyXG5cdFx0aWYgKCEkKHRoaXMpLmxlbmd0aCkge1xyXG5cdFx0XHRyZXR1cm4gdGhpcztcclxuXHRcdH1cclxuXHJcblx0XHQkKHRoaXMpXHJcblx0XHRcdC5kYXRhKCdmYW5jeWJveCcsICQuZXh0ZW5kKHt9LCBvcHRpb25zLCAoJC5tZXRhZGF0YSA/ICQodGhpcykubWV0YWRhdGEoKSA6IHt9KSkpXHJcblx0XHRcdC51bmJpbmQoJ2NsaWNrLmZiJylcclxuXHRcdFx0LmJpbmQoJ2NsaWNrLmZiJywgZnVuY3Rpb24oZSkge1xyXG5cdFx0XHRcdGUucHJldmVudERlZmF1bHQoKTtcclxuXHJcblx0XHRcdFx0aWYgKGJ1c3kpIHtcclxuXHRcdFx0XHRcdHJldHVybjtcclxuXHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdGJ1c3kgPSB0cnVlO1xyXG5cclxuXHRcdFx0XHQkKHRoaXMpLmJsdXIoKTtcclxuXHJcblx0XHRcdFx0c2VsZWN0ZWRBcnJheSA9IFtdO1xyXG5cdFx0XHRcdHNlbGVjdGVkSW5kZXggPSAwO1xyXG5cclxuXHRcdFx0XHR2YXIgcmVsID0gJCh0aGlzKS5hdHRyKCdyZWwnKSB8fCAnJztcclxuXHJcblx0XHRcdFx0aWYgKCFyZWwgfHwgcmVsID09ICcnIHx8IHJlbCA9PT0gJ25vZm9sbG93Jykge1xyXG5cdFx0XHRcdFx0c2VsZWN0ZWRBcnJheS5wdXNoKHRoaXMpO1xyXG5cclxuXHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0c2VsZWN0ZWRBcnJheSA9ICQoXCJhW3JlbD1cIiArIHJlbCArIFwiXSwgYXJlYVtyZWw9XCIgKyByZWwgKyBcIl1cIik7XHJcblx0XHRcdFx0XHRzZWxlY3RlZEluZGV4ID0gc2VsZWN0ZWRBcnJheS5pbmRleCggdGhpcyApO1xyXG5cdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0X3N0YXJ0KCk7XHJcblxyXG5cdFx0XHRcdHJldHVybjtcclxuXHRcdFx0fSk7XHJcblxyXG5cdFx0cmV0dXJuIHRoaXM7XHJcblx0fTtcclxuXHJcblx0JC5mYW5jeWJveCA9IGZ1bmN0aW9uKG9iaikge1xyXG5cdFx0dmFyIG9wdHM7XHJcblxyXG5cdFx0aWYgKGJ1c3kpIHtcclxuXHRcdFx0cmV0dXJuO1xyXG5cdFx0fVxyXG5cclxuXHRcdGJ1c3kgPSB0cnVlO1xyXG5cdFx0b3B0cyA9IHR5cGVvZiBhcmd1bWVudHNbMV0gIT09ICd1bmRlZmluZWQnID8gYXJndW1lbnRzWzFdIDoge307XHJcblxyXG5cdFx0c2VsZWN0ZWRBcnJheSA9IFtdO1xyXG5cdFx0c2VsZWN0ZWRJbmRleCA9IHBhcnNlSW50KG9wdHMuaW5kZXgsIDEwKSB8fCAwO1xyXG5cclxuXHRcdGlmICgkLmlzQXJyYXkob2JqKSkge1xyXG5cdFx0XHRmb3IgKHZhciBpID0gMCwgaiA9IG9iai5sZW5ndGg7IGkgPCBqOyBpKyspIHtcclxuXHRcdFx0XHRpZiAodHlwZW9mIG9ialtpXSA9PSAnb2JqZWN0Jykge1xyXG5cdFx0XHRcdFx0JChvYmpbaV0pLmRhdGEoJ2ZhbmN5Ym94JywgJC5leHRlbmQoe30sIG9wdHMsIG9ialtpXSkpO1xyXG5cdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHRvYmpbaV0gPSAkKHt9KS5kYXRhKCdmYW5jeWJveCcsICQuZXh0ZW5kKHtjb250ZW50IDogb2JqW2ldfSwgb3B0cykpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0c2VsZWN0ZWRBcnJheSA9IGpRdWVyeS5tZXJnZShzZWxlY3RlZEFycmF5LCBvYmopO1xyXG5cclxuXHRcdH0gZWxzZSB7XHJcblx0XHRcdGlmICh0eXBlb2Ygb2JqID09ICdvYmplY3QnKSB7XHJcblx0XHRcdFx0JChvYmopLmRhdGEoJ2ZhbmN5Ym94JywgJC5leHRlbmQoe30sIG9wdHMsIG9iaikpO1xyXG5cdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdG9iaiA9ICQoe30pLmRhdGEoJ2ZhbmN5Ym94JywgJC5leHRlbmQoe2NvbnRlbnQgOiBvYmp9LCBvcHRzKSk7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdHNlbGVjdGVkQXJyYXkucHVzaChvYmopO1xyXG5cdFx0fVxyXG5cclxuXHRcdGlmIChzZWxlY3RlZEluZGV4ID4gc2VsZWN0ZWRBcnJheS5sZW5ndGggfHwgc2VsZWN0ZWRJbmRleCA8IDApIHtcclxuXHRcdFx0c2VsZWN0ZWRJbmRleCA9IDA7XHJcblx0XHR9XHJcblxyXG5cdFx0X3N0YXJ0KCk7XHJcblx0fTtcclxuXHJcblx0JC5mYW5jeWJveC5zaG93QWN0aXZpdHkgPSBmdW5jdGlvbigpIHtcclxuXHRcdGNsZWFySW50ZXJ2YWwobG9hZGluZ1RpbWVyKTtcclxuXHJcblx0XHRsb2FkaW5nLnNob3coKTtcclxuXHRcdGxvYWRpbmdUaW1lciA9IHNldEludGVydmFsKF9hbmltYXRlX2xvYWRpbmcsIDY2KTtcclxuXHR9O1xyXG5cclxuXHQkLmZhbmN5Ym94LmhpZGVBY3Rpdml0eSA9IGZ1bmN0aW9uKCkge1xyXG5cdFx0bG9hZGluZy5oaWRlKCk7XHJcblx0fTtcclxuXHJcblx0JC5mYW5jeWJveC5uZXh0ID0gZnVuY3Rpb24oKSB7XHJcblx0XHRyZXR1cm4gJC5mYW5jeWJveC5wb3MoIGN1cnJlbnRJbmRleCArIDEpO1xyXG5cdH07XHJcblxyXG5cdCQuZmFuY3lib3gucHJldiA9IGZ1bmN0aW9uKCkge1xyXG5cdFx0cmV0dXJuICQuZmFuY3lib3gucG9zKCBjdXJyZW50SW5kZXggLSAxKTtcclxuXHR9O1xyXG5cclxuXHQkLmZhbmN5Ym94LnBvcyA9IGZ1bmN0aW9uKHBvcykge1xyXG5cdFx0aWYgKGJ1c3kpIHtcclxuXHRcdFx0cmV0dXJuO1xyXG5cdFx0fVxyXG5cclxuXHRcdHBvcyA9IHBhcnNlSW50KHBvcyk7XHJcblxyXG5cdFx0c2VsZWN0ZWRBcnJheSA9IGN1cnJlbnRBcnJheTtcclxuXHJcblx0XHRpZiAocG9zID4gLTEgJiYgcG9zIDwgY3VycmVudEFycmF5Lmxlbmd0aCkge1xyXG5cdFx0XHRzZWxlY3RlZEluZGV4ID0gcG9zO1xyXG5cdFx0XHRfc3RhcnQoKTtcclxuXHJcblx0XHR9IGVsc2UgaWYgKGN1cnJlbnRPcHRzLmN5Y2xpYyAmJiBjdXJyZW50QXJyYXkubGVuZ3RoID4gMSkge1xyXG5cdFx0XHRzZWxlY3RlZEluZGV4ID0gcG9zID49IGN1cnJlbnRBcnJheS5sZW5ndGggPyAwIDogY3VycmVudEFycmF5Lmxlbmd0aCAtIDE7XHJcblx0XHRcdF9zdGFydCgpO1xyXG5cdFx0fVxyXG5cclxuXHRcdHJldHVybjtcclxuXHR9O1xyXG5cclxuXHQkLmZhbmN5Ym94LmNhbmNlbCA9IGZ1bmN0aW9uKCkge1xyXG5cdFx0aWYgKGJ1c3kpIHtcclxuXHRcdFx0cmV0dXJuO1xyXG5cdFx0fVxyXG5cclxuXHRcdGJ1c3kgPSB0cnVlO1xyXG5cclxuXHRcdCQuZXZlbnQudHJpZ2dlcignZmFuY3lib3gtY2FuY2VsJyk7XHJcblxyXG5cdFx0X2Fib3J0KCk7XHJcblxyXG5cdFx0c2VsZWN0ZWRPcHRzLm9uQ2FuY2VsKHNlbGVjdGVkQXJyYXksIHNlbGVjdGVkSW5kZXgsIHNlbGVjdGVkT3B0cyk7XHJcblxyXG5cdFx0YnVzeSA9IGZhbHNlO1xyXG5cdH07XHJcblxyXG5cdC8vIE5vdGU6IHdpdGhpbiBhbiBpZnJhbWUgdXNlIC0gcGFyZW50LiQuZmFuY3lib3guY2xvc2UoKTtcclxuXHQkLmZhbmN5Ym94LmNsb3NlID0gZnVuY3Rpb24oKSB7XHJcblx0XHRpZiAoYnVzeSB8fCB3cmFwLmlzKCc6aGlkZGVuJykpIHtcclxuXHRcdFx0cmV0dXJuO1xyXG5cdFx0fVxyXG5cclxuXHRcdGJ1c3kgPSB0cnVlO1xyXG5cclxuXHRcdGlmIChjdXJyZW50T3B0cyAmJiBmYWxzZSA9PT0gY3VycmVudE9wdHMub25DbGVhbnVwKGN1cnJlbnRBcnJheSwgY3VycmVudEluZGV4LCBjdXJyZW50T3B0cykpIHtcclxuXHRcdFx0YnVzeSA9IGZhbHNlO1xyXG5cdFx0XHRyZXR1cm47XHJcblx0XHR9XHJcblxyXG5cdFx0X2Fib3J0KCk7XHJcblxyXG5cdFx0JChjbG9zZS5hZGQoIG5hdl9sZWZ0ICkuYWRkKCBuYXZfcmlnaHQgKSkuaGlkZSgpO1xyXG5cclxuXHRcdCQoY29udGVudC5hZGQoIG92ZXJsYXkgKSkudW5iaW5kKCk7XHJcblxyXG5cdFx0JCh3aW5kb3cpLnVuYmluZChcInJlc2l6ZS5mYiBzY3JvbGwuZmJcIik7XHJcblx0XHQkKGRvY3VtZW50KS51bmJpbmQoJ2tleWRvd24uZmInKTtcclxuXHJcblx0XHRjb250ZW50LmZpbmQoJ2lmcmFtZScpLmF0dHIoJ3NyYycsIGlzSUU2ICYmIC9eaHR0cHMvaS50ZXN0KHdpbmRvdy5sb2NhdGlvbi5ocmVmIHx8ICcnKSA/ICdqYXZhc2NyaXB0OnZvaWQoZmFsc2UpJyA6ICdhYm91dDpibGFuaycpO1xyXG5cclxuXHRcdGlmIChjdXJyZW50T3B0cy50aXRsZVBvc2l0aW9uICE9PSAnaW5zaWRlJykge1xyXG5cdFx0XHR0aXRsZS5lbXB0eSgpO1xyXG5cdFx0fVxyXG5cclxuXHRcdHdyYXAuc3RvcCgpO1xyXG5cclxuXHRcdGZ1bmN0aW9uIF9jbGVhbnVwKCkge1xyXG5cdFx0XHRvdmVybGF5LmZhZGVPdXQoJ2Zhc3QnKTtcclxuXHJcblx0XHRcdHRpdGxlLmVtcHR5KCkuaGlkZSgpO1xyXG5cdFx0XHR3cmFwLmhpZGUoKTtcclxuXHJcblx0XHRcdCQuZXZlbnQudHJpZ2dlcignZmFuY3lib3gtY2xlYW51cCcpO1xyXG5cclxuXHRcdFx0Y29udGVudC5lbXB0eSgpO1xyXG5cclxuXHRcdFx0Y3VycmVudE9wdHMub25DbG9zZWQoY3VycmVudEFycmF5LCBjdXJyZW50SW5kZXgsIGN1cnJlbnRPcHRzKTtcclxuXHJcblx0XHRcdGN1cnJlbnRBcnJheSA9IHNlbGVjdGVkT3B0c1x0PSBbXTtcclxuXHRcdFx0Y3VycmVudEluZGV4ID0gc2VsZWN0ZWRJbmRleCA9IDA7XHJcblx0XHRcdGN1cnJlbnRPcHRzID0gc2VsZWN0ZWRPcHRzXHQ9IHt9O1xyXG5cclxuXHRcdFx0YnVzeSA9IGZhbHNlO1xyXG5cdFx0fVxyXG5cclxuXHRcdGlmIChjdXJyZW50T3B0cy50cmFuc2l0aW9uT3V0ID09ICdlbGFzdGljJykge1xyXG5cdFx0XHRzdGFydF9wb3MgPSBfZ2V0X3pvb21fZnJvbSgpO1xyXG5cclxuXHRcdFx0dmFyIHBvcyA9IHdyYXAucG9zaXRpb24oKTtcclxuXHJcblx0XHRcdGZpbmFsX3BvcyA9IHtcclxuXHRcdFx0XHR0b3BcdCA6IHBvcy50b3AgLFxyXG5cdFx0XHRcdGxlZnQgOiBwb3MubGVmdCxcclxuXHRcdFx0XHR3aWR0aCA6XHR3cmFwLndpZHRoKCksXHJcblx0XHRcdFx0aGVpZ2h0IDogd3JhcC5oZWlnaHQoKVxyXG5cdFx0XHR9O1xyXG5cclxuXHRcdFx0aWYgKGN1cnJlbnRPcHRzLm9wYWNpdHkpIHtcclxuXHRcdFx0XHRmaW5hbF9wb3Mub3BhY2l0eSA9IDE7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdHRpdGxlLmVtcHR5KCkuaGlkZSgpO1xyXG5cclxuXHRcdFx0ZngucHJvcCA9IDE7XHJcblxyXG5cdFx0XHQkKGZ4KS5hbmltYXRlKHsgcHJvcDogMCB9LCB7XHJcblx0XHRcdFx0IGR1cmF0aW9uIDogY3VycmVudE9wdHMuc3BlZWRPdXQsXHJcblx0XHRcdFx0IGVhc2luZyA6IGN1cnJlbnRPcHRzLmVhc2luZ091dCxcclxuXHRcdFx0XHQgc3RlcCA6IF9kcmF3LFxyXG5cdFx0XHRcdCBjb21wbGV0ZSA6IF9jbGVhbnVwXHJcblx0XHRcdH0pO1xyXG5cclxuXHRcdH0gZWxzZSB7XHJcblx0XHRcdHdyYXAuZmFkZU91dCggY3VycmVudE9wdHMudHJhbnNpdGlvbk91dCA9PSAnbm9uZScgPyAwIDogY3VycmVudE9wdHMuc3BlZWRPdXQsIF9jbGVhbnVwKTtcclxuXHRcdH1cclxuXHR9O1xyXG5cclxuXHQkLmZhbmN5Ym94LnJlc2l6ZSA9IGZ1bmN0aW9uKCkge1xyXG5cdFx0aWYgKG92ZXJsYXkuaXMoJzp2aXNpYmxlJykpIHtcclxuXHRcdFx0b3ZlcmxheS5jc3MoJ2hlaWdodCcsICQoZG9jdW1lbnQpLmhlaWdodCgpKTtcclxuXHRcdH1cclxuXHJcblx0XHQkLmZhbmN5Ym94LmNlbnRlcih0cnVlKTtcclxuXHR9O1xyXG5cclxuXHQkLmZhbmN5Ym94LmNlbnRlciA9IGZ1bmN0aW9uKCkge1xyXG5cdFx0dmFyIHZpZXcsIGFsaWduO1xyXG5cclxuXHRcdGlmIChidXN5KSB7XHJcblx0XHRcdHJldHVybjtcdFxyXG5cdFx0fVxyXG5cclxuXHRcdGFsaWduID0gYXJndW1lbnRzWzBdID09PSB0cnVlID8gMSA6IDA7XHJcblx0XHR2aWV3ID0gX2dldF92aWV3cG9ydCgpO1xyXG5cclxuXHRcdGlmICghYWxpZ24gJiYgKHdyYXAud2lkdGgoKSA+IHZpZXdbMF0gfHwgd3JhcC5oZWlnaHQoKSA+IHZpZXdbMV0pKSB7XHJcblx0XHRcdHJldHVybjtcdFxyXG5cdFx0fVxyXG5cclxuXHRcdHdyYXBcclxuXHRcdFx0LnN0b3AoKVxyXG5cdFx0XHQuYW5pbWF0ZSh7XHJcblx0XHRcdFx0J3RvcCcgOiBwYXJzZUludChNYXRoLm1heCh2aWV3WzNdIC0gMjAsIHZpZXdbM10gKyAoKHZpZXdbMV0gLSBjb250ZW50LmhlaWdodCgpIC0gNDApICogMC41KSAtIGN1cnJlbnRPcHRzLnBhZGRpbmcpKSxcclxuXHRcdFx0XHQnbGVmdCcgOiBwYXJzZUludChNYXRoLm1heCh2aWV3WzJdIC0gMjAsIHZpZXdbMl0gKyAoKHZpZXdbMF0gLSBjb250ZW50LndpZHRoKCkgLSA0MCkgKiAwLjUpIC0gY3VycmVudE9wdHMucGFkZGluZykpXHJcblx0XHRcdH0sIHR5cGVvZiBhcmd1bWVudHNbMF0gPT0gJ251bWJlcicgPyBhcmd1bWVudHNbMF0gOiAyMDApO1xyXG5cdH07XHJcblxyXG5cdCQuZmFuY3lib3guaW5pdCA9IGZ1bmN0aW9uKCkge1xyXG5cdFx0aWYgKCQoXCIjZmFuY3lib3gtd3JhcFwiKS5sZW5ndGgpIHtcclxuXHRcdFx0cmV0dXJuO1xyXG5cdFx0fVxyXG5cclxuXHRcdCQoJ2JvZHknKS5hcHBlbmQoXHJcblx0XHRcdHRtcFx0PSAkKCc8ZGl2IGlkPVwiZmFuY3lib3gtdG1wXCI+PC9kaXY+JyksXHJcblx0XHRcdGxvYWRpbmdcdD0gJCgnPGRpdiBpZD1cImZhbmN5Ym94LWxvYWRpbmdcIj48ZGl2PjwvZGl2PjwvZGl2PicpLFxyXG5cdFx0XHRvdmVybGF5XHQ9ICQoJzxkaXYgaWQ9XCJmYW5jeWJveC1vdmVybGF5XCI+PC9kaXY+JyksXHJcblx0XHRcdHdyYXAgPSAkKCc8ZGl2IGlkPVwiZmFuY3lib3gtd3JhcFwiPjwvZGl2PicpXHJcblx0XHQpO1xyXG5cclxuXHRcdG91dGVyID0gJCgnPGRpdiBpZD1cImZhbmN5Ym94LW91dGVyXCI+PC9kaXY+JylcclxuXHRcdFx0LmFwcGVuZCgnPGRpdiBjbGFzcz1cImZhbmN5Ym94LWJnXCIgaWQ9XCJmYW5jeWJveC1iZy1uXCI+PC9kaXY+PGRpdiBjbGFzcz1cImZhbmN5Ym94LWJnXCIgaWQ9XCJmYW5jeWJveC1iZy1uZVwiPjwvZGl2PjxkaXYgY2xhc3M9XCJmYW5jeWJveC1iZ1wiIGlkPVwiZmFuY3lib3gtYmctZVwiPjwvZGl2PjxkaXYgY2xhc3M9XCJmYW5jeWJveC1iZ1wiIGlkPVwiZmFuY3lib3gtYmctc2VcIj48L2Rpdj48ZGl2IGNsYXNzPVwiZmFuY3lib3gtYmdcIiBpZD1cImZhbmN5Ym94LWJnLXNcIj48L2Rpdj48ZGl2IGNsYXNzPVwiZmFuY3lib3gtYmdcIiBpZD1cImZhbmN5Ym94LWJnLXN3XCI+PC9kaXY+PGRpdiBjbGFzcz1cImZhbmN5Ym94LWJnXCIgaWQ9XCJmYW5jeWJveC1iZy13XCI+PC9kaXY+PGRpdiBjbGFzcz1cImZhbmN5Ym94LWJnXCIgaWQ9XCJmYW5jeWJveC1iZy1ud1wiPjwvZGl2PicpXHJcblx0XHRcdC5hcHBlbmRUbyggd3JhcCApO1xyXG5cclxuXHRcdG91dGVyLmFwcGVuZChcclxuXHRcdFx0Y29udGVudCA9ICQoJzxkaXYgaWQ9XCJmYW5jeWJveC1jb250ZW50XCI+PC9kaXY+JyksXHJcblx0XHRcdGNsb3NlID0gJCgnPGEgaWQ9XCJmYW5jeWJveC1jbG9zZVwiPjwvYT4nKSxcclxuXHRcdFx0dGl0bGUgPSAkKCc8ZGl2IGlkPVwiZmFuY3lib3gtdGl0bGVcIj48L2Rpdj4nKSxcclxuXHJcblx0XHRcdG5hdl9sZWZ0ID0gJCgnPGEgaHJlZj1cImphdmFzY3JpcHQ6O1wiIGlkPVwiZmFuY3lib3gtbGVmdFwiPjxzcGFuIGNsYXNzPVwiZmFuY3ktaWNvXCIgaWQ9XCJmYW5jeWJveC1sZWZ0LWljb1wiPjwvc3Bhbj48L2E+JyksXHJcblx0XHRcdG5hdl9yaWdodCA9ICQoJzxhIGhyZWY9XCJqYXZhc2NyaXB0OjtcIiBpZD1cImZhbmN5Ym94LXJpZ2h0XCI+PHNwYW4gY2xhc3M9XCJmYW5jeS1pY29cIiBpZD1cImZhbmN5Ym94LXJpZ2h0LWljb1wiPjwvc3Bhbj48L2E+JylcclxuXHRcdCk7XHJcblxyXG5cdFx0Y2xvc2UuY2xpY2soJC5mYW5jeWJveC5jbG9zZSk7XHJcblx0XHRsb2FkaW5nLmNsaWNrKCQuZmFuY3lib3guY2FuY2VsKTtcclxuXHJcblx0XHRuYXZfbGVmdC5jbGljayhmdW5jdGlvbihlKSB7XHJcblx0XHRcdGUucHJldmVudERlZmF1bHQoKTtcclxuXHRcdFx0JC5mYW5jeWJveC5wcmV2KCk7XHJcblx0XHR9KTtcclxuXHJcblx0XHRuYXZfcmlnaHQuY2xpY2soZnVuY3Rpb24oZSkge1xyXG5cdFx0XHRlLnByZXZlbnREZWZhdWx0KCk7XHJcblx0XHRcdCQuZmFuY3lib3gubmV4dCgpO1xyXG5cdFx0fSk7XHJcblxyXG5cdFx0aWYgKCQuZm4ubW91c2V3aGVlbCkge1xyXG5cdFx0XHR3cmFwLmJpbmQoJ21vdXNld2hlZWwuZmInLCBmdW5jdGlvbihlLCBkZWx0YSkge1xyXG5cdFx0XHRcdGlmIChidXN5KSB7XHJcblx0XHRcdFx0XHRlLnByZXZlbnREZWZhdWx0KCk7XHJcblxyXG5cdFx0XHRcdH0gZWxzZSBpZiAoJChlLnRhcmdldCkuZ2V0KDApLmNsaWVudEhlaWdodCA9PSAwIHx8ICQoZS50YXJnZXQpLmdldCgwKS5zY3JvbGxIZWlnaHQgPT09ICQoZS50YXJnZXQpLmdldCgwKS5jbGllbnRIZWlnaHQpIHtcclxuXHRcdFx0XHRcdGUucHJldmVudERlZmF1bHQoKTtcclxuXHRcdFx0XHRcdCQuZmFuY3lib3hbIGRlbHRhID4gMCA/ICdwcmV2JyA6ICduZXh0J10oKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH0pO1xyXG5cdFx0fVxyXG5cclxuXHRcdGlmICghJC5zdXBwb3J0Lm9wYWNpdHkpIHtcclxuXHRcdFx0d3JhcC5hZGRDbGFzcygnZmFuY3lib3gtaWUnKTtcclxuXHRcdH1cclxuXHJcblx0XHRpZiAoaXNJRTYpIHtcclxuXHRcdFx0bG9hZGluZy5hZGRDbGFzcygnZmFuY3lib3gtaWU2Jyk7XHJcblx0XHRcdHdyYXAuYWRkQ2xhc3MoJ2ZhbmN5Ym94LWllNicpO1xyXG5cclxuXHRcdFx0JCgnPGlmcmFtZSBpZD1cImZhbmN5Ym94LWhpZGUtc2VsLWZyYW1lXCIgc3JjPVwiJyArICgvXmh0dHBzL2kudGVzdCh3aW5kb3cubG9jYXRpb24uaHJlZiB8fCAnJykgPyAnamF2YXNjcmlwdDp2b2lkKGZhbHNlKScgOiAnYWJvdXQ6YmxhbmsnICkgKyAnXCIgc2Nyb2xsaW5nPVwibm9cIiBib3JkZXI9XCIwXCIgZnJhbWVib3JkZXI9XCIwXCIgdGFiaW5kZXg9XCItMVwiPjwvaWZyYW1lPicpLnByZXBlbmRUbyhvdXRlcik7XHJcblx0XHR9XHJcblx0fTtcclxuXHJcblx0JC5mbi5mYW5jeWJveC5kZWZhdWx0cyA9IHtcclxuXHRcdHBhZGRpbmcgOiAxMCxcclxuXHRcdG1hcmdpbiA6IDQwLFxyXG5cdFx0b3BhY2l0eSA6IGZhbHNlLFxyXG5cdFx0bW9kYWwgOiBmYWxzZSxcclxuXHRcdGN5Y2xpYyA6IGZhbHNlLFxyXG5cdFx0c2Nyb2xsaW5nIDogJ2F1dG8nLFx0Ly8gJ2F1dG8nLCAneWVzJyBvciAnbm8nXHJcblxyXG5cdFx0d2lkdGggOiA1NjAsXHJcblx0XHRoZWlnaHQgOiAzNDAsXHJcblxyXG5cdFx0YXV0b1NjYWxlIDogdHJ1ZSxcclxuXHRcdGF1dG9EaW1lbnNpb25zIDogdHJ1ZSxcclxuXHRcdGNlbnRlck9uU2Nyb2xsIDogZmFsc2UsXHJcblxyXG5cdFx0YWpheCA6IHt9LFxyXG5cdFx0c3dmIDogeyB3bW9kZTogJ3RyYW5zcGFyZW50JyB9LFxyXG5cclxuXHRcdGhpZGVPbk92ZXJsYXlDbGljayA6IHRydWUsXHJcblx0XHRoaWRlT25Db250ZW50Q2xpY2sgOiBmYWxzZSxcclxuXHJcblx0XHRvdmVybGF5U2hvdyA6IHRydWUsXHJcblx0XHRvdmVybGF5T3BhY2l0eSA6IDAuNyxcclxuXHRcdG92ZXJsYXlDb2xvciA6ICcjNzc3JyxcclxuXHJcblx0XHR0aXRsZVNob3cgOiB0cnVlLFxyXG5cdFx0dGl0bGVQb3NpdGlvbiA6ICdmbG9hdCcsIC8vICdmbG9hdCcsICdvdXRzaWRlJywgJ2luc2lkZScgb3IgJ292ZXInXHJcblx0XHR0aXRsZUZvcm1hdCA6IG51bGwsXHJcblx0XHR0aXRsZUZyb21BbHQgOiBmYWxzZSxcclxuXHJcblx0XHR0cmFuc2l0aW9uSW4gOiAnZmFkZScsIC8vICdlbGFzdGljJywgJ2ZhZGUnIG9yICdub25lJ1xyXG5cdFx0dHJhbnNpdGlvbk91dCA6ICdmYWRlJywgLy8gJ2VsYXN0aWMnLCAnZmFkZScgb3IgJ25vbmUnXHJcblxyXG5cdFx0c3BlZWRJbiA6IDMwMCxcclxuXHRcdHNwZWVkT3V0IDogMzAwLFxyXG5cclxuXHRcdGNoYW5nZVNwZWVkIDogMzAwLFxyXG5cdFx0Y2hhbmdlRmFkZSA6ICdmYXN0JyxcclxuXHJcblx0XHRlYXNpbmdJbiA6ICdzd2luZycsXHJcblx0XHRlYXNpbmdPdXQgOiAnc3dpbmcnLFxyXG5cclxuXHRcdHNob3dDbG9zZUJ1dHRvblx0IDogdHJ1ZSxcclxuXHRcdHNob3dOYXZBcnJvd3MgOiB0cnVlLFxyXG5cdFx0ZW5hYmxlRXNjYXBlQnV0dG9uIDogdHJ1ZSxcclxuXHRcdGVuYWJsZUtleWJvYXJkTmF2IDogdHJ1ZSxcclxuXHJcblx0XHRvblN0YXJ0IDogZnVuY3Rpb24oKXt9LFxyXG5cdFx0b25DYW5jZWwgOiBmdW5jdGlvbigpe30sXHJcblx0XHRvbkNvbXBsZXRlIDogZnVuY3Rpb24oKXt9LFxyXG5cdFx0b25DbGVhbnVwIDogZnVuY3Rpb24oKXt9LFxyXG5cdFx0b25DbG9zZWQgOiBmdW5jdGlvbigpe30sXHJcblx0XHRvbkVycm9yIDogZnVuY3Rpb24oKXt9XHJcblx0fTtcclxuXHJcblx0JChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKSB7XHJcblx0XHQkLmZhbmN5Ym94LmluaXQoKTtcclxuXHR9KTtcclxuXHJcbn0pKGpRdWVyeSk7Il0sImZpbGUiOiJqcXVlcnkuZmFuY3lib3gtMS4zLjQuanMifQ==
